﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Class1
    {
    }

    public class resultObj
    {
        public bool isSuccess = false;

        /// <summary>是否有取到任何資料</summary>
        public bool hasData = false;
        /// <summary>主要資料集合</summary>
        public object data1 = null;
        public object data2 = null;
        public object data3 = null;
        public List<int> errorCode = new List<int>();

    }

    public class QueryObj_ABOUT : ABOUT
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_ABOUT()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref ABOUT data)
        {
            if (data.title == null) data.title = "";
            if (data.keyword == null) data.keyword = "";
            if (data.description == null) data.description = "";
            if (data.content == null) data.content = "";
            if (data.file_id == null) data.file_id = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
        }
    }

    public class QueryObj_INTRO : INTRO
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_INTRO()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref INTRO data)
        {
            if (data.title == null) data.title = "";
            if (data.keyword == null) data.keyword = "";
            if (data.description == null) data.description = "";
            if (data.content == null) data.content = "";
            if (data.file_id == null) data.file_id = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
        }
    }


    public class QueryObj_MAKER_CAR : MAKER_CAR
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_MAKER_CAR()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref MAKER_CAR data)
        {
            if (data.title == null) data.title = "";
            if (data.keyword == null) data.keyword = "";
            if (data.description == null) data.description = "";
            if (data.content == null) data.content = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
        }
    }

    public class QueryObj_TOUR_POINT : TOUR_POINT
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }

        public int history_year { get; set; }

        public QueryObj_TOUR_POINT()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
            date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;

            history_year = 0;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref TOUR_POINT data)
        {
            if (data.title == null) data.title = "";
            if (data.location == null) data.location = "";
            if (data.address == null) data.address = "";
            if (data.website == null) data.website = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.latitude == null) data.latitude = "";
            if (data.longitude == null) data.longitude = "";
            if (data.date_start == null || data.date_start == DateTime.MinValue || data.date_start.AddHours(-8) == DateTime.MinValue)
            {
                data.date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_end == null || data.date_end == DateTime.MinValue || data.date_end.AddHours(-8) == DateTime.MinValue)
            {
                data.date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.time == null || data.time == DateTime.MinValue || data.time.AddHours(-8) == DateTime.MinValue)
            {
                data.time = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.leave_time == null || data.leave_time == DateTime.MinValue || data.leave_time.AddHours(-8) == DateTime.MinValue)
            {
                data.leave_time = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_MAKER : MAKER
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_MAKER()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref MAKER data)
        {
            if (data.title == null) data.title = "";
            if (data.tel == null) data.tel = "";
            if (data.email == null) data.email = "";
            if (data.website == null) data.website = "";
            if (data.member == null) data.member = "";
            if (data.exp == null) data.exp = "";
            if (data.intro == null) data.intro = "";
            if (data.file_id == null) data.file_id = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
        }
    }

    public class QueryObj_FILES : FILES
    {
        //public string keyword { get; set; }
        //public string urlParam { get; set; }
        //public int pageNo { get; set; }
        //public int pageSize { get; set; }
        public QueryObj_FILES()
        {
            //keyword = "";
            //urlParam = "";
            //pageNo = 1;
            //pageSize = 20;
            lang = lang_type.zh_hant.ToString();
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref FILES data)
        {
            if (data.obj_id == null) data.obj_id = "";
            if (data.@class == null) data.@class = "";
            if (data.type == null) data.type = "";
            if (data.name == null) data.name = "";
            if (data.exten == null) data.exten = "";
            if (data.tag == null) data.tag = "";
            if (data.checkcode == null) data.checkcode = "";
            if (data.img_size == null) data.img_size = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.note == null) data.note = "";
        }
    }

    public class ExtenObj_COURSE 
    {
        public int id { get; set; }
        public string recurit_status { get; set; }
        public string survey_status { get; set; }
        public List<COURSE_MEMBER> sign_up_list { get; set; }
        public List<SURVEY> survey_list { get; set; }
        public ExtenObj_COURSE()
        {
            id = 0;
            recurit_status = "";
            survey_status = "";
            sign_up_list = new List<COURSE_MEMBER>();
            survey_list = new List<SURVEY>();
        }
    }

    public class QueryObj_COURSE : COURSE
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        
        public int is_recruiting { get; set; }
        public int is_unopened { get; set; }
        public int is_closed { get; set; }
        public QueryObj_COURSE()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
            date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            is_recruiting = 1;
            is_unopened = 1;
            is_closed = 1;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref COURSE data)
        {
            if (data.title == null) data.title = "";
            if (data.location == null) data.location = "";
            if (data.address == null) data.address = "";
            if (data.website == null) data.website = "";
            if (data.content == null) data.content = "";
            if (data.file_id == null) data.file_id = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.date_start == null || data.date_start == DateTime.MinValue || data.date_start.AddHours(-8) == DateTime.MinValue)
            {
                data.date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_end == null || data.date_end == DateTime.MinValue || data.date_end.AddHours(-8) == DateTime.MinValue)
            {
                data.date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.time == null || data.time == DateTime.MinValue || data.time.AddHours(-8) == DateTime.MinValue)
            {
                data.time = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }


    public class QueryObj_NEWS : NEWS
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_NEWS()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
            date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref NEWS data)
        {
            if (data.title == null) data.title = "";
            if (data.content == null) data.content = "";
            if (data.file_id == null) data.file_id = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.date_start == null || data.date_start == DateTime.MinValue || data.date_start.AddHours(-8) == DateTime.MinValue)
            {
                data.date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_end == null || data.date_end == DateTime.MinValue || data.date_end.AddHours(-8) == DateTime.MinValue)
            {
                data.date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_ALBUM : ALBUM
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_ALBUM()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
            date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref ALBUM data)
        {
            if (data.title == null) data.title = "";
            if (data.file_id == null) data.file_id = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.date_start == null || data.date_start == DateTime.MinValue || data.date_start.AddHours(-8) == DateTime.MinValue)
            {
                data.date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_end == null || data.date_end == DateTime.MinValue || data.date_end.AddHours(-8) == DateTime.MinValue)
            {
                data.date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_VIDEO : VIDEO
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_VIDEO()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
            date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref VIDEO data)
        {
            if (data.title == null) data.title = "";
            if (data.url == null) data.url = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.date_start == null || data.date_start == DateTime.MinValue || data.date_start.AddHours(-8) == DateTime.MinValue)
            {
                data.date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_end == null || data.date_end == DateTime.MinValue || data.date_end.AddHours(-8) == DateTime.MinValue)
            {
                data.date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }


    public class QueryObj_EQUIP_CATE : EQUIP_CATE
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_EQUIP_CATE()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
            date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref EQUIP_CATE data)
        {
            if (data.title == null) data.title = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.date_start == null || data.date_start == DateTime.MinValue || data.date_start.AddHours(-8) == DateTime.MinValue)
            {
                data.date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_end == null || data.date_end == DateTime.MinValue || data.date_end.AddHours(-8) == DateTime.MinValue)
            {
                data.date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_EQUIP : EQUIP
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_EQUIP()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
            date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref EQUIP data)
        {
            if (data.title == null) data.title = "";
            if (data.content == null) data.content = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.date_start == null || data.date_start == DateTime.MinValue || data.date_start.AddHours(-8) == DateTime.MinValue)
            {
                data.date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_end == null || data.date_end == DateTime.MinValue || data.date_end.AddHours(-8) == DateTime.MinValue)
            {
                data.date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_WORKS : WORKS
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_WORKS()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
            date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref WORKS data)
        {
            if (data.title == null) data.title = "";
            if (data.group == null) data.group = "";
            if (data.tool == null) data.tool = "";
            if (data.content == null) data.content = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.date_start == null || data.date_start == DateTime.MinValue || data.date_start.AddHours(-8) == DateTime.MinValue)
            {
                data.date_start = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_end == null || data.date_end == DateTime.MinValue || data.date_end.AddHours(-8) == DateTime.MinValue)
            {
                data.date_end = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_MEMBER : MEMBER
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }

        public bool exportExample { get; set; }
        public bool exportAll { get; set; }
        public List<int> exportIDs { get; set; }

        public QueryObj_MEMBER()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;

            exportExample = false;
            exportAll = false;
            exportIDs = new List<int>();
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref MEMBER data)
        {
            if (data.account == null) data.account = "";
            if (data.password == null) data.password = "";
            if (data.checkcode == null) data.checkcode = "";
            if (data.name == null) data.name = "";
            if (data.gender == null) data.gender = "";
            if (data.school == null) data.school = "";
            if (data.company == null) data.company = "";
            if (data.title == null) data.title = "";
            if (data.department == null) data.department = "";
            if (data.tel == null) data.tel = "";
            if (data.email == null) data.email = "";
            if (data.location == null) data.location = "";
            if (data.motive == null) data.motive = "";
            if (data.note == null) data.note = "";
            if (data.birth == null || data.birth == DateTime.MinValue || data.birth.AddHours(-8) == DateTime.MinValue)
            {
                data.birth = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_COURSE_MEMBER : COURSE_MEMBER
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }

        public DateTime date { get; set; }

        public string survey_status { get; set; }

        public QueryObj_COURSE_MEMBER()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            is_enabled = -1;
            date = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            survey_status = "";
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref COURSE_MEMBER data)
        {
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_AD : AD
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_AD()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
            lang = lang_type.zh_hant.ToString();
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref AD data)
        {
            if (data.title == null) data.title = "";
            if (data.url == null) data.url = "";
            if (data.target == null) data.target = "";
            if (data.content == null) data.content = "";
            if (data.file_id == null) data.file_id = "";
            if (data.lang == null) data.lang = lang_type.zh_hant.ToString();
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_SURVEY : SURVEY
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_SURVEY()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref SURVEY data)
        {
            if (data.f3 == null) data.f3 = "";
            if (data.f4 == null) data.f4 = "";
            if (data.f5 == null) data.f5 = "";
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }


    public class QueryObj_LANGUAGE_DATA : LANGUAGE_DATA
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_LANGUAGE_DATA()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref LANGUAGE_DATA data)
        {
            if (data.unit == null) data.unit = "";
            if (data.lang == null) data.lang = "";
            if (data.text1 == null) data.text1 = "";
            if (data.text2 == null) data.text2 = "";
            if (data.text3 == null) data.text3 = "";
            if (data.text4 == null) data.text4 = "";
            if (data.content1 == null) data.content1 = "";
            if (data.date_created == null || data.date_created == DateTime.MinValue || data.date_created.AddHours(-8) == DateTime.MinValue)
            {
                data.date_created = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.date_modify == null || data.date_modify == DateTime.MinValue || data.date_modify.AddHours(-8) == DateTime.MinValue)
            {
                data.date_modify = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_DETAIL_VIEW : DETAIL_VIEW
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_DETAIL_VIEW()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref DETAIL_VIEW data)
        {
            if (data.unit == null) data.unit = "";
        }
    }

    public class QueryObj_TODAY_VISITOR : TODAY_VISITOR
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_TODAY_VISITOR()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref TODAY_VISITOR data)
        {
            if (data.ip == null) data.ip = "";
            if (data.unit == null) data.unit = "";
            if (data.time == null || data.time == DateTime.MinValue || data.time.AddHours(-8) == DateTime.MinValue)
            {
                data.time = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }

    public class QueryObj_BACKEND_LOGIN_LOG : BACKEND_LOGIN_LOG
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_BACKEND_LOGIN_LOG()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
        }
        //需要填預設值，不允許null的欄位
        public static void stringAttrInit(ref BACKEND_LOGIN_LOG data)
        {
            if (data.ip == null) data.ip = "";
            if (data.name == null) data.name = "";
            if (data.checkcode == null) data.checkcode = "";
            if (data.login_time == null || data.login_time == DateTime.MinValue || data.login_time.AddHours(-8) == DateTime.MinValue)
            {
                data.login_time = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            if (data.logout_time == null || data.logout_time == DateTime.MinValue || data.logout_time.AddHours(-8) == DateTime.MinValue)
            {
                data.logout_time = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
            }
        }
    }


    public class QueryObj_Common
    {
        public string search_keyword { get; set; }
        public string urlParam { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public QueryObj_Common()
        {
            search_keyword = "";
            urlParam = "";
            pageNo = 1;
            pageSize = 20;
        }
    }


    public enum lang_type
    {
        zh_hant,
        zh_hans,
        en_us,
        pt,
    }

    public class pagerItem
    {
        public int no = 0;
        public string text = "";
        public string href = "";
        public string active = "";
    }


    public enum image_size_width : int
    {
        S = 200,
        M = 600,
        L = 1000
    }
    public enum image_size_height : int
    {
        S = 200,
        M = 600,
        L = 1000
    }
    

    public enum ViewInitMode
    {
        front, //前台模式
        list, //後台列表模式
        edit, //後台編輯模式
    }

    public enum MemberDataExcelColumn
    {
        id,
        account,
        name,
        gender,
        birth,
        occupation,
        school,
        company,
        degree,
        title,
        department,
        tel,
        email,
        location,
        motive,
        source,
        employ_status,
        date_created,
        date_modify,
        is_enabled,
    }


    public enum option_type
    {
        highest_education, //學歷
        course_source, //課程資訊得知來源
        employ_status, //待業狀況
        occupation, //職業
    }

    public enum recurit_options
    {
        recruiting, //招生中
        unopened, //尚未招生
        closed, //招生截止
        signed, //已報名
    }

    public enum survey_status_options
    {
        unopened, //課程尚未結束
        opened, //開放填寫
        done, //已填寫
        
    }

}

