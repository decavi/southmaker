jQuery.validator.addMethod("mobileTaiwan", function( value, element ) {
	var str = value;
	var result = false;
	if(str.length > 0){
		//是否只有數字;
		var patt_mobile = /^[\d]{1,}$/;
		result = patt_mobile.test(str);

		if(result){
			//檢查前兩個字是否為 09
			//檢查前四個字是否為 8869
			var firstTwo = str.substr(0,2);
			var firstFour = str.substr(0,4);
			var afterFour = str.substr(4,str.length-1);
			if(firstFour == '8869'){
				$(element).val('09'+afterFour);
				if(afterFour.length == 8){
					result = true;
				} else {
					result = false;
				}
			} else if(firstTwo == '09'){
				if(str.length == 10){
					result = true;
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
	} else {
		result = true;
	}
	return result;
}, "手機號碼不符合格式，僅允許09開頭的10碼數字");

jQuery.validator.addMethod("phoneStyle", function( value, element ) {
	var str = value;
	var result = false;

	if(str.length > 0){
		var patt_phone = /^[\d\-\(\)\#]{1,}$/;
		result = patt_phone.test(str);
	} else {
		result = true;
	}

	return result;
}, "電話號碼不符合格式，僅接受數字、#-()等符號");
