//首頁輪播
$(document).ready(function (e) {
    $('.loop').owlCarousel({
        center:true,
        items: 1,
        loop: true,//循環播放
        autoplay:true,//自動撥放
        autoplayHoverPause: true,
        nav: true, //左右箭頭
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
    });
    $(".owl-prev").attr("title", "上一則 preview");
    $(".owl-next").attr("title", "下一則 next");
    $(".owl-prev").attr("tabindex", "0");
    $(".owl-next").attr("tabindex", "0");                         
    $(".owl-theme .item").show(); //為無障隱藏的圖顯示出來。
    $(".owl-carousel").attr("tabindex", "0");

    //focus輪播圖 停止自動撥放 從第一個跳起
    var owl = $('.owl-carousel');

    $(".owl-carousel").on('keydown', function(e) {
        owl.trigger('stop.owl.autoplay');
        // $(this).find(".owl-item.cloned").hide();
        // $(this).find(".owl-item").css("float", "left");
        // $(this).find(".owl-stage").css("transform", "inherit");
    });

    $('.owl-next').on('keydown', function(e) {
        if (e.keyCode == 13){
            $(this).click();
        }
    })
    $('.owl-prev').on('keydown', function(e) {
        if (e.keyCode == 13){
            $(this).click();
        }
    })

});

$(document).ready(function () {

    //table header固定
    $(".sticky-header").floatThead({scrollingTop:50});
    
    //變換字體大小
    var change_font_size = function(e) {
        e.preventDefault();
        var fz = $(this).data('size');
        $('body').css('font-size', fz);
    };
    $(".update-fz").keypress(function() {
        if (event.which == 13) change_font_size();
    });
    $('.update-fz').click(change_font_size);

    $(function(){
        var len1 = 100; // 超過100個字以"..."取代
        $(".ellipsis").each(function(i){
            if($(this).text().length>len1){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len1-1)+"...";
                $(this).text(text);
            }
        });
    });

    $(function(){
        var len2 = 80; // 超過80個字以"..."取代
        $(".ellipsis80").each(function(i){
            if($(this).text().length>len2){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len2-1)+"...";
                $(this).text(text);
            }
        });
    });

    $(function(){
        var len3 = 50; // 超過80個字以"..."取代
        $(".ellipsis50").each(function(i){
            if($(this).text().length>len3){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len3-1)+"...";
                $(this).text(text);
            }
        });
    });

    $(function(){
		$(window).scroll(function(){
			if($(this).scrollTop()>1){
				$(".navbar-default").addClass("change-bg");
			}else{
				$(".navbar-default").removeClass("change-bg");
			}
		});
    });
    
    $(".top").hide();
	$(function(){
		$(window).scroll(function(){
			if($(this).scrollTop()>1){
				$(".top").fadeIn();
			}else{
				$(".top").fadeOut();
			}
		});
	});
    $(".top").click(function(){
		$("html,body").animate({scrollTop:0},900);
		return false;
    });
    //列印
    $("#print").click(function(text){
        text=document;
        print(text);
    });
    //年月日判斷
    var yyyy = 2015;
    for (var i = 0; i < 10; i++, yyyy++) {
        $('#start-year').append("<option value="+yyyy+">"+yyyy+"</option>");
        $('#end-year').append("<option value="+yyyy+">"+yyyy+"</option>");
    };
    
    for (var w = 1; w < 13; w++){
        if(w<10){
            $('#start-month').append("<option value=0"+w+">0"+w+"</option>");
            $('#end-month').append("<option value=0"+w+">0"+w+"</option>");
        }else{
            $('#start-month').append("<option value="+w+">"+w+"</option>");
            $('#end-month').append("<option value="+w+">"+w+"</option>");
        }
    }
    //當開始月份被點擊
    var start_month = function() {
        var select_month = $('#start-month').val();
        var select_year = $('#start-year').val();
        if(select_year == "請選擇"){
            alert("請先選擇開始時間的年份");
        }else{
            if(select_month == 04 || select_month == 06 || select_month == 09 || select_month == 11){
                $('#start-date').empty();
                $('#start-date').append("<option value='請選擇'>請選擇</option>");
                for(var z = 1; z < 31 ;z++){
                    if(z<10){
                        $('#start-date').append("<option value=0"+z+">0"+z+"</option>");
                    }else{
                        $('#start-date').append("<option value="+z+">"+z+"</option>");
                    }
                }
            }else if(select_month == 02){
                if((select_year % 4) == 0){
                    $('#start-date').empty();
                    $('#start-date').append("<option value='請選擇'>請選擇</option>");
                    for(var t = 1; t < 30 ;t++){
                        if(t<10){
                            $('#start-date').append("<option value=0"+t+">0"+t+"</option>");
                        }else{
                            $('#start-date').append("<option value="+t+">"+t+"</option>");
                        }
                    }
                }else{
                    $('#start-date').empty();
                    $('#start-date').append("<option value='請選擇'>請選擇</option>");
                    for(var e = 1; e < 29 ;e++){
                        if(e<10){
                            $('#start-date').append("<option value=0"+e+">0"+e+"</option>");
                        }else{
                            $('#start-date').append("<option value="+e+">"+e+"</option>");
                        }
                    }
                }
            }else{
                $('#start-date').empty();
                $('#start-date').append("<option value='請選擇'>請選擇</option>");
                for(var o = 1; o < 32 ;o++){
                    if(o<10){
                        $('#start-date').append("<option value=0"+o+">0"+o+"</option>");
                    }else{
                        $('#start-date').append("<option value="+o+">"+o+"</option>");
                    }
                }
            }
        }
    }
    $("#start-month").keypress(function() {
        if (event.which == 13) start_month();
    });
    $('#start-month').click(start_month);

    //當開始日期被點擊
    var start_date = function() {
        var select_month = $('#start-month').val();
        var select_year = $('#start-year').val();
        if(select_year == "請選擇" || select_month == "請選擇"){
            alert("請先選擇開始時間的年份與月份");
        }
    }
    $("#start-date").keypress(function() {
        if (event.which == 13) start_date();
    });
    $('#start-date').click(start_date);

    //當結束月份被點擊
    var end_month = function() {
        var end_month = $('#end-month').val();
        var end_year = $('#end-year').val();
        if(end_year == "請選擇"){
            alert("請先選擇結束時間的年份");
        }else{
            if(end_month == 04 || end_month == 06 || end_month == 09 || end_month == 11){
                $('#end-date').empty();
                $('#end-date').append("<option value='請選擇'>請選擇</option>");
                for(var z = 1; z < 31 ;z++){
                    if(z<10){
                        $('#end-date').append("<option value=0"+z+">0"+z+"</option>");
                    }else{
                        $('#end-date').append("<option value="+z+">"+z+"</option>");
                    }
                }
            }else if(end_month == 02){
                if((end_year % 4) == 0){
                    $('#end-date').empty();
                    $('#end-date').append("<option value='請選擇'>請選擇</option>");
                    for(var t = 1; t < 30 ;t++){
                        if(t<10){
                            $('#end-date').append("<option value=0"+t+">0"+t+"</option>");
                        }else{
                            $('#end-date').append("<option value="+t+">"+t+"</option>");
                        }
                    }
                }else{
                    $('#end-date').empty();
                    $('#end-date').append("<option value='請選擇'>請選擇</option>");
                    for(var e = 1; e < 29 ;e++){
                        if(e<10){
                            $('#end-date').append("<option value=0"+e+">0"+e+"</option>");
                        }else{
                            $('#end-date').append("<option value="+e+">"+e+"</option>");
                        }
                    }
                }
            }else{
                $('#end-date').empty();
                $('#end-date').append("<option value='請選擇'>請選擇</option>");
                for(var o = 1; o < 32 ;o++){
                    if(o<10){
                        $('#end-date').append("<option value=0"+o+">0"+o+"</option>");
                    }else{
                        $('#end-date').append("<option value="+o+">"+o+"</option>");
                    }
                }
            }
        }
    }
    $("#end-month").keypress(function() {
        if (event.which == 13) end_month();
    });
    $('#end-month').click(end_month);

    //當結束日期被點擊
    var end_date = function() {
        var end_month = $('#end-month').val();
        var end_year = $('#end-year').val();
        if(end_year == "請選擇" || end_month == "請選擇"){
            alert("請先選擇結束時間的年份與月份");
        }
    }
    $("#end-date").keypress(function() {
        if (event.which == 13) end_date();
    });
    $('#end-date').click(end_date);

    //控制頁數顯示
    var $Width = $(window).width();

    $(function () {
        if($Width < 480){
            var obj = $('#pagination').twbsPagination({
                totalPages: 28,
                visiblePages: 3,
                // Text labels
                first: '',
                prev: '上一頁',
                next: '下一頁',
                last: '',
                // pagination Classes
                paginationClass: 'pagination',
                nextClass: 'next',
                prevClass: 'previous',
                pageClass: 'page',
                activeClass: 'active',
                disabledClass: 'disabled',
                onPageClick: function (event, page) {
                    console.info(page);
                }
            });
            console.info(obj.data());
        }else{
            var obj = $('#pagination').twbsPagination({
                totalPages: 28,
                visiblePages: 10,
                // Text labels
                first: '',
                prev: '上一頁',
                next: '下一頁',
                last: '',
                // pagination Classes
                paginationClass: 'pagination',
                nextClass: 'next',
                prevClass: 'previous',
                pageClass: 'page',
                activeClass: 'active',
                disabledClass: 'disabled',
                onPageClick: function (event, page) {
                    console.info(page);
                }
            });
            console.info(obj.data());
        }
        
    });
    
});
