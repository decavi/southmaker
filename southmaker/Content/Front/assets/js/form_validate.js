$(document).ready(function () {
    //編輯個人資料
    $("#member-edit").validate({
        submitHandler:function(form){
            // debug:true
            //表單驗證成功後取值
            var name = $("#name").val();
            var birth_date = $("#birth-date").val();
			var gender = $('input[name=gender]:checked', '#member-edit').val();            
            var occupation = $("#occupation").val();
            var eduction = $("#eduction").val();
            var telphone = $("#telphone").val();
            var email = $("#email").val();
            var country = $("#country").val();
            var learning_motivation = $("#learning-motivation").val();
            var work_status = $("#work-status").val();

            //確定資料成功從資料庫中更改，再傳回以下訊息
            $(".returnmessage").html("<span style='color:#009FE8'>已成功編輯個人資料!</span>"); 
            $("#member-edit")[0].reset();
        }
    });

    //變更密碼
    $("#password-change").validate({
        rules: {
            old_password: {
                required: true,
                minlength: 6,
                maxlength:8
            },
            password: {
                required: true,
                minlength: 6,
                maxlength:8
            },
            confirm_password: {
                required: true,
                minlength: 6,
                maxlength:8,
                equalTo: "#password"
            },
        },
        messages: {
            old_password: {
                required: "請輸入密碼",
                minlength: "密碼長度為6-8個英數字元",
                maxlength: "密碼長度為6-8個英數字元"
            },
            password: {
                required: "請輸入密碼",
                minlength: "密碼長度為6-8個英數字元",
                maxlength: "密碼長度為6-8個英數字元"
            },
            confirm_password: {
                required: "請輸入密碼",
                minlength: "密碼長度為6-8個英數字元",
                maxlength: "密碼長度為6-8個英數字元",
                equalTo: "兩次輸入密碼不一致"
            },
        },
        submitHandler:function(form){
            //表單驗證成功後取值
            var old_password = $("#old_password").val();
            var password = $("#password").val();
            var confirm_password = $("#confirm_password").val();

            //確定資料成功從資料庫中更改，再傳回以下訊息
            $(".returnmessage").html("<span style='color:#009FE8'>已成功變更密碼!</span>"); 
            $("#password-change")[0].reset();
        }
    });

    //會員課程問卷
    $("#survey").validate({
        submitHandler:function(form){
            // debug:true
            //表單驗證成功後取值
            var survey_name = $("#survey_name").val();
            var survey_gender = $('input[name=survey_gender]:checked', '#survey').val();
            var survey_ages = $("#survey_ages").val();
            var survey_occupation = $("#survey_occupation").val();
            var survey_eduction = $("#survey_eduction").val();
            var news = $("#news").val();
            var reason_for_participate = $("#reason-for-participate").val();
            var hope_to_hold = $("#hope-to-hold").val();
            var improve = $("#improve").val();  
            var the_most_impression = $("#the-most-impression").val();

            //確定資料成功存入資料庫中，再傳回以下訊息
            $("#returnmessage_survey").html("<span style='color:#009FE8'>已收到您的問卷!感謝您撥空填寫!</span>"); 
            $("#survey")[0].reset();
        }
    });

    //會員登入
    $("#member-login").validate({
        rules: {
            login_email: {
                required: true,
                email: true
            },
            login_password: {
                required: true
            },
            login_captcha: {
                required: true
            },
        },
        messages: {
            login_email: "請輸入有效的電子信箱",
            login_password: {
                required: "請輸入密碼",
            },
            login_captcha: {
                required: "請輸入驗證碼"
            },
        },
        submitHandler:function(form){
            //表單驗證成功後取值
            var login_email = $("#login_email").val();
            var login_password = $("#login_password").val();
            var login_captcha = $("#login_captcha").val();

            //確定驗證碼輸入正確與資料成功存入資料庫中，再傳回以下訊息
            $(".returnmessage").html("<span style='color:#009FE8'>已成功登入!</span>"); 
            $("#member-login")[0].reset();
        }
    });

    //成為新會員
    $("#new_member").validate({
        rules: {
            add_password: {
                required: true,
                minlength: 6,
                maxlength: 8
            },
        },
        messages: {
            add_password: {
                required: "請輸入密碼",
                minlength: "密碼長度為6-8個英數字元",
                maxlength: "密碼長度為6-8個英數字元"
            },
        },
        submitHandler:function(form){
            // debug:true
            //表單驗證成功後取值
            var new_name = $("#new_name").val();
            var new_birth_date = $("#new_birth_date").val();
			var new_gender = $('input[name=new_gender]:checked', '#new_member').val();            
            var new_occupation = $("#new_occupation").val();
            var new_eduction = $("#new_eduction").val();
            var new_telphone = $("#new_telphone").val();
            var new_email = $("#new_email").val();
            var new_country = $("#new_country").val();
            var new_learning_motivation = $("#new_learning-motivation").val();
            var new_work_status = $("#new_work-status").val();
            var add_password = $("#add_password").val();

            //確定資料成功存入資料庫中，再傳回以下訊息
            $(".returnmessage").html("<span style='color:#009FE8'>已成功加入新會員!</span>"); 
            $("#new_member")[0].reset();
        }
    });

    //聯絡我們
    $(".contact-us").validate({
        submitHandler:function(form){
            //表單驗證成功後取值
            var contact_name = $("#contact_name").val();
            var contact_email = $("#contact_email").val();
            var contact_telphone = $("#contact_telphone").val();
            var contact_mail_header = $("#contact_mail_header").val();
            var contact_message = $("#contact_message").val();
            var contact_captcha = $("#contact_captcha").val();

            //確定驗證碼輸入正確與資料成功存入資料庫中，再傳回以下訊息
            $(".returnmessage").html("<span style='color:#009FE8'>已收到您的需求，我們會盡快聯絡您!</span>"); 
            $(".contact-us")[0].reset();
        }
    });

    //忘記密碼
    $("#forget-password").validate({
        submitHandler:function(form){
            //表單驗證成功後取值
            var forget_email = $("#forget_email").val();
            var forget_name = $("#forget_name").val();
            var forget_telphone = $("#forget_telphone").val();

            //確定成功寄出索取密碼信，再傳回以下訊息
            $(".returnmessage").html("<span style='color:#009FE8'>已成功寄出您的新密碼!</span>"); 
            $("#forget-password")[0].reset();
        }
    });
});