$(document).ready(function () {
    //google日曆
    $('#calendar-place').fullCalendar({ 
        googleCalendarApiKey: 'AIzaSyAhdvE6nU1J8I7IS9Dw3TspkUkF94K8DYE',
        events: {
            googleCalendarId: 'ed7j5faeoom8g9bljqquh81tos@group.calendar.google.com',
        }
    });
    $('#calendar-equiment').fullCalendar({
        googleCalendarApiKey: 'AIzaSyAhdvE6nU1J8I7IS9Dw3TspkUkF94K8DYE',
        events: {
            googleCalendarId: 'uq0n6k08ph5uqai9hn63lr2kqk@group.calendar.google.com',
        }
    });
    $('.fc-prev-button').attr("aria-label","上個月last month");
    $('.fc-next-button').attr("aria-label","下個月next month");
});

//google map maker car
$(document).ready(initMap);
function initMap() {
    var infowindow = new google.maps.InfoWindow();
    var uluru = new google.maps.LatLng(23.4462119, 120.4946172);
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: uluru
    });
    var image='assets/img/base/map-icon.png';
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon:image,
        html:"<ul><li>日期：2017-06-18</li><li>時間：9:30-16:00</li><li>名稱：嘉義縣客家文化會館</li><li>地址：嘉義縣中埔鄉金蘭村頂山門29號</li><li>課程名稱：3D列印機&數位刻字機教學</li></ul>"
    });
    google.maps.event.addListener(marker, 'click', function(){ 
        infowindow.setContent(this.html);
        infowindow.open(map,this);
    });
}
//contact-us
$(document).ready(initMap2);
function initMap2() {
    var southmaker=new google.maps.LatLng(23.3106768, 120.32265259999997);
    var image='assets/img/base/map-icon.png';
    var map_southmaker =new google.maps.Map(document.getElementById('map-southmaker'), {
        zoom: 15,
        center: southmaker
    });
    var marker = new google.maps.Marker({
        position: southmaker,
        map: map_southmaker,
        icon:image
    });
}