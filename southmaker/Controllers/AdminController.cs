﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;

namespace southmaker.Controllers
{
    public class AdminController : Controller
    {
        public resultObj ret = new resultObj();

        public ActionResult Index() { return View(); }
        public ActionResult Main() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult About() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult AboutEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult Intro() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult IntroEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult MakerCar() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult MakerCarEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult TourPoint() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult TourPointEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult Maker() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult MakerEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }

        public ActionResult Course() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult CourseEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult CourseMember() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult CourseMemberEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }

        public ActionResult CourseSurvey() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult SurveySummary() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult SurveyMember() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult SurveyDetail() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }

        public ActionResult News() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult NewsEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult Album() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult AlbumEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult Video() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult VideoEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }

        public ActionResult Equip() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult EquipEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult EquipCate() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult EquipCateEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }

        public ActionResult Works() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult WorksEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }

        public ActionResult Member() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }
        public ActionResult MemberEdit() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }

        public ActionResult AD() { if (isLogin() == false) Response.Redirect("/Admin/Index"); return View(); }


        public bool isLogin()
        {
            bool isLogin = false;
            //bool isLogin = true;
            if (Session["admin"] != null) isLogin = true;
            return isLogin;
        }



        [HttpPost]
        public JsonResult Login(string account, string pwd, string captcha)
        {
            ret = new resultObj();

            try
            {
                if (account == null) account = ""; if (pwd == null) pwd = ""; if (captcha == null) captcha = "";

                List<checkRuleObj> rules = new List<checkRuleObj>();
                int msg = 0;
                checkRuleObj ckObj = new checkRuleObj("summary");
                //if (Session["AdminLoginCaptchaObj"] != null)
                if (Session["admin_login_captcha"] != null)
                {
                    msg = checkClass.ck_admin_backend_login(account, pwd, Session["admin_login_captcha"], captcha); if (msg != 0) ckObj.checkResult.Add(msg);
                    if (ckObj.checkResult.Count > 0) rules.Add(ckObj);
                }
                if (rules.Count > 0)
                {
                    ret.data2 = rules;
                    return Json(ret);
                }

                BACKEND_LOGIN_LOG log = new BACKEND_LOGIN_LOG();            //新建一個管理者登入物件
                log.ip = Request.UserHostAddress;
                log.name = account;
                log.checkcode = Guid.NewGuid().ToString().Substring(0, 7);
                log.login_time = DateTime.Now;
                QueryObj_BACKEND_LOGIN_LOG.stringAttrInit(ref log);
                using (var DB = new SOUTHMAKEREntities())
                {
                    DB.BACKEND_LOGIN_LOG.Add(log);                          //將log儲存到DB
                    DB.SaveChanges();

                    //檢查有沒有其他log紀錄 超過8小時但還沒登出的 (自動登出機制)
                    DateTime now = DateTime.Now;
                    var findObjs = DB.BACKEND_LOGIN_LOG.Where(x => x.is_logout == 0).ToList();
                    foreach (var item in findObjs)
                    {
                        if (item.login_time.AddHours(8) <= now)
                        {
                            item.is_logout = 1;
                            item.logout_time = now;
                            DB.SaveChanges();
                        }
                    }
                }

                Session["admin"] = log;                             //將log暫存到SESSION
                ret.isSuccess = true;
            }
            catch(Exception ex)
            {
                ret.data2 = ex;
            }

            return Json(ret);
        }

        [HttpPost]
        public JsonResult Logout()
        {
            ret = new resultObj();

            BACKEND_LOGIN_LOG log = new BACKEND_LOGIN_LOG();
            if (Session["admin"] != null)
            {
                log = (BACKEND_LOGIN_LOG)Session["admin"];
                using (var DB = new SOUTHMAKEREntities())
                {
                    var findObj = DB.BACKEND_LOGIN_LOG.Where(x => x.id == log.id && x.checkcode == log.checkcode && x.is_logout == 0).FirstOrDefault();
                    if(findObj != null)
                    {
                        findObj.is_logout = 1;
                        findObj.logout_time = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
            }

            Session["admin"] = null;
            ret.isSuccess = true;
            return Json(ret);
        }


    }
}