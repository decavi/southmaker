﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using System.IO;
using NPOI.HSSF.UserModel;

namespace southmaker.Controllers
{
    public class CourseController : Controller
    {
        public resultObj ret = new resultObj();


        public ActionResult Calendar() { return View(); }
        public ActionResult List() { return View(); }
        public ActionResult Detail() { helper.SaveDetailViewAndTodayVisitor("course", Request["id"]); return View(); }
        public ActionResult Signup() { if (isLogin() == false) Response.Redirect("/Member/Login"); return View(); }

        public bool isLogin()
        {
            bool isLogin = false;
            //bool isLogin = true;
            if (Session["user"] != null) isLogin = true;
            return isLogin;
        }

        [HttpPost]
        public JsonResult CourseSearch(QueryObj_COURSE q)
        {
            if (q == null) q = new QueryObj_COURSE();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.COURSE
                    where
                    a.is_deleted == 0 &&

                    //招生狀態
                    (
                        (q.is_recruiting == 1 ? (a.date_start <= DateTime.Now && DateTime.Now <= a.date_end) : false) ||
                        (q.is_unopened == 1 ? (a.date_start >= DateTime.Now) : false) ||
                        (q.is_closed == 1 ? (a.date_end <= DateTime.Now) : false)
                    ) &&

                    //發佈期間內篩選
                    //(
                    (q.is_enabled != 1 ? true : a.is_enabled == 1) &&                     //is_enabled值不為1 無視此篩選條件 (值為1才繼續篩選  此篩選條件在前台一定得為1)
                    //((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                    //    (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                    //    ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) && 
                    //     (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    //)) &&

                    //時間區間篩選
                    (
                        ((q.date_start.Year == 1753) ? true : a.time >= q.date_start) &&
                        ((q.date_end.Year == 1753) ? true : a.time <= q.date_end)
                    ) &&
                    
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(課程名稱 地址)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.location.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.COURSE
                    where
                    a.is_deleted == 0 &&

                    //招生狀態
                    (
                        (q.is_recruiting == 1 ? (a.date_start <= DateTime.Now && DateTime.Now <= a.date_end) : (a.date_start >= DateTime.Now && DateTime.Now >= a.date_end)) ||
                        (q.is_unopened == 1 ? (a.date_start >= DateTime.Now) : (a.date_start <= DateTime.Now)) ||
                        (q.is_closed == 1 ? (a.date_end <= DateTime.Now) : (a.date_end >= DateTime.Now))
                    ) &&

                    //發佈期間內篩選
                    //(
                    (q.is_enabled != 1 ? true : a.is_enabled == 1) &&                     //is_enabled值不為1 無視此篩選條件 (值為1才繼續篩選  此篩選條件在前台一定得為1)
                    //((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                    //    (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                    //    ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) && 
                    //     (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    //)) &&

                    //時間區間篩選
                    (
                        ((q.date_start.Year == 1753) ? true : a.time >= q.date_start) &&
                        ((q.date_end.Year == 1753) ? true : a.time <= q.date_end)
                    ) &&

                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(課程名稱 地址)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.address.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }



        [HttpPost]
        public JsonResult CourseSignUpDeadlineLeaveTime(int id)
        {
            ret = new resultObj();
            ret.data1 = 0;
            if (id == 0) return Json(ret);

            using (var DB = new SOUTHMAKEREntities())
            {
                var item = DB.COURSE.Where(x => x.id == id && x.is_deleted == 0).FirstOrDefault();
                if(item != null)
                {
                    DateTime now = DateTime.Now;
                    DateTime deadline = item.date_end;
                    if(deadline > now)
                    {
                        ret.data1 = deadline.Subtract(now).TotalSeconds;
                    }
                    ret.isSuccess = true;
                }
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult CourseExtenSearch(int[] ids)
        {
            ret = new resultObj();

            if(ids.Length == 0) return Json(ret);

            List<ExtenObj_COURSE> extens = new List<ExtenObj_COURSE>();
            using (var DB = new SOUTHMAKEREntities())
            {
                var items = DB.COURSE.Where(x => ids.Contains(x.id) && x.is_deleted == 0).ToList();
                if(items.Count > 0)
                {
                    DateTime now = DateTime.Now;
                    foreach (var item in items)
                    {
                        ExtenObj_COURSE exten = new ExtenObj_COURSE();
                        exten.id = item.id;     //課程ID

                        //sign_up_list處理 (目前報名清單)
                        var sign_up_list = DB.COURSE_MEMBER.Where(x => x.course_id == item.id && x.is_deleted == 0).ToList();
                        if (sign_up_list.Count > 0) exten.sign_up_list = sign_up_list;

                        //recurit_status處理 (招生狀態)
                        if (item.date_start <= now && now <= item.date_end)
                        {
                            exten.recurit_status = recurit_options.recruiting.ToString();

                            //如果報名清單含有目前的session會員編號，則改為已報名狀態
                            if (Session["user"] != null)
                            {
                                MEMBER loginMember = (MEMBER)Session["user"];
                                if (sign_up_list.Any(x => x.member_id == loginMember.id)) exten.recurit_status = recurit_options.signed.ToString();
                            }
                        }
                        if (item.date_start >= now) exten.recurit_status = recurit_options.unopened.ToString();
                        if (item.date_end <= now) exten.recurit_status = recurit_options.closed.ToString();

                        //survey_list處理 (已填寫問卷清單)
                        var survey_list = DB.SURVEY.Where(x => x.course_id == item.id && x.is_deleted == 0).ToList();
                        if (survey_list.Count > 0) exten.survey_list = survey_list;

                        //survey_status處理 (問卷開放狀態)
                        if (item.time < DateTime.Now)
                        {
                            exten.survey_status = survey_status_options.opened.ToString();
                            if (Session["user"] != null)
                            {
                                MEMBER loginMember = (MEMBER)Session["user"];
                                if(survey_list.Any(x => x.is_deleted == 0 && x.member_id == loginMember.id))
                                {
                                    exten.survey_status = survey_status_options.done.ToString();
                                }
                            }
                        }
                        else
                        {
                            exten.survey_status = survey_status_options.unopened.ToString();
                        }

                        extens.Add(exten);
                    }
                    ret.data1 = extens;
                    ret.hasData = true;
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult CourseSurveySearch(QueryObj_SURVEY q)
        {
            if (q == null) q = new QueryObj_SURVEY();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.SURVEY
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    //是否為發佈期間內
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (q.member_id < 1 ? true : a.member_id == q.member_id) &&               //會員編號篩選
                    (q.course_id < 1 ? true : a.course_id == q.course_id)                  //課程編號篩選
                    //((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(課程名稱 地址)
                    //                                                a.title.Contains(q.search_keyword) ||
                    //                                                a.address.Contains(q.search_keyword)
                    //                                            ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();
                    //).OrderByDescending(x => x.date_created).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.SURVEY
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    //是否為發佈期間內
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (q.member_id < 1 ? true : a.member_id == q.member_id) &&               //會員編號篩選
                    (q.course_id < 1 ? true : a.course_id == q.course_id)                  //課程編號篩選
                    //((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(課程名稱 地址)
                    //                                                a.title.Contains(q.search_keyword) ||
                    //                                                a.address.Contains(q.search_keyword)
                    //                                            ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }




        [HttpPost]
        public JsonResult SurveyMemberSearch(QueryObj_SURVEY q)
        {
            if (q == null) q = new QueryObj_SURVEY();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.SURVEY
                    join b in DB.MEMBER on a.member_id equals b.id
                    join c in DB.COURSE on a.course_id equals c.id
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    //是否為發佈期間內
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (q.member_id < 1 ? true : a.member_id == q.member_id) &&               //會員編號篩選
                    (q.course_id < 1 ? true : a.course_id == q.course_id)                  //課程編號篩選
                    //((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(課程名稱 地址)
                    //                                                a.title.Contains(q.search_keyword) ||
                    //                                                a.address.Contains(q.search_keyword)
                    //                                            ))
                    select new {
                        survey = a,
                        member = b,
                        course = c,
                    }
                    ).OrderByDescending(x => x.survey.date_created).Skip(skipCount).Take(q.pageSize).ToList();
                //).OrderByDescending(x => x.date_created).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.SURVEY
                    join b in DB.MEMBER on a.member_id equals b.id
                    join c in DB.COURSE on a.course_id equals c.id
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    //是否為發佈期間內
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (q.member_id < 1 ? true : a.member_id == q.member_id) &&               //會員編號篩選
                    (q.course_id < 1 ? true : a.course_id == q.course_id)                  //課程編號篩選
                    //((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(課程名稱 地址)
                    //                                                a.title.Contains(q.search_keyword) ||
                    //                                                a.address.Contains(q.search_keyword)
                    //                                            ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }



        [HttpPost]
        public JsonResult CourseMemberSearch(QueryObj_COURSE_MEMBER q)
        {
            if (q == null) q = new QueryObj_COURSE_MEMBER();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            MEMBER loginMember = new MEMBER();
            if(Session["user"] != null) loginMember = (MEMBER)Session["user"];

                //設定時間區間 目標為抓出今天日期的資料
                DateTime upper = q.date.AddDays(-1);
                upper = new DateTime(upper.Year, upper.Month, upper.Day, 23, 59, 59);
                DateTime lower = q.date.AddDays(1);
                lower = new DateTime(lower.Year, lower.Month, lower.Day, 0, 0, 0);

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.COURSE_MEMBER
                    join b in DB.COURSE on a.course_id equals b.id
                    join c in DB.MEMBER on a.member_id equals c.id
                    where
                    a.is_deleted == 0 &&
                    (q.date.Year == 1753 ? true : (upper < b.time && b.time < lower)) &&     //時間區間篩選
                    //是否為發佈期間內
                    (q.member_id < 1 ? true : a.member_id == q.member_id) &&            //會員編號篩選
                    (q.course_id < 1 ? true : a.course_id == q.course_id) &&            //課程篩選
                    (q.id < 1 ? true : a.id == q.id) &&                                 //資料編號篩選
                    (q.is_enabled == -1 ? true : a.is_enabled == q.is_enabled) &&       //審核狀況篩選
                    //問卷狀況篩選
                    (
                        string.IsNullOrEmpty(q.survey_status) ? true :          //無篩選
                        (
                            q.survey_status == survey_status_options.unopened.ToString() ? (b.time >= DateTime.Now) :               //未開放填寫的CASE
                            (
                                q.survey_status == survey_status_options.done.ToString() ? (DB.SURVEY.Any(x => x.is_deleted == 0        //已填寫的CASE
                                                                                                            && x.course_id == b.id 
                                                                                                            && x.member_id == loginMember.id)) :
                                (
                                    q.survey_status == survey_status_options.opened.ToString() ? (b.time <= DateTime.Now &&                 //開放填寫但未填寫的CASE
                                                                                                 (DB.SURVEY.Any(x => x.is_deleted == 0 
                                                                                                                  && x.course_id == b.id 
                                                                                                                  && x.member_id == loginMember.id) == false)) : 
                                    (
                                    false       //無法辨認參數的CASE
                                    )
                                )
                            )
                        )
                    ) &&

                    (string.IsNullOrEmpty(q.search_keyword) ? true :                    //關鍵字搜尋(課程名稱 課程地點)
                                            (b.title.Contains(q.search_keyword) || 
                                             b.location.Contains(q.search_keyword)))
                    select new
                    {
                        course_member = a,
                        course = b,
                        member = c,
                    }
                    ).OrderByDescending(x => x.course_member.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.COURSE_MEMBER
                    join b in DB.COURSE on a.course_id equals b.id
                    join c in DB.MEMBER on a.member_id equals c.id
                    where
                    a.is_deleted == 0 &&
                    (q.date.Year == 1753 ? true : (upper < b.time && b.time < lower)) &&     //時間區間篩選
                    //是否為發佈期間內
                    (q.member_id < 1 ? true : a.member_id == q.member_id) &&            //會員編號篩選
                    (q.course_id < 1 ? true : a.course_id == q.course_id) &&            //課程篩選
                    (q.id < 1 ? true : a.id == q.id) &&                                 //資料編號篩選
                    (q.is_enabled == -1 ? true : a.is_enabled == q.is_enabled) &&       //審核狀況篩選
                    //問卷狀況篩選
                    (
                        string.IsNullOrEmpty(q.survey_status) ? true :          //無篩選
                        (
                            q.survey_status == survey_status_options.unopened.ToString() ? (b.time >= DateTime.Now) :               //未開放填寫的CASE
                            (
                                q.survey_status == survey_status_options.done.ToString() ? (DB.SURVEY.Any(x => x.is_deleted == 0        //已填寫的CASE
                                                                                                            && x.course_id == b.id
                                                                                                            && x.member_id == loginMember.id)) :
                                (
                                    q.survey_status == survey_status_options.opened.ToString() ? (b.time <= DateTime.Now &&                 //開放填寫但未填寫的CASE
                                                                                                 (DB.SURVEY.Any(x => x.is_deleted == 0
                                                                                                                  && x.course_id == b.id
                                                                                                                  && x.member_id == loginMember.id) == false)) :
                                    (
                                    false       //無法辨認參數的CASE
                                    )
                                )
                            )
                        )
                    ) &&

                    (string.IsNullOrEmpty(q.search_keyword) ? true :                    //關鍵字搜尋(課程名稱 課程地點)
                                            (b.title.Contains(q.search_keyword) ||
                                             b.location.Contains(q.search_keyword)))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }















        [HttpPost]
        public JsonResult CourseSave(COURSE data)
        {
            ret = new resultObj();

            QueryObj_COURSE.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.COURSE.Add(data);
                }
                else
                {
                    var obj = DB.COURSE.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult CourseMemberSave(COURSE_MEMBER data)
        {
            ret = new resultObj();

            if (data.course_id == 0 || data.member_id == 0) return Json(ret);

            QueryObj_COURSE_MEMBER.stringAttrInit(ref data);

            List<checkRuleObj> rules = new List<checkRuleObj>();
            int msg = 0;
            checkRuleObj ckObj = new checkRuleObj("alert");
            msg = checkClass.ck_course_sign_up_repeat(data.course_id, data.member_id); if (msg != 0) ckObj.checkResult.Add(msg);
            if (msg == 0) { msg = checkClass.ck_course_sign_up_fully(data.course_id); if (msg != 0) ckObj.checkResult.Add(msg); }
            
            if (ckObj.checkResult.Count > 0) rules.Add(ckObj);
            if (rules.Count > 0)
            {
                ret.data2 = rules;
                return Json(ret);
            }

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.COURSE_MEMBER.Add(data);
                }
                else
                {
                    var obj = DB.COURSE_MEMBER.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }









        [HttpPost]
        public JsonResult CourseRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.COURSE.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult CourseMemberRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.COURSE_MEMBER.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult CourseMemberEnableChange(int[] ids, int is_enabled)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.COURSE_MEMBER.Find(id);
                    if (obj != null)
                    {
                        obj.is_enabled = is_enabled;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }










        public ActionResult ExportMemberData(QueryObj_COURSE_MEMBER q)
        {
            ret = new resultObj();

            //QueryObj_MEMBER q = new QueryObj_MEMBER();

            MemberDataExport(q);
            //QueryObj_Member q = new QueryObj_Member();
            //q.pageSize = 0;
            ////q.scope = "A4";
            //mc.MemberRealnameExport_Common(q);

            return View();
        }


        public void MemberDataExport(QueryObj_COURSE_MEMBER q)
        {
            string file_title = "";         //檔案名稱依課程名稱為主
            if (q == null) q = new QueryObj_COURSE_MEMBER();

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            using (var DB = new SOUTHMAKEREntities())
            {
                var list = (
                                from a in DB.COURSE_MEMBER
                                join b in DB.COURSE on a.course_id equals b.id
                                join c in DB.MEMBER on a.member_id equals c.id
                                where
                                a.is_deleted == 0 &&
                                //時間區間篩選
                                //是否為發佈期間內
                                (q.course_id < 1 ? true : a.course_id == q.course_id) &&        //課程篩選
                                (q.id < 1 ? true : a.id == q.id)                                //資料編號篩選
                                select new
                                {
                                    course_member = a,
                                    course = b,
                                    member = c,
                                }
                                ).OrderBy(x => x.course_member.date_created).ToList();

                int excelFirstReadRowIndex = 0;

                HSSFWorkbook workbook = new HSSFWorkbook();
                MemoryStream ms = new MemoryStream();
                HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("data");

                //寫入前置文字敘述
                //HSSFRow row1 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
                //HSSFRow row2 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
                //row2.CreateCell(0).SetCellValue("Institutional User Database (source: 機構註冊 / 實名驗證 / 重設登入密碼 / 設置交易密碼)");
                //HSSFRow row3 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
                //row3.CreateCell(0).SetCellValue("Report Date:           YYYY/MM/DD，HH:MM:SS");
                //HSSFRow row4 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
                //row4.CreateCell(0).SetCellValue("Report Period: from    YYYY/MM/DD，HH:MM:SS     to      YYYY/MM/DD，HH:MM:SS");
                //HSSFRow row5 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
                //HSSFRow row6 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);

                //載入欄位順序設定
                MemberDataExcelColumn[] columnSort = getMemberColumnSort();
                //寫入欄位名稱
                int colIndex = 0;
                HSSFRow row = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
                foreach (var c in columnSort)
                {
                    sheet.SetColumnWidth(colIndex, MemberColumnWidth(c));
                    row.CreateCell(colIndex++).SetCellValue(MemberDataExcelColumnChNameTitle(c));
                }

                if(list.Count > 0) file_title = list[0].course.title;

                //寫入各個欄位值
                foreach (var data in list)
                {
                    colIndex = 0;
                    HSSFRow dataRow = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
                    foreach (var c in columnSort)
                    {
                        dataRow.CreateCell(colIndex++).SetCellValue(MemberDataExcelColumnValue(c, data.course_member, data.course, data.member));
                    }
                }

                //寫入輸出流
                workbook.Write(ms);
                workbook = null;
                HttpContext.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=course_member(" + file_title + ").xls"));
                HttpContext.Response.ContentType = "application/text";
                HttpContext.Response.ContentEncoding = System.Text.Encoding.UTF8;
                HttpContext.Response.BinaryWrite(ms.ToArray());
                HttpContext.Response.End();

            }

        }


        public string MemberDataExcelColumnChNameTitle(MemberDataExcelColumn c)
        {
            string ret = "";
            switch (c)
            {
                case MemberDataExcelColumn.account: ret = "E-Mail"; break;
                case MemberDataExcelColumn.name: ret = "會員名稱"; break;
                case MemberDataExcelColumn.company: ret = "公司"; break;
                case MemberDataExcelColumn.tel: ret = "電話"; break;
                case MemberDataExcelColumn.is_enabled: ret = "是否審核通過"; break;

                default: break;
            }
            return ret;
        }

        public string MemberDataExcelColumnValue(MemberDataExcelColumn c, COURSE_MEMBER CMObj, COURSE CObj, MEMBER MObj)
        {
            string ret = "";
            switch (c)
            {
                //case MemberDataExcelColumn.account_id: ret = Member.account_id; break;

                case MemberDataExcelColumn.account: ret = MObj.account; break;
                case MemberDataExcelColumn.name: ret = MObj.name; break;
                case MemberDataExcelColumn.company: ret = MObj.company; break;
                case MemberDataExcelColumn.tel: ret = MObj.tel; break;
                case MemberDataExcelColumn.is_enabled: ret = (CMObj.is_enabled == 1 ? "審核成功" : "尚未審核"); break;
                default: break;
            }
            return ret;
        }

        public MemberDataExcelColumn[] getMemberColumnSort()
        {
            return new MemberDataExcelColumn[] {
                    MemberDataExcelColumn.name,
                    MemberDataExcelColumn.company,
                    MemberDataExcelColumn.account,
                    MemberDataExcelColumn.tel,
                    MemberDataExcelColumn.is_enabled,
                };
        }

        public int MemberColumnWidth(MemberDataExcelColumn c)
        {
            int width = 4000;
            switch (c)
            {
                //case MemberDataExcelColumn.account: width = 4000; break;

                default: break;
            }
            return width;
        }



    }
}



