﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;

namespace southmaker.Controllers
{
    public class EquipController : Controller
    {
        public resultObj ret = new resultObj();


        public ActionResult List() { return View(); }


        [HttpPost]
        public JsonResult EquipSearch(QueryObj_EQUIP q)
        {
            if (q == null) q = new QueryObj_EQUIP();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.EQUIP
                    where
                    a.is_deleted == 0 &&
                    (q.is_enabled != 1 ? true :                                         //無視此篩選
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    )) &&
                    (q.equip_cate < 1 ? true : a.equip_cate == q.equip_cate) &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題)
                                                                    a.title.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.EQUIP
                    where
                    a.is_deleted == 0 &&
                    (q.is_enabled != 1 ? true :                                         //無視此篩選
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    )) &&
                    (q.equip_cate < 1 ? true : a.equip_cate == q.equip_cate) &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題)
                                                                    a.title.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult EquipCateSearch(QueryObj_EQUIP_CATE q)
        {
            if (q == null) q = new QueryObj_EQUIP_CATE();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.EQUIP_CATE
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    //是否為發佈期間內
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題)
                                                                    a.title.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.EQUIP_CATE
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    //是否為發佈期間內
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題)
                                                                    a.title.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }


















        [HttpPost]
        public JsonResult EquipSave(EQUIP data)
        {
            ret = new resultObj();

            QueryObj_EQUIP.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.EQUIP.Add(data);
                }
                else
                {
                    var obj = DB.EQUIP.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult EquipCateSave(EQUIP_CATE data)
        {
            ret = new resultObj();

            QueryObj_EQUIP_CATE.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.EQUIP_CATE.Add(data);
                }
                else
                {
                    var obj = DB.EQUIP_CATE.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }








        [HttpPost]
        public JsonResult EquipRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.EQUIP.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult EquipCateRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.EQUIP_CATE.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }


    }
}