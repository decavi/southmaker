﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;

namespace southmaker.Controllers
{
    public class HomeController : Controller
    {
        public resultObj ret = new resultObj();

        public ActionResult Index() { return View(); }
        public ActionResult Contact() { return View(); }
        public ActionResult Map() { return View(); }
        public ActionResult Search() { return View(); }

        public ActionResult About() { return View(); }
        public ActionResult Intro() { return View(); }
        public ActionResult MakerCar() { return View(); }
        public ActionResult Maker() { return View(); }
        public ActionResult MakerDetail() { helper.SaveDetailViewAndTodayVisitor("maker", Request["id"]); return View(); }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(string id) { return View(); }



        [HttpPost]
        public JsonResult AboutSearch(QueryObj_ABOUT q)
        {
            if (q == null) q = new QueryObj_ABOUT();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.ABOUT
                    where
                    a.is_deleted == 0 &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 內容)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.content.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.ABOUT
                    where
                    a.is_deleted == 0 &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 內容)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.content.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }




        [HttpPost]
        public JsonResult IntroSearch(QueryObj_INTRO q)
        {
            if (q == null) q = new QueryObj_INTRO();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.INTRO
                    where
                    a.is_deleted == 0 &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 內容)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.content.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.INTRO
                    where
                    a.is_deleted == 0 &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 內容)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.content.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult MakerCarSearch(QueryObj_MAKER_CAR q)
        {
            if (q == null) q = new QueryObj_MAKER_CAR();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.MAKER_CAR
                    where
                    a.is_deleted == 0 &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 內容)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.content.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.MAKER_CAR
                    where
                    a.is_deleted == 0 &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 內容)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.content.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult TourPointSearch(QueryObj_TOUR_POINT q)
        {
            if (q == null) q = new QueryObj_TOUR_POINT();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                var query = (
                    from a in DB.TOUR_POINT
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    (q.is_enabled != 1 ? true :                                         //無視此篩選
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    )) &&

                    //時間區間篩選
                    (
                        ((q.date_start.Year == 1753) ? true : a.time >= q.date_start) &&
                        ((q.date_end.Year == 1753) ? true : a.time <= q.date_end)
                    ) &&

                    (q.district < 1 ? true : a.district == q.district) &&                 //縣市篩選

                    //歷史年篩選
                    (q.history_year == 0 ? true : q.history_year == a.time.Year) &&

                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(課程名稱 地點 地址)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.location.Contains(q.search_keyword) ||
                                                                    a.address.Contains(q.search_keyword)
                                                                ))
                    select a
                    );


                //須分頁的Query

                //抓目前限制範圍的資料
                //int skipCount = (q.pageNo - 1) * q.pageSize;
                //var result = (
                //    from a in DB.TOUR_POINT
                //    where
                //    a.is_deleted == 0 &&
                //    //時間區間篩選
                //    (q.is_enabled != 1 ? true :                                         //無視此篩選
                //    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                //        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                //        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                //         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                //    )) &&
                //    (q.district < 1 ? true : a.district == q.district) &&                 //縣市篩選
                //    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                //    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                //    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(課程名稱 地點 地址)
                //                                                    a.title.Contains(q.search_keyword) ||
                //                                                    a.location.Contains(q.search_keyword) ||
                //                                                    a.address.Contains(q.search_keyword)
                //                                                ))
                //    select a
                //    ).OrderByDescending(x => x.time).Skip(skipCount).Take(q.pageSize).ToList();

                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = query.OrderByDescending(x => x.time).Skip(skipCount).Take(q.pageSize).ToList();
                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                //int totalCount = (
                //    from a in DB.TOUR_POINT
                //    where
                //    a.is_deleted == 0 &&
                //    //時間區間篩選
                //    (q.is_enabled != 1 ? true :                                         //無視此篩選
                //    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                //        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                //        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                //         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                //    )) &&
                //    (q.district < 1 ? true : a.district == q.district) &&                 //縣市篩選
                //    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                //    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                //    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(課程名稱 地點 地址)
                //                                                    a.title.Contains(q.search_keyword) ||
                //                                                    a.location.Contains(q.search_keyword) ||
                //                                                    a.address.Contains(q.search_keyword)
                //                                                ))
                //    select a
                //        ).Distinct().Count();
                int totalCount = query.Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult MakerSearch(QueryObj_MAKER q)
        {
            if (q == null) q = new QueryObj_MAKER();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.MAKER
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    (q.is_enabled != 1 ? true : a.is_enabled == 1) &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 介紹)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.intro.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.MAKER
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    (q.is_enabled != 1 ? true : a.is_enabled == 1) &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 介紹)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.intro.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult ADSearch(QueryObj_AD q)
        {
            if (q == null) q = new QueryObj_AD();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.AD
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    //是否為發佈期間內
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 介紹)
                                                                    a.title.Contains(q.search_keyword) //||
                                                                    //a.intro.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.AD
                    where
                    a.is_deleted == 0 &&
                    //時間區間篩選
                    //是否為發佈期間內
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(標題 介紹)
                                                                    a.title.Contains(q.search_keyword) //||
                                                                                                       //a.intro.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }







        [HttpPost]
        public JsonResult GlobalSearch(QueryObj_Common q)
        {
            if (q == null) q = new QueryObj_Common();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                int skipCount = (q.pageNo - 1) * q.pageSize;        //抓目前限制範圍的資料

                //MAKER
                var result = (
                    from a in DB.MAKER
                    where a.is_deleted == 0 && a.is_enabled == 1 &&
                    (a.title.Contains(q.search_keyword) || a.intro.Contains(q.search_keyword))
                    select new
                    {
                        unit = "maker",
                        title = a.title,
                        content = a.intro,
                        date = a.date_created,
                        url = "/Home/MakerDetail?id=" + a.id.ToString(),
                        view = DB.DETAIL_VIEW.Any(x => x.unit == "maker" && x.obj_id == a.id) ? DB.DETAIL_VIEW.Where(x => x.unit == "maker" && x.obj_id == a.id).Select(x => x.count).FirstOrDefault() : 0,
                        logo_img = DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "maker_logo" && x.img_size == "S") ?
                        DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "maker_logo" && x.img_size == "S").Select(x => "/upload/" + x.@class + "/" + x.obj_id + "/" + x.name + "@" + x.img_size + "." + x.exten).FirstOrDefault() :
                        (   //如初次搜尋不到，改搜尋看看舊站資料
                            DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.@class == "") ?
                            DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.img_size == "").Select(x => x.name).FirstOrDefault() : helper.default_img_url
                        ),
                    }
                    );

                var course_result = (
                    from a in DB.COURSE
                    where a.is_deleted == 0 && a.is_enabled == 1 &&
                    (a.title.Contains(q.search_keyword) || a.content.Contains(q.search_keyword))
                    select new
                    {
                        unit = "course",
                        title = a.title,
                        content = a.content,
                        date = a.date_created,
                        url = "/Course/Detail?id=" + a.id.ToString(),
                        view = DB.DETAIL_VIEW.Any(x => x.unit == "course" && x.obj_id == a.id) ? DB.DETAIL_VIEW.Where(x => x.unit == "course" && x.obj_id == a.id).Select(x => x.count).FirstOrDefault() : 0,
                        logo_img = DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "course_logo" && x.img_size == "S") ?
                                    DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "course_logo" && x.img_size == "S").Select(x => "/upload/" + x.@class + "/" + x.obj_id + "/" + x.name + "@" + x.img_size + "." + x.exten).FirstOrDefault() :
                        (   //如初次搜尋不到，改搜尋看看舊站資料
                            DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.@class == "") ?
                            DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.img_size == "").Select(x => x.name).FirstOrDefault() : helper.default_img_url
                        ),
                    }
                    );
                result = result.Union(course_result);

                var news_result = (
                    from a in DB.NEWS
                    where a.is_deleted == 0 &&
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    ) &&
                    (a.title.Contains(q.search_keyword) || a.content.Contains(q.search_keyword))
                    select new
                    {
                        unit = "news",
                        title = a.title,
                        content = a.content,
                        date = a.date_created,
                        url = "/News/Detail?id=" + a.id.ToString(),
                        view = DB.DETAIL_VIEW.Any(x => x.unit == "news" && x.obj_id == a.id) ? DB.DETAIL_VIEW.Where(x => x.unit == "news" && x.obj_id == a.id).Select(x => x.count).FirstOrDefault() : 0,
                        logo_img = DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "news_logo" && x.img_size == "S") ?
                                    DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "news_logo" && x.img_size == "S").Select(x => "/upload/" + x.@class + "/" + x.obj_id + "/" + x.name + "@" + x.img_size + "." + x.exten).FirstOrDefault() :
                        (   //如初次搜尋不到，改搜尋看看舊站資料
                            DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.@class == "") ?
                            DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.img_size == "").Select(x => x.name).FirstOrDefault() : helper.default_img_url
                        ),
                    }
                    );
                result = result.Union(news_result);

                var album_result = (
                    from a in DB.ALBUM
                    where a.is_deleted == 0 &&
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    ) &&
                    a.title.Contains(q.search_keyword)
                    select new
                    {
                        unit = "album",
                        title = a.title,
                        content = "",
                        date = a.date_created,
                        url = "/News/Photo?id=" + a.id.ToString(),
                        view = DB.DETAIL_VIEW.Any(x => x.unit == "album" && x.obj_id == a.id) ? DB.DETAIL_VIEW.Where(x => x.unit == "album" && x.obj_id == a.id).Select(x => x.count).FirstOrDefault() : 0,
                        logo_img = DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "album" && x.img_size == "S") ?
                                    DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "album" && x.img_size == "S").Select(x => "/upload/" + x.@class + "/" + x.obj_id + "/" + x.name + "@" + x.img_size + "." + x.exten).FirstOrDefault() :
                        (   //如初次搜尋不到，改搜尋看看舊站資料
                            DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.@class == "") ?
                            DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.img_size == "").Select(x => x.name).FirstOrDefault() : helper.default_img_url
                        ),
                    }
                    );
                result = result.Union(album_result);

                var video_result = (
                    from a in DB.VIDEO
                    where a.is_deleted == 0 &&
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    ) &&
                    a.title.Contains(q.search_keyword)
                    select new
                    {
                        unit = "video",
                        title = a.title,
                        content = "",
                        date = a.date_created,
                        url = a.url.Contains("http") ? a.url : "http://" + a.url,
                        view = DB.DETAIL_VIEW.Any(x => x.unit == "video" && x.obj_id == a.id) ? DB.DETAIL_VIEW.Where(x => x.unit == "video" && x.obj_id == a.id).Select(x => x.count).FirstOrDefault() : 0,
                        //logo_img = "https://img.youtube.com/vi/" + (a.url.Contains("?v=") ? a.url.Split(new string[] { "?v=" }, StringSplitOptions.None)[1].Split('&')[0] : "") + "/0.jpg",
                        logo_img = "",
                    }
                    );
                result = result.Union(video_result);

                var equip_result = (
                    from a in DB.EQUIP
                    where a.is_deleted == 0 &&
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    ) &&
                    (a.title.Contains(q.search_keyword) || a.content.Contains(q.search_keyword))
                    select new
                    {
                        unit = "equip",
                        title = a.title,
                        content = a.content,
                        date = a.date_created,
                        url = "",
                        view = 0,
                        logo_img = DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "equip" && x.img_size == "S") ?
                                    DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "equip" && x.img_size == "S").Select(x => "/upload/" + x.@class + "/" + x.obj_id + "/" + x.name + "@" + x.img_size + "." + x.exten).FirstOrDefault() :
                        (   //如初次搜尋不到，改搜尋看看舊站資料
                            DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.@class == "") ?
                            DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.img_size == "").Select(x => x.name).FirstOrDefault() : helper.default_img_url
                        ),
                    }
                    );
                result = result.Union(equip_result);

                var works_result = (
                    from a in DB.WORKS
                    where a.is_deleted == 0 &&
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    ) &&
                    (a.title.Contains(q.search_keyword) || a.content.Contains(q.search_keyword))
                    select new
                    {
                        unit = "works",
                        title = a.title,
                        content = a.content,
                        date = a.date_created,
                        url = "",
                        view = 0,
                        logo_img = DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "works" && x.img_size == "S") ?
                                    DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "works" && x.img_size == "S").Select(x => "/upload/" + x.@class + "/" + x.obj_id + "/" + x.name + "@" + x.img_size + "." + x.exten).FirstOrDefault() :
                        (   //如初次搜尋不到，改搜尋看看舊站資料
                            DB.FILES.Any(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.@class == "") ?
                            DB.FILES.Where(x => x.is_deleted == 0 && x.is_enabled == 1 && x.obj_id == a.file_id && x.tag == "" && x.img_size == "").Select(x => x.name).FirstOrDefault() : helper.default_img_url
                        ),
                    }
                    );
                result = result.Union(works_result);

                int totalCount = result.Count();
                var globalResult = result.OrderBy(x => x.date).Skip(skipCount).Take(q.pageSize).ToList();
                if (globalResult.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = globalResult;
                    ret.data2 = totalCount;
                }

                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }











        [HttpPost]
        public JsonResult AboutSave(ABOUT data)
        {
            ret = new resultObj();

            QueryObj_ABOUT.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.ABOUT.Add(data);
                }
                else
                {
                    var obj = DB.ABOUT.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult IntroSave(INTRO data)
        {
            ret = new resultObj();

            QueryObj_INTRO.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.INTRO.Add(data);
                }
                else
                {
                    var obj = DB.INTRO.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult MakerCarSave(MAKER_CAR data)
        {
            ret = new resultObj();

            QueryObj_MAKER_CAR.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.MAKER_CAR.Add(data);
                }
                else
                {
                    var obj = DB.MAKER_CAR.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult TourPointSave(TOUR_POINT data)
        {
            ret = new resultObj();

            QueryObj_TOUR_POINT.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.TOUR_POINT.Add(data);
                }
                else
                {
                    var obj = DB.TOUR_POINT.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult MakerSave(MAKER data)
        {
            ret = new resultObj();

            QueryObj_MAKER.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.MAKER.Add(data);
                }
                else
                {
                    var obj = DB.MAKER.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult ADSave(AD data)
        {
            ret = new resultObj();

            QueryObj_AD.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.AD.Add(data);
                }
                else
                {
                    var obj = DB.AD.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }













        [HttpPost]
        public JsonResult AboutRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.ABOUT.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult IntroRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.INTRO.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult MakerCarRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.MAKER_CAR.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult TourPointRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.TOUR_POINT.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult MakerRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.MAKER.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult ADRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.AD.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }


        
        [HttpPost]
        public JsonResult ContactUs(string name, string email, string tel, string title, string content, string captcha)
        {
            ret = new resultObj();

            try
            {
                if (name == null) name = ""; if (email == null) email = ""; if (tel == null) tel = "";
                if (title == null) title = ""; if (content == null) content = "";

                List<checkRuleObj> rules = new List<checkRuleObj>();
                int msg = 0;
                checkRuleObj ckObj = new checkRuleObj("contact_captcha");
                //if (Session["ContactCaptchaObj"] != null)
                if (Session["user_contact_captcha"] != null)
                {
                    msg = checkClass.ck_captcha_correct(Session["user_contact_captcha"], captcha); if (msg != 0) ckObj.checkResult.Add(msg);
                    if (ckObj.checkResult.Count > 0) rules.Add(ckObj);
                }
                if (rules.Count > 0)
                {
                    ret.data2 = rules;
                    return Json(ret);
                }

                //寄mail

                using (System.Net.Mail.SmtpClient mysmtp = helper.getMailSetting())
                {
                    content = content + "<br /><br />" + "來信者： " + name + "<br />Email： " + email + "<br />聯絡電話： " + tel;
                    //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("\"" + "南方創客" + "\"<" + helper.senderMail + ">", "decavi400@hotmail.com", title, content);
                    System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("\"" + "南方創客" + "\"<" + helper.senderMail + ">", "stmakercenter@gmail.com", title, content);
                    mm.CC.Add("decavi400@hotmail.com");
                    mm.IsBodyHtml = true;
                    mysmtp.Send(mm);
                }
                ret.isSuccess = true;
            }
            catch (Exception ex)
            {
                ret.isSuccess = false;
                ret.data2 = ex;
            }

            return Json(ret);
        }

    }
}