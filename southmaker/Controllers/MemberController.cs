﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using System.IO;
using NPOI.HSSF.UserModel;

namespace southmaker.Controllers
{
    public class MemberController : Controller
    {
        public resultObj ret = new resultObj();


        public ActionResult Course() { if (isLogin() == false) Response.Redirect("/Member/Login"); return View(); }
        public ActionResult Edit() { if (isLogin() == false) Response.Redirect("/Member/Login"); return View(); }
        public ActionResult SurveyList() { if (isLogin() == false) Response.Redirect("/Member/Login"); return View(); }
        public ActionResult Survey() { if (isLogin() == false) Response.Redirect("/Member/Login"); return View(); }

        public ActionResult Login() { return View(); }
        public ActionResult Forgot() { return View(); }

        public ActionResult FacebookAuth() { return View(); }
        public ActionResult GoogleAuth() { return View(); }


        public bool isLogin()
        {
            bool isLogin = false;
            //bool isLogin = true;
            if (Session["user"] != null) isLogin = true;
            return isLogin;
        }

        //帳號驗證
        public ActionResult Activate(string id)
        {
            try
            {
                //先在個人用戶中找資料
                QueryObj_MEMBER q = new QueryObj_MEMBER();
                q.checkcode = id;
                using (var DB = new SOUTHMAKEREntities())
                {
                    var memberObj = DB.MEMBER.Where(x => x.checkcode == id).FirstOrDefault();
                    if(memberObj != null)       //找到該用戶
                    {
                        memberObj.is_enabled = 1;                           //開通帳號
                        var obj = DB.MEMBER.Find(memberObj.id);
                        DB.Entry(obj).CurrentValues.SetValues(memberObj);
                        DB.SaveChanges();
                        Session["user"] = memberObj;                        //給予Session
                        return RedirectToAction("Edit", "Member");        //導頁至會員中心
                    }
                    else                        //找不到該用戶    自然導至首頁
                    {
                                    
                    }
                }
            }
            catch (Exception ex) { }
            return RedirectToAction("Index", "Home");
        }


        [HttpPost]
        public JsonResult GetAPILoginURI(string tag, string feed)
        {
            string URI = "";
            if (tag == null) tag = "";

            switch (tag)
            {
                case "fb_login":
                    {
                        string authUrl = Request.Url.Scheme + "://" + Request.Url.Authority + "/Member/FacebookAuth";
                        URI = "https://www.facebook.com/dialog/oauth?client_id=1704458536312347&scope=email&redirect_uri=" + authUrl + "&state=southmaker";
                    }
                    break;
                case "fb_token":
                    {
                        if (feed == null) feed = "";
                        string authUrl = Request.Url.Scheme + "://" + Request.Url.Authority + "/Member/FacebookAuth";
                        URI = "https://graph.facebook.com/oauth/access_token?client_id=1704458536312347&redirect_uri=" + authUrl + "&client_secret=e14611d5366beac92f54b4c7133d134b&code=" + feed;
                    }
                    break;
                case "fb_profile":
                    {
                        if (feed == null) feed = "";
                        URI = "https://graph.facebook.com/me?fields=id,name,gender,email&access_token=" + feed;
                    }
                    break;

                case "google_login":
                    {
                        string authUrl = Request.Url.Scheme + "://" + Request.Url.Authority + "/Member/GoogleAuth";
                        URI = "https://accounts.google.com/o/oauth2/auth?response_type=token&client_id=644724830221-r03ujojj9uhqbta75cilscrj3mv5bpbe.apps.googleusercontent.com&redirect_uri=" + authUrl + "&scope=https://www.googleapis.com/auth/userinfo.email";
                    }
                    break;
                case "google_userinfo":
                    {
                        if (feed == null) feed = "";
                        URI = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + feed;
                    }
                    break;

                default:break;
            }
            return Json(URI);
        }




        [HttpPost]
        public JsonResult FacebookLogin(string email, string gender, string name)
        {
            ret = new resultObj();
            if (string.IsNullOrEmpty(email)) return Json(ret);

            if (gender == null) gender = ""; if (name == null) name = "";
            using (var DB = new SOUTHMAKEREntities())
            {
                MEMBER findObj = DB.MEMBER.Where(x => x.account == email && x.is_deleted == 0).FirstOrDefault();
                if (findObj == null)     //未找到資料 先保存基本資料
                {
                    MEMBER newObj = new MEMBER();
                    newObj.account = email;
                    newObj.email = email;
                    newObj.gender = gender;
                    newObj.name = name;
                    newObj.note = "facebook";
                    newObj.is_enabled = 1;
                    newObj.date_created = DateTime.Now;
                    newObj.date_modify = DateTime.Now;

                    QueryObj_MEMBER.stringAttrInit(ref newObj);
                    DB.MEMBER.Add(newObj);
                    DB.SaveChanges();
                    Session["user"] = newObj;
                    ret.data1 = newObj;
                }
                else            //有找到資料 直接授權Session
                {
                    Session["user"] = findObj;
                    ret.data1 = findObj;
                }
            }

            ret.isSuccess = true;
            return Json(ret);
        }


        [HttpPost]
        public JsonResult GoogleLogin(string email, string gender, string name)
        {
            ret = new resultObj();
            if (string.IsNullOrEmpty(email)) return Json(ret);

            if (gender == null) gender = ""; if (name == null) name = "";
            using (var DB = new SOUTHMAKEREntities())
            {
                MEMBER findObj = DB.MEMBER.Where(x => x.account == email && x.is_deleted == 0).FirstOrDefault();
                if(findObj == null)     //未找到資料 先保存基本資料
                {
                    MEMBER newObj = new MEMBER();
                    newObj.account = email;
                    newObj.email = email;
                    newObj.gender = gender;
                    newObj.name = name;
                    newObj.note = "google";
                    newObj.is_enabled = 1;
                    newObj.date_created = DateTime.Now;
                    newObj.date_modify = DateTime.Now;

                    QueryObj_MEMBER.stringAttrInit(ref newObj);
                    DB.MEMBER.Add(newObj);
                    DB.SaveChanges();
                    Session["user"] = newObj;
                    ret.data1 = newObj;
                }
                else            //有找到資料 直接授權Session
                {
                    Session["user"] = findObj;
                    ret.data1 = findObj;
                }
            }

            ret.isSuccess = true;
            return Json(ret);
        }




        [HttpPost]
        public JsonResult Login(string account, string pwd, string captcha)
        {
            ret = new resultObj();

            if (account == null) account = ""; if (pwd == null) pwd = ""; if (captcha == null) captcha = "";

            List<checkRuleObj> rules = new List<checkRuleObj>();
            //rules.Add(new checkRuleObj("account", checkCode.memberEmailExist, checkCode.memberAccountActivate));
            //rules.Add(new checkRuleObj("password", checkCode.memberPasswordCorrect));
            //rules.Add(new checkRuleObj("captcha", checkCode.captchaCorrect));
            int msg = 0;
            checkRuleObj ckObj = new checkRuleObj("");
            //if (Session["LoginCaptchaObj"] != null)
            //{
                //msg = checkClass.ck_member_account_login(account, pwd, Session["LoginCaptchaObj"], captcha); if (msg != 0) ckObj.checkResult.Add(msg);
                msg = checkClass.ck_member_account_login(account, pwd, Session["user_login_captcha"], captcha); if (msg != 0) ckObj.checkResult.Add(msg);
                if (msg == (int)checkCode.memberEmailExist) ckObj.checkTag = "account";
                if (msg == (int)checkCode.memberAccountActivate) ckObj.checkTag = "account";
                if (msg == (int)checkCode.memberPasswordCorrect) ckObj.checkTag = "password";
                if (msg == (int)checkCode.captchaCorrect) ckObj.checkTag = "captcha";
                if (ckObj.checkResult.Count > 0) rules.Add(ckObj);
            //}
            if (rules.Count > 0)
            {
                ret.data2 = rules;
                return Json(ret);
            }

            //if (Session["LoginCaptchaObj"] != null)
            //{
            //    ckc.captchaObj = Session["LoginCaptchaObj"];
            //    ckc.captcha = captcha;
            //}
            using (var DB = new SOUTHMAKEREntities())
            {
                MEMBER memberObj = DB.MEMBER.Where(x => x.is_deleted == 0 && x.account == account).FirstOrDefault();
                if (memberObj != null)
                {
                    Session["user"] = memberObj;
                    ret.data1 = memberObj;
                }
            }
            
            ret.isSuccess = true;
            return Json(ret);
        }


        [HttpPost]
        public JsonResult Logout()
        {
            ret = new resultObj();
            Session["user"] = null;
            ret.isSuccess = true;
            return Json(ret);
        }







        [HttpPost]
        public JsonResult MemberSearch(QueryObj_MEMBER q)
        {
            if (q == null) q = new QueryObj_MEMBER();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.MEMBER
                    where
                    a.is_deleted == 0 &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.account) ? true : a.account == q.account) &&
                    //(string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(帳號 姓名)
                                                                    a.account.Contains(q.search_keyword) ||
                                                                    a.name.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.MEMBER
                    where
                    a.is_deleted == 0 &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    //(string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(帳號 姓名)
                                                                    a.account.Contains(q.search_keyword) ||
                                                                    a.name.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }
        












        

        [HttpPost]
        public JsonResult MemberSave(MEMBER data)
        {
            ret = new resultObj();

            QueryObj_MEMBER.stringAttrInit(ref data);

            List<checkRuleObj> rules = new List<checkRuleObj>();
            int msg = 0;
            checkRuleObj ckObj = new checkRuleObj("account");        //帳號重複性檢查
            msg = checkClass.ck_member_account_repeat(data.id, data.account); if (msg != 0) ckObj.checkResult.Add(msg);
            if (ckObj.checkResult.Count > 0) rules.Add(ckObj);
            if (rules.Count > 0)
            {
                ret.data2 = rules;
                return Json(ret);
            }

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.MEMBER.Add(data);
                }
                else
                {
                    var obj = DB.MEMBER.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult SurveySave(SURVEY data)
        {
            ret = new resultObj();

            QueryObj_SURVEY.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.SURVEY.Add(data);
                }
                else
                {
                    var obj = DB.SURVEY.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }








        [HttpPost]
        public JsonResult MemberRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.MEMBER.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult SurveyRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.SURVEY.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }










        [HttpPost]
        public JsonResult PasswordChange(string account, string _old, string _new, string mode)
        {
            ret = new resultObj();

            if (account == null) account = "";
            if (_old == null) _old = "";
            if (_new == null) _new = "";

            //前台需要舊密碼驗證  後台不用
            if (mode == "front")
            {
                List<checkRuleObj> rules = new List<checkRuleObj>();
                int msg = 0;
                checkRuleObj ckObj = new checkRuleObj("password");
                //if (Session["LoginCaptchaObj"] != null)
                //{
                msg = checkClass.ck_member_password_correct(account, _old); if (msg != 0) ckObj.checkResult.Add(msg);
                if (ckObj.checkResult.Count > 0) rules.Add(ckObj);
                //}
                if (rules.Count > 0)
                {
                    ret.data2 = rules;
                    return Json(ret);
                }
            }

            using (var DB = new SOUTHMAKEREntities())
            {
                var memberObj = DB.MEMBER.Where(x => x.is_deleted == 0 && x.account == account).FirstOrDefault();
                if (memberObj != null)
                {
                    //memberObj.password = helper.PasswordCrypt(_new);
                    memberObj.password = _new;
                    DB.SaveChanges();
                }
            }
            ret.isSuccess = true;

            return Json(ret);
        }

        [HttpPost]
        public JsonResult PasswordForgot(string account, string name, string tel)
        {
            ret = new resultObj();

            if (account == null) account = "";
            if (name == null) name = "";
            if (tel == null) tel = "";

            List<checkRuleObj> rules = new List<checkRuleObj>();
            int msg = 0;
            checkRuleObj ckObj = new checkRuleObj("");
            msg = checkClass.ck_member_password_forgot(account, name, tel); if (msg != 0) ckObj.checkResult.Add(msg);
            if (msg == (int)checkCode.memberEmailExist) ckObj.checkTag = "account";
            if (msg == (int)checkCode.memberNameCorrect) ckObj.checkTag = "name";
            if (msg == (int)checkCode.memberTelCorrect) ckObj.checkTag = "tel";
            if (ckObj.checkResult.Count > 0) rules.Add(ckObj);
            if (rules.Count > 0)
            {
                ret.data2 = rules;
                return Json(ret);
            }

            using (var DB = new SOUTHMAKEREntities())
            {
                var memberObj = DB.MEMBER.Where(x => x.is_deleted == 0 && x.account == account).FirstOrDefault();
                if (memberObj != null)
                {
                    memberObj.password = Guid.NewGuid().ToString().Substring(0, 8);             //將密碼洗為隨機8碼
                    DB.SaveChanges();
                    ret.data1 = memberObj;
                }
            }
            ret.isSuccess = true;

            return Json(ret);
        }












        //[HttpPost]
        //public ActionResult ExportMemberData(QueryObj_MEMBER q)
        //public ActionResult ExportMemberData(string keyword)
        public ActionResult ExportMemberData(QueryObj_MEMBER q)
        {
            ret = new resultObj();

            //QueryObj_MEMBER q = new QueryObj_MEMBER();

            MemberDataExport(q);
            //QueryObj_Member q = new QueryObj_Member();
            //q.pageSize = 0;
            ////q.scope = "A4";
            //mc.MemberRealnameExport_Common(q);

            return View();
        }


        public void MemberDataExport(QueryObj_MEMBER q)
        {
            if (q == null) q = new QueryObj_MEMBER();

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            List<MEMBER> list = new List<MEMBER>();

            if (q.exportExample == false)                   //非範例資料
            {
                using (var DB = new SOUTHMAKEREntities())
                {
                    list = (
                                    from a in DB.MEMBER
                                    where
                                    a.is_deleted == 0 &&

                                    //(q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(帳號 姓名)
                                                                                    a.account.Contains(q.search_keyword) ||
                                                                                    a.name.Contains(q.search_keyword)
                                                                                ))
                                    select a
                                    ).OrderBy(x => x.date_created).ToList();
                }
            }
            else                //匯出範例資料
            {
                list.Add(GetMemberExample());
            }


            int excelFirstReadRowIndex = 0;

            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream ms = new MemoryStream();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("data");

            //寫入前置文字敘述
            HSSFRow row1 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
            row1.CreateCell(0).SetCellValue("1. 欲新增資料請於【資料編號】欄留白; 欲更新資料則於該欄填入該筆資料編號)");
            HSSFRow row2 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
            row2.CreateCell(0).SetCellValue("2. 欄位名稱前有'*'的代表該欄位為必填");
            HSSFRow row3 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
            row3.CreateCell(0).SetCellValue("3. 【性別】欄位值: 【男】/【女】，如不填可留白");
            HSSFRow row4 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
            row4.CreateCell(0).SetCellValue("4. 【生日】欄位格式: YYYY/MM/DD，如不填可留白");
            HSSFRow row5 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
            var opts = helper.highest_education_options();
            string optStr = string.Join("/", opts.Where(x => x.value != "0").Select(x => "【" + x.text + "】").ToArray());
            row5.CreateCell(0).SetCellValue("5. 【學歷】欄位值: " + optStr + "，如不填可留白");
            HSSFRow row6 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
            opts = helper.course_source_options();
            optStr = string.Join("/", opts.Where(x => x.value != "0").Select(x => "【" + x.text + "】").ToArray());
            row6.CreateCell(0).SetCellValue("6. 【課程報名資訊】欄位值: " + optStr + "，如不填可留白");
            HSSFRow row7 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
            opts = helper.employ_status_options();
            optStr = string.Join("/", opts.Where(x => x.value != "0").Select(x => "【" + x.text + "】").ToArray());
            row7.CreateCell(0).SetCellValue("7. 【就業狀況】欄位值: " + optStr + "，如不填可留白");
            HSSFRow row8 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
            row8.CreateCell(0).SetCellValue("8. 【審核情況】欄位值: 【審核成功】/【尚未審核】");
            HSSFRow row9 = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);                  //第9行空一行
            row9.CreateCell(0).SetCellValue("");        


            //載入欄位順序設定
            MemberDataExcelColumn[] columnSort = getMemberColumnSort();
            //寫入欄位名稱
            int colIndex = 0;
            HSSFRow row = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
            foreach (var c in columnSort)
            {
                sheet.SetColumnWidth(colIndex, MemberColumnWidth(c));
                row.CreateCell(colIndex++).SetCellValue(MemberDataExcelColumnChNameTitle(c));
            }
            //寫入各個欄位值
            foreach (var data in list)
            {
                colIndex = 0;
                HSSFRow dataRow = (HSSFRow)sheet.CreateRow(excelFirstReadRowIndex++);
                foreach (var c in columnSort)
                {
                    dataRow.CreateCell(colIndex++).SetCellValue(MemberDataExcelExportColumnValue(c, data));
                }
            }

            //寫入輸出流
            workbook.Write(ms);
            workbook = null;

            string fileName = "";
            if (q.exportExample == true) fileName = "會員管理匯入範例";
            if (q.exportExample == false) fileName = "會員管理_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            HttpContext.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + fileName + ".xls"));
            HttpContext.Response.ContentType = "application/text";
            HttpContext.Response.ContentEncoding = System.Text.Encoding.UTF8;
            HttpContext.Response.BinaryWrite(ms.ToArray());
            HttpContext.Response.End();
        }


        public string MemberDataExcelColumnChNameTitle(MemberDataExcelColumn c)
        {
            string ret = "";
            switch (c)
            {
                case MemberDataExcelColumn.id: ret = "資料編號"; break;
                case MemberDataExcelColumn.account: ret = "*帳號"; break;
                case MemberDataExcelColumn.name: ret = "*姓名"; break;
                case MemberDataExcelColumn.gender: ret = "性別"; break;
                case MemberDataExcelColumn.birth: ret = "生日"; break;
                case MemberDataExcelColumn.occupation: ret = "職業"; break;
                case MemberDataExcelColumn.school: ret = "學校"; break;
                case MemberDataExcelColumn.company: ret = "公司"; break;
                case MemberDataExcelColumn.degree: ret = "學歷"; break;
                case MemberDataExcelColumn.title: ret = "職稱"; break;
                case MemberDataExcelColumn.department: ret = "科系"; break;
                case MemberDataExcelColumn.tel: ret = "*電話"; break;
                case MemberDataExcelColumn.email: ret = "E-Mail"; break;
                case MemberDataExcelColumn.location: ret = "居住縣市"; break;
                case MemberDataExcelColumn.motive: ret = "*學習動機"; break;
                case MemberDataExcelColumn.source: ret = "課程報名資訊"; break;
                case MemberDataExcelColumn.employ_status: ret = "就業狀況"; break;
                case MemberDataExcelColumn.date_created: ret = "註冊時間"; break;
                case MemberDataExcelColumn.date_modify: ret = "修改時間"; break;
                case MemberDataExcelColumn.is_enabled: ret = "*審核情況"; break;

                default: break;
            }
            return ret;
        }

        public string MemberDataExcelExportColumnValue(MemberDataExcelColumn c, MEMBER data)
        {
            string ret = "";
            switch (c)
            {
                //case MemberDataExcelColumn.account_id: ret = Member.account_id; break;

                case MemberDataExcelColumn.id: ret = data.id > 0 ? data.id.ToString() : ""; break;
                case MemberDataExcelColumn.account: ret = data.account; break;
                case MemberDataExcelColumn.name: ret = data.name; break;
                case MemberDataExcelColumn.gender: ret = (data.gender == "male" ? "男" : "女"); break;
                case MemberDataExcelColumn.birth: ret = ( data.birth.Year == 1753 ? "" : data.birth.ToString("yyyy/MM/dd")); break;
                case MemberDataExcelColumn.occupation: ret = helper.GetOptionText(option_type.occupation, data.occupation.ToString()); break;
                case MemberDataExcelColumn.school: ret = data.school; break;
                case MemberDataExcelColumn.company: ret = data.company; break;
                case MemberDataExcelColumn.degree: ret = helper.GetOptionText(option_type.highest_education, data.degree.ToString()); break;
                case MemberDataExcelColumn.title: ret = data.title; break;
                case MemberDataExcelColumn.department: ret = data.department; break;
                case MemberDataExcelColumn.tel: ret = data.tel; break;
                case MemberDataExcelColumn.email: ret = data.email; break;
                case MemberDataExcelColumn.location: ret = data.location; break;
                case MemberDataExcelColumn.motive: ret = data.motive; break;
                case MemberDataExcelColumn.source: ret = helper.GetOptionText(option_type.course_source, data.source.ToString()); break;
                case MemberDataExcelColumn.employ_status: ret = helper.GetOptionText(option_type.employ_status, data.employ_status.ToString()); break;
                case MemberDataExcelColumn.date_created: ret = data.date_created.ToString("yyyy/MM/dd HH:mm:ss"); break;
                case MemberDataExcelColumn.date_modify: ret = data.date_modify.ToString("yyyy/MM/dd HH:mm:ss"); break;
                case MemberDataExcelColumn.is_enabled: ret = (data.is_enabled == 1 ? "審核成功" : "尚未審核"); break;
                default: break;
            }
            return ret;
        }




        public MemberDataExcelColumn[] getMemberColumnSort()
        {
            return new MemberDataExcelColumn[] {
                    MemberDataExcelColumn.id,   
                    MemberDataExcelColumn.account,
                    MemberDataExcelColumn.name,
                    MemberDataExcelColumn.gender,
                    MemberDataExcelColumn.birth,
                    MemberDataExcelColumn.occupation,
                    MemberDataExcelColumn.school,
                    MemberDataExcelColumn.company,
                    MemberDataExcelColumn.degree,
                    MemberDataExcelColumn.title,
                    MemberDataExcelColumn.department,
                    MemberDataExcelColumn.tel,
                    MemberDataExcelColumn.email,
                    MemberDataExcelColumn.location,
                    MemberDataExcelColumn.motive,
                    MemberDataExcelColumn.source,
                    MemberDataExcelColumn.employ_status,
                    //MemberDataExcelColumn.date_created,
                    //MemberDataExcelColumn.date_modify,
                    MemberDataExcelColumn.is_enabled,
                };
        }

        public int MemberColumnWidth(MemberDataExcelColumn c)
        {
            int width = 4000;
            switch (c)
            {
                //case MemberDataExcelColumn.account: width = 4000; break;

                default: break;
            }
            return width;
        }







        [HttpPost]
        public JsonResult MemberDataImport(string tag)
        {
            int excelFirstReadRowIndex = 0;         //read row number
            excelFirstReadRowIndex += 9;       //跳過8行使用說明 + 1行留白

            List<importCheckObj> ckDic = new List<importCheckObj>();        //error dictionary
            List<MEMBER> datas = new List<MEMBER>();                        //data list

            try
            {
                //classname = classname.ToLower();

                ret = new resultObj();
                HttpFileCollectionBase objs = Request.Files;
                if (objs.Count != 1)
                {
                    ret.isSuccess = false;
                    return Json(ret);
                }

                HSSFWorkbook wb = new HSSFWorkbook(objs[0].InputStream);
                HSSFSheet sheet = (HSSFSheet)wb.GetSheetAt(0);

                //由第一列取標題做為欄位名稱
                HSSFRow headerRow = (HSSFRow)sheet.GetRow(excelFirstReadRowIndex);
                int cellCount = headerRow.LastCellNum;

                //載入欄位順序設定
                MemberDataExcelColumn[] columnSort = getMemberColumnSort();
                
                //略過標題列，一直處理至最後一列
                for (int i = (excelFirstReadRowIndex + 1); i <= sheet.LastRowNum; i++)
                {
                    List<checkRuleObj> ckObjs = new List<checkRuleObj>();

                    HSSFRow row = (HSSFRow)sheet.GetRow(i);
                    if (row == null) continue;

                    int colIndex = 0;

                    //依欄位順序來取值
                    MEMBER data = new MEMBER();
                    foreach (var c in columnSort)
                    {
                        string value = row.GetCell(colIndex) == null ? "" : row.GetCell(colIndex).ToString().Trim(); colIndex++;        //取值
                        
                        checkRuleObj ckObj = new checkRuleObj(MemberDataExcelColumnChNameTitle(c));         //依不同欄位case的資料處理 / 設定到MEMBER物件的對應欄位 / 回傳錯誤訊息
                        MemberDataExcelImportColumnValue(c, value, ref data, ref ckObj);
                        if (ckObj.checkResult.Count > 0) ckObjs.Add(ckObj);
                    }


                    if (ckObjs.Count > 0)
                    {
                        ckDic.Add(new importCheckObj(i + 1, ckObjs));         //add error object to Dictionary
                    }
                    else
                    {
                        QueryObj_MEMBER.stringAttrInit(ref data);           
                        datas.Add(data);                                    //add data object to list / 可執行DB的資料集合
                    }
                }

                //db save
                if (datas.Count > 0)
                {
                    using (var DB = new SOUTHMAKEREntities())
                    {
                        foreach (var data in datas)
                        {
                            data.date_modify = DateTime.Now;
                            if (data.id == 0)
                            {
                                data.date_created = DateTime.Now;
                                DB.MEMBER.Add(data);
                            }
                            else
                            {
                                var obj = DB.MEMBER.Find(data.id);
                                data.account = obj.account;             //帳號不可被更新，需抓原值
                                DB.Entry(obj).CurrentValues.SetValues(data);
                            }
                            DB.SaveChanges();
                        }
                    }
                }

                if(ckDic.Count > 0)
                {
                    ret.isSuccess = false;
                    ret.data1 = ckDic;
                }
                else
                {
                    ret.isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                ret.isSuccess = false;
                ret.data2 = ex.ToString();
            }

            return Json(ret);
        }




        public void MemberDataExcelImportColumnValue(MemberDataExcelColumn c, string v, ref MEMBER data, ref checkRuleObj ckObj)
        {
            v = v.Trim();
            int msg = 0;

            switch (c)
            {

                case MemberDataExcelColumn.id:
                    {
                        data.id = 0;                    //預設值

                        msg = checkClass.ck_int_format(v); if (msg != 0) ckObj.checkResult.Add(msg);            //數字整數檢查
                        if (ckObj.checkResult.Count > 0) break;

                        if (string.IsNullOrEmpty(v) == false) data.id = int.Parse(v);         //驗證成功給值
                    }
                    break;
                case MemberDataExcelColumn.account:
                    {
                        data.account = "";              //預設值

                        if (data.id == 0)       //ID為0(新增模式)才需檢查，編輯模式不需檢查，因為DB層不會更新帳號
                        {
                            msg = checkClass.ck_text_empty(v); if (msg != 0) ckObj.checkResult.Add(msg);            //必填檢查
                            msg = checkClass.ck_email_format(v); if (msg != 0) ckObj.checkResult.Add(msg);          //email格式檢查
                            msg = checkClass.ck_member_account_repeat(data.id, v); if (msg != 0) ckObj.checkResult.Add(msg);          //帳號重複檢查
                            if (ckObj.checkResult.Count > 0) break;
                        }

                        data.account = v;               //驗證成功給值
                    }
                    break;
                case MemberDataExcelColumn.name:
                    {
                        data.name = "";              //預設值

                        msg = checkClass.ck_text_empty(v); if (msg != 0) ckObj.checkResult.Add(msg);            //必填檢查
                        if (ckObj.checkResult.Count > 0) break;

                        data.name = v;               //驗證成功給值
                    }
                    break;
                case MemberDataExcelColumn.gender:
                    {
                        data.gender = "";              //預設值

                        msg = checkClass.ck_gender_format(v); if (msg != 0) ckObj.checkResult.Add(msg);            //性別格式檢查
                        if (ckObj.checkResult.Count > 0) break;

                        if (v == "男") data.gender = "male";     //驗證成功給值
                        if (v == "女") data.gender = "female";
                    }
                    break;
                case MemberDataExcelColumn.birth:
                    {
                        data.birth = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;              //預設值

                        msg = checkClass.ck_date_format(v); if (msg != 0) ckObj.checkResult.Add(msg);            //時間格式檢查
                        if (ckObj.checkResult.Count > 0) break;

                        if (string.IsNullOrEmpty(v) == false) data.birth = DateTime.Parse(v);               //驗證成功給值
                    }
                    break;
                case MemberDataExcelColumn.occupation:
                    {
                        data.occupation = 0;              //預設值

                        msg = checkClass.ck_select_option_format(option_type.occupation, v); if (msg != 0) ckObj.checkResult.Add(msg);            //職業選項格式檢查
                        if (ckObj.checkResult.Count > 0) break;

                        if (string.IsNullOrEmpty(v) == false) data.occupation = int.Parse(helper.GetOptionValueByText(option_type.occupation, v));               //驗證成功給值
                    }
                    break;
                case MemberDataExcelColumn.school: data.school = v; break;
                case MemberDataExcelColumn.company: data.company = v; break;
                case MemberDataExcelColumn.degree:
                    {
                        data.degree = 0;              //預設值

                        msg = checkClass.ck_select_option_format(option_type.highest_education, v); if (msg != 0) ckObj.checkResult.Add(msg);            //學歷選項格式檢查
                        if (ckObj.checkResult.Count > 0) break;

                        if (string.IsNullOrEmpty(v) == false) data.degree = int.Parse(helper.GetOptionValueByText(option_type.highest_education, v));               //驗證成功給值
                        //helper.GetOptionText(option_type.highest_education, data.degree.ToString());
                    }
                    break;
                case MemberDataExcelColumn.title: data.title = v; break;
                case MemberDataExcelColumn.department: data.department = v; break;
                case MemberDataExcelColumn.tel:
                    {
                        data.tel = "";              //預設值

                        msg = checkClass.ck_text_empty(v); if (msg != 0) ckObj.checkResult.Add(msg);            //必填檢查
                        if (ckObj.checkResult.Count > 0) break;

                        data.tel = v;               //驗證成功給值
                    }
                    break;
                case MemberDataExcelColumn.email:
                    {
                        data.email = "";              //預設值

                        msg = checkClass.ck_email_format(v); if (msg != 0) ckObj.checkResult.Add(msg);            //email格式檢查
                        if (ckObj.checkResult.Count > 0) break;

                        data.email = v;               //驗證成功給值
                    }
                    break;
                case MemberDataExcelColumn.location: data.location = v; break;
                case MemberDataExcelColumn.motive:
                    {
                        data.motive = "";              //預設值

                        msg = checkClass.ck_text_empty(v); if (msg != 0) ckObj.checkResult.Add(msg);            //必填檢查
                        if (ckObj.checkResult.Count > 0) break;

                        data.motive = v;               //驗證成功給值
                    }
                    break;
                case MemberDataExcelColumn.source:
                    {
                        data.source = 0;              //預設值

                        msg = checkClass.ck_select_option_format(option_type.course_source, v); if (msg != 0) ckObj.checkResult.Add(msg);            //課程消息得知來源選項格式檢查
                        if (ckObj.checkResult.Count > 0) break;

                        if (string.IsNullOrEmpty(v) == false) data.source = int.Parse(helper.GetOptionValueByText(option_type.course_source, v));               //驗證成功給值
                        //helper.GetOptionText(option_type.course_source, data.source.ToString());
                    }
                    break;
                case MemberDataExcelColumn.employ_status:
                    {
                        data.employ_status = 0;              //預設值

                        msg = checkClass.ck_select_option_format(option_type.employ_status, v); if (msg != 0) ckObj.checkResult.Add(msg);            //工作狀況選項格式檢查
                        if (ckObj.checkResult.Count > 0) break;

                        if (string.IsNullOrEmpty(v) == false) data.employ_status = int.Parse(helper.GetOptionValueByText(option_type.employ_status, v));               //驗證成功給值
                        //helper.GetOptionText(option_type.employ_status, data.employ_status.ToString());
                    }
                    break;
                case MemberDataExcelColumn.is_enabled:
                    {
                        data.is_enabled = 0;              //預設值

                        msg = checkClass.ck_member_enabled_format(v); if (msg != 0) ckObj.checkResult.Add(msg);            //會員審核狀況格式檢查
                        if (ckObj.checkResult.Count > 0) break;

                        if (v == "審核成功") data.is_enabled = 1;     //驗證成功給值
                        if (v == "尚未審核") data.is_enabled = 0;
                        //(data.is_enabled == 1 ? "審核成功" : "尚未審核");
                    }
                    break;
                default: break;
            }
        }

        public MEMBER GetMemberExample()
        {
            MEMBER ret = new MEMBER();
            ret.account = "123@abc.com";
            ret.name = "xxx";
            ret.gender = "male";
            ret.birth = DateTime.Now;
            ret.occupation = 1;
            ret.school = "xxx學校";
            ret.company = "xxx公司";
            ret.degree = 1;
            ret.title = "xxx職稱";
            ret.department = "xxx科系";
            ret.tel = "02-12345678";
            ret.email = "123@abc.com";
            ret.location = "台北市";
            ret.motive = "xxx";
            ret.source = 1;
            ret.employ_status = 1;
            return ret;
        }



    }
}