﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using System.IO;
using System.Drawing;

namespace southmaker.Controllers
{
    public class SharedController : Controller
    {
        public resultObj ret = new resultObj();

        [HttpPost]
        public JsonResult QueryObjInit(string type)
        {
            switch (type.ToLower())
            {
                case "about": return Json(new QueryObj_ABOUT());
                case "intro": return Json(new QueryObj_INTRO());
                case "maker_car": return Json(new QueryObj_MAKER_CAR());
                case "tour_point": return Json(new QueryObj_TOUR_POINT());
                case "files": return Json(new QueryObj_FILES());
                case "maker": return Json(new QueryObj_MAKER());
                case "course": return Json(new QueryObj_COURSE());
                case "news": return Json(new QueryObj_NEWS());
                case "album": return Json(new QueryObj_ALBUM());
                case "video": return Json(new QueryObj_VIDEO());
                case "equip_cate": return Json(new QueryObj_EQUIP_CATE());
                case "equip": return Json(new QueryObj_EQUIP());
                case "works": return Json(new QueryObj_WORKS());
                case "member": return Json(new QueryObj_MEMBER());
                case "course_member": return Json(new QueryObj_COURSE_MEMBER());
                case "ad": return Json(new QueryObj_AD());
                case "survey": return Json(new QueryObj_SURVEY());
                case "language_data": return Json(new QueryObj_LANGUAGE_DATA());
                case "detail_view": return Json(new QueryObj_DETAIL_VIEW());
                case "today_visitor": return Json(new QueryObj_TODAY_VISITOR());
                case "backend_login_log": return Json(new QueryObj_BACKEND_LOGIN_LOG());
                default: return Json(new QueryObj_Common());
            }
        }

        [HttpPost]
        public JsonResult DataObjInit(string type)
        {
            switch (type.ToLower())
            {
                case "about":
                    {
                        var obj = new ABOUT();
                        obj.file_id = helper.GetNewFileID();
                        return Json(obj);
                    }
                case "intro"://有附檔的類別需要額外設定預設的檔案編號
                    {
                        var obj = new INTRO();
                        obj.file_id = helper.GetNewFileID();
                        return Json(obj);
                    }
                case "maker_car": return Json(new MAKER_CAR());
                case "tour_point": return Json(new TOUR_POINT());
                case "files": return Json(new FILES());
                case "maker"://有附檔的類別需要額外設定預設的檔案編號
                    {
                        var obj = new MAKER();
                        obj.file_id = helper.GetNewFileID();
                        return Json(obj);
                    }
                case "course"://有附檔的類別需要額外設定預設的檔案編號
                    {
                        var obj = new COURSE();
                        obj.file_id = helper.GetNewFileID();
                        return Json(obj);
                    }
                case "news"://有附檔的類別需要額外設定預設的檔案編號
                    {
                        var obj = new NEWS();
                        obj.file_id = helper.GetNewFileID();
                        return Json(obj);
                    }
                case "album"://有附檔的類別需要額外設定預設的檔案編號
                    {
                        var obj = new ALBUM();
                        obj.file_id = helper.GetNewFileID();
                        return Json(obj);
                    }
                case "video": return Json(new VIDEO());
                case "equip_cate": return Json(new EQUIP_CATE());
                case "equip"://有附檔的類別需要額外設定預設的檔案編號
                    {
                        var obj = new EQUIP();
                        obj.file_id = helper.GetNewFileID();
                        return Json(obj);
                    }
                case "works"://有附檔的類別需要額外設定預設的檔案編號
                    {
                        var obj = new WORKS();
                        obj.file_id = helper.GetNewFileID();
                        return Json(obj);
                    }
                case "member": return Json(new MEMBER());
                case "course_member": return Json(new AD());
                case "ad"://有附檔的類別需要額外設定預設的檔案編號
                    {
                        var obj = new AD();
                        obj.file_id = helper.GetNewFileID();
                        return Json(obj);
                    }
                case "survey": return Json(new SURVEY());
                case "language_data": return Json(new LANGUAGE_DATA());
                case "detail_view": return Json(new DETAIL_VIEW());
                case "today_visitor": return Json(new TODAY_VISITOR());
                case "backend_login_log": return Json(new BACKEND_LOGIN_LOG());

                default: return Json("");
            }
        }










        [HttpPost]
        public JsonResult ImgUpload(string obj_id, string classname, string tag, string checkcode)
        {
            classname = classname.ToLower();

            ret = new resultObj();
            HttpFileCollectionBase objs = Request.Files;

            if (objs.Count > 0)
            {
                //儲存的資料夾路徑
                string folderPath = string.Format("../upload/{0}/{1}/", classname, obj_id);
                //目前資料夾路徑不存在就馬上建立
                if (Directory.Exists(Server.MapPath(folderPath)) == false)
                {
                    Directory.CreateDirectory(Server.MapPath(folderPath));
                }

                List<FILES> fileList = new List<FILES>();
                FILES obj = new FILES();
                for (int i = 0; i < objs.Count; i++)
                {
                    string exten = Path.GetExtension(objs[i].FileName).TrimStart('.').ToLower();
                    string fileName = helper.GetResetFileName(Path.GetFileNameWithoutExtension(objs[i].FileName));

                    //原圖儲存路徑
                    string imgSavePath = string.Format("{0}{1}.{2}", folderPath, fileName, exten);
                    objs[i].SaveAs(Server.MapPath(imgSavePath));//上傳原圖到到指定目錄下
                    obj = helper.GetNewFileObj(obj_id, classname, "img", fileName, exten, tag, "", checkcode);
                    FilesSave(obj);//將檔案資訊寫進DB

                    //中型尺寸縮圖
                    imgSavePath = string.Format("{0}{1}@M.{2}", folderPath, fileName, exten);
                    helper.imgZoomAndSave(objs[i].InputStream, imgSavePath, image_size_width.M, image_size_height.M);//縮圖並儲存到到指定目錄下
                    obj = helper.GetNewFileObj(obj_id, classname, "img", fileName, exten, tag, "M", checkcode);
                    FilesSave(obj);//將檔案資訊寫進DB

                    //小型尺寸縮圖
                    imgSavePath = string.Format("{0}{1}@S.{2}", folderPath, fileName, exten);
                    helper.imgZoomAndSave(objs[i].InputStream, imgSavePath, image_size_width.S, image_size_height.S);//縮圖並儲存到到指定目錄下
                    obj = helper.GetNewFileObj(obj_id, classname, "img", fileName, exten, tag, "S", checkcode);
                    FilesSave(obj);//將檔案資訊寫進DB
                }
                ret.isSuccess = true;
                ret.data1 = fileList;
            }

            return Json(ret);
        }


        [HttpPost]
        public JsonResult FileUpload(string obj_id, string classname, string tag, string checkcode)
        {
            try
            {
                classname = classname.ToLower();

                ret = new resultObj();
                HttpFileCollectionBase objs = Request.Files;

                //儲存的資料夾路徑
                string folderPath = string.Format("../upload/{0}/{1}/", classname, obj_id);
                //目前資料夾路徑不存在就馬上建立
                if (Directory.Exists(Server.MapPath(folderPath)) == false)
                {
                    Directory.CreateDirectory(Server.MapPath(folderPath));
                }

                List<FILES> fileList = new List<FILES>();
                for (int i = 0; i < objs.Count; i++)
                {
                    string exten = Path.GetExtension(objs[i].FileName).TrimStart('.').ToLower();
                    string fileName = helper.GetResetFileName(Path.GetFileNameWithoutExtension(objs[i].FileName));
                    //上傳檔案到到指定目錄下
                    objs[i].SaveAs(Server.MapPath(string.Format("{0}{1}.{2}", folderPath, fileName, exten)));
                    //將檔案資訊寫進DB
                    FILES obj = helper.GetNewFileObj(obj_id, classname, "file", fileName, exten, tag, "", checkcode);
                    FilesSave(obj);
                    fileList.Add(obj);
                }

                ret.isSuccess = true;
                ret.data1 = fileList;
            }
            catch (Exception ex)
            {
                ret.isSuccess = false;
                ret.data2 = ex.ToString();
            }

            return Json(ret);
        }




        [HttpPost]
        //public JsonResult FilesSearch(QueryObj_FILES q)
        //public JsonResult FilesSearch(string obj_id)
        public JsonResult FilesSearch(string obj_id, string endmode, string checkcode)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                var objs = (                    //抓檔案資料
                    from a in DB.FILES
                    where
                    //a.is_deleted == 0 &&
                    //a.is_enabled == 1 &&
                    a.obj_id == obj_id &&
                    (endmode == "front" ? (a.is_deleted == 0 && a.is_enabled == 1) :                                     //endmode是前台的情況
                      endmode == "back" ? (a.is_deleted == 0 && a.is_enabled == 1) ||                                    //後台 舊資料
                                           (a.is_deleted == 0 && a.is_enabled == 0 && a.checkcode == checkcode) :         //後台 新上傳的資料
                    a.is_enabled == -1)                                                                                  //都不是的情況
                    select a
                    ).OrderByDescending(x => x.date_created).ToList();

                if (objs.Count > 0)
                {
                    ret.data1 = objs;
                    ret.hasData = true;
                }

                int totalCount = 0;
                totalCount = (                  //抓總筆數
                    from a in DB.FILES
                    where
                    //a.is_deleted == 0 &&
                    //a.is_enabled == 1 &&
                    a.obj_id == obj_id &&
                    (endmode == "front" ? (a.is_deleted == 0 && a.is_enabled == 1) :                                     //endmode是前台的情況
                      endmode == "back" ? (a.is_deleted == 0 && a.is_enabled == 1) ||                                    //後台 舊資料
                                           (a.is_deleted == 0 && a.is_enabled == 0 && a.checkcode == checkcode) :        //後台 新上傳的資料
                    a.is_enabled == -1)                                                                                  //都不是的情況
                    select a
                    ).Count();

                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }

                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult FilesSave(FILES data)
        {
            ret = new resultObj();
            QueryObj_FILES.stringAttrInit(ref data);
            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = data.date_created = DateTime.Now;
                if (data.obj_id != "")
                {
                    DB.FILES.Add(data);
                    DB.SaveChanges();
                    ret.isSuccess = true;
                    ret.data1 = data;
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult FilesNoteSave(int file_id, string note)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                if (file_id > 0)
                {
                    string fileName = "";
                    var findObj = DB.FILES.Where(x => x.id == file_id).FirstOrDefault();
                    if(findObj != null) fileName = findObj.name;

                    var objs = DB.FILES.Where(x => x.name == fileName).ToList();
                    if(objs != null && objs.Count > 0)
                    {
                        foreach(var obj in objs)
                        {
                            obj.note = note;
                            obj.lang_obj_id = file_id;
                        }
                        DB.SaveChanges();
                        ret.isSuccess = true;
                    }
                    
                }
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult FilesSwitch(List<FILES> on, List<string> off)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //處理OFF
                if (off != null && off.Count > 0)
                {
                    foreach (string command in off)
                    {
                        var param = command.Split(';');
                        string obj_id = param[0];
                        string type = param[1];
                        string tag = param[2];
                        string name = param[3];
                        if (type == "file")
                        {
                            var obj = DB.FILES.Find(int.Parse(name));
                            if (obj != null)
                            {
                                obj.is_deleted = 1;
                                obj.date_modify = DateTime.Now;
                            }
                            DB.SaveChanges();
                        }
                        if (type == "img")
                        {
                            List<FILES> removeImgs = DB.FILES.Where(x => x.type == "img" && x.obj_id == obj_id && x.tag == tag && x.name == name).ToList();
                            if (removeImgs != null && removeImgs.Count > 0)
                            {
                                for (int i = 0; i < removeImgs.Count; i++)
                                {
                                    removeImgs[i].is_deleted = 1;
                                    removeImgs[i].date_modify = DateTime.Now;
                                }
                            }
                            DB.SaveChanges();
                        }
                    }
                }

                //處理ON
                if (on != null && on.Count > 0)
                {
                    foreach (var command in on)
                    {
                        var obj = DB.FILES.Where(x => x.id == command.id && x.is_deleted == 0).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.is_enabled = 1;
                            obj.date_modify = DateTime.Now;
                            DB.SaveChanges();
                        }
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult FilesRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.FILES.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult ImageRemove(string obj_id, string img_name)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                List<FILES> removeImgs = DB.FILES.Where(x => x.type == "img" && x.obj_id == obj_id && x.name == img_name).ToList();
                if (removeImgs != null && removeImgs.Count > 0)
                {
                    for (int i = 0; i < removeImgs.Count; i++)
                    {
                        removeImgs[i].is_deleted = 1;
                        removeImgs[i].date_modify = DateTime.Now;
                    }
                }
                DB.SaveChanges();

                ret.isSuccess = true;
            }
            return Json(ret);
        }








        [HttpPost]
        public JsonResult SendMail(string lang, string mailTag, string[] infos)
        {
            //info[0]: account_id, info[1]: email, info[2]: checkcode, info[3]: mail_title, info[4]: mail_content
            ret = new resultObj();

            if(infos.Length != 5) return Json(ret);             //資訊不足直接離開

            try
            {
                using (System.Net.Mail.SmtpClient mysmtp = helper.getMailSetting())
                {
                    string topic = infos[3];            //信件標題
                    string content = infos[4];          //信件內容


                    //信件內容中的標籤替換
                    if (content.Contains("[[account_id]]"))                         //會員帳號替換
                    {
                        content = content.Replace("[[account_id]]", infos[0]);
                    }

                    if (content.Contains("[[activate_url]]"))                       //會員驗證網址替換
                    {
                        string activateUrl = string.Format("https://{0}/Member/Activate/{1}", Request.Url.Authority, infos[2]);
                        string activateLinkHtml = string.Format("<a href=\"{0}\" >{0}</a>", activateUrl);
                        content = content.Replace("[[activate_url]]", activateLinkHtml);
                    }

                    //mysmtp.Send("\"" + "111" + "\"<" + "decavi400@gmail.com" + ">", "decavi400@hotmail.com", topic, content);
                    System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("\"" + "南方創客" + "\"<" + "stmakercenter@gmail.com" + ">", infos[1], topic, content);
                    mm.IsBodyHtml = true;
                    mysmtp.Send(mm);
                }
                ret.isSuccess = true;
            }
            catch(Exception ex)
            {
                ret.isSuccess = false;
                ret.data2 = ex;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult SendNewPasswordMail(string lang, string account)
        {
            if (lang == null) lang = ""; if (account == null) account = "";
            
            ret = new resultObj();
            
            try
            {
                string password = "";
                using (var DB = new SOUTHMAKEREntities())
                {
                    MEMBER memberObj = DB.MEMBER.Where(x => x.is_deleted == 0 && x.account == account).FirstOrDefault();
                    if(memberObj != null) password = memberObj.password;
                }

                using (System.Net.Mail.SmtpClient mysmtp = helper.getMailSetting())
                {
                    string topic = "會員新密碼確認信";            //信件標題
                    string content = "歡迎加入南方創客基地會員。以下為您的新密碼，請妥善保管。<br /><br />新密碼： " + password;          //信件內容
                    
                    //mysmtp.Send("\"" + "111" + "\"<" + "decavi400@gmail.com" + ">", "decavi400@hotmail.com", topic, content);
                    System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("\"" + "南方創客" + "\"<" + helper.senderMail + ">", account, topic, content);
                    mm.IsBodyHtml = true;
                    mysmtp.Send(mm);
                }
                ret.isSuccess = true;
            }
            catch (Exception ex)
            {
                ret.isSuccess = false;
                ret.data2 = ex;
            }
            return Json(ret);
        }





        [HttpPost]
        public JsonResult LangDataSearch(QueryObj_LANGUAGE_DATA q)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                var objs = (                    //抓檔案資料
                    from a in DB.LANGUAGE_DATA
                    where
                    //a.unit == q.unit &&
                    (string.IsNullOrEmpty(q.unit) ? true : a.unit == q.unit) &&                         //資料單元
                    (q.obj_id < 1 ? true : a.obj_id == q.obj_id) &&                                     //主資料ID
                    //a.obj_id == q.obj_id &&
                    (string.IsNullOrEmpty(q.lang) ? true : a.lang == q.lang) &&                         //語系
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選
                                                a.text1.Contains(q.search_keyword) ||
                                                a.text2.Contains(q.search_keyword) ||
                                                a.text3.Contains(q.search_keyword) ||
                                                a.text4.Contains(q.search_keyword) ||
                                                a.content1.Contains(q.search_keyword)
                                            ))
                    select a
                    ).OrderByDescending(x => x.date_created).ToList();

                if (objs.Count > 0)
                {
                    ret.data1 = objs;
                    ret.hasData = true;
                }

                int totalCount = 0;
                totalCount = (                  //抓總筆數
                    from a in DB.LANGUAGE_DATA
                    where
                    //a.unit == q.unit &&
                    (string.IsNullOrEmpty(q.unit) ? true : a.unit == q.unit) &&                         //資料單元
                    (q.obj_id < 1 ? true : a.obj_id == q.obj_id) &&                                     //主資料ID
                    //a.obj_id == q.obj_id &&
                    (string.IsNullOrEmpty(q.lang) ? true : a.lang == q.lang) &&                         //語系
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選
                                                a.text1.Contains(q.search_keyword) ||
                                                a.text2.Contains(q.search_keyword) ||
                                                a.text3.Contains(q.search_keyword) ||
                                                a.text4.Contains(q.search_keyword) ||
                                                a.content1.Contains(q.search_keyword)
                                            ))
                    select a
                    ).Count();

                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }

                ret.isSuccess = true;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult HasLangData(string unit, int obj_id, lang_type langType)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                ret.hasData = DB.LANGUAGE_DATA.Any(x =>
                                            x.unit == unit &&
                                            x.obj_id == obj_id &&
                                            x.lang == langType.ToString() &&
                                            x.text1 != ""
                                            );
                ret.isSuccess = true;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult LangDataSave(LANGUAGE_DATA data)
        {
            ret = new resultObj();
            QueryObj_LANGUAGE_DATA.stringAttrInit(ref data);

            if(string.IsNullOrEmpty(data.lang) || string.IsNullOrEmpty(data.unit) || data.obj_id < 1) return Json(ret);       //資料不齊全 離開

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.LANGUAGE_DATA.Add(data);
                }
                else
                {
                    var obj = DB.LANGUAGE_DATA.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }


        [HttpPost]
        public JsonResult SaveDetailViewFromJS(string unit, string data_id)
        {
            helper.SaveDetailViewAndTodayVisitor(unit, data_id);
            return Json(null);
        }

        [HttpPost]
        public JsonResult DetailViewSearch(string unit, string obj_id)
        {
            int view = 0;
            int _obj_id = 0;
            int.TryParse(obj_id, out _obj_id);
            using (var DB = new SOUTHMAKEREntities())
            {
                var findObj = DB.DETAIL_VIEW.Where(x => x.unit == unit && x.obj_id == _obj_id).FirstOrDefault();
                if (findObj != null) view = findObj.count;
            }
            return Json(view);
        }


        //[HttpPost]
        //public JsonResult LanguageSwitch(string lang)       //js呼叫用
        //{
        //    Session["lang"] = lang;
        //    return Json("");
        //}

        public ActionResult LanguageSwitch(string lang, string url)     //.NET post用
        {
            Session["lang"] = lang;
            if (url.Contains("aspxerrorpath") || url.Contains("://") == false)
            {
                url = "/Home/Index";
            }
            return Redirect(url);
        }




        [HttpPost]
        public JsonResult UpdateCaptchaCode(string tag)
        {
            ret = new resultObj();

            try
            {
                int NumCount = 4;//預設產生4位亂數
                //取得亂數
                string captcha_code = this.GetRandomNumberString(NumCount);
                /*用於驗證的Session*/

                if (tag == "user_login") Session["user_login_captcha"] = captcha_code;
                if (tag == "user_contact") Session["user_contact_captcha"] = captcha_code;
                if (tag == "admin_login") Session["admin_login_captcha"] = captcha_code;

                //取得圖片物件
                Image image = this.CreateCheckCodeImage(captcha_code);
                //image.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                string folderPath = "../upload/captcha/";                   //儲存路徑
                if (Directory.Exists(Server.MapPath(folderPath)) == false)
                {
                    Directory.CreateDirectory(Server.MapPath(folderPath));      //目前資料夾路徑不存在就馬上建立
                }

                string imageName = folderPath + Guid.NewGuid().ToString() + ".jpg";
                image.Save(HttpContext.Server.MapPath(imageName), System.Drawing.Imaging.ImageFormat.Jpeg);
                //Response.End();
                ret.isSuccess = true;
                ret.data1 = imageName;
            }
            catch (Exception ex)
            {
                //
            }
            return Json(ret);
        }

        [HttpPost]
        public JsonResult GetCaptchaVoice(string tag, int pos)
        {
            ret = new resultObj();

            try
            {
                string code = "";
                if (tag == "user_login" && Session["user_login_captcha"] != null) code = Session["user_login_captcha"].ToString();
                if (tag == "user_contact" && Session["user_contact_captcha"] != null) code = Session["user_contact_captcha"].ToString();
                if (tag == "admin_login" && Session["admin_login_captcha"] != null) code = Session["admin_login_captcha"].ToString();
                
                if(string.IsNullOrEmpty(code) == false)
                {
                    ret.isSuccess = true;
                    ret.data1 = code.Substring(pos - 1, 1);
                }
            }
            catch (Exception ex)
            {
                //
            }
            return Json(ret);
        }

        public string GetRandomNumberString(int int_NumberLength)
        {
            System.Text.StringBuilder str_Number = new System.Text.StringBuilder();//字串儲存器
            Random rand = new Random(Guid.NewGuid().GetHashCode());//亂數物件

            for (int i = 1; i <= int_NumberLength; i++)
            {
                str_Number.Append(rand.Next(0, 10).ToString());//產生0~9的亂數
            }

            return str_Number.ToString();
        }

        public Image CreateCheckCodeImage(string checkCode)
        {

            Bitmap image = new Bitmap((checkCode.Length * 20), 40);//產生圖片，寬20*位數，高40像素
            Graphics g = Graphics.FromImage(image);


            //生成隨機生成器
            Random random = new Random(Guid.NewGuid().GetHashCode());
            int int_Red = 0;
            int int_Green = 0;
            int int_Blue = 0;
            int_Red = random.Next(256);//產生0~255
            int_Green = random.Next(256);//產生0~255
            int_Blue = (int_Red + int_Green > 400 ? 0 : 400 - int_Red - int_Green);
            int_Blue = (int_Blue > 255 ? 255 : int_Blue);

            //清空圖片背景色
            g.Clear(Color.FromArgb(int_Red, int_Green, int_Blue));

            //畫圖片的背景噪音線
            for (int i = 0; i <= 24; i++)
            {


                int x1 = random.Next(image.Width);
                int x2 = random.Next(image.Width);
                int y1 = random.Next(image.Height);
                int y2 = random.Next(image.Height);

                g.DrawLine(new Pen(Color.Silver), x1, y1, x2, y2);

                g.DrawEllipse(new Pen(Color.DarkViolet), new System.Drawing.Rectangle(x1, y1, x2, y2));
            }

            Font font = new System.Drawing.Font("Arial", 20, (System.Drawing.FontStyle.Bold));
            System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height), Color.Blue, Color.DarkRed, 1.2F, true);

            g.DrawString(checkCode, font, brush, 2, 2);
            for (int i = 0; i <= 99; i++)
            {

                //畫圖片的前景噪音點
                int x = random.Next(image.Width);
                int y = random.Next(image.Height);

                image.SetPixel(x, y, Color.FromArgb(random.Next()));
            }

            //畫圖片的邊框線
            g.DrawRectangle(new Pen(Color.Silver), 0, 0, image.Width - 1, image.Height - 1);


            return image;

        }

    }
}