﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;

namespace southmaker.Controllers
{
    public class WorksController : Controller
    {
        public resultObj ret = new resultObj();


        public ActionResult List() { return View(); }
        public ActionResult Detail() { helper.SaveDetailViewAndTodayVisitor("works", Request["id"]); return View(); }


        [HttpPost]
        public JsonResult WorksSearch(QueryObj_WORKS q)
        {
            if (q == null) q = new QueryObj_WORKS();
            if (string.IsNullOrEmpty(q.search_keyword) == false) q.search_keyword = HttpUtility.UrlDecode(q.search_keyword);

            List<string> param = q.urlParam.TrimStart('?').Split('&').ToList();
            param.RemoveAll(x => x.Split('=')[0] == "p");

            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                //須分頁的Query

                //抓目前限制範圍的資料
                int skipCount = (q.pageNo - 1) * q.pageSize;
                var result = (
                    from a in DB.WORKS
                    where
                    a.is_deleted == 0 &&
                    (q.is_enabled != 1 ? true :                                         //無視此篩選
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    )) &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(作品名稱 團隊名稱)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.@group.Contains(q.search_keyword)
                                                                ))
                    select a
                    ).OrderByDescending(x => x.date_created).Skip(skipCount).Take(q.pageSize).ToList();

                if (result.Count > 0)
                {
                    ret.hasData = true;
                    ret.data1 = result;
                }


                //抓總筆數
                int totalCount = (
                    from a in DB.WORKS
                    where
                    a.is_deleted == 0 &&
                    (q.is_enabled != 1 ? true :                                         //無視此篩選
                    ((a.is_enabled == 1 && a.is_force_post == 1) ||                     //不下架 無視發佈時間區間的情況
                        (a.is_enabled == 1 && a.is_force_post == 0 &&                   //需依發佈時間區間篩選
                        ((a.date_start.Year == 1753 ? true : a.date_start <= DateTime.Now) &&
                         (a.date_end.Year == 1753 ? true : DateTime.Now <= a.date_end)))
                    )) &&
                    (q.id < 1 ? true : a.id == q.id) &&                 //資料編號篩選
                    (string.IsNullOrEmpty(q.lang) ? a.lang == lang_type.zh_hant.ToString() : a.lang == q.lang) &&       //語言篩選
                    ((string.IsNullOrEmpty(q.search_keyword)) ? true : (                                       //關鍵字篩選(作品名稱 團隊名稱)
                                                                    a.title.Contains(q.search_keyword) ||
                                                                    a.@group.Contains(q.search_keyword)
                                                                ))
                    select a
                        ).Distinct().Count();
                if (totalCount > 0)
                {
                    ret.data2 = totalCount;
                }
                //抓分頁器資料
                int maxIndex = 0;
                List<pagerItem> pager = helper.getPagerItem(q.pageSize, q.pageNo, totalCount, "", string.Join("&", param.ToArray()), ref maxIndex);
                ret.data3 = pager;

                ret.isSuccess = true;
            }
            return Json(ret);
        }
        












        

        [HttpPost]
        public JsonResult WorksSave(WORKS data)
        {
            ret = new resultObj();

            QueryObj_WORKS.stringAttrInit(ref data);

            using (var DB = new SOUTHMAKEREntities())
            {
                data.date_modify = DateTime.Now;
                if (data.id == 0)
                {
                    data.date_created = DateTime.Now;
                    DB.WORKS.Add(data);
                }
                else
                {
                    var obj = DB.WORKS.Find(data.id);
                    DB.Entry(obj).CurrentValues.SetValues(data);
                }
                DB.SaveChanges();

                ret.isSuccess = true;
                ret.data1 = data;
            }
            return Json(ret);
        }







        

        [HttpPost]
        public JsonResult WorksRemove(int[] ids)
        {
            ret = new resultObj();
            using (var DB = new SOUTHMAKEREntities())
            {
                foreach (int id in ids)
                {
                    var obj = DB.WORKS.Find(id);
                    if (obj != null)
                    {
                        obj.is_deleted = 1;
                        obj.date_modify = DateTime.Now;
                        DB.SaveChanges();
                    }
                }
                ret.isSuccess = true;
            }
            return Json(ret);
        }
        


    }
}