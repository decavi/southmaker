﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Model;








public class checkClass
{
    /**********EXAMPLE**********/
    //public void example()
    //{
    //    List<checkRuleObj> rules = new List<checkRuleObj>();

    //    int msg = 0;
    //    checkRuleObj ckObj = new checkRuleObj("ch_name");        //中文機構名稱重複性檢查
    //    msg = checkClass2.ck_corp_ch_name_repeat(corp_info.corp_id, corp_info.ch_name); if (msg != 0) ckObj.checkResult.Add(msg);
    //    if (ckObj.checkResult.Count > 0) rules.Add(ckObj);
    //    ckObj = new checkRuleObj("pt_name");        //葡文機構名稱重複性檢查
    //    msg = checkClass2.ck_corp_pt_name_repeat(corp_info.corp_id, corp_info.pt_name); if (msg != 0) ckObj.checkResult.Add(msg);
    //    if (ckObj.checkResult.Count > 0) rules.Add(ckObj);
    //    ckObj = new checkRuleObj("en_name");        //英文機構名稱重複性檢查
    //    msg = checkClass2.ck_corp_en_name_repeat(corp_info.corp_id, corp_info.en_name); if (msg != 0) ckObj.checkResult.Add(msg);
    //    if (ckObj.checkResult.Count > 0) rules.Add(ckObj);

    //    if (rules.Count > 0)
    //    {
    //        ret.data2 = rules;
    //        return Json(ret);
    //    }
    //}
    /**********EXAMPLE**********/

    public static int intTest = 0;
    public static DateTime dtTest = new DateTime();

    public static string emailFormat = @"^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$";


    public static int ck_text_empty(string v)
    {
        return string.IsNullOrEmpty(v) == false ? 0 : (int)checkCode.ck_text_empty;
    }

    public static int ck_email_format(string v)
    {
        if (string.IsNullOrEmpty(v)) return 0;          //空值就跳過不檢查

        Regex regex = new Regex(emailFormat);
        return regex.IsMatch(v) ? 0 : (int)checkCode.ck_text_format;
    }

    public static int ck_date_format(string v)
    {
        if (string.IsNullOrEmpty(v)) return 0;          //空值就跳過不檢查

        return DateTime.TryParse(v, out dtTest) ? 0 : (int)checkCode.ck_text_format;
    }

    public static int ck_int_format(string v)
    {
        if (string.IsNullOrEmpty(v)) return 0;          //空值就跳過不檢查

        return int.TryParse(v, out intTest) ? 0 : (int)checkCode.ck_text_format;
    }

    public static int ck_gender_format(string v)
    {
        return (new string[]{ "男", "女" }).Contains(v) ? 0 : (int)checkCode.ck_text_format;
    }

    public static int ck_member_enabled_format(string v)
    {
        return (new string[] { "尚未審核", "審核成功" }).Contains(v) ? 0 : (int)checkCode.ck_text_format;
    }

    public static int ck_select_option_format(Model.option_type opt, string v)
    {
        if (string.IsNullOrEmpty(v)) return 0;          //空值就跳過不檢查

        string value = southmaker.helper.GetOptionValueByText(opt, v);
        return string.IsNullOrEmpty(value) == false ? 0 : (int)checkCode.ck_text_format;
    }



    public static int ck_member_account_repeat(int data_id, string account)
    {
        int ret = 0;
        using (var DB = new SOUTHMAKEREntities())
        {
            if (DB.MEMBER.Any(x => x.is_deleted == 0 && x.id != data_id && x.account == account))
            {
                ret = (int)checkCode.memberAccountRepeat;
            }
        }
        return ret;
    }

    public static int ck_captcha_correct(object captchaObj, string captcha)
    {
        int ret = 0;
        if (captchaObj != null)
        {
            if (captchaObj.ToString() != captcha)
            {
                return (int)checkCode.captchaCorrect;
            }
            //BotDetect.Web.Mvc.MvcCaptcha _CaptchaObj = (BotDetect.Web.Mvc.MvcCaptcha)captchaObj;
            //if (_CaptchaObj.Validate(captcha, _CaptchaObj.CurrentInstanceId) == false)
            //{
            //    return (int)checkCode.captchaCorrect;
            //}
        }
        return ret;
    }

    public static int ck_member_account_login(string account, string pwd, object captchaObj, string captcha)
    {
        int ret = 0;
        //這裡還需要驗證碼檢查
        //if(captchaObj != null)
        //{
        //    string captcha_answer = captchaObj.ToString();
        //    if(captcha != captcha_answer) return (int)checkCode.captchaCorrect;
        //}
        //if(captchaObj != null)
        //{
        //    BotDetect.Web.Mvc.MvcCaptcha _CaptchaObj = (BotDetect.Web.Mvc.MvcCaptcha)captchaObj;
        //    if (_CaptchaObj.Validate(captcha, _CaptchaObj.CurrentInstanceId) == false)
        //    {
        //        return (int)checkCode.captchaCorrect;
        //    }
        //}
        using (var DB = new SOUTHMAKEREntities())
        {
            MEMBER memberObj = DB.MEMBER.Where(x => x.is_deleted == 0 && x.account == account).FirstOrDefault();
            if (memberObj == null)
            {
                return (int)checkCode.memberEmailExist;
            }
            if (memberObj.password != pwd)
            {
                return (int)checkCode.memberPasswordCorrect;
            }
            if (captchaObj != null)
            {
                string captcha_answer = captchaObj.ToString();
                if (captcha != captcha_answer) return (int)checkCode.captchaCorrect;
            }
            if (memberObj.is_enabled == 0)
            {
                ret = (int)checkCode.memberAccountActivate;
            }
            return ret;
        }
    }

    public static int ck_admin_backend_login(string account, string pwd, object captchaObj, string captcha)
    {
        int ret = 0;
        //這裡還需要驗證碼檢查
        if (captchaObj != null)
        {
            //BotDetect.Web.Mvc.MvcCaptcha _CaptchaObj = (BotDetect.Web.Mvc.MvcCaptcha)captchaObj;
            //if (_CaptchaObj.Validate(captcha, _CaptchaObj.CurrentInstanceId) == false)
            if(captchaObj.ToString() != captcha)
            {
                return (int)checkCode.captchaCorrect;
            }
        }
        if (string.IsNullOrEmpty(account) || account != System.Web.Configuration.WebConfigurationManager.AppSettings["admin_account"])
        {
            return (int)checkCode.adminAccountExist;
        }
        if(pwd != System.Web.Configuration.WebConfigurationManager.AppSettings["admin_password"])
        {
            return (int)checkCode.memberPasswordCorrect;
        }
        return ret;
    }


    public static int ck_member_password_forgot(string account, string name, string tel)
    {
        int ret = 0;
        using (var DB = new SOUTHMAKEREntities())
        {
            MEMBER memberObj = DB.MEMBER.Where(x => x.is_deleted == 0 && x.account == account).FirstOrDefault();
            if (memberObj == null)
            {
                return (int)checkCode.memberEmailExist;
            }
            if (memberObj.name != name)
            {
                return (int)checkCode.memberNameCorrect;
            }
            if (memberObj.tel != tel)
            {
                ret = (int)checkCode.memberTelCorrect;
            }
            return ret;
        }
    }

    public static int ck_member_password_correct(string account, string pwd)
    {
        int ret = 0;
        using (var DB = new SOUTHMAKEREntities())
        {
            var memberObj = DB.MEMBER.Where(x => x.is_deleted == 0 && x.account == account).FirstOrDefault();
            if(memberObj != null)
            {
                if(memberObj.password != pwd) ret = (int)checkCode.memberPasswordCorrect;
            }
            else
            {
                ret = (int)checkCode.memberEmailExist;
            }
        }
        return ret;
    }


    public static int ck_course_sign_up_fully(int course_id)
    {
        int ret = 0;
        using (var DB = new SOUTHMAKEREntities())
        {
            int course_seat = 0;
            var courseObj = DB.COURSE.Where(x => x.id == course_id && x.is_deleted == 0).FirstOrDefault();
            if(courseObj != null) course_seat = courseObj.seat;
            var member_count = DB.COURSE_MEMBER.Count(x => x.course_id == course_id && x.is_deleted == 0);

            if(member_count >= course_seat) ret = (int)checkCode.courseMemberFully;
        }
        return ret;
    }

    public static int ck_course_sign_up_repeat(int course_id, int member_id)
    {
        int ret = 0;
        using (var DB = new SOUTHMAKEREntities())
        {
            var member_count = DB.COURSE_MEMBER.Count(x => x.course_id == course_id && x.member_id == member_id && x.is_deleted == 0);

            if (member_count > 0) ret = (int)checkCode.courseMemberRepeat;
        }
        return ret;
    }


}


public class importCheckObj
{
    public int row_number { get; set; }
    public List<checkRuleObj> checkResult { get; set; }
    public importCheckObj(int _row_number, List<checkRuleObj> _checkResult)
    {
        row_number = _row_number;
        checkResult = _checkResult;
    }
}

public class checkRuleObj
{
    public string checkTag { get; set; }
    public checkCode[] checkItems { get; set; }
    public List<int> checkResult { get; set; }
    public checkRuleObj(string tag, params checkCode[] ckItems)
    {
        checkTag = tag;
        checkItems = ckItems;
        checkResult = new List<int>();
    }
}


public enum checkCode : int
{
    /// <summary>必填檢查</summary>
    ck_text_empty = 200002,
    /// <summary>格式檢查(共用訊息)</summary>
    ck_text_format = 200003,
    /// <summary>email格式檢查</summary>
    //ck_email_format = 0,
    /// <summary>日期格式檢查</summary>
    //ck_date_format = 0,

    /// <summary>會員帳號重覆設定</summary>
    memberAccountRepeat = 210101,


    /// <summary>用戶帳號是否存在</summary>
    memberEmailExist = 210102,
    /// <summary>用戶帳號是否激活</summary>
    memberAccountActivate = 210103,

    /// <summary>密碼是否正確</summary>
    memberPasswordCorrect = 210201,

    /// <summary>登入密碼錯誤已達指定次數</summary>
    memberPasswordIncorrectOverTimes = 210202,

    /// <summary>交易密碼是否正確</summary>
    transPasswordCorrect = 212601,


    /// <summary>驗證碼是否正確</summary>
    captchaCorrect = 212501,

    /// <summary>會員姓名是否正確</summary>
    memberNameCorrect = 210001,
    /// <summary>會員電話是否正確</summary>
    memberTelCorrect = 210002,

    /// <summary>報名是否滿額</summary>
    courseMemberFully = 210003,
    /// <summary>是否已報名過</summary>
    courseMemberRepeat = 210004,

    /// <summary>無此後台管理者帳號</summary>
    adminAccountExist = 210005,
}

