﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using Model;
using System.Net;
using System.Net.Mail;

namespace southmaker
{
    public class helper
    {
        public static string mailServer = "smtp.gmail.com";
        public static int mailServerPort = 587;
        //public static string senderMail = "decavi400@gmail.com"; public static string senderPassword = "swnndmgkdcvgedlt";
        public static string senderMail = "stmakercenter@gmail.com"; public static string senderPassword = "stmc1234";
        public static bool mailSSLEnable = true;
        //public static bool mailSSLEnable = false;

        public static string default_img_url = "../Content/Front/assets/img/activity/activity01.jpg";

        public static string datetimeFormat = "yyyy/MM/dd HH:mm:ss";
        public static string dateFormat = "yyyy/MM/dd";


        public static List<pagerItem> getPagerItem(int pageSize, int pageIndex, int totalItemCount, string pageName, string otherParam, ref int maxPageIndex)
        {
            List<pagerItem> pagerList = new List<pagerItem>();
            if (pageSize > 0)
            {
                maxPageIndex = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(totalItemCount) / Convert.ToDouble(pageSize)));

                int startNum = 1;
                int endNum = maxPageIndex;

                if (otherParam != "") otherParam += "&";

                for (int i = startNum; i <= endNum; i++)
                {
                    pagerItem item = new pagerItem();
                    item.no = i;
                    item.text = i.ToString();
                    item.href = string.Format("{0}?{1}p={2}", pageName, otherParam, i);
                    if (pageIndex == i) item.active = "active";
                    pagerList.Add(item);
                }
            }

            return pagerList;
        }

        public static string GetNewFileID()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmssfff") + new Random().Next(999).ToString("000");
        }

        public static string GetResetFileName(string fileName)
        {
            return new Random().Next(9999).ToString("0000") + "@" + fileName;
        }

        public static FILES GetNewFileObj(string obj_id, string className, string type, string fileName, string exten, string tag, string img_size, string checkcode)
        {
            FILES obj = new FILES();
            obj.obj_id = obj_id;
            obj.@class = className;
            obj.type = type;
            obj.name = fileName;
            obj.exten = exten;
            obj.tag = tag;
            obj.img_size = img_size;
            obj.is_enabled = 0;
            obj.is_deleted = 0;
            obj.date_created = obj.date_modify = DateTime.Now;
            obj.checkcode = checkcode;
            return obj;
        }


        //縮放圖片尺寸比例並儲存
        public static void imgZoomAndSave(Stream imgStream, string imgSavePath, image_size_width _limitWidth, image_size_height _limitHeight)
        {
            Bitmap image = new Bitmap(imgStream);
            System.Drawing.Image smallImage = image;

            double zoomRate = 1, limitWidth = Convert.ToDouble((int)_limitWidth), limitHeight = Convert.ToDouble((int)_limitHeight);
            //最寬限制
            if (smallImage.Width > limitWidth) zoomRate = limitWidth / smallImage.Width;
            //最高限制
            if (smallImage.Height > limitHeight) zoomRate = limitHeight / smallImage.Height;
            // 產生縮圖
            Bitmap thumbnailBitmap = new Bitmap(Convert.ToInt32(smallImage.Width * zoomRate), Convert.ToInt32(smallImage.Height * zoomRate));
            Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            //將各像成像品質設定為HighQuality
            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //設定重畫縮圖範圍
            Rectangle imageRectangle = new Rectangle(0, 0, thumbnailBitmap.Width, thumbnailBitmap.Height);
            //重畫縮圖
            thumbnailGraph.DrawImage(image, imageRectangle);
            //輸出縮圖
            thumbnailBitmap.Save(HttpContext.Current.Server.MapPath(imgSavePath), ImageFormat.Jpeg);

            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
            image.Dispose();
        }

        public static SmtpClient getMailSetting()
        {
            SmtpClient mysmtp = new SmtpClient(mailServer, mailServerPort);//MAIL伺服器設定
            mysmtp.Credentials = new NetworkCredential(senderMail, senderPassword);//帳號,密碼設定
                                                                                   //mysmtp.Credentials = new NetworkCredential();
            mysmtp.EnableSsl = mailSSLEnable;

            return mysmtp;
        }

        //密碼加密
        public static string PasswordCrypt(string password)
        {
            string salt = System.Web.Configuration.WebConfigurationManager.AppSettings["salt"];
            byte[] saltByte = System.Text.Encoding.UTF8.GetBytes(salt);
            byte[] passwordByte = System.Text.Encoding.UTF8.GetBytes(password);
            System.Security.Cryptography.Rfc2898DeriveBytes rfcKey = new System.Security.Cryptography.Rfc2898DeriveBytes(passwordByte, saltByte, 2);
            return Convert.ToBase64String(rfcKey.GetBytes(20));
        }


        //資訊物件瀏覽及訪客瀏覽紀錄
        public static void SaveDetailViewAndTodayVisitor(string unit, string request_id)
        {
            if (unit == null) unit = "";
            int obj_id = 0;
            if (string.IsNullOrEmpty(request_id) == false) int.TryParse(request_id, out obj_id);

            string ip = HttpContext.Current.Request.UserHostAddress;

            DateTime now = DateTime.Now;
            DateTime todayStartPoint = new DateTime(now.Year, now.Month, now.Day);
            using (var DB = new SOUTHMAKEREntities())
            {
                //刪除昨日的訪客紀錄
                var old_data = DB.TODAY_VISITOR.Where(x => x.time < todayStartPoint).ToList();
                if (old_data.Count > 0)
                {
                    DB.TODAY_VISITOR.RemoveRange(old_data);
                    DB.SaveChanges();
                }

                //紀錄今日訪客紀錄 如今日出現通一筆紀錄(ip, unit, obj_id)則不新增
                var visitorFindObj = DB.TODAY_VISITOR.Where(x => x.unit == unit && x.obj_id == obj_id && x.ip == ip && x.time > todayStartPoint).FirstOrDefault();
                if (visitorFindObj == null)      //無紀錄
                {
                    //新增訪客瀏覽紀錄
                    TODAY_VISITOR tv = new TODAY_VISITOR(); tv.ip = ip; tv.unit = unit; tv.obj_id = obj_id; tv.time = now;
                    DB.TODAY_VISITOR.Add(tv);

                    //於DETAIL_VIEW更新該資料瀏覽紀錄，如已存在就更新瀏覽數，無就新增一筆瀏覽數為1的紀錄
                    var detailViewFindObj = DB.DETAIL_VIEW.Where(x => x.unit == unit && x.obj_id == obj_id).FirstOrDefault();
                    if (detailViewFindObj != null)       //已有此物件瀏覽紀錄
                    {
                        detailViewFindObj.count += 1;
                    }
                    else        //尚無物件瀏覽紀錄
                    {
                        DETAIL_VIEW dv = new DETAIL_VIEW();
                        dv.unit = unit;
                        dv.obj_id = obj_id;
                        dv.count = 1;
                        DB.DETAIL_VIEW.Add(dv);
                    }
                    DB.SaveChanges();
                }
            }
        }




        public class options
        {
            public string text = "";
            public string value = "";
            public int p = 0;
            public options(string _text, string _value, int _p)
            {
                this.text = _text;
                this.value = _value;
                this.p = _p;
            }
        }


        public static string GetOptionValueByText(option_type optionType, string text)
        {
            string ret = "";

            if (string.IsNullOrEmpty(text)) return "";     //value為null, 空字串 或是0 直接離開

            List<options> opts = new List<options>();

            switch (optionType)
            {
                case option_type.highest_education: opts = highest_education_options(); break;
                case option_type.course_source: opts = course_source_options(); break;
                case option_type.employ_status: opts = employ_status_options(); break;
                case option_type.occupation: opts = occupation_options(); break;

                default: break;
            }
            var opt = opts.Where(x => x.text == text).FirstOrDefault();
            if (opt != null) ret = opt.value;

            return ret;
        }


        public static string GetOptionText(option_type optionType, string value)
        {
            string ret = "";

            if (string.IsNullOrEmpty(value) || value == "0") return "";     //value為null, 空字串 或是0 直接離開

            List<options> opts = new List<options>();

            switch(optionType)
            {
                case option_type.highest_education: opts = highest_education_options(); break;
                case option_type.course_source: opts = course_source_options(); break;
                case option_type.employ_status: opts = employ_status_options(); break;
                case option_type.occupation: opts = occupation_options(); break;
                default:break;
            }
            var opt = opts.Where(x => x.value == value).FirstOrDefault();
            if (opt != null) ret = opt.text;

            return ret;
        }

        public static List<options> highest_education_options()
        {
            List<options> ret = new List<options>();
            ret.Add(new options("請選擇", "0", 0));
            ret.Add(new options("高中以下", "1", 0));
            ret.Add(new options("高中", "2", 0));
            ret.Add(new options("大學", "3", 0));
            ret.Add(new options("碩士", "4", 0));
            ret.Add(new options("博士", "5", 0));
            
            return ret;
        }

        public static List<options> course_source_options()
        {
            List<options> ret = new List<options>();
            ret.Add(new options("企業機構", "1", 0));
            ret.Add(new options("政府機構", "2", 0));
            ret.Add(new options("社團機構", "3", 0));
            ret.Add(new options("公益慈善", "4", 0));
            return ret;
        }

        public static List<options> employ_status_options()
        {
            List<options> ret = new List<options>();
            ret.Add(new options("請選擇", "0", 0));
            ret.Add(new options("在職中", "1", 0));
            ret.Add(new options("待業中", "2", 0));
            ret.Add(new options("創業中", "3", 0));
            ret.Add(new options("就學中", "4", 0));

            return ret;
        }

        public static List<options> occupation_options()
        {
            List<options> ret = new List<options>();
            ret.Add(new options("請選擇職業", "0", 0));
            ret.Add(new options("軍公教人員", "1", 0));
            ret.Add(new options("教師", "2", 0));
            ret.Add(new options("學生", "3", 0));
            ret.Add(new options("服務業", "4", 0));
            ret.Add(new options("自由業", "5", 0));
            ret.Add(new options("製造業", "6", 0));
            ret.Add(new options("商業", "7", 0));
            ret.Add(new options("農牧漁業", "8", 0));
            ret.Add(new options("衛生保健業", "9", 0));
            ret.Add(new options("資訊業", "10", 0));
            ret.Add(new options("其他", "11", 0));
            return ret;
        }



        public static List<ABOUT> AboutTranslate(List<ABOUT> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);

                    datas[i].title = langObj.text1;
                    datas[i].keyword = langObj.text2;
                    datas[i].description = langObj.text3;
                    datas[i].content = langObj.content1;
                }
            }
            return datas;
        }
        public static List<INTRO> IntroTranslate(List<INTRO> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                    datas[i].keyword = langObj.text2;
                    datas[i].description = langObj.text3;
                    datas[i].content = langObj.content1;
                }
            }
            return datas;
        }

        public static List<MAKER_CAR> MakerCarTranslate(List<MAKER_CAR> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                    datas[i].keyword = langObj.text2;
                    datas[i].description = langObj.text3;
                    datas[i].content = langObj.content1;
                }
            }
            return datas;
        }

        public static List<TOUR_POINT> TourPointTranslate(List<TOUR_POINT> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                    datas[i].location = langObj.text2;
                    datas[i].address = langObj.text3;
                }
            }
            return datas;
        }

        public static List<MAKER> MakerTranslate(List<MAKER> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                    datas[i].member = langObj.text2;
                    datas[i].exp = langObj.text3;
                    datas[i].intro = langObj.content1;
                }
            }
            return datas;
        }

        public static List<COURSE> CourseTranslate(List<COURSE> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                    datas[i].location = langObj.text2;
                    datas[i].address = langObj.text3;
                    datas[i].content = langObj.content1;
                }
            }
            return datas;
        }

        public static List<NEWS> NewsTranslate(List<NEWS> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                    datas[i].content = langObj.content1;
                }
            }
            return datas;
        }
        public static List<EQUIP> EquipTranslate(List<EQUIP> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                    datas[i].content = langObj.content1;
                }
            }
            return datas;
        }

        public static List<ALBUM> AlbumTranslate(List<ALBUM> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                }
            }
            return datas;
        }
        public static List<VIDEO> VideoTranslate(List<VIDEO> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                }
            }
            return datas;
        }

        public static List<EQUIP_CATE> EquipCateTranslate(List<EQUIP_CATE> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0 ; i < datas.Count ; i++ ) {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                }
            }
            return datas;
        }

        public static List<WORKS> WorksTranslate(List<WORKS> datas)
        {
            string a = lang_type.zh_hant.ToString(); if (HttpContext.Current.Session["lang"] != null) { a = HttpContext.Current.Session["lang"].ToString(); }
            if (datas == null || datas.Count < 1 || a == lang_type.zh_hant.ToString()) return datas;

            string unitName = datas[0].GetType().Name.ToLower();
            using (var DB = new SOUTHMAKEREntities())
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    var item = datas[i];
                    var langObj = DB.LANGUAGE_DATA.Where(x => x.unit == unitName && x.obj_id == item.id && x.lang == a).FirstOrDefault();
                    if (langObj == null) { langObj = new LANGUAGE_DATA(); }
                    QueryObj_LANGUAGE_DATA.stringAttrInit(ref langObj);
                    datas[i].title = langObj.text1;
                    datas[i].group = langObj.text2;
                    datas[i].tool = langObj.text3;
                    datas[i].content = langObj.content1;
                }
            }
            return datas;
        }


    }
}