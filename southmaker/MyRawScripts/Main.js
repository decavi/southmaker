﻿


//依每個類別的頁面節點來判斷上一頁
window.getLastViewPath = function (nodes) {
    var path = '';

    var pathObj = getControllerAndPageName();
    var n = nodes.filter(x => x.controller == pathObj.controller && x.name == pathObj.pagename);
    if (n.length > 0) {
        if (n[0].previous != '') {
            path = '/' + pathObj.controller + '/' + n[0].previous;
        }
        else {
            path = location;
        }
    }

    return path;
};

window.getControllerAndPageName = function () {
    var pathArray = location.pathname.split('/');
    var controller = pathArray[pathArray.length - 2];
    var pagename = pathArray[pathArray.length - 1];

    return { controller : controller, pagename : pagename };
};


//麵包屑產生器 & 頁面主標題設定
window.getCrumbHtml = function (html, nodes, nodename) {

    //一開始的進入點，nodename參數需為null
    //最後一個節點，使用當下頁面的節點資料
    if (nodename == null) {
        var pathObj = getControllerAndPageName();

        $('#labTitleName').text(langTrans(pathObj.pagename));

        html = '<span>' + langTrans(pathObj.pagename) + '</span>';
        nodes = nodes.filter(x => x.controller == pathObj.controller);
        var n = nodes.filter(x => x.name == pathObj.pagename);
        if (n.length > 0) {
            html = getCrumbHtml(html, nodes, n[0].previous);
        }
    }
    else {
        var n = nodes.filter(x => x.name == nodename);
        if (n.length > 0) {
            html = '<a ' + 'href=\'\\' + n[0].controller + '\\' + n[0].name + '\'' + '><span>' + langTrans(n[0].name) + '</span></a> > ' + html;
            html = getCrumbHtml(html, nodes, n[0].previous);
        }
    }

    return html;
};



window.buildFilePath = function (fileObj) {
    var path = '';

    if (fileObj.type == 'img') {
        if (fileObj.img_size == '') {
            path = '/upload/' + fileObj.class + '/' + fileObj.obj_id + '/' + fileObj.name + '.' + fileObj.exten;
        }
        else {
            path = '/upload/' + fileObj.class + '/' + fileObj.obj_id + '/' + fileObj.name + '@' + fileObj.img_size + '.' + fileObj.exten;
        }
    }
    else if (fileObj.type == 'file') {
        path = '/upload/' + fileObj.class + '/' + fileObj.obj_id + '/' + fileObj.name + '.' + fileObj.exten;
    }

    return path;
}; window.buildImgOriPath = function (fileObj) {
    var path = '';
    if (fileObj.type == 'img') {
        path = '/upload/' + fileObj.class + '/' + fileObj.obj_id + '/' + fileObj.name + '.' + fileObj.exten;
    }
    return path;
};

//console = { log: function () { } };
window.cl = function (objTitle, obj) {
    console.log('==========' + objTitle + '=========='); console.log(obj);
};

window.transMSDate = function (_date) {
    if (typeof _date == 'string') {
        var newDate = new Date(parseInt(_date.replace('/Date(', '').replace(')/', '')));
        if (moment(newDate).format('YYYYMMDD') == '17530101') {
            return '';
        }
        return newDate;
    }
    else
        return _date;
};
window.SetDefaultMSDate = function () {
    return new Date(1753, 1, 1);
};

window.DeleteConfirm = function () {
    return confirm("確定刪除？");
}

window.errorcallback = function ($data) {
    console.log($data);
    ExceptionFilter($data);
};

window.for = function ($i) {
    var temp = [];
    for (var i = 0; i < $i; i++) {
        temp[i] = i;
    }
    return temp;
}

window.ws = function ($method) {
    return window.location.pathname + "/" + $method;
}
window.ws2 = function ($path, $method) {
    return location.origin + "/" + $path + "/" + $method;
}

window.ExceptionFilter = function ($object) {
    if ($object) {
        if ($object.ExceptionType == "MTC.Survey.MessageException") {
            alert($object.Message);
        }
    }
}

window.UploadOption = function ($url, $d) {
    return {
        url: $url,
        method: "POST",
        data: $d,
        transformRequest: angular.identity,
        headers: { 'Content-Type': undefined }
    };
};

window.transDate = function ($data) {
    if (angular.isArray($data)) {
        for (var i = 0; i < $data.length; i++) {
            window.transDate($data[i]);
        }
    } else {
        for (var p in $data) {
            if (typeof ($data[p]) == "string" && $data[p].isMSDate()) {
                $data[p] = $data[p].toDate();
            }
        }
    }
    return $data;
};

/* String擴充 */
if (!String.prototype.isMSDate) {
    String.prototype.isMSDate = function () {
        var result = this.match(/\/Date\((-)?\d+\)\//);
        return result != null;
    };
}

if (!String.prototype.toDate) {
    String.prototype.toDate = function () {
        var stamp = this.match(/(-)?\d+/);
        if (stamp == null) { return new Date(); } else { stamp = stamp[0]; }
        return new Date(parseInt(stamp));
    };
}

/* Array 擴充 */
if (!Array.prototype.find) {
    Array.prototype.find = function (predicate) {
        if (this == null) { throw new TypeError('Array.prototype.find called on null or undefined'); }
        if (typeof predicate !== 'function') { throw new TypeError('predicate must be a function'); }

        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    };
}

if (!Array.prototype.remove) {
    Array.prototype.remove = function () {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };
}

//抓request參數值
window.getReq = function (name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
        return decodeURIComponent(name[1]);
};

window.urlRequestExcept = function (excepts) {
    //ex: excepts = ['p', 'size']
    var urlParam = location.search;
    var paramArray = urlParam.substring(1, urlParam.length).split('&');
    for (var i = 0; i < paramArray.length; i++) {
        if (excepts.indexOf(paramArray[i].split('=')[0].toLowerCase()) > -1) {
            paramArray.splice(i, 1);
            i = 0;
        }
    }
    return paramArray.join('&');
};



String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

var optionCreater = function (val, title) {
    return "<option value='" + val + "'>" + langTrans(title) + "</option>";
};





//轉址小幫手
var redirectAssistant = function (url, p) {
    var ret = url;
    if (p && p.length > 0) {
        ret += '?';
        var a = [];
        for (var i = 0; i < p.length; i++) {
            a.push(p[i].name + '=' + p[i].v);
        }
        ret += a.join('&');
    }
    return ret;
};




//系統訊息填入(新增紅框醒目提示)
var alert_push2 = function (form_name, errorMap) {

    $("#" + form_name + " [data-alert]").each(function () {

        $(this).html('');                                                       //clear alert text
        var tag = $(this).attr('data-alert');                                   //get alert element tag name
        var inputByTag = $('#' + form_name).find('#' + tag);                    //get input element by tag (default)
        var errArray = errorMap.get(tag);                                       //get specific errors by tag (default)

        var row_name = ''; var temp_id = 0;                           
        if (tag == 'lang_type' || tag == 'lang_proficiency') {
            row_name = $(this).closest('div').attr('data-row');                 //get row name
            if (row_name) temp_id = row_name.split('_')[2];                     //get row's id
            inputByTag = $('[data-row=' + row_name + ']').find('#' + tag);      //get input element by tag (special cases)
            errArray = errorMap.get(tag + '_' + temp_id);                       //get specific errors by tag (special cases)

            cl('alert_push2 . inputByTag', '[data-row=' + row_name + '] #' + tag);
        }
        
        if (inputByTag.length == 1) {

            if (inputByTag.attr('class')) {

                if (tag == 'agree') {
                    inputByTag.removeClass('checkbox_alert');
                }
                else {
                    inputByTag.removeClass('required-border');
                }
            }

            if (errArray && errArray.length > 0) {
                $(this).html(errArray.join(', '));
                if (tag == 'agree') {
                    inputByTag.addClass('checkbox_alert');
                }
                else {
                    inputByTag.addClass('required-border');
                }
            }
        }

        if (tag == 'summary') {
            if (errArray && errArray.length > 0) {
                $(this).html(errArray.join(', '));
            }
        }
    });

};


var CovertErrorResultToMap = function (errorObj) {

    var checkResultMap = new Map();
    if (errorObj != null) {
        for (var i = 0; i < errorObj.length; i++) {
            var ckObj = errorObj[i];
            for (var j = 0; j < ckObj.checkResult.length; j++) {
                ckObj.checkResult[j] = langTrans('error_' + ckObj.checkResult[j]);
            }
            checkResultMap.set(ckObj.checkTag, ckObj.checkResult);
        }
    }
    return checkResultMap;
};

var setCookie = function (name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
};
var getCookie = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
};
var eraseCookie = function (name) {
    document.cookie = name + '=; Max-Age=-99999999;';
};





var all_row_check = function all_row_check(obj) {

    var checked = $(obj).prop('checked');
    $('[name=ckb_delete]').each(function () { $(this).prop('checked', checked); });
};