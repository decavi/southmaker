﻿
var pp = GetPageParam();
var about_role = {

    unit_name: 'about',
    list_name: 'about_table',
    form_name: 'about_form',
    search_form_name: 'about_search',
    search_api: 'AboutSearch',
    submit_api: 'AboutSave',
    remove_api: 'AboutRemove',
    export_path: '/Shared/ExportData',
    import_path: '/Shared/ImportData',

    admin_list_search: async function () {
        this.search_form_push_helper();

        var q = await QueryObjInit(this.unit_name); q = paramController(q);
        var $ret = await ajax_post2('Home', this.search_api, q, pp); cl('admin_list_search . $ret', $ret);
        var list = []; if ($ret && $ret.hasData) list = $ret.data1;

        await this.has_language_data_helper(list);
        this.list_push_helper(list); pagerHelper($ret, pp);
    },

    front_list_search: async function () { },

    admin_form_push: async function () {
        this.form_init_helper();
        collect_all_check_input_and_bind_event(this.unit_name);
        var q = await QueryObjInit(this.unit_name); q = paramController(q);
        if (!q.id || q.id < 1) return;

        var $ret = await ajax_post2('Home', this.search_api, q, pp); cl('admin_form_push . $ret', $ret);
        var data; if ($ret && $ret.hasData) data = $ret.data1[0];
        if (data) data = await lang_obj_load(this.unit_name, q.id, data, pp);            //抓語言資料
        this.form_push_helper(data, false);
        form_validate(this.unit_name);
    },

    front_form_push: async function () { },

    front_view_push: async function () { },

    admin_form_submit: async function () {
        var langSwitch = $('#lang_switch');
        if (langSwitch.length == 1) { langSwitch.val('zh_hant'); langSwitch.change(); }           //切換回中文版表單資料

        var ckMap = new Map(); alert_push2(this.form_name, ckMap);
        ckMap = this.form_validate_helper(data);
        if (ckMap.size > 0) return;

        var data = this.form_pull_helper();
        var $ret = await ajax_post3('Home', this.submit_api, data, pp); cl('admin_form_submit . $ret', $ret);
        if ($ret && $ret.isSuccess) await this.admin_submit_result_flow($ret, data);
    },

    front_form_submit: async function () { },

    admin_submit_result_flow: async function ($ret, data) {
        if (!$ret || !$ret.isSuccess) return;

        var $file_ret = await file_command_execute(this.form_name); cl('file_command_execute', $file_ret);
        var $lang_ret = await lang_obj_save(data, $ret.data1.id); cl('lang_obj_save()', $lang_ret);
        if ($file_ret && $file_ret.isSuccess) location = '/Admin/About';
    },

    front_submit_result_flow: async function () { },

    admin_data_remove: async function (id) {
        var ids = [];
        if (id) ids.push(id);
        if (!id) $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });

        if (ids.length < 1) { alert('請先勾選要刪除的項目'); return; }
        if (confirm('確定刪除?') == false) return;

        var $ret = await ajax_post3('Home', this.remove_api, { ids: ids }, null);
        if ($ret && $ret.isSuccess) this.admin_list_search();
    },


    has_language_data_helper: async function (list) {
        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: this.unit_name, obj_id: list[i].id, langType: 'en_us' }, pp);
            list[i].has_en_us = langRet.hasData;
        }
    },

    list_push_helper: function (list) {
        var tags = ['id', 'title', 'has_en_us'];
        common_list_push(this.list_name, tags, list);
    },

    form_push_helper: function (data, isLockForm) {
        var tags = ['file']; //特殊欄位標記
        data.file_tag = 'about';
        common_form_push(this.form_name, tags, data, pp, isLockForm);
    },

    form_pull_helper: function () {
        var table = $('#' + this.form_name);
        var tags = ['title', 'keyword', 'description', 'content', 'sort'];
        return common_data_pull(table, tags);
    },

    form_init_helper: function () {
        var tags = ['ckContent', 'dataInit', 'file', 'language'];
        common_form_init(this.form_name, this.unit_name, tags, pp);
    },

    form_validate_helper: function (data) {
        return form_validate('about');
    },

    form_edit_cancel: function () {
        common_edit_cancel('About');
    },

    form_clear_helper: function () {
        var form = $('#' + this.form_name);
        if (form.length == 0) return;

        form.find('input[type=text]').each(function () { $(this).val(''); });
        form.find('input[type=number]').each(function () { $(this).val('0'); });
        form.find('textarea').each(function () { $(this).val(''); });
        form.find('select').each(function () { $(this).val($(this).find('option:eq(0)').val()); });
        form.find('[name*=_file_view]').html('');
        form.find('[name=file_command]').each(function () { $(this).data({ on: [], off: [] }); });
        if (CKEDITOR.instances['content']) CKEDITOR.instances.content.setData('');
    },

    search_redirect: function () {
        var p = this.search_form_pull_helper();
        location = redirectAssistant(location.pathname, p);
    },

    admin_data_export: function () {
        var p = this.search_form_pull_helper();
        location = redirectAssistant(this.export_path, p);
    },

    search_form_pull_helper: function () {
        var p = [];
        var form = $('#' + this.search_form_name); if (form.length != 1) return;
        var keyword = form.find("#keyword").length == 1 ? form.find("#keyword").val().trim() : '';      //關鍵字篩選
        if (keyword) p.push({ name: 'search_keyword', v: keyword });
        return p;
    },

    search_form_push_helper: function () {
        var form = $('#' + this.search_form_name); if (form.length != 1) return;
        form.find('#keyword').val(getReq('search_keyword'));
    },

};
