﻿
//取得預設資料物件
var DataObjInit = function (type) {
    var data = {
        type: type,
    };
    return ajax_post('Shared', 'DataObjInit', data);
};
//取得預設資料查詢物件
var QueryObjInit = function (type) {
    var data = {
        type: type,
    };
    return ajax_post('Shared', 'QueryObjInit', data);
};

var CaptchaObjValid = function (input) {
    var data = {
        input: input,
    };
    return ajax_post('Shared', 'CaptchaValidate', data);
};


var ServiceSearch = function (q, pp) {



};




//主POST function
var ajax_post = function (ctrl, fun, input) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: ws2(ctrl, fun),
            data: JSON.stringify(input),
            type: 'POST',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: "application/json; charset=utf-8",
            success: function ($data) { $data = dataExtraOutputProcess(ctrl, fun, $data, null); resolve($data); },
            error: function ($data) { errorcallback(); },
        });
    });
};

//主POST function type2 (output用)
var ajax_post2 = function (ctrl, fun, input, pp) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: ws2(ctrl, fun),
            data: JSON.stringify(input),
            type: 'POST',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: "application/json; charset=utf-8",
            success: function ($data) { $data = dataExtraOutputProcess(ctrl, fun, $data, pp); resolve($data); },
            error: function ($data) { errorcallback(); },
        });
    });
};

//主POST function type3 (input用)
var ajax_post3 = function (ctrl, fun, input, pp) {
    input = dataExtraInputProcess(ctrl, fun, input, pp);
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: ws2(ctrl, fun),
            data: JSON.stringify(input),
            type: 'POST',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: "application/json; charset=utf-8",
            success: function ($data) { resolve($data); },
            error: function ($data) { errorcallback(); },
        });
    });
};


var common_ajax_post = function (url) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: "application/json; charset=utf-8",
            success: function ($data) { resolve($data); },
            error: function ($data) { errorcallback(); },
        });
    });
};

var common_ajax_get = function (url) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: "application/json; charset=utf-8",
            success: function ($data) { resolve($data); },
            error: function ($data) { errorcallback(); },
        });
    });
};

var fb_ajax_get = function (url) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: "application/x-www-form-urlencoded",
            success: function ($data) { resolve($data); },
            error: function ($data) { errorcallback(); },
        });
    });
};




var dataExtraOutputProcess = function (ctrl, fun, data, pp) {

    if (fun) {

        switch (fun) {

            case 'CourseMemberSearch': {
                if (data && data.hasData) {
                    $(data.data1).each(function () {
                        this.member.birth = transMSDate(this.member.birth);
                        this.course_member.date_created = transMSDate(this.course_member.date_created);
                        this.course_member.date_modify = transMSDate(this.course_member.date_modify);
                    });
                }
                //cl('data', data);
            } break;

            case 'MemberSearch': {
                if (data && data.hasData) {
                    $(data.data1).each(function () {
                        this.birth = transMSDate(this.birth);
                        this.date_created = transMSDate(this.date_created);
                        this.date_modify = transMSDate(this.date_modify);
                    });
                }
                //cl('data', data);
            } break;

            case 'CourseSearch': 
            case 'TourPointSearch': {
                if (data && data.hasData) {
                    $(data.data1).each(function () {
                        this.time = transMSDate(this.time);
                        this.leave_time = transMSDate(this.leave_time);
                        this.date_created = transMSDate(this.date_created);
                        this.date_modify = transMSDate(this.date_modify);
                        this.date_start = transMSDate(this.date_start);
                        this.date_end = transMSDate(this.date_end);
                    });
                }
                //cl('data', data);
            } break;

            case 'AlbumSearch': 
            case 'VideoSearch': 
            //case 'EquipSearch': 
            case 'EquipCateSearch': 
            case 'WorksSearch': 
            case 'NewsSearch': {
                if (data && data.hasData) {
                    $(data.data1).each(function () {
                        this.date_created = transMSDate(this.date_created);
                        this.date_modify = transMSDate(this.date_modify);
                        this.date_start = transMSDate(this.date_start);
                        this.date_end = transMSDate(this.date_end);
                    });
                }
                //cl('data', data);
            } break;

            case 'EquipSearch': {
                if (data && data.hasData) {
                    $(data.data1).each(function () {

                        this.date_created = transMSDate(this.date_created);
                        this.date_modify = transMSDate(this.date_modify);
                        this.date_start = transMSDate(this.date_start);
                        this.date_end = transMSDate(this.date_end);
                    });
                }
                //cl('data', data);
            } break;

            case 'FilesSearch': {

                if (data && data.data1) {

                    $(data.data1).each(function () { 
                        this.path = buildFilePath(this);
                        this.oriPath = buildImgOriPath(this);
                    });
                }
                //cl('data', data);
            } break;

            default: break;
        }

    }

    if (ctrl) {
        switch (ctrl) {
            case 'Shared': {

                switch (fun) {
                    case 'QueryObjInit': {

                        if (data.date_created) {
                            //data.date_created = transMSDate(data.date_created);
                            data.date_created = '';
                        }
                        if (data.date_modify) {
                            //data.date_modify = transMSDate(data.date_modify);
                            data.date_modify = '';
                        }

                    } break;

                    case 'DataObjInit': {

                    } break;

                    default: break;
                }

            } break;

            default: break;
        }
    }

    return data;
};



var dataExtraInputProcess = function (ctrl, fun, data, pp) {

    if (fun) {    
        switch (fun) {

            case 'LangDataSave':
            case 'ADSave':
            case 'IntroSave':
            case 'MakerCarSave':
            case 'TourPointSave':
            case 'MakerSave':
            case 'CourseSave':
            case 'NewsSave':
            case 'AlbumSave':
            case 'VideoSave':
            case 'EquipSave':
            case 'EquipCateSave':
            case 'WorksSave':
            case 'MemberSave':
            case 'CourseMemberSave':
            case 'AboutSave':{
                if (data) {
                    if (data.date_created) { data.date_created = transMSDate(data.date_created); }
                    if (data.date_modify) { data.date_modify = transMSDate(data.date_modify); }
                }
                //return data;
            } break;
            

            default: break;

        }
    }

    return data;
};