﻿//[string]
var emailFormat = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
var ch_langFormat = /^[\d|\u4E00-\u9FA5]*$/;
var pt_langFormat = /^[\s\d|a-zA-Z\u00C0-\u00ff]*$/;
var en_langFormat = /^[\s\d|a-zA-Z]*$/;
var ch_pt_en_langFormat = /^[\s\d|a-zA-Z|\u4E00-\u9FA5|\u00C0-\u00ff'?,;.)(\-]*$/;
var urlFormat = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/;

//var latitudeFormat = /^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/;
//var longitudeFormat = /^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/;
var latitudeFormat = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$/;
var longitudeFormat = /^[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;

var pay_account_format = /^\d{12}$/;

//文字框必填檢查
var ck_text_empty = function (str) {
    var ret = '';
    if (!str) { ret = 'error_200002'; }
    return ret;
};

//付款帳號格式檢查
var ck_pay_account_format = function (str) {
    var ret = '';
    if (str) {
        if (pay_account_format.test(str) == false) {
            ret = 'error_200003';
        }
    }
    return ret;
};

//浮點數格式檢查
var ck_float_format = function (str) {
    var ret = '';
    if (Number.isInteger(str) == false) {
        ret = 'error_200007';
    }
    return ret;
};

//整數格式檢查
var ck_int_format = function (str) {
    var ret = '';
    str = parseInt(str);
    if (Number.isInteger(str) == false) {
        ret = 'error_200007';
    }
    return ret;
};

//郵件格式檢查
var ck_email_format = function (str) {
    var ret = '';
    if (str) {
        if (emailFormat.test(str) == false) {
            ret = 'error_200003';
        }
    }
    return ret;
};

//語言限制檢查
var ck_text_language = function (str) {
    var ret = '';

    //var isOK = false;
    if (str) {
        if (ch_pt_en_langFormat.test(str) == false) { ret = 'error_200006'; }
    }
    return ret;
};

//機構名稱中葡英至少一種檢查
var ck_text_corp_name_at_least = function (ch_name, pt_name, en_name) {
    var ret = '';
    if (!ch_name && !pt_name && !en_name) {
        ret = 'error_200004'; 
    }
    return ret;
};

//履歷名字中英至少一種檢查
var ck_text_cv_name_at_least = function (ch_name, en_name) {
    var ret = '';
    if (!ch_name && !en_name) {
        ret = 'error_200004';
    }
    return ret;
};

//語言中文檢查
var ck_text_chinese_limit = function (str) {
    var ret = '';

    var isOK = false;
    if (str) {
        if (ch_langFormat.test(str) == false) { ret = 'error_200013'; }
    }
    return ret;
};

//語言葡文檢查
var ck_text_portuguese_limit = function (str) {
    var ret = '';

    var isOK = false;
    if (str) {
        if (pt_langFormat.test(str) == false) { ret = 'error_200014'; }
    }
    return ret;
};

//語言英文檢查
var ck_text_english_limit = function (str) {
    var ret = '';

    var isOK = false;
    if (str) {
        if (en_langFormat.test(str) == false) { ret = 'error_200012'; }
    }
    return ret;
};

//語言英數檢查
var ck_text_english_number_limit = function (str) {
    var ret = '';

    var isOK = false;
    if (str) {
        if (en_langFormat.test(str) == false) { ret = 'error_200009'; }
    }
    return ret;
};

//網址格式檢查
var ck_url_format = function (str) {
    var ret = '';
    if (str) {
        if (urlFormat.test(str) == false) { ret = 'error_200003'; }
    }
    return ret;
};

//地址格式檢查(不可跳行填寫)
var ck_address_format = function (addressA, addressB, addressC, addressD) {
    var ret = '';
    if (addressD) {
        if (addressB && !addressC) {
            ret = 'error_200015';
        }
        else if (addressA && (!addressB || !addressC)) {
            ret = 'error_200015';
        }
    }
    return ret;
};

//經度格式檢查
var ck_latitude_format = function (str) {
    var ret = '';
    if (latitudeFormat.test(str) == false) ret = 'error_200003';
    return ret;
};
//緯度格式檢查
var ck_longitude_format = function (str) {
    var ret = '';
    if (longitudeFormat.test(str) == false) ret = 'error_200003';
    return ret;
};



//[date]
//雙日期檢查(開始日期不可比結束日期晚)
var ck_double_date_order = function (date1, date2) {
    var ret = '';
    var _date1 = new Date(date1);
    var _date2 = new Date(date2);
    if (_date1 && _date2) {
        if (_date1.getTime() > _date2.getTime()) {
            ret = 'error_200008';
        }
    }
    return ret;
};

//日期格式檢查
var ck_date_format = function (date) {
    var ret = '';
    if (date) {
        var d = new Date(date);
        if (d.toString() == 'Invalid Date') {
            ret = 'error_200012';
        }
        if (date.indexOf('/') > -1) {
            if (d.getDate() != date.toString().split('/')[2].split(' ')[0]) {
                ret = 'error_200012';
            }
        }
        if (date.indexOf('-') > -1) {
            if (d.getDate() != date.toString().split('-')[2].split(' ')[0]) {
                ret = 'error_200012';
            }
        }
    }
    return ret;
}


//[select]
//select必選檢查
var ck_select_empty = function (val) {
    var ret = '';
    if (!val || val == 0) { ret = 'error_200005'; }
    return ret;
};

//select新增選項檢查
var ck_select_option_empty = function (val) {
    var ret = '';
    if (!val) { ret = 'error_200010'; }
    return ret;
};




//[tel]
//電話格式檢查
var ck_tel_format = function (areacode, tel) {
    var ret = '';

    if (!areacode) areacode = '';
    if (!tel) tel = '';
    var isOK = true;

    //var rule = /^[0-9].{0,}$/;
    var rule = /^[\d]+$/;
    //港澳地區限制為7-8碼
    if (areacode == '+853' || areacode == '+852') {
        if (tel.length < 7 || tel.length > 8) {
            isOK = false;
        }
        else {
            isOK = rule.test(tel);
        }
    }
    //其他地區限制不超過20碼
    else if (areacode) {
        if (tel.length > 20) {
            isOK = false;
        }
        else {
            isOK = rule.test(tel);
        }
    }
    else {
        isOK = false;
    }

    if (!isOK) { ret = 'error_200003'; }
    return ret;
};

//直線與辦公室電話至少需填1項檢查
var ck_direct_or_office = function (direct_line, office_line) {
    var ret = '';
    if (!direct_line && !office_line) { ret = 'error_200004'; }
    return ret;
};

//[file]
//檔案必須上傳檢查
var ck_file_empty = function (files) {
    var ret = '';
    if (!files[0]) { ret = 'error_200011'; }
    return ret;
};


//[agree]
//須先同意政策
var ck_policy_agree = function (agree) {
    var ret = '';
    if (!agree) { ret = 'error_200001'; }
    return ret;
};


//[password]
//密碼字元長度最小最大限制
var ck_password_length_limit = function (pwd, minLength, maxLength) {
    var ret = '';
    if (pwd.length < minLength || pwd.length > maxLength) {
        ret = langTrans('error_200201').replace('[[minLimit]]', minLength).replace('[[maxLimit]]', maxLength);
    }
    return ret;
};

//密碼須同時包括數字及英文字母(區分大小寫)
var ck_password_english_number_format = function (pwd) {
    var ret = '';
    if (en_langFormat.test(pwd) == false) {
        ret = 'error_200202';
    }
    return ret;
};

//[confirm_password]
//密碼與確認密碼須相同
var ck_confirm_password_same = function (pwd, confirm_pwd) {
    var ret = '';
    if (pwd != confirm_pwd) {
        ret = 'error_200204';
    }
    return ret;
};




