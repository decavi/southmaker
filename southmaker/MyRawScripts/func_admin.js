﻿

//主流程
var func_admin_start = async function () {

    if ($('#captcha_img').length == 1) admin_login_update_captcha();

    backend_menu_push();

};



//管理者登入送出表單
var admin_login = async function () {

    var ckMap = new Map(); alert_push2('login_form', ckMap);        //初始訊息物件
    

    cl('login_submit ckMap', ckMap);
    if (ckMap.size > 0) { return; }                 //驗證有誤 離開

    var form = $('#login_form');
    var input = {
        account: form.find('#account').val(),
        pwd: form.find('#password').val(),
        captcha: form.find('#captcha').val(),
    };

    var $ret = await ajax_post3('Admin', 'Login', input, null);               //建立機構用戶正式資料
    cl('admin_login . Login . $ret', $ret);
    if ($ret && $ret.isSuccess) {

        var userObj = $ret.data1;
        location = '/Admin/Intro';                                 //登入成功，導頁至預設頁
    }
    else {                                                          //機構用戶正式資料建立失敗
        ckMap = CovertErrorResultToMap($ret.data2);
        alert_push2('login_form', ckMap);                     //更新訊息顯示
    }

    //$(".BDC_ReloadIcon").click();                                   //更新圖形驗證碼顯示
    admin_login_update_captcha();
};


var admin_logout = async function () {            //登出

    if (confirm(langTrans('q_100004')) == false) return;
    var $ret = await ajax_post3('Admin', 'Logout', {}, null);
    if ($ret && $ret.isSuccess) { location = '/Admin/Index'; }
};



func_admin_start();








