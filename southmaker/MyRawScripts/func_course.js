﻿

//主流程
var func_course_start = async function () {




    var pp = GetPageParam();    //設定page參數
    cl('func_course_start . GetPageParam', pp);

    if (pp && pp.unit) {

        page_title_controller(pp);            //設定頁面標題


        if (pp.unit == 'signup') {

            if (pp.end == 'front') {

                member_form_init(pp);

                var q = await QueryObjInit('course'); q = paramController(q);                                       //準備query參數
                var $ret = await ajax_post2('Course', 'CourseSearch', q, pp); cl('CourseSearch . $ret', $ret);      //抓資料
                if ($ret && $ret.hasData) {
                    $ret.data1 = await translate_multi(pp, $ret.data1);     //translate
                    var data = $ret.data1[0];

                    var extenRet = await ajax_post2('Course', 'CourseExtenSearch', [data.id], null);          //抓資料
                    if (extenRet && extenRet.hasData) {
                        data.recurit_status = extenRet.data1[0].recurit_status;
                        data.sign_up_list = extenRet.data1[0].sign_up_list;
                    }
                    //cl('extenRet', extenRet);
                    course_view_push(data);         //data push to view

                    if (data.recurit_status == 'recruiting') {      //更新報名截止時間倒數狀態(狀態為招生中才執行)
                        sign_up_time_countdown(data.id);
                    }

                    //var login_account = $('#login_account').val();
                    var login_account = pp.login_account;
                    if (login_account) {

                        var member_q = await QueryObjInit('member'); member_q.account = login_account;
                        var memberRet = await ajax_post2('Member', 'MemberSearch', member_q, pp); cl('MemberSearch . $ret', memberRet);      //抓資料
                        if (memberRet && memberRet.hasData) {
                            var memberObj = memberRet.data1[0];
                            cl('memberObj', memberObj);
                            member_form_push(memberObj, pp, true);


                            //隱藏會員資料表單處理(用於檢測會員的所有必填資料)
                            hide_member_form_init(pp);
                            hide_member_form_push(memberObj, pp, true);
                        }
                    }
                }
            }
        }
        

        //course頁面流程開始
        if (pp.unit == 'course') {

            if (pp.end == 'front') {

                var q = await QueryObjInit('course');
                q = paramController(q);


                //月曆模式
                if (pp.datamode == 'calendar') {            

                    var theDay = new Date(new Date().getFullYear(), new Date().getMonth(), 1);


                    var ym = getReq('ym');
                    var year = 0, month = 0;
                    if (ym && ym.indexOf('/') > -1) {
                        year = ym.split('/')[0];
                        month = ym.split('/')[1];
                        if (month < 1 || month > 12) month = '-';
                        else month = parseInt(month) - 1;
                        theDay = new Date(year, month, 1);
                    }

                    if (theDay.getTime() !== theDay.getTime()) {
                        var now = new Date();
                        theDay = new Date(now.getFullYear(), now.getMonth(), 1);        //抓user電腦當月1號
                    }

                    var firstDay = new Date(theDay);

                    var nowDayOfWeek = theDay.getDay();                             //抓該日期是星期幾
                    var isFillYet = false;

                    var calendar_table_row = $('#calendar_table').find('tr');
                    //cl('calendar_table_row', calendar_table_row);

                    $(calendar_table_row).each(function () {            //每列

                        var calendar_table_td = $(this).find('td');
                        //cl('calendar_table_td', calendar_table_td);
                        var dayOfWeek = 0;
                        $(calendar_table_td).each(function () {         //每欄

                            if (isFillYet == false) {             //初次填  找到當月1號的起填點
                                if (dayOfWeek == nowDayOfWeek) { isFillYet = true; }
                                else { $(this).find('[name=day]').attr('empty', dayOfWeek); }       //標記略過的日期
                            }
                            if (isFillYet) {
                                $(this).find('[name=day]').html(theDay.getDate());                      //找到當月1號的起填點之後就依序往下填日期
                                $(this).attr('data-date', moment(theDay).format('YYYY-MM-DD'));         //於TD做標記
                                theDay.setDate(theDay.getDate() + 1);                         //日期往後加1天
                            }

                            dayOfWeek++;        //go next
                        });
                    });

                    //開始填上個月的剩餘空格日期
                    var _firstDay = new Date(firstDay);
                    _firstDay.setDate(_firstDay.getDate() - 1);
                    for (var i = _firstDay.getDay() ; i >= 0; i--){
                        
                        if ($('[empty=' + i + ']').length == 1) {
                            $('[empty=' + i + ']').html(_firstDay.getDate());                      
                            $('[empty=' + i + ']').closest('td').attr('data-date', moment(_firstDay).format('YYYY-MM-DD'));         //於TD做標記
                            $('[empty=' + i + ']').removeAttr('empty');
                            _firstDay.setDate(_firstDay.getDate() - 1);                         //日期往前減1天
                        }
                    }


                    var calendar_date_start = $('#calendar_table').find('td:eq(0)').attr('data-date');
                    var calendar_date_end = $('#calendar_table').find('td:eq(41)').attr('data-date');
                    if (calendar_date_start) q.date_start = calendar_date_start;
                    if (calendar_date_end) q.date_end = calendar_date_end;


                    $('#calendar_title').html(moment(firstDay).format('YYYY/MM'));                           //填月曆標題
                    var prevDate = new Date(firstDay.getFullYear(), firstDay.getMonth(), 1);                    //填導向上個月的redirect event
                    prevDate.setDate(prevDate.getDate() - 1); var prevMonth = prevDate.getMonth() + 1;
                    $('.week-prev').attr('onclick', "calendar_redirect(" + prevDate.getFullYear() + ", " + prevMonth + ")");    
                    var nextDate = new Date(firstDay.getFullYear(), firstDay.getMonth(), 1);                    //填導向下個月的redirect event
                    nextDate.setMonth(nextDate.getMonth() + 1); var nextMonth = nextDate.getMonth() + 1;
                    $('.week-next').attr('onclick', "calendar_redirect(" + nextDate.getFullYear() + ", " + nextMonth + ")");    
                }

                q.is_enabled = 1;
                var $ret = await ajax_post2('Course', 'CourseSearch', q, pp);          //抓資料
                cl('CourseSearch . $ret', $ret);


                if (pp.datamode == 'calendar') {

                    if ($ret && $ret.hasData) {
                        $ret.data1 = await translate_multi(pp, $ret.data1);     //translate
                        //cl('$ret.data1', $ret.data1);

                        $($ret.data1).each(function () {
                            
                            var time = moment(this.time).format('YYYY-MM-DD');
                            if ($('[data-date=' + time + ']').length == 1) {
                                var oriHtml = $('[data-date=' + time + ']').html();

                                var title = this.title.length > 7 ? this.title.substr(0, 7) + '...' : this.title;
                                var eventHtml = "<div class='course-type'><a class='type" + this.day_type + "' href='/Course/Detail?id=" + this.id + "' title='" + this.title + "'><p>" + title + "</p></a></div>";

                                //<div class="course-type">
                                //    <a class="type3" href="course_deadline.html" title="女力創客工作坊-編織手球器"><p>【女力創客工...</p></a>
                                //</div>

                                $('[data-date=' + time + ']').html(oriHtml + eventHtml);
                            }
                        });
                    }

                    course_search_value_push();                 //req push to search form
                }


                if (pp.datamode == 'list') {
                    var list = [];
                    if ($ret && $ret.hasData) {
                        $ret.data1 = await translate_multi(pp, $ret.data1);     //translate
                        list = $ret.data1;
                        //for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                        //    var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                        //    list[i].has_en_us = langRet.hasData;
                        //}
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    course_list_push(list, pp);                  //data row push

                    course_search_value_push();                 //req push to search form
                }

                if (pp.datamode == 'view') {

                    if ($ret && $ret.hasData) {
                        $ret.data1 = await translate_multi(pp, $ret.data1);     //translate
                        var data = $ret.data1[0];

                        var extenRet = await ajax_post2('Course', 'CourseExtenSearch', [data.id], null);          //抓資料
                        if (extenRet && extenRet.hasData) {
                            data.recurit_status = extenRet.data1[0].recurit_status;
                            data.sign_up_list = extenRet.data1[0].sign_up_list;
                        }
                        //cl('extenRet', extenRet);
                        course_view_push(data);         //data push to view

                        if (data.recurit_status == 'recruiting') {      //更新報名截止時間倒數狀態(狀態為招生中才執行)
                            sign_up_time_countdown(data.id);       
                        }

                        //顯示文章更新時間
                        if ($('#article_update_time').length == 1) {
                            var time = (data.date_modify.getFullYear()-1911) + moment(data.date_modify).format('-MM-DD');
                            $('#date_modify').html(time);
                            $('#article_update_time').show();
                        }
                    }
                }
            }

            if (pp.end == 'back') {                             //後台流程

                var q = await QueryObjInit('course');
                q = paramController(q);
                var $ret = await ajax_post2('Course', 'CourseSearch', q, pp);          //抓資料
                cl('CourseSearch . $ret', $ret);

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    course_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    course_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            course_form_push(data, pp, false);
                        }
                    }

                    collect_all_check_input_and_bind_event('course');
                }

            }

        }
        //course頁面流程結束

        //course_survey頁面流程開始
        if (pp.unit == 'course_survey') {

            var q = await QueryObjInit('course');
            q = paramController(q);
            var $ret = await ajax_post2('Course', 'CourseSearch', q, pp);          //抓資料
            cl('CourseSearch . $ret', $ret);


            if (pp.end == 'back') {                             //後台流程

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }

                    var surveyQ = await QueryObjInit('survey');             //抓課程問卷數量
                    for (var i = 0; i < list.length; i++){
                        list[i].survey_count = 0;
                        surveyQ.pageSize = 10000;
                        surveyQ.course_id = list[i].id;
                        var surveyRet = await ajax_post2('Course', 'CourseSurveySearch', surveyQ, pp);
                        if (surveyRet && surveyRet.hasData) {
                            list[i].survey_count = surveyRet.data2;
                        }
                    }
                    cl('after CourseSurveySearch list', list);

                    course_list_push(list, pp);                  //data row push
                }
            }

        }
        //course_survey頁面流程結束



        //survey_member頁面流程開始
        if (pp.unit == 'survey_member') {

            var q = await QueryObjInit('course');
            q = paramController(q);
            var $ret = await ajax_post2('Course', 'SurveyMemberSearch', q, pp);          //抓資料
            cl('SurveyMemberSearch . $ret', $ret);

            if (pp.end == 'back') {                             //後台流程

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }

                    survey_member_list_push(list, pp);                  //data row push
                }
            }

        }
        //survey_member頁面流程結束


        //survey_detail頁面流程開始
        if (pp.unit == 'survey_detail') {

            if ($('#data_empty_tip').length == 1) $('#data_empty_tip').show();
            if ($('#survey_detail_form').length == 1) $('#survey_detail_form').hide();

            var q = await QueryObjInit('course');
            q = paramController(q);

            if (q.id < 1) return;               //無ID參數直接離開

            var $ret = await ajax_post2('Course', 'SurveyMemberSearch', q, pp);          //抓資料
            cl('SurveyMemberSearch . $ret', $ret);

            if (pp.end == 'back') {                             //後台流程

                if ($ret && $ret.hasData) {

                    if ($('#data_empty_tip').length == 1) $('#data_empty_tip').hide();
                    if ($('#survey_detail_form').length == 1) $('#survey_detail_form').show();

                    var data = $ret.data1[0];
                    survey_detail_form_push(data, pp, false);
                }
            }

        }
        //survey_detail頁面流程結束



        //survey_summary頁面流程開始
        if (pp.unit == 'survey_summary') {

            var q = await QueryObjInit('survey');
            q = paramController(q);
            q.pageSize = 10000;                 //資料全撈
            var $ret = await ajax_post2('Course', 'CourseSurveySearch', q, pp);          //抓資料
            cl('CourseSurveySearch . $ret', $ret);

            var list = []; var num = 0;
            if ($ret && $ret.hasData) {

                list = $ret.data1;
                num = $ret.data2;

                if ($('#data_empty_tip').length == 1) $('#data_empty_tip').hide();
            }

            if (list.length < 1 || num < 1) {       //資料不齊全 離開
                if ($('#data_empty_tip').length == 1) $('#data_empty_tip').show();
                return;           
            }

            var avgArray = [];
            var t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0;
            $(list).each(function () {
                t1 += this.a1;
                t2 += this.a2;
                t3 += this.a3;
                t4 += this.a4;
                t5 += this.a5;
            });
            avgArray.push((t1 / num).toFixed(2));
            avgArray.push((t2 / num).toFixed(2));
            avgArray.push((t3 / num).toFixed(2));
            avgArray.push((t4 / num).toFixed(2));
            avgArray.push((t5 / num).toFixed(2));
            //var chartMax = Math.ceil(Math.max(...avgArray));
            cl('avgArray', avgArray);
            //cl('max', Math.ceil(Math.max(...avgArray)));

            for (var i = 0; i < avgArray.length ; i++){
                if ($('#survey_summary_table').find('[name=avg' + (i + 1) + ']').length == 1) $('#survey_summary_table').find('[name=avg' + (i + 1) + ']').html(avgArray[i]);
            }

            var canvas1Data = {
                labels: ["學習目標清楚", "學習內容有幫助", "學習內容條理分明", "學習內容生動有趣", "課程時間安排恰當"],
                datasets: [{
                    backgroundColor: ["#317c21", "#333", "#317c21", "#333", "#317c21", "#333"],
                    //data: ['1.2', '2.3', '3.4', '4.5', '5.7']
                    data: avgArray
                }]
            };

            var ctx = document.getElementById("canvas1").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: canvas1Data,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                max: 5
                            }
                        }]
                    },
                    legend: {
                        display: false
                    },
                    responsive: true

                }
            });

        }
        //survey_summary頁面流程結束

        //course_member頁面流程開始
        if (pp.unit == 'course_member') {

            var q = await QueryObjInit('course_member');
            q = paramController(q);
            var $ret = await ajax_post2('Course', 'CourseMemberSearch', q, pp);          //抓資料
            cl('CourseMemberSearch . $ret', $ret);


            if (pp.end == 'back') {                             //後台流程

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    course_member_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    course_member_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            course_member_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //course_member頁面流程結束

        


    }
};



//管理者端 course 保存
var admin_course_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('course_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = course_form_pull(pp);
        //ckMap = admin_course_form_validate(data, pp);                //form validate
        ckMap = form_validate('course');
        cl('ckMap', ckMap);
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = course_row_pull(trigger, pp);
        //ckMap = admin_course_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_course_save . data', data);

    
    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Course', 'CourseSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            var $file_ret = await file_command_execute('course_form');
            if ($file_ret && $file_ret.isSuccess) {
                await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
                location = '/Admin/Course';
            }
        }
        if (pp.datamode == 'list') {
            alert(langTrans('msg_100005'));
            func_course_start();          //重整頁面
        }
    }
};


//前端 course_member 保存 (課程報名)
var user_course_sign_up_submit = async function () {

    if (confirm(langTrans('q_100001')) == false) return;

    var pp = GetPageParam();

    //檢測會員資料是否填寫完整
    var hide_member_ck_map = new Map();
    if ($('#hide_member_form').length == 1) {
        hide_member_ck_map = form_validate('hide_member');
        cl('hide_member_ck_map', hide_member_ck_map);
    }

    if (hide_member_ck_map.size > 0) {                              //會員資料尚不完整
        alert('會員資料尚不完整，請至會員資訊頁將資料填寫完整。');
        location = '/Member/Edit';
    }
    else {              //會員資料沒問題，進行報名程序

        var data = await DataObjInit('course_member');
        data.member_id = pp.login_member_id;

        //從req取
        var course_id = getReq('id');
        if (course_id && course_id > 0) data.course_id = course_id;

        var $ret = await ajax_post3('Course', 'CourseMemberSave', data, null);
        if ($ret && $ret.isSuccess) {
            alert(langTrans('msg_100010'));
            location = '/Course/Detail?id=' + course_id;
        }
        else {
            var ckMap = CovertErrorResultToMap($ret.data2);
            alert(ckMap.get('alert').join(','));
        }
        cl('user_course_sign_up_submit . data', data);
    }
};




//管理者端 course_member 保存
var admin_course_member_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('course_member_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式

        data = course_member_form_pull(pp);

        var ids = [];
        ids.push(getReq('id'));
        var input = { ids: ids, is_enabled: data.course_member_is_enabled };
        var $ret = await ajax_post3('Course', 'CourseMemberEnableChange', input, null);
        if ($ret && $ret.isSuccess) {
            location = '/Admin/CourseMember?course_id=' + getReq('course_id');
        }
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式(暫時無功能)

        //if (!trigger) return;
        //data = course_member_row_pull(trigger, pp);
        //ckMap = admin_course_member_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_course_member_save . data', data);
};







var admin_course_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert(langTrans('msg_100014')); return;
    }

    if (confirm(langTrans('q_100002'))) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Course', 'CourseRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_course_start();          //重整頁面
        }
    }
};


var admin_course_member_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert(langTrans('msg_100014')); return;
    }

    if (confirm(langTrans('q_100002'))) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Course', 'CourseMemberRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_course_start();          //重整頁面
        }
    }
};



var admin_course_member_enable = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert(langTrans('msg_100015')); return;
    }

    if (confirm(langTrans('q_100003'))) {
        var input = { ids: ids, is_enabled: 1 };
        var $ret = await ajax_post3('Course', 'CourseMemberEnableChange', input, null);
        if ($ret && $ret.isSuccess) {

            func_course_start();          //重整頁面
        }
    }
};

var admin_course_member_not_enable = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert(langTrans('msg_100015')); return;
    }

    if (confirm(langTrans('q_100003'))) {
        var input = { ids: ids, is_enabled: 0 };
        var $ret = await ajax_post3('Course', 'CourseMemberEnableChange', input, null);
        if ($ret && $ret.isSuccess) {

            func_course_start();          //重整頁面
        }
    }
};



var admin_course_member_export = async function () {

    var req = '';

    var course_id = getReq('course_id');
    if (course_id) {
        req += '?'; req += 'course_id=' + course_id;
    }

    location = '/Course/ExportMemberData' + req;
};


var calendar_redirect = function (y, m) {
    if (m == 0) m = 12;
    var new_req = 'ym=' + y + '/' + m;
    var href = location.href;
    if (href.indexOf('ym=') > -1) {
        href = href.replace('ym=' + getReq('ym'), new_req);
    }
    else {
        if (href.indexOf('?') > -1) href += '&' + new_req;
        else { href += '?' + new_req; }
    }

    location = href;
};




var course_search_redirect = function () {
    var p = [];
    var form_name = 'course_search';
    var form = $('#' + form_name);
    
    var ckMap = new Map(); alert_push2(form_name, ckMap);
    ckMap = form_validate(form_name, 'date_order');
    if (ckMap.size == 0) {

        ckMap = form_validate(form_name, 'date_start');         //開始時間篩選
        if (ckMap.size == 0) {
            var date_start = form.find('#start-year').val() + '/' + form.find('#start-month').val() + '/' + form.find('#start-date').val();
            if (date_start) p.push({ name: 'date_start', v: date_start });
        }
        ckMap = form_validate(form_name, 'date_end');           //結束時間篩選
        if (ckMap.size == 0) {
            var date_end = form.find('#end-year').val() + '/' + form.find('#end-month').val() + '/' + form.find('#end-date').val();
            if (date_end) p.push({ name: 'date_end', v: date_end });
        }

        var keyword = form.find("#keyword").length == 1 ? form.find("#keyword").val().trim() : '';      //關鍵字篩選
        if (keyword) p.push({ name: 'search_keyword', v: keyword });

        var is_recruiting = form.find('[name=recurit][value=recruiting]:checked').length;
        p.push({ name: 'is_recruiting', v: is_recruiting });
        var is_unopened = form.find('[name=recurit][value=unopened]:checked').length;
        p.push({ name: 'is_unopened', v: is_unopened });
        var is_closed = form.find('[name=recurit][value=closed]:checked').length;
        p.push({ name: 'is_closed', v: is_closed });

        location = redirectAssistant(location.pathname, p);
    }
    else {
        alert(langTrans(ckMap.get('date_order')[0]));
    }
};


//搜尋值填入
var course_search_value_push = function () {

    var form = $('#course_search');

    form.find('#keyword').val(getReq('search_keyword'));
    var date_start = getReq('date_start');
    if (date_start) {
        var date_start_split = date_start.split('/');
        if (date_start_split.length == 3) {
            form.find('#start-year').val(date_start_split[0]);
            form.find('#start-month').val(date_start_split[1]); form.find('#start-month').click();
            form.find('#start-date').val(date_start_split[2]);
        }
    }
    var date_end = getReq('date_end');
    if (date_end) {
        var date_end_split = date_end.split('/');
        if (date_end_split.length == 3) {
            form.find('#end-year').val(date_end_split[0]);
            form.find('#end-month').val(date_end_split[1]); form.find('#end-month').click();
            form.find('#end-date').val(date_end_split[2]);
        }
    }

    //var checked = true;
    var is_recruiting = getReq('is_recruiting');
    form.find('[name=recurit][value=recruiting]').attr('checked', (is_recruiting && is_recruiting == 0) ? false : true);
    //if (!is_recruiting) form.find('[name=recurit][value=recruiting]').attr('checked', false);
    var is_unopened = getReq('is_unopened');
    //if (is_unopened && is_unopened == 1) form.find('[name=recurit][value=unopened]').attr('checked', 'checked'); else form.find('[name=recurit][value=unopened]').attr('checked', false);
    form.find('[name=recurit][value=unopened]').attr('checked', (is_unopened && is_unopened == 0) ? false : true);
    var is_closed = getReq('is_closed');
    //if (is_closed && is_closed == 1) form.find('[name=recurit][value=closed]').attr('checked', 'checked'); else form.find('[name=recurit][value=closed]').attr('checked', false);
    form.find('[name=recurit][value=closed]').attr('checked', (is_closed && is_closed == 0) ? false : true);
};




var sign_up_time_countdown = function (id) {
    
    var refreshIntervalId = setInterval(async function () {             //跟server要目前離截止日期還剩多久時間(秒)

        var $ret = await ajax_post2('Course', 'CourseSignUpDeadlineLeaveTime', { id: id }, null);          //要報名截止剩餘秒數
        if ($ret && $ret.data1 && $ret.data1 != 0) {
            var totalSec = $ret.data1.toString().split('.')[0]
            var day = Math.floor(totalSec / 3600 / 24);
            var hour = Math.floor((totalSec - (day * 24 * 3600)) / 3600);
            var min = Math.floor((totalSec - (day * 24 * 3600) - (hour * 3600)) / 60);
            var sec = totalSec - (day * 24 * 3600) - (hour * 3600) - (min * 60);

            //console.log(day + '天' + hour + '時' + min + '分' + sec + '秒');
            var info =
                day + ' ' + langTrans('common-date-day-label-name') + ' ' +
                hour + ' ' + langTrans('common-time-hour-label-name') + ' ' +
                min + ' ' + langTrans('common-time-minute-label-name') + ' ' +
                sec + ' ' + langTrans('common-time-second-label-name');
            $('#course_form').find('#deadline_leave_time').html(info);

            //clearInterval(refreshIntervalId);         //stop interval
        }
    }, 950);

};







func_course_start();







