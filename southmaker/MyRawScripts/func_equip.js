﻿

//主流程
var func_equip_start = async function () {




    var pp = GetPageParam();    //設定page參數
    cl('func_equip_start . GetPageParam', pp);

    if (pp && pp.unit) {

        page_title_controller(pp);            //設定頁面標題

        

        //equip頁面流程開始
        if (pp.unit == 'equip') {

            var q = await QueryObjInit('equip');
            q = paramController(q);


            if (pp.end == 'front') {

                if (q.equip_cate == 0) {
                    if ($('#cate_id').length == 1) q.equip_cate = $('#cate_id').val();
                }

                q.is_enabled = 1;
                q.pageSize = 6;
                var $ret = await ajax_post2('Equip', 'EquipSearch', q, pp);          //抓資料
                cl('EquipSearch . $ret', $ret);

                var html = "";
                if ($ret && $ret.hasData) {
                    $ret.data1 = await translate_multi(pp, $ret.data1);     //translate

                    for (var i = 0; i < $ret.data1.length; i++) {

                        var item = $ret.data1[i];
                        html += "<div class='col-sm-4 grid-item grid-item-space'>";
                        var logo_img_url = default_img_url;
                        var files = await fileReload(item.file_id); cl('files', files);
                        if (files && files.length > 0) {
                            //files = files.filter(x => x.tag == 'equip' && x.img_size == 'S');
                            //if (files.length > 0) logo_img_url = files[0].path;
                            var findObjs = files.filter(x => x.tag == 'equip' && x.img_size == 'S');
                            if (findObjs.length > 0) logo_img_url = findObjs[0].path;
                            else {
                                var findObjs2 = files.filter(x => x.tag == '' && x.class == '');
                                if (findObjs2.length > 0) logo_img_url = findObjs2[0].name;
                            }
                        }
                        html += "<img class='image-responsive' style='height:231px' src= '" + logo_img_url + "' alt= '" + item.title + "' >";
                        html += "<div class='space-grid'>";
                        html += "<h3>" + item.title + "</h3>";
                        html += "<p>" + item.content.replaceAll('\\r\\n', '') + "</p>";
                        html += "</div></div>";
                    }
                    $("#data-content").html(html);          //填資料
                    pagerHelper($ret, pp);                  //分頁相關的處理
                }
                equip_search_value_push();
            }


            if (pp.end == 'back') {                             //後台流程

                var $ret = await ajax_post2('Equip', 'EquipSearch', q, pp);          //抓資料
                cl('EquipSearch . $ret', $ret);

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;

                        //抓出equip_cate的所有資料
                        var cates = [];
                        var cate_Q = await QueryObjInit('equip_cate');
                        var cateRet = await ajax_post2('Equip', 'EquipCateSearch', cate_Q, pp);
                        if (cateRet && cateRet.hasData) cates = cateRet.data1;

                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;

                            var cate = cates.filter(x => x.id == list[i].equip_cate);           //串出equip_cate_name
                            if (cate.length > 0) list[i].equip_cate_name = cate[0].title;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    equip_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    await equip_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            equip_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //equip頁面流程結束


        //equip_cate頁面流程開始
        if (pp.unit == 'equip_cate') {

            var q = await QueryObjInit('equip_cate');
            q = paramController(q);
            var $ret = await ajax_post2('Equip', 'EquipCateSearch', q, pp);          //抓資料
            cl('EquipCateSearch . $ret', $ret);


            if (pp.end == 'back') {                             //後台流程

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    equip_cate_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    equip_cate_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            equip_cate_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //equip_cate頁面流程結束


        


    }
};



//管理者端 equip 保存
var admin_equip_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('equip_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = equip_form_pull(pp);
        ckMap = admin_equip_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = equip_row_pull(trigger, pp);
        //ckMap = admin_equip_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_equip_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Equip', 'EquipSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            var $file_ret = await file_command_execute('equip_form');
            if ($file_ret && $file_ret.isSuccess) {
                await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
                location = '/Admin/Equip';
            }
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_equip_start();          //重整頁面
        }
    }
};


//管理者端 equip_cate 保存
var admin_equip_cate_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('equip_cate_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = equip_cate_form_pull(pp);
        ckMap = admin_equip_cate_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = equip_cate_row_pull(trigger, pp);
        //ckMap = admin_equip_cate_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_equip_cate_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Equip', 'EquipCateSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
            location = '/Admin/EquipCate';
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_equip_start();          //重整頁面
        }
    }
};












var admin_equip_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Equip', 'EquipRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_equip_start();          //重整頁面
        }
    }
};

var admin_equip_cate_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Equip', 'EquipCateRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_equip_start();          //重整頁面
        }
    }
};



var equip_search_redirect = function () {
    var p = [];
    var form_name = 'equip_search';
    var form = $('#' + form_name);

    var equip_cate = getReq('equip_cate') ? getReq('equip_cate') : '';      //設備類別編號
    if (equip_cate) p.push({ name: 'equip_cate', v: equip_cate });

    var keyword = form.find("#keyword").length == 1 ? form.find("#keyword").val().trim() : '';      //關鍵字篩選
    if (keyword) p.push({ name: 'search_keyword', v: keyword });

    location = redirectAssistant(location.pathname, p);
};


//搜尋值填入
var equip_search_value_push = function () {

    var form = $('#equip_search');
    form.find('#keyword').val(getReq('search_keyword'));
};










func_equip_start();







