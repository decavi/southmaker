﻿

//主流程
var func_home_start = async function () {


    var pp = GetPageParam();    //設定page參數
    cl('func_home_start . GetPageParam', pp);

    if (pp && pp.unit) {

        page_title_controller(pp);            //設定頁面標題


        if (pp.unit == 'search') {
            if (pp.end == 'front') {

                var q = await QueryObjInit('');
                q = paramController(q);
                var $ret = await ajax_post2('Home', 'GlobalSearch', q, pp);          //抓資料
                cl('GlobalSearch . $ret', $ret);
                var list = [];
                if ($ret && $ret.hasData) {
                    $ret.data1 = await translate_multi(pp, $ret.data1);     //translate
                    list = $ret.data1;
                    //for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                    //    var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                    //    list[i].has_en_us = langRet.hasData;
                    //}
                    pagerHelper($ret, pp);                  //分頁相關的處理
                }
                search_list_push(list, pp);                  //data row push

                $('[name=search_btn]').each(function () {
                    if ($(this).attr('href').indexOf('javascript') > -1) {
                        $(this).closest('[data-row=db_row]').find('a').removeAttr('href');
                        $(this).hide();
                    }
                });

                global_search_value_push();
            }
        }


        if (pp.unit == 'contact') {

            if (pp.end == 'front') {

                user_contact_update_captcha();
                collect_all_check_input_and_bind_event('contact');
            }
        }

        if (pp.unit == 'index') {

            if (pp.end == 'front') {

                //開始塞最新消息的前6筆資訊
                var NEWS_Q = await QueryObjInit('news');
                NEWS_Q.is_enabled = 1;
                NEWS_Q.pageSize = 6;
                var NEWS_RET = await ajax_post2('News', 'NewsSearch', NEWS_Q, pp); cl('NewsSearch . NEWS_RET', NEWS_RET);
                var NEWS_HTML = "";
                if (NEWS_RET && NEWS_RET.hasData) {
                    $(NEWS_RET.data1).each(function () { this.unit = 'news'; });
                    NEWS_RET.data1 = await translate_multi(pp, NEWS_RET.data1);     //translate

                    var rowItems = [];
                    for (var i = 0; i < NEWS_RET.data1.length; i++) {
                        var _item = NEWS_RET.data1[i];
                        if (rowItems.length < 3) rowItems.push(_item);

                        if (rowItems.length == 3 || i == NEWS_RET.data1.length - 1) {
                            NEWS_HTML += "<div class='row grid'>";
                            for (var ii = 0; ii < rowItems.length; ii++) {
                                var item = rowItems[ii];

                                //var item = NEWS_RET.data1[i];
                                NEWS_HTML += "<div class='col-sm-4 grid-item'>";
                                NEWS_HTML += "<a title='" + item.title + "' href='/News/Detail?id=" + item.id + "'>";

                                NEWS_HTML += "<div class='img-hover'>";
                                var logo_img_url = default_img_url;
                                var files = await fileReload(item.file_id); cl('news files', files);
                                if (files && files.length > 0) {
                                    files = files.filter(x => x.tag == 'news_logo' && x.img_size == 'S');
                                    if (files.length > 0) logo_img_url = files[0].path;
                                }
                                NEWS_HTML += "<img class='image-responsive' style='height:231px' src= '" + logo_img_url + "' alt= '" + item.title + "' >";
                                NEWS_HTML += "<div class='overlay'><div class='text'>" + langTrans('common-go-view-msg') + "</div></div>";
                                NEWS_HTML += "</div>";

                                NEWS_HTML += "<h3>" + item.title + "</h3>";

                                $('#html_peel_area').html(item.content);
                                var peelHtml = $('#html_peel_area').text();
                                var brief = peelHtml.length > 50 ? peelHtml.substr(0, 50) + '...' : peelHtml;
                                NEWS_HTML += "<p class='ellipsis50'>" + brief + "</p>";

                                NEWS_HTML += "</a></div>";
                            }
                            NEWS_HTML += "</div>";
                            rowItems = [];
                        }
                    }
                    $("#news-content").html(NEWS_HTML);          //填資料
                }

                //開始塞課程的前6筆資訊
                var COURSE_Q = await QueryObjInit('course');
                COURSE_Q.is_enabled = 1;
                COURSE_Q.pageSize = 6;
                var COURSE_RET = await ajax_post2('Course', 'CourseSearch', COURSE_Q, pp); cl('CourseSearch . COURSE_RET', COURSE_RET);
                var COURSE_HTML = "";
                if (COURSE_RET && COURSE_RET.hasData) {
                    $(COURSE_RET.data1).each(function () { this.unit = 'course'; });
                    COURSE_RET.data1 = await translate_multi(pp, COURSE_RET.data1);     //translate

                    var rowItems = [];
                    for (var i = 0; i < COURSE_RET.data1.length; i++) {
                        var _item = COURSE_RET.data1[i];
                        if (rowItems.length < 3) rowItems.push(_item);

                        if (rowItems.length == 3 || i == COURSE_RET.data1.length - 1) {
                            COURSE_HTML += "<div class='row grid'>";
                            for (var ii = 0; ii < rowItems.length; ii++){
                                var item = rowItems[ii];

                                COURSE_HTML += "<div class='col-sm-4 grid-item'>";
                                COURSE_HTML += "<a title='" + item.title + "' href='/Course/Detail?id=" + item.id + "'>";

                                COURSE_HTML += "<div class='img-hover'>";
                                var logo_img_url = default_img_url;
                                var files = await fileReload(item.file_id); cl('course files', files);
                                if (files && files.length > 0) {
                                    files = files.filter(x => x.tag == 'course_logo' && x.img_size == 'S');
                                    if (files.length > 0) logo_img_url = files[0].path;
                                }
                                COURSE_HTML += "<img class='image-responsive' style='height:231px' src= '" + logo_img_url + "' alt= '" + item.title + "' >";
                                COURSE_HTML += "<div class='overlay'><div class='text'>" + langTrans('common-go-register-msg') + "</div></div>";
                                COURSE_HTML += "</div>";

                                COURSE_HTML += "<h3>" + item.title + "</h3>";

                                $('#html_peel_area').html(item.content);
                                var peelHtml = $('#html_peel_area').text();
                                var brief = peelHtml.length > 50 ? peelHtml.substr(0, 50) + '...' : peelHtml;
                                COURSE_HTML += "<p class='ellipsis50'>" + brief + "</p>";


                                COURSE_HTML += "</a></div>";
                            }
                            COURSE_HTML += "</div>";
                            rowItems = [];
                        }
                    }
                    $("#course-content").html(COURSE_HTML);          //填資料
                }

                //開始塞作品的前6筆資訊
                var WORKS_Q = await QueryObjInit('works');
                WORKS_Q.is_enabled = 1;
                WORKS_Q.pageSize = 6;
                var WORKS_RET = await ajax_post2('Works', 'WorksSearch', WORKS_Q, pp); cl('WorksSearch . WORKS_RET', WORKS_RET);
                var WORKS_HTML = "";
                if (WORKS_RET && WORKS_RET.hasData) {
                    $(WORKS_RET.data1).each(function () { this.unit = 'works'; });
                    WORKS_RET.data1 = await translate_multi(pp, WORKS_RET.data1);     //translate

                    for (var i = 0; i < WORKS_RET.data1.length; i++) {

                        var _item = WORKS_RET.data1[i];
                        if (rowItems.length < 3) rowItems.push(_item);

                        if (rowItems.length == 3 || i == WORKS_RET.data1.length - 1) {
                            WORKS_HTML += "<div class='row grid'>";
                            for (var ii = 0; ii < rowItems.length; ii++) {
                                var item = rowItems[ii];

                                //var item = WORKS_RET.data1[i];
                                WORKS_HTML += "<div class='col-sm-4 grid-item'>";
                                WORKS_HTML += "<a title='" + item.title + "' href='/Works/Detail?id=" + item.id + "'>";

                                WORKS_HTML += "<div class='img-hover'>";
                                var logo_img_url = default_img_url;
                                var files = await fileReload(item.file_id); cl('works files', files);
                                if (files && files.length > 0) {
                                    files = files.filter(x => x.tag == 'works_cover' && x.img_size == 'S');
                                    if (files.length > 0) logo_img_url = files[0].path;
                                }
                                WORKS_HTML += "<img class='image-responsive' style='height:231px' src= '" + logo_img_url + "' alt= '" + item.title + "' >";
                                WORKS_HTML += "<div class='overlay'><div class='text'>" + langTrans('common-go-view-msg') + "</div></div>";
                                WORKS_HTML += "</div>";

                                WORKS_HTML += "<h3>" + item.title + "</h3>";

                                $('#html_peel_area').html(item.content);
                                var peelHtml = $('#html_peel_area').text();
                                var brief = peelHtml.length > 50 ? peelHtml.substr(0, 50) + '...' : peelHtml;
                                WORKS_HTML += "<p class='ellipsis50'>" + brief + "</p>";

                                WORKS_HTML += "</a></div>";
                            }
                            COURSE_HTML += "</div>";
                            rowItems = [];
                        }
                    }
                    $("#works-content").html(WORKS_HTML);          //填資料
                }

                //20180925 在BANNER的title屬性後面多加"將會開啟新視窗"文字
                $('[data-note=video-link-target-blank-msg]').each(function () {
                    var title = this.getAttribute('title');
                    title = title + '(' + langTrans('video-link-target-blank-msg') + ')';
                    this.setAttribute('title', title);

                    var img = this.querySelector('img');
                    if (img) img.setAttribute('title', title);
                });
            }
        }


        //About頁面流程開始
        if (pp.unit == 'about') {

            var q = await QueryObjInit('about');
            q = paramController(q);
            var $ret = await ajax_post2('Home', 'AboutSearch', q, pp);          //抓資料
            cl('AboutSearch . $ret', $ret);


            if (pp.end == 'back') {                             //後台流程

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length ; i++){         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }

                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    about_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式
                    about_form_init(pp);
                    
                    if (q.id && q.id > 0) {                     //編輯模式

                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            about_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //About頁面流程結束



        //Intro頁面流程開始
        if (pp.unit == 'intro') {

            var q = await QueryObjInit('intro');
            q = paramController(q);
            var $ret = await ajax_post2('Home', 'IntroSearch', q, pp);          //抓資料
            cl('IntroSearch . $ret', $ret);


            if (pp.end == 'back') {                             //後台流程

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    intro_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    intro_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            await intro_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //Intro頁面流程結束


        //MakerCar頁面流程開始
        if (pp.unit == 'maker_car') {


            if (pp.end == 'front') {

                tour_point_search_form_init(pp);        //搜尋表單初始

                //抓巡迴點資料
                var q = await QueryObjInit('tour_point');
                q.is_enabled = 1;
                q.pageSize = 10000; //全撈
                await front_update_tour_point_view(q);
            }

            if (pp.end == 'back') {                             //後台流程

                var q = await QueryObjInit('maker_car');
                q = paramController(q);
                var $ret = await ajax_post2('Home', 'MakerCarSearch', q, pp);          //抓資料
                cl('MakerCarSearch . $ret', $ret);

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    maker_car_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    maker_car_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            maker_car_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //MakerCar頁面流程結束

        //TourPoint頁面流程開始 tour_point
        if (pp.unit == 'tour_point') {

            var q = await QueryObjInit('tour_point');
            q = paramController(q);
            var $ret = await ajax_post2('Home', 'TourPointSearch', q, pp);          //抓資料
            cl('TourPointSearch . $ret', $ret);


            if (pp.end == 'back') {                             //後台流程

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    tour_point_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    tour_point_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            tour_point_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //TourPoint頁面流程結束 tour_point


        //Maker頁面流程開始 maker
        if (pp.unit == 'maker') {

            var q = await QueryObjInit('maker');
            q = paramController(q);
            

            if (pp.end == 'front') {
                q.is_enabled = 1;
                q.pageSize = 6;
                var $ret = await ajax_post2('Home', 'MakerSearch', q, pp);          //抓資料
                cl('MakerSearch . $ret', $ret);

                var html = "";
                if ($ret && $ret.hasData) {
                    $ret.data1 = await translate_multi(pp, $ret.data1);     //translate

                    for (var i = 0; i < $ret.data1.length ; i++) {

                        var item = $ret.data1[i];
                        html += "<div class='col-sm-4 grid-item grid-item-space'>";
                        html += "<a title='" + item.title + "' href='/Home/MakerDetail?id=" + item.id + "'>";

                        var logo_img_url = default_img_url;
                        var files = await fileReload(item.file_id); cl('files', files);
                        if (files && files.length > 0) {
                            files = files.filter(x => x.tag == 'maker_logo' && x.img_size == 'S');
                            if (files.length > 0) logo_img_url = files[0].path;
                        }
                        html += "<img class='image-responsive' style='height:231px' src= '" + logo_img_url + "' alt= '" + item.title + "' ></a>";
                        html += "<div class='space-grid'>";
                        html += "<h3>" + item.title + "</h3>";

                        $('#html_peel_area').html(item.intro);
                        var peelHtml = $('#html_peel_area').text();
                        //var brief = item.intro.length > 30 ? item.intro.substr(0, 30) + '...' : item.intro;
                        var brief = peelHtml.length > 50 ? peelHtml.substr(0, 50) + '...' : peelHtml;
                        html += "<p class='ellipsis50'>" + brief + "</p>";
                        html += "<a class='more' title='" + item.title + "-" + langTrans('home-maker-more-button-name') + "' href='/Home/MakerDetail?id=" + item.id + "'>" + langTrans('home-maker-more-button-name') + "</a>";
                        html += "</div></div>";
                    }
                    $("#data-content").html(html);          //填資料
                    pagerHelper($ret, pp);                  //分頁相關的處理
                }

                home_search_value_push();
            }


            if (pp.end == 'back') {                             //後台流程

                var $ret = await ajax_post2('Home', 'MakerSearch', q, pp);          //抓資料
                cl('MakerSearch . $ret', $ret);
                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    maker_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    maker_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            await maker_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //Maker頁面流程結束 maker


        //AD頁面流程開始
        if (pp.unit == 'ad') {

            var q = await QueryObjInit('ad');
            q = paramController(q);
            var $ret = await ajax_post2('Home', 'ADSearch', q, pp);          //抓資料
            cl('ADSearch . $ret', $ret);


            if (pp.end == 'back') {                             //後台流程

                if (pp.datamode == 'list') {                    //清單模式

                    ad_form_init(pp);
                    ad_form_clear();

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    ad_list_push(list, pp);                  //data row push
                }

            }

        }
        //AD頁面流程結束


    }
};

















//管理者端 about 保存
var admin_about_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('about_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式

        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料

        data = about_form_pull(pp);
        ckMap = admin_about_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = about_row_pull(trigger, pp);
        ckMap = admin_about_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_about_save . data', data);
    
    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Home', 'AboutSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            var $file_ret = await file_command_execute('about_form');
            cl('admin_about_save  file_command_execute()', $file_ret);
            if ($file_ret && $file_ret.isSuccess) {
                await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
                location = '/Admin/About';
            }

            //await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
            //location = '/Admin/About';
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_home_start();          //重整頁面
        }
    }
};



//管理者端 intro 保存
var admin_intro_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('intro_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = intro_form_pull(pp);
        ckMap = admin_intro_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = intro_row_pull(trigger, pp);
        ckMap = admin_intro_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_intro_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Home', 'IntroSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            var $file_ret = await file_command_execute('intro_form');
            cl('admin_intro_save  file_command_execute()', $file_ret);
            if ($file_ret && $file_ret.isSuccess) {
                await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
                location = '/Admin/Intro';
            }
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_home_start();          //重整頁面
        }
    }
};



//管理者端 maker_car 保存
var admin_maker_car_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('maker_car_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = maker_car_form_pull(pp);
        ckMap = admin_maker_car_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = maker_car_row_pull(trigger, pp);
        ckMap = admin_maker_car_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_maker_car_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Home', 'MakerCarSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
            location = '/Admin/MakerCar';
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_home_start();          //重整頁面
        }
    }
};

//管理者端 tour_point 保存
var admin_tour_point_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('tour_point_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = tour_point_form_pull(pp);
        ckMap = admin_tour_point_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = tour_point_row_pull(trigger, pp);
        //ckMap = admin_tour_point_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_tour_point_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Home', 'TourPointSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
            location = '/Admin/TourPoint';
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_home_start();          //重整頁面
        }
    }
};



//管理者端 maker 保存
var admin_maker_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('maker_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = maker_form_pull(pp);
        ckMap = admin_maker_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = maker_row_pull(trigger, pp);
        //ckMap = admin_maker_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_maker_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Home', 'MakerSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            var $file_ret = await file_command_execute('maker_form');
            if ($file_ret && $file_ret.isSuccess) {
                await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
                location = '/Admin/Maker';
            }
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_home_start();          //重整頁面
        }
    }
};


//管理者端 ad 保存
var admin_ad_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('ad_form', ckMap);

    data = ad_form_pull(pp);
    ckMap = admin_ad_form_validate(data, pp);                //form validate

    cl('admin_ad_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Home', 'ADSave', data, pp);
    if ($ret && $ret.isSuccess) {

        $ret = await file_command_execute('ad_form');
        cl('admin_ad_save  file_command_execute(ad_form)', $ret);
        if ($ret && $ret.isSuccess) {
            func_home_start();          //重整頁面
        }
    }
};

//管理者端 列表 ad 更新
var admin_row_ad_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); //alert_push2('ad_form', ckMap);

    data = ad_row_pull(trigger, pp);
    cl('admin_ad_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Home', 'ADSave', data, pp);
    if ($ret && $ret.isSuccess) {

        $ret = await trigger_file_command_execute(trigger);
        cl('admin_row_ad_save  trigger_file_command_execute()', $ret);
        if ($ret && $ret.isSuccess) {
            alert('更新成功');
            //func_home_start();          //重整頁面
        }
    }
};










var admin_about_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Home', 'AboutRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_home_start();          //重整頁面
        }
    }
};


var admin_intro_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Home', 'IntroRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_home_start();          //重整頁面
        }
    }
};


var admin_maker_car_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Home', 'MakerCarRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_home_start();          //重整頁面
        }
    }
};



var admin_tour_point_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Home', 'TourPointRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_home_start();          //重整頁面
        }
    }
};


var admin_maker_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Home', 'MakerRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_home_start();          //重整頁面
        }
    }
};


var admin_ad_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Home', 'ADRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_home_start();          //重整頁面
        }
    }
};



var ad_form_clear = function () {
    
    var form = $('#ad_form');
    if (form.length == 0) return;

    form.find('input[type=text]').each(function () { $(this).val(''); });
    form.find('textarea').each(function () { $(this).val(''); });
    //form.find('select').each(function () { $(this).val($(this).find('option:eq(0)').val()); });
    form.find('[name*=_img_view]').html('');
    form.find('[name=file_command]').each(function () { $(this).data({ on: [], off: [] }); });
};


var contact_mail_submit = async function () {

    var ckMap = form_validate('contact');
    if (ckMap.size > 0) {
        var first_invalid_obj = take_first_invalid_obj('contact');
        if (first_invalid_obj) first_invalid_obj.focus();
        return;
    }

    var form = $('#contact_form');
    var input = {
        name: form.find('#contact_name').val().trim(),
        email: form.find('#contact_email').val().trim(),
        tel: form.find('#contact_telphone').val().trim(),
        title: form.find('#contact_mail_header').val().trim(),
        content: form.find('#contact_message').val().trim(),
        captcha: form.find('#contact_captcha').val().trim(),
    };
    var $ret = await ajax_post3('Home', 'ContactUs', input, null);
    if ($ret && $ret.isSuccess) {
        $('[data-alert=success]').html(langTrans('msg_100013'));
        $('#btnReset').click();
    }
    else {
        ckMap = CovertErrorResultToMap($ret.data2);
        alert_push2('contact_form', ckMap);
    }
    //$(".BDC_ReloadIcon").click();
    user_contact_update_captcha();

    
};





var home_search_redirect = function () {
    var p = [];
    var form_name = 'home_search';
    var form = $('#' + form_name);
    
    var keyword = form.find("#keyword").length == 1 ? form.find("#keyword").val().trim() : '';      //關鍵字篩選
    if (keyword) p.push({ name: 'search_keyword', v: keyword });

    location = redirectAssistant(location.pathname, p);
};


//搜尋值填入
var home_search_value_push = function () {

    var form = $('#home_search');
    form.find('#keyword').val(getReq('search_keyword'));
};




//前台巡迴點資料篩選
var front_tour_point_search_redirect = async function () {

    var q = await QueryObjInit('tour_point');
    q.is_enabled = 1;
    q.pageSize = 10000; //全撈
    
    //var p = [];
    var form_name = 'tour_point_search';
    var form = $('#' + form_name);

    var keyword = form.find("#search").length == 1 ? form.find("#search").val().trim() : '';      //關鍵字篩選
    if (keyword) q.search_keyword = keyword;

    var district = form.find("#district").length == 1 ? form.find("#district").val() : 0;      //地區篩選
    if (district) q.district = district;

    var history_year = form.find("#history").length == 1 ? form.find("#history").val() : 0;      //歷史年分篩選
    if (history_year) q.history_year = history_year;

    var date_start = form.find("#start-date").length == 1 ? form.find("#start-date").val() : '';      //起始篩選日期
    if (date_start) q.date_start = date_start;
    var date_end = form.find("#end-date").length == 1 ? form.find("#end-date").val() : '';      //結束篩選日期
    if (date_end) q.date_end = date_end;

    await front_update_tour_point_view(q);
};

//更新前台巡迴點資料
var front_update_tour_point_view = async function (q) {
    var pp = GetPageParam();

    $('#tour_point_table').find('[data-row=db_row]').each(function () { $(this).remove(); });       //刪除舊有資料

    var $ret = await ajax_post2('Home', 'TourPointSearch', q, null);          //抓資料
    cl('TourPointSearch . $ret', $ret);
    var list = [];
    if ($ret && $ret.hasData) {
        $ret.data1 = await translate_multi(pp, $ret.data1);     //translate
        list = $ret.data1;
        //for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
        //    var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
        //    list[i].has_en_us = langRet.hasData;
        //}
        //pagerHelper($ret, pp);                  //分頁相關的處理
    }
    tour_point_list_push(list, null);                  //data row push
    update_tour_point_map_point(list);                  //google map view update
};


var update_tour_point_map_point = function (list) {
    var pp = GetPageParam();

    var infowindow = new google.maps.InfoWindow();
    var uluru = { lat: 23.4462119, lng: 120.4946172 };                  //預設地圖中心位置

    if (list.length > 0) {                                              //如有資料 則初始地圖中心位置設定為第一筆資料的經緯度
        uluru = { lat: parseFloat(list[0].latitude), lng: parseFloat(list[0].longitude) };                  
    }

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: uluru
    });
    var image = '/Content/Front/assets/img/base/map-icon.png';          //指定座標圖片

    //開始設定座標 / 擺放座標
    $(list).each(function () {

        var address = '';
        if (this.district != 0 && this.address) {
            if (pp.now_lang == 'zh_hant')
                address = GetOptionText('taiwan_district', this.district) + this.address;
            else if (pp.now_lang == 'en_us')
                address = this.address + ', ' + GetOptionText('taiwan_district', this.district) + ', Taiwan (R.O.C.)';
        }

        var marker = new google.maps.Marker({
            position: { lat: parseFloat(this.latitude) , lng: parseFloat(this.longitude) },
            map: map,
            icon: image,
            //html: "<ul><li>日期：2017-06-18</li><li>時間：9:30-16:00</li><li>名稱：嘉義縣客家文化會館</li><li>地址：嘉義縣中埔鄉金蘭村頂山門29號</li><li>課程名稱：3D列印機&數位刻字機教學</li></ul>"
            html: "<ul><li>" + langTrans('tour-point-search-table-date-column-name') + "：" + moment(this.time).format('YYYY-MM-DD') +
            //"</li><li>時間：" + moment(this.time).format('HH:mm') +
            "</li><li>" + langTrans('tour-point-search-table-time-column-name') + "：" + moment(this.time).format('HH:mm') + '-' + moment(this.leave_time).format('HH:mm') +
            "</li><li>" + langTrans('tour-point-search-table-location-column-name') + "：" + this.location +
            //"</li><li>" + langTrans('tour-point-search-table-addrrss-column-name') + "：" + GetOptionText('taiwan_district', this.district) + this.address +
            "</li><li>" + langTrans('tour-point-search-table-addrrss-column-name') + "：" + address +
            "</li><li>" + langTrans('tour-point-search-table-course-name-column-name') + "：" + "<a " + (this.website ? ("href='" + (this.website.indexOf('http') > -1 ? this.website : ("http://" + this.website)) + "'") : "") + " target=_blank >" + this.title + "</a>" + "</li></ul>"
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(this.html);
            infowindow.open(map, this);
        });
    });
    
};




func_home_start();
