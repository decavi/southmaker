﻿

//主流程
var func_member_start = async function () {




    var pp = GetPageParam();    //設定page參數
    cl('func_member_start . GetPageParam', pp);

    if (pp && pp.unit) {

        page_title_controller(pp);            //設定頁面標題

        

        //member頁面流程開始
        if (pp.unit == 'member') {

            if (pp.end == 'front') {

                member_form_init(pp);
                
                if (pp.login_account) {        //有抓到login帳號代表已登入狀態，開始抓會員資料

                    var q = await QueryObjInit('member');
                    q.account = pp.login_account;
                    var $ret = await ajax_post2('Member', 'MemberSearch', q, pp);          //抓資料
                    cl('Member/Edit . $ret', $ret);

                    if ($ret && $ret.hasData) {                     //成功抓到資料就push
                        var memberObj = $ret.data1[0];
                        member_form_push(memberObj, pp, false);
                    }
                }

                collect_all_check_input_and_bind_event('member');       //對會員資料表單的控制項綁定驗證事件

                collect_all_check_input_and_bind_event('password');     //對密碼修改表單的控制項綁定驗證事件
                alert_push2('password_form', new Map());                //隱藏驗證提示
            }


            if (pp.end == 'back') {                             //後台流程

                var q = await QueryObjInit('member');
                q = paramController(q);
                var $ret = await ajax_post2('Member', 'MemberSearch', q, pp);          //抓資料
                cl('MemberSearch . $ret', $ret);

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    member_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    member_form_init(pp);
                    
                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            member_form_push(data, pp, false);
                        }
                    }
                    collect_all_check_input_and_bind_event('member');
                }

            }

        }
        //member頁面流程結束

        //會員課程頁面流程結束
        if (pp.unit == 'member_course' || pp.unit == 'member_survey') {

            if (pp.end == 'front') {

                var q = await QueryObjInit('course_member');        //準備query參數
                q = paramController(q);
                q.member_id = pp.login_member_id;                                       
                var $ret = await ajax_post2('Course', 'CourseMemberSearch', q, pp); cl('CourseMemberSearch . $ret', $ret);      //抓資料

                var course_list = [];
                if ($ret && $ret.hasData) {
                    for (var i = 0; i < $ret.data1.length; i++){

                        $ret.data1[i].course.unit = 'course';
                        $ret.data1[i].course = await translate(pp, $ret.data1[i].course);     //translate
                        //$($ret.data1).each(function () {

                        var extenRet = await ajax_post2('Course', 'CourseExtenSearch', [$ret.data1[i].course.id], null);          //抓課程特別資料
                        if (extenRet && extenRet.hasData) {
                            $ret.data1[i].course.survey_status = extenRet.data1[0].survey_status;
                            $ret.data1[i].course.survey_list = extenRet.data1[0].survey_list;
                        }

                        $ret.data1[i].course.member_is_enabled = $ret.data1[i].course_member.is_enabled;
                        course_list.push($ret.data1[i].course);
                        //});
                    }

                    //course_list_push(course_list, pp);
                    //cl('course_list', course_list);
                    pagerHelper($ret, pp);
                }
                course_list_push(course_list, pp);
                cl('course_list', course_list);
                
                member_course_search_value_push();
            }
        }
        //會員課程頁面流程結束


        //會員登入/成為新會員頁面流程開始
        if (pp.unit == 'login') {

            if (pp.end == 'front') {
                user_login_update_captcha();        //更新captcha圖片

                member_form_init(pp);
                collect_all_check_input_and_bind_event('login');        //綁定登入表單所有控制項的驗證事件
                alert_push2('login_form', new Map());

                collect_all_check_input_and_bind_event('member');       //綁定註冊表單所有控制項的驗證事件
            }
        }
        //會員登入/成為新會員頁面流程結束

        //忘記密碼頁面流程開始
        if (pp.unit == 'forgot') {

            if (pp.end == 'front') {

                collect_all_check_input_and_bind_event('forgot');        //綁定忘記密碼表單所有控制項的驗證事件
                alert_push2('forgot_form', new Map());
                
            }
        }
        //忘記密碼頁面流程結束


        //survey頁面流程開始
        if (pp.unit == 'survey') {

            if (pp.end == 'front') {

                survey_form_init();
            }

        }
        //survey頁面流程結束


        //fb登入成功後授權頁面流程開始
        if (pp.unit == 'fb_auth') {

            var code = '';
            if (location.toString().indexOf('code=') > -1) {
                code = location.toString().split('code=')[1].split('&')[0];
                cl('code', code);
                if (code) {

                    //這邊的URI要改從Server要
                    var uri = await ajax_post2('Member', 'GetAPILoginURI', { tag: 'fb_token', feed: code }, null);
                    //cl('access_token uri', uri);
                    var tokenRet = await fb_ajax_get(uri);
                    cl('fb_ajax_get tokenRet', tokenRet);
                    if (tokenRet) {      //拿到token資料

                        var get_profile_uri = await ajax_post2('Member', 'GetAPILoginURI', { tag: 'fb_profile', feed: tokenRet.access_token }, null);
                        var infoRet = await fb_ajax_get(get_profile_uri);
                        cl('fb_ajax_get infoRet', infoRet);
                        if (infoRet) {

                            var loginRet = await ajax_post3('Member', 'FacebookLogin', infoRet, null);
                            if (loginRet && loginRet.isSuccess) {
                                location = '/Member/Course';                                 //登入成功，導頁至會員中心
                            }
                        }
                    }
                }
            }

        }
        //fb登入成功後授權頁面流程結束


        //google登入成功後授權頁面流程開始
        if (pp.unit == 'google_auth') {

            //alert(location);
            var token = '';
            if (location.toString().indexOf('access_token=') > -1) { 
                token = location.toString().split('access_token=')[1].split('&')[0];

                var get_user_info_url = await ajax_post2('Member', 'GetAPILoginURI', { tag: 'google_userinfo', feed: token }, null);
                //alert(get_user_info_url);
                var ret = await common_ajax_get(get_user_info_url);
                if (ret) {  //成功得到google使用者資料
                    
                    var loginRet = await ajax_post3('Member', 'GoogleLogin', ret, null);
                    if (loginRet && loginRet.isSuccess) {
                        //var userObj = loginRet.data1;
                        location = '/Member/Course';                                 //登入成功，導頁至會員中心
                    }
                }
            }
        }
        //google登入成功後授權頁面流程結束
        


    }
};


var facebook_login = async function () {

    var URI = await ajax_post2('Member', 'GetAPILoginURI', { tag: 'fb_login' }, null);
    if (URI) location = URI;
};

var google_login = async function () {

    var URI = await ajax_post2('Member', 'GetAPILoginURI', { tag: 'google_login' }, null);
    if (URI) location = URI;
};




//管理者端 member 保存
var admin_member_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('member_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式

        data = member_form_pull(pp);
        //ckMap = admin_member_form_validate(data, pp);                //form validate
        ckMap = form_validate('member');
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = member_row_pull(trigger, pp);
        //ckMap = admin_member_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_member_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Member', 'MemberSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            location = '/Admin/Member';
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_member_start();          //重整頁面
        }
    }
};

//會員端 會員資料保存
var user_member_save = async function () {

    var pp = GetPageParam();

    var ckMap = new Map(); alert_push2('member_form', ckMap);
    ckMap = form_validate('member');
    cl('user_member_save ckMap', ckMap);
    if (ckMap.size > 0) return;

    var data = member_form_pull(pp);
    cl('user_member_save . data', data);

    var $ret = await ajax_post3('Member', 'MemberSave', data, pp);
    cl('MemberSave ret', $ret);
    if ($ret && $ret.isSuccess) {

        alert(langTrans('msg_100005'));
    }
};

//會員端 會員密碼修改
var user_password_change = async function () {

    var pp = GetPageParam();

    var ckMap = new Map(); alert_push2('password_form', ckMap);
    ckMap = form_validate('password');
    cl('user_password_change ckMap', ckMap);
    if (ckMap.size > 0) return;

    var form = $('#password_form');
    var data = {
        account: $('#login_account').val(),
        _old: form.find('#password').val(),
        _new: form.find('#new_password').val(),
        mode: 'front',
    };
    cl('user_password_change . data', data);

    var $ret = await ajax_post3('Member', 'PasswordChange', data, pp);
    if ($ret && $ret.isSuccess) {

        password_form_clear();
        alert(langTrans('msg_100016'));
    }
    else {
        cl('user_password_change server ckMap', ckMap);
        ckMap = CovertErrorResultToMap($ret.data2);
        //alert(langTrans('msg_100002'));
        //ckMap.set('fail', [langTrans('msg_100002')]);
        alert_push2('password_form', ckMap);
    }
};

//會員端 會員密碼忘記
var user_password_forgot = async function () {

    var pp = GetPageParam();

    var ckMap = new Map(); alert_push2('forgot_form', ckMap);
    ckMap = form_validate('forgot');
    cl('user_password_forgot ckMap', ckMap);
    if (ckMap.size > 0) return;

    var form = $('#forgot_form');
    var data = {
        account: form.find('#account').val(),
        name: form.find('#name').val(),
        tel: form.find('#tel').val(),
    };
    cl('user_password_forgot . data', data);

    var $ret = await ajax_post3('Member', 'PasswordForgot', data, pp);      //進行忘記密碼驗證
    if ($ret && $ret.isSuccess) {                                           //忘記密碼的驗證成功

        var mailRet = await member_password_forgot_mail(data.account);      //發新密碼信件
        if (mailRet) {                                                      //發信成功
            forgot_form_clear();                                            //清空忘記密碼表單
            $('[data-alert=success]').html(langTrans('msg_100011'));        //顯示成功提示
        }
    }
    else {
        cl('user_password_forgot server ckMap', ckMap);
        ckMap = CovertErrorResultToMap($ret.data2);
        //alert(langTrans('msg_100002'));
        //ckMap.set('fail', [langTrans('msg_100002')]);
        alert_push2('forgot_form', ckMap);
    }
};



var admin_member_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Member', 'MemberRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_member_start();          //重整頁面
        }
    }
};

var member_activate_mail = async function (account_id, email, checkcode) {

    var pp = GetPageParam();

    //var title = '會員帳號驗證確認信';
    var title = langTrans('member-activate-mail-subject');
    //var content = '歡迎加入南方創客基地會員。請點擊連結 [[activate_url]] 以驗證您的會員帳號。';
    var content = langTrans('member-activate-mail-content');

    var SendMailInput = {                                                           //準備發送會員驗證信的參數資料
        lang: '',
        mailTag: '',
        infos: [account_id, email, checkcode, title, content],
    };
    var $ret = await ajax_post3('Shared', 'SendMail', SendMailInput, null);             //發送會員驗證信
    cl('member_activate_mail $ret', $ret);
    if ($ret && $ret.isSuccess) {

        if (pp.end == 'back') alert(langTrans('msg_100017'));
    }
};

var member_password_forgot_mail = async function (account) {
    var isSuccess = false;
    var SendMailInput = {                                                           //準備發送會員驗證信的參數資料
        lang: '', account: account,
    };
    var $ret = await ajax_post3('Shared', 'SendNewPasswordMail', SendMailInput, null);             //發送會員驗證信
    cl('member_password_forgot_mail $ret', $ret);
    if ($ret) isSuccess = $ret.isSuccess;
    return isSuccess;
};



var admin_member_export = function () {

    location = '/Member/ExportMemberData';
};

var member_export_example = function () {

    location = '/Member/ExportMemberData?exportExample=true';
};




//user端 問卷送出
var survey_submit = async function () {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('survey', ckMap);
    ckMap = user_survey_form_validate(data, pp);                    //form validate
    
    data = survey_form_pull(pp);
    data.member_id = pp.login_member_id;
    data.course_id = getReq('course_id');                                //為了測試，暫時固定ID
    cl('survey_submit . data', data);

    if (ckMap.size > 0) { $('#btnSubmit').click(); return; }            //觸發必填提示並離開

    var $ret = await ajax_post3('Member', 'SurveySave', data, pp);
    if ($ret && $ret.isSuccess) {
        //$('#btnSubmit').click();                //觸發前台成功送出提示
        alert(langTrans('msg_100012'));
        location = '/Member/SurveyList';
    }
};

//user端 註冊新會員
var register_submit = async function () {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map();
    ckMap = form_validate('member');                //form validate
    cl('register_submit client ckMap', ckMap);
    if (ckMap.size > 0) {
        var first_invalid_obj = take_first_invalid_obj('member');
        if (first_invalid_obj) first_invalid_obj.focus();
        return;
    }            //離開

    data = member_form_pull(pp);
    cl('register_submit . data', data);

    var $ret = await ajax_post3('Member', 'MemberSave', data, pp);
    if ($ret && $ret.isSuccess) {

        await member_activate_mail('', data.account, data.checkcode);           //寄送帳號驗證信
        member_form_clear();                                                    //表單欄位清空
        $('[data-alert=success]').html(langTrans('msg_100001'));                //前台成功送出提示
    }
    else {
        cl('register_submit server ckMap', ckMap);
        ckMap = CovertErrorResultToMap($ret.data2);
        alert(langTrans('msg_100002'));
        //ckMap.set('fail', [langTrans('msg_100002')]);
        alert_push2('member_form', ckMap);
    }
};

//用戶登入送出表單
var login_submit = async function () {

    var ckMap = new Map();                                     //初始訊息物件
    ckMap = form_validate('login');
    //ckMap = login_validate(data, null);                 //validate

    cl('login_submit ckMap', ckMap);
    if (ckMap.size > 0) {
        var first_invalid_obj = take_first_invalid_obj('login');
        if (first_invalid_obj) first_invalid_obj.focus();
        return;
    }                 //驗證有誤 離開

    var form = $('#login_form');
    var input = {
        account: form.find('#account').val(),
        pwd: form.find('#password').val(),
        captcha: form.find('#captcha').val(),
    };

    var $ret = await ajax_post3('Member', 'Login', input, null);               //建立機構用戶正式資料
    cl('login_submit . Login . $ret', $ret);
    if ($ret && $ret.isSuccess) {

        var userObj = $ret.data1;
        location = '/Member/Course';                                 //登入成功，導頁至會員中心
    }
    else {                                                          //機構用戶正式資料建立失敗
        ckMap = CovertErrorResultToMap($ret.data2);
        ckMap.set('summary', [langTrans('msg_100002')]);       //資料驗證失敗
        alert_push2('login_form', ckMap);                     //更新訊息顯示
    }

    //$(".BDC_ReloadIcon").click();                                   //更新圖形驗證碼顯示
    user_login_update_captcha();
    //cl('cookie after setting', getCookie('loginemailkeep'));
};


var logout = async function () {            //登出

    if (confirm(langTrans('q_100004')) == false) return;
    var $ret = await ajax_post3('Member', 'Logout', {}, null);
    if ($ret && $ret.isSuccess) { location = '/Home/Index'; }
};


//會員表單的清空按鈕
var member_form_clear = function () {
    var form = $('#member_form');
    if (form.length == 0) return;

    form.find('input[type=text]').each(function () { $(this).val(''); });
    form.find('input[type=date]').each(function () { $(this).val(''); });
    form.find('input[type=email]').each(function () { $(this).val(''); });
    form.find('input[type=password]').each(function () { $(this).val(''); });
    form.find('select').each(function () { $(this).val($(this).find('option:eq(0)').val()); });
};

//密碼修改表單的清空按鈕
var password_form_clear = function () {
    var form = $('#password_form');
    if (form.length == 0) return;
    form.find('input[type=password]').each(function () { $(this).val(''); });
};

//密碼忘記表單的清空按鈕
var forgot_form_clear = function () {
    var form = $('#forgot_form');
    if (form.length == 0) return;
    form.find('input[type=text]').each(function () { $(this).val(''); });
};






var member_course_search_redirect = function () {
    var p = [];
    var form_name = 'member_course_search';
    var form = $('#' + form_name);

    var ckMap = new Map(); alert_push2(form_name, ckMap);       //時間篩選
    ckMap = form_validate(form_name, 'date_start');
    if (ckMap.size == 0) {
        var date_start = form.find('#start-year').val() + '/' + form.find('#start-month').val() + '/' + form.find('#start-date').val();
        if (date_start.indexOf('請選擇') < 0) {
            if (date_start) p.push({ name: 'date', v: date_start });
        }

        var mode = getReq('mode');
        if (mode) p.push({ name: 'mode', v: mode });

        var enabled = form.find("#enabled").length == 1 ? form.find("#enabled").val() : '';      //審核狀況篩選
        if (enabled) p.push({ name: 'enabled', v: enabled });

        var survey_status = form.find("#survey_status").length == 1 ? form.find("#survey_status").val() : '';      //問卷狀況篩選
        if (survey_status) p.push({ name: 'survey_status', v: survey_status });

        var keyword = form.find("#keyword").length == 1 ? form.find("#keyword").val().trim() : '';      //關鍵字篩選
        if (keyword) p.push({ name: 'search_keyword', v: keyword });

        location = redirectAssistant(location.pathname, p);
    }
    else {
        alert(langTrans(ckMap.get('date_start')[0]));
    }

    
};


//搜尋值填入
var member_course_search_value_push = function () {

    var form = $('#member_course_search');

    var date_start = getReq('date');
    if (date_start) {
        var date_start_split = date_start.split('/');
        if (date_start_split.length == 3) {
            form.find('#start-year').val(date_start_split[0]);
            form.find('#start-month').val(date_start_split[1]); form.find('#start-month').click();
            form.find('#start-date').val(date_start_split[2]);
        }
    }

    form.find('#survey_status').val(getReq('survey_status'));
    form.find('#enabled').val(getReq('enabled'));
    form.find('#keyword').val(getReq('search_keyword'));
};









func_member_start();







