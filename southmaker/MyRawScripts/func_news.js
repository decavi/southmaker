﻿

//主流程
var func_news_start = async function () {




    var pp = GetPageParam();    //設定page參數
    cl('func_news_start . GetPageParam', pp);

    if (pp && pp.unit) {

        page_title_controller(pp);            //設定頁面標題

        

        //news頁面流程開始
        if (pp.unit == 'news') {

            var q = await QueryObjInit('news');
            q = paramController(q);
            

            if (pp.end == 'front') {
                q.is_enabled = 1;
                q.pageSize = 6;
                var $ret = await ajax_post2('News', 'NewsSearch', q, pp);          //抓資料
                cl('NewsSearch . $ret', $ret);

                var html = "";
                if ($ret && $ret.hasData) {
                    $ret.data1 = await translate_multi(pp, $ret.data1);     //translate

                    for (var i = 0; i < $ret.data1.length; i++) {
                        
                        var item = $ret.data1[i];
                        html += "<div class='col-sm-4 grid-item grid-item-space'>";
                        html += "<a title='" + item.title + "' href='/News/Detail?id=" + item.id + "'>";

                        var logo_img_url = default_img_url;
                        var files = await fileReload(item.file_id); cl('files', files);
                        if (files && files.length > 0) {
                            //files = files.filter(x => x.tag == 'news_logo' && x.img_size == 'S');
                            var findObjs = files.filter(x => x.tag == 'news_logo' && x.img_size == 'S');
                            if (findObjs.length > 0) logo_img_url = findObjs[0].path;
                            else {
                                var findObjs2 = files.filter(x => x.class == '' && x.tag == ''); cl('findObjs2', findObjs2);
                                if (findObjs2.length > 0) logo_img_url = findObjs2[0].name;
                            }
                        }
                        html += "<img class='image-responsive' style='height:231px' src= '" + logo_img_url + "' alt= '" + item.title + "' ></a>";
                        html += "<div class='space-grid'>";
                        html += "<div class='date'>" + moment(item.date_created).format('MMM. DD . YYYY ') + "</div>";

                        var view = await getDetailView('news', item.id); if (!view) view = 0;
                        html += "<div class='users'><i class='fa fa-users' aria-hidden='true'></i> | " + view + "</div>";
                        html += "<div class='clear'></div>";
                        html += "<h3>" + item.title + "</h3>";

                        $('#html_peel_area').html(item.content);
                        var peelHtml = $('#html_peel_area').text();
                        var brief = peelHtml.length > 50 ? peelHtml.substr(0, 50) + '...' : peelHtml;
                        html += "<p class='ellipsis50'>" + brief + "</p>";

                        html += "<a class='more' title='READ MORE " + item.title + "' href='/News/Detail?id=" + item.id + "'>READ MORE ...</a>";
                        html += "</div></div>";
                    }
                    $("#data-content").html(html);          //填資料
                    pagerHelper($ret, pp);                  //分頁相關的處理
                }
            }


            if (pp.end == 'back') {                             //後台流程

                var $ret = await ajax_post2('News', 'NewsSearch', q, pp);          //抓資料
                cl('NewsSearch . $ret', $ret);
                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    news_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    news_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            news_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //news頁面流程結束


        //album頁面流程開始
        if (pp.unit == 'album') {

            var q = await QueryObjInit('album');
            q = paramController(q);

            if (pp.end == 'front') {
                q.is_enabled = 1;
                q.pageSize = 6;
                var $ret = await ajax_post2('News', 'AlbumSearch', q, pp);          //抓資料
                cl('AlbumSearch . $ret', $ret);

                var html = "";
                if ($ret && $ret.hasData) {
                    $ret.data1 = await translate_multi(pp, $ret.data1);     //translate

                    for (var i = 0; i < $ret.data1.length; i++) {

                        var item = $ret.data1[i];
                        html += "<div class='col-sm-4 grid-item grid-item-space'>";
                        html += "<a title='" + item.title + "' href='/News/Photo?id=" + item.id + "'>";

                        var logo_img_url = default_img_url;
                        var files = await fileReload(item.file_id); cl('files', files);
                        if (files && files.length > 0) {
                            //files = files.filter(x => x.tag == 'album' && x.img_size == 'S');
                            var findObjs = files.filter(x => x.tag == 'album' && x.img_size == 'S');
                            if (findObjs.length > 0) logo_img_url = findObjs[0].path;
                            else {
                                var findObjs2 = files.filter(x => x.class == '' && x.tag == '');
                                if (findObjs2.length > 0) logo_img_url = findObjs2[0].name;
                            }
                        }
                        html += "<img class='image-responsive' style='height:231px' src= '" + logo_img_url + "' alt= '" + item.title + "' ></a>";
                        html += "<div class='space-grid news-photo'>";
                        html += "<div class='date'>" + moment(item.date_created).format('MMM. DD . YYYY ') + "</div>";

                        var view = await getDetailView('album', item.id); if (!view) view = 0;
                        html += "<div class='users'><i class='fa fa-users' aria-hidden='true'></i> | " + view + "</div>";
                        html += "<div class='clear'></div>";
                        html += "<h3>" + item.title + "</h3>";
                        html += "<a class='more' title='READ MORE " + item.title + "' href='/News/Photo?id=" + item.id + "'>READ MORE ...</a>";
                        html += "</div></div>";
                    }
                    $("#data-content").html(html);          //填資料
                    pagerHelper($ret, pp);                  //分頁相關的處理
                }
            }


            if (pp.end == 'back') {                             //後台流程

                var $ret = await ajax_post2('News', 'AlbumSearch', q, pp);          //抓資料
                cl('AlbumSearch . $ret', $ret);
                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    album_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    album_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            album_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //album頁面流程結束


        //video頁面流程開始
        if (pp.unit == 'video') {

            var q = await QueryObjInit('video');
            q = paramController(q);

            if (pp.end == 'front') {
                q.is_enabled = 1;
                q.pageSize = 6;
                var $ret = await ajax_post2('News', 'VideoSearch', q, pp);          //抓資料
                cl('VideoSearch . $ret', $ret);

                var html = "";
                if ($ret && $ret.hasData) {
                    $ret.data1 = await translate_multi(pp, $ret.data1);     //translate

                    for (var i = 0; i < $ret.data1.length; i++) {

                        var item = $ret.data1[i];
                        html += "<div class='col-sm-4 grid-item grid-item-space'>";
                        //html += "<iframe width='370' height='231' src='" + item.url + "' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>";

                        var ytUrl = item.url.indexOf('http') > -1 ? item.url : "http://" + item.url;
                        html += "<a title='" + item.title + "(" + langTrans('video-link-target-blank-msg') + ")' href='" + item.url + "' onclick='saveDetailViewFromJS(\"video\", " + item.id + "); ' target='_blank'>";
                        html += "<div class='play'>";
                        var ytCode = item.url.indexOf('?v=') > -1 ? item.url.split('?v=')[1].split('&')[0] : '';
                        html += "<img class='image-responsive' src='https://img.youtube.com/vi/" + ytCode + "/0.jpg' height=230px alt='" + item.title + "'>";
                        html += "<div class='circle-play'><i class='fa fa-play' aria-hidden='true'></i></div>"
                        html += "</div>";
                        html += "</a>";

                        html += "<div class='space-grid news-videos'>";
                        html += "<div class='date'>" + moment(item.date_created).format('MMM. DD . YYYY ') + "</div>";

                        var view = await getDetailView('video', item.id); if (!view) view = 0;
                        html += "<div class='users'><i class='fa fa-users' aria-hidden='true'></i> | " + view + "</div>";
                        html += "<div class='clear'></div>";
                        html += "<h3>" + item.title + "</h3>";
                        html += "</div></div>";
                    }
                    $("#data-content").html(html);          //填資料
                    pagerHelper($ret, pp);                  //分頁相關的處理
                }
            }


            if (pp.end == 'back') {                             //後台流程

                var $ret = await ajax_post2('News', 'VideoSearch', q, pp);          //抓資料
                cl('VideoSearch . $ret', $ret);
                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    video_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    video_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            video_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //video頁面流程結束

        


    }
};



//管理者端 news 保存
var admin_news_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('news_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = news_form_pull(pp);
        ckMap = admin_news_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = news_row_pull(trigger, pp);
        //ckMap = admin_news_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_news_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('News', 'NewsSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            var $file_ret = await file_command_execute('news_form');
            if ($file_ret && $file_ret.isSuccess) {
                await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
                location = '/Admin/News';
            }
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_news_start();          //重整頁面
        }
    }
};


//管理者端 album 保存
var admin_album_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('album_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = album_form_pull(pp);
        ckMap = admin_album_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = album_row_pull(trigger, pp);
        //ckMap = admin_album_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_album_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('News', 'AlbumSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            var $file_ret = await file_command_execute('album_form');       //設定檔案的啟用開關
            if ($file_ret && $file_ret.isSuccess) {
                await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料

                //設定檔案的note欄位
                await file_note_save();
                //設定檔案的外文資料

                location = '/Admin/Album';
            }
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_news_start();          //重整頁面
        }
    }
};

//管理者端 video 保存
var admin_video_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('video_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = video_form_pull(pp);
        ckMap = admin_video_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = video_row_pull(trigger, pp);
        //ckMap = admin_video_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_video_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('News', 'VideoSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
            location = '/Admin/Video';
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_news_start();          //重整頁面
        }
    }
};











var admin_news_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('News', 'NewsRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_news_start();          //重整頁面
        }
    }
};

var admin_album_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('News', 'AlbumRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_news_start();          //重整頁面
        }
    }
};

var admin_video_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('News', 'VideoRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_news_start();          //重整頁面
        }
    }
};










func_news_start();
