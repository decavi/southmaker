﻿

//主流程
var func_works_start = async function () {




    var pp = GetPageParam();    //設定page參數
    cl('func_works_start . GetPageParam', pp);

    if (pp && pp.unit) {

        page_title_controller(pp);            //設定頁面標題

        

        //works頁面流程開始
        if (pp.unit == 'works') {

            var q = await QueryObjInit('works');
            q = paramController(q);

            if (pp.end == 'front') {
                q.is_enabled = 1;
                q.pageSize = 6;
                var $ret = await ajax_post2('Works', 'WorksSearch', q, pp);          //抓資料
                cl('WorksSearch . $ret', $ret);

                var html = "";
                if ($ret && $ret.hasData) {
                    $ret.data1 = await translate_multi(pp, $ret.data1);     //translate

                    for (var i = 0; i < $ret.data1.length; i++) {

                        var item = $ret.data1[i];
                        html += "<div class='col-sm-4 grid-item grid-item-space'>";
                        html += "<a title='" + item.title + "' href='/Works/Detail?id=" + item.id + "'>";

                        var logo_img_url = default_img_url;
                        var files = await fileReload(item.file_id); cl('files', files);
                        if (files && files.length > 0) {
                            files = files.filter(x => x.tag == 'works_cover' && x.img_size == 'S');
                            if (files.length > 0) logo_img_url = files[0].path;
                        }
                        html += "<img class='image-responsive' style='height:231px' src= '" + logo_img_url + "' alt= '" + item.title + "' ></a>";
                        html += "<div class='space-grid'>";
                        html += "<h3>" + item.title + "</h3>";

                        $('#html_peel_area').html(item.content);
                        var peelHtml = $('#html_peel_area').text();
                        var brief = peelHtml.length > 50 ? peelHtml.substr(0, 50) + '...' : peelHtml;
                        html += "<p class='ellipsis50'>" + brief + "</p>";

                        html += "<a class='more' title='" + langTrans('works-list-more-button-name') + "' href='/Works/Detail?id=" + item.id + "'>" + langTrans('works-list-more-button-name') + "</a>";
                        html += "</div></div>";
                    }
                    $("#data-content").html(html);          //填資料
                    pagerHelper($ret, pp);                  //分頁相關的處理
                }
                works_search_value_push();

                //$('title').html('yee');
                //alert($('.breadcrumb').html());
            }


            if (pp.end == 'back') {                             //後台流程

                var $ret = await ajax_post2('Works', 'WorksSearch', q, pp);          //抓資料
                cl('WorksSearch . $ret', $ret);

                if (pp.datamode == 'list') {                    //清單模式

                    var list = [];
                    if ($ret && $ret.hasData) {
                        list = $ret.data1;
                        for (var i = 0; i < list.length; i++) {         //檢查每筆資料是否有英文版
                            var langRet = await ajax_post2('Shared', 'HasLangData', { unit: pp.unit, obj_id: list[i].id, langType: 'en_us' }, pp);
                            list[i].has_en_us = langRet.hasData;
                        }
                        pagerHelper($ret, pp);                  //分頁相關的處理
                    }
                    works_list_push(list, pp);                  //data row push
                }

                if (pp.datamode == 'edit') {                    //資料 新增 或 編輯模式

                    works_form_init(pp);

                    if (q.id && q.id > 0) {                     //編輯模式
                        if ($ret && $ret.hasData) {
                            var data = $ret.data1[0];
                            data = await lang_obj_load(pp.unit, q.id, data, pp);            //抓語言資料
                            works_form_push(data, pp, false);
                        }
                    }
                }

            }

        }
        //works頁面流程結束

        


    }
};



//管理者端 works 保存
var admin_works_save = async function (trigger) {

    var pp = GetPageParam();

    var data = '';
    var ckMap = new Map(); alert_push2('works_form', ckMap);

    if (pp.datamode == 'edit') {                    //在完整表單觸發更新的模式
        $('#lang_switch').val('zh_hant'); $('#lang_switch').change();           //切換回中文版表單資料
        data = works_form_pull(pp);
        ckMap = admin_works_form_validate(data, pp);                //form validate
    }
    if (pp.datamode == 'list') {                    //在列表直接觸發更新的模式

        if (!trigger) return;
        data = works_row_pull(trigger, pp);
        //ckMap = admin_works_row_validate(trigger, data, pp);        //row validate
    }
    cl('admin_works_save . data', data);

    if (ckMap.size > 0) return;

    var $ret = await ajax_post3('Works', 'WorksSave', data, pp);
    if ($ret && $ret.isSuccess) {

        if (pp.datamode == 'edit') {
            var $file_ret = await file_command_execute('works_form');
            if ($file_ret && $file_ret.isSuccess) {
                await lang_obj_save(data, $ret.data1.id);           //主資料保存成功，接著保存語言資料
                location = '/Admin/Works';
            }
        }
        if (pp.datamode == 'list') {
            alert('更新成功');
            func_work_start();          //重整頁面
        }
    }
};







var admin_works_remove = async function (id) {

    var ids = [];
    if (id) {
        ids.push(id);
    }
    else {
        $("[name=ckb_delete]:checked").each(function () { ids.push($(this).attr('data-id')); });
    }
    if (ids.length < 1) {
        alert('請先勾選要刪除的項目'); return;
    }

    if (confirm('確定刪除?')) {
        var input = { ids: ids, };
        var $ret = await ajax_post3('Works', 'WorksRemove', input, null);
        if ($ret && $ret.isSuccess) {

            func_works_start();          //重整頁面
        }
    }
};




var works_search_redirect = function () {
    var p = [];
    var form_name = 'works_search';
    var form = $('#' + form_name);
    
    var keyword = form.find("#keyword").length == 1 ? form.find("#keyword").val().trim() : '';      //關鍵字篩選
    if (keyword) p.push({ name: 'search_keyword', v: keyword });

    location = redirectAssistant(location.pathname, p);
};


//搜尋值填入
var works_search_value_push = function () {

    var form = $('#works_search');
    form.find('#keyword').val(getReq('search_keyword'));
};






func_works_start();







