﻿


//抓about表單欄位的資料
var about_form_pull = function (pp) {
    var table = $('#about_form');
    var tags = ['title', 'keyword', 'description', 'content', 'sort'];
    return common_data_pull(table, tags);
};

//抓about列表欄位上的資料
var about_row_pull = function (trigger, pp) {
    var row = $(trigger).closest('[data-row=db_row]');
    var tags = ['title', 'sort'];
    return common_data_pull(row, tags);
};


//抓intro表單欄位的資料
var intro_form_pull = function (pp) {
    var table = $('#intro_form');
    var tags = ['title', 'keyword', 'description', 'content', 'sort'];
    return common_data_pull(table, tags);
};

//抓intro列表欄位上的資料
var intro_row_pull = function (trigger, pp) {
    var row = $(trigger).closest('[data-row=db_row]');
    var tags = ['title', 'sort'];
    return common_data_pull(row, tags);
};


//抓maker_car表單欄位的資料
var maker_car_form_pull = function (pp) {
    var table = $('#maker_car_form');
    var tags = ['title', 'keyword', 'description', 'content', 'sort'];
    return common_data_pull(table, tags);
};

//抓maker_car列表欄位上的資料
var maker_car_row_pull = function (trigger, pp) {
    var row = $(trigger).closest('[data-row=db_row]');
    var tags = ['title', 'sort'];
    return common_data_pull(row, tags);
};


//抓tour_point表單欄位的資料
var tour_point_form_pull = function (pp) {
    var table = $('#tour_point_form');
    var tags = ['title', 'location', 'district', 'address', 'latitude', 'longitude', 'time', 'leave_time', 'website', 'date_start', 'date_end', 'is_enabled', 'is_force_post'];
    return common_data_pull(table, tags);
};

//抓tour_point列表欄位上的資料(不完整)
var tour_point_row_pull = function (trigger, pp) {
    var row = $(trigger).closest('[data-row=db_row]');
    var tags = ['title', 'sort'];
    return common_data_pull(row, tags);
};



//抓maker表單欄位的資料
var maker_form_pull = function (pp) {
    var table = $('#maker_form');
    var tags = ['title', 'tel', 'email', 'website', 'member', 'exp', 'intro', 'sort', 'is_enabled'];
    return common_data_pull(table, tags);
};


//抓course表單欄位的資料
var course_form_pull = function (pp) {
    var table = $('#course_form');
    var tags = ['title', 'location', 'district', 'address', 'time', 'day_type', 'website', 'content', 'date_start', 'date_end', 'seat', 'sort', 'is_force_post', 'is_enabled'];
    return common_data_pull(table, tags);
};

//抓news表單欄位的資料
var news_form_pull = function (pp) {
    var table = $('#news_form');
    var tags = ['title', 'content', 'date_start', 'date_end', 'sort', 'is_force_post', 'is_enabled'];
    return common_data_pull(table, tags);
};

//抓album表單欄位的資料
var album_form_pull = function (pp) {
    var table = $('#album_form');
    var tags = ['title', 'date_start', 'date_end', 'is_force_post', 'is_enabled'];
    return common_data_pull(table, tags);
};

//抓video表單欄位的資料
var video_form_pull = function (pp) {
    var table = $('#video_form');
    var tags = ['title', 'url', 'date_start', 'date_end', 'is_force_post', 'is_enabled'];
    return common_data_pull(table, tags);
};

//抓equip表單欄位的資料
var equip_form_pull = function (pp) {
    var table = $('#equip_form');
    var tags = ['title', 'equip_cate', 'content', 'date_start', 'date_end', 'is_force_post', 'is_enabled'];
    return common_data_pull(table, tags);
};

//抓equip_cate表單欄位的資料
var equip_cate_form_pull = function (pp) {
    var table = $('#equip_cate_form');
    var tags = ['title', 'date_start', 'date_end', 'is_force_post', 'is_enabled'];
    return common_data_pull(table, tags);
};

//抓works表單欄位的資料
var works_form_pull = function (pp) {
    var table = $('#works_form');
    var tags = ['title', 'group', 'tool', 'content', 'date_start', 'date_end', 'is_force_post', 'is_enabled'];
    return common_data_pull(table, tags);
};

//抓member表單欄位的資料
var member_form_pull = function (pp) {
    var table = $('#member_form');
    var tags = [
        'account', 'password', 'checkcode', 'name', 'gender', 'birth', 'occupation', 'school', 'company', 'degree',
        'member_title', 'department', 'tel', 'email', 'member_location', 'motive', 'source', 'employ_status', 'is_enabled',
        'password',
    ];
    return common_data_pull(table, tags);
};

//抓course_member表單欄位的資料
var course_member_form_pull = function (pp) {
    var table = $('#course_member_form');
    var tags = [ 'course_member_is_enabled' ];
    return common_data_pull(table, tags);
};


//抓ad表單欄位的資料
var ad_form_pull = function (pp) {
    var table = $('#ad_form');
    var tags = ['title', 'ad_url', 'ad_content', 'form_target', 'form_is_enabled'];
    return common_data_pull(table, tags);
};

//抓ad列表欄位上的資料
var ad_row_pull = function (trigger, pp) {
    var row = $(trigger).closest('[data-row=db_row]');
    var tags = ['title', 'ad_url', 'ad_content', 'row_target', 'row_is_enabled', 'sort'];
    return common_data_pull(row, tags);
};



//抓survey表單欄位的資料
var survey_form_pull = function (pp) {
    var table = $('#survey');
    var tags = ['a1', 'a2', 'a3', 'a4', 'a5', 'f1', 'f2', 'f3', 'f4', 'f5'];
    return common_data_pull(table, tags);
};



var common_data_pull = function (form, tags) {

    var data = form.data();

    if (!form || !tags || tags.length == 0 || !data) return;      //參數不齊全就直接離開

    $(tags).each(function () {
        var tag = this.toString();

        var colObj = form.find("#" + tag);          //以#...識別欄位物件
        if (colObj.length == 1) {                   //有抓到欄位物件才繼續

            switch (tag) {
                //文字框類
                case 'title': data.title = colObj.val(); break;
                case 'member_title': data.title = colObj.val(); break;
                case 'keyword': data.keyword = colObj.val(); break;
                case 'description': data.description = colObj.val(); break;
                case 'sort': data.sort = colObj.val(); break;
                case 'location': data.location = colObj.val(); break;
                case 'member_location': data.location = colObj.val(); break;
                case 'district': data.district = colObj.val(); break;
                case 'address': data.address = colObj.val(); break;
                case 'latitude': data.latitude = colObj.val(); break;
                case 'longitude': data.longitude = colObj.val(); break;
                case 'website': data.website = colObj.val(); break;
                case 'tel': data.tel = colObj.val(); break;
                case 'email': data.email = colObj.val(); break;
                case 'member': data.member = colObj.val(); break;
                case 'exp': data.exp = colObj.val(); break;
                case 'seat': data.seat = colObj.val(); break;
                case 'url': data.url = colObj.val(); break;
                case 'ad_url': data.url = colObj.val(); break;
                case 'group': data.group = colObj.val(); break;
                case 'tool': data.tool = colObj.val(); break;
                case 'ad_content': data.content = colObj.val(); break;

                case 'account': data.account = colObj.val(); break;
                case 'password': data.password = colObj.val(); break;
                case 'checkcode': data.checkcode = colObj.val(); break;
                case 'name': data.name = colObj.val(); break;
                
                case 'occupation': data.occupation = colObj.val(); break;
                case 'school': data.school = colObj.val(); break;
                case 'company': data.company = colObj.val(); break;
                case 'department': data.department = colObj.val(); break;
                case 'motive': data.motive = colObj.val(); break;

                case 'f3': data.f3 = colObj.val(); break;
                case 'f4': data.f4 = colObj.val(); break;
                case 'f5': data.f5 = colObj.val(); break;

                //下拉選單類
                case 'equip_cate': data.equip_cate = colObj.val(); break;
                case 'degree': data.degree = colObj.val(); break;
                case 'source': data.source = colObj.val(); break;
                case 'employ_status': data.employ_status = colObj.val(); break;
                case 'course_member_is_enabled': data.course_member_is_enabled = colObj.val(); break;
                case 'day_type': data.day_type = colObj.val(); break;

                case 'f1': data.f1 = colObj.val(); break;
                case 'f2': data.f2 = colObj.val(); break;

                //ckeditor類
                case 'content': data.content = CKEDITOR.instances.content.getData(); break;
                case 'intro': data.intro = CKEDITOR.instances.intro.getData(); break;

                //時間類
                case 'time': data.time = colObj.val(); break;
                case 'leave_time': data.leave_time = colObj.val(); break;
                case 'date_start': data.date_start = colObj.val(); break;
                case 'date_end': data.date_end = colObj.val(); break;
                case 'birth': data.birth = colObj.val(); break;

                //checkbox類
                case 'is_force_post': data.is_force_post = colObj.prop('checked') ? '1' : '0'; break;



                default: break;
            }
        }

        colObj = form.find("[name=" + tag + "]");           //以[name=...]識別欄位物件
        if (colObj.length > 0) {                            //有抓到欄位物件才繼續

            switch (tag) {
                //radio類
                case 'is_enabled': data.is_enabled = form.find("[name=" + tag + "]:checked").val(); break;
                case 'gender': data.gender = form.find("[name=" + tag + "]:checked").val(); break;

                case 'form_target': data.target = form.find("[name=" + tag + "]:checked").val(); break;
                case 'form_is_enabled': data.is_enabled = form.find("[name=" + tag + "]:checked").val(); break;

                case 'a1': data.a1 = form.find("[name=" + tag + "]:checked").val(); break;
                case 'a2': data.a2 = form.find("[name=" + tag + "]:checked").val(); break;
                case 'a3': data.a3 = form.find("[name=" + tag + "]:checked").val(); break;
                case 'a4': data.a4 = form.find("[name=" + tag + "]:checked").val(); break;
                case 'a5': data.a5 = form.find("[name=" + tag + "]:checked").val(); break;

                default: break;
            }
        }

        colObj = form.find("[name*=" + tag + "]");           //以[name*=...]識別欄位物件
        if (colObj.length > 0) {                            //有抓到欄位物件才繼續

            switch (tag) {
                //radio類
                case 'row_target': data.target = form.find("[name*=" + tag + "]:checked").val(); break;
                case 'row_is_enabled': data.is_enabled = form.find("[name*=" + tag + "]:checked").val(); break;

                default: break;
            }
        }

    });
    return data;
};