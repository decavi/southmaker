﻿

//ABOUT資料列欄位填入
var about_list_push = function (list, pp) {

    var tags = ['id', 'title', 'has_en_us'];
    common_list_push('about_table', tags, list);
};


//INTRO資料列欄位填入
var intro_list_push = function (list, pp) {

    var tags = ['id', 'title', 'file', 'sort', 'has_en_us'];
    $(list).each(function () { this.file_tag = 'intro'; });
    common_list_push('intro_table', tags, list);
};

//MAKER CAR資料列欄位填入
var maker_car_list_push = function (list, pp) {

    var tags = ['id', 'title', 'sort', 'has_en_us'];
    common_list_push('maker_car_table', tags, list);
};

//tour_point資料列欄位填入
var tour_point_list_push = function (list, pp) {

    var tags = ['id', 'title', 'address', 'date_start', 'date_end', 'is_enabled', 'has_en_us', 'tour_point_title', 'tour_point_location', 'tour_point_address', 'tour_point_date', 'tour_point_time',];
    common_list_push('tour_point_table', tags, list);
};

//maker資料列欄位填入
var maker_list_push = function (list, pp) {

    var tags = ['id', 'title', 'tel', 'email', 'website', 'is_enabled', 'has_en_us'];
    common_list_push('maker_table', tags, list);
};

//course資料列欄位填入
var course_list_push = function (list, pp) {

    var tags = ['id', 'title', 'address', 'date_start', 'date_end', 'is_enabled', 'survey_count', 'has_en_us', 'course_location', 'course_time', 'day_type', 'day_type_text', 'course_member_is_enabled_html', 'survey_status_html'];
    common_list_push('course_table', tags, list);
};

//news資料列欄位填入
var news_list_push = function (list, pp) {

    var tags = ['id', 'title', 'date_start', 'date_end', 'is_enabled', 'has_en_us'];
    common_list_push('news_table', tags, list);
};

//album資料列欄位填入
var album_list_push = function (list, pp) {

    var tags = ['id', 'title', 'date_start', 'date_end', 'is_enabled', 'has_en_us'];
    common_list_push('album_table', tags, list);
};

//video資料列欄位填入
var video_list_push = function (list, pp) {

    var tags = ['id', 'title', 'url', 'date_start', 'date_end', 'is_enabled', 'has_en_us'];
    common_list_push('video_table', tags, list);
};

//equip資料列欄位填入
var equip_list_push = function (list, pp) {

    var tags = ['id', 'title', 'equip_cate', 'date_start', 'date_end', 'is_enabled', 'has_en_us'];
    common_list_push('equip_table', tags, list);
};

//equip_cate資料列欄位填入
var equip_cate_list_push = function (list, pp) {

    var tags = ['id', 'title', 'date_start', 'date_end', 'is_enabled', 'has_en_us'];
    common_list_push('equip_cate_table', tags, list);
};

//works資料列欄位填入
var works_list_push = function (list, pp) {

    var tags = ['id', 'title', 'group', 'date_start', 'date_end', 'is_enabled', 'file', 'has_en_us'];
    $(list).each(function () { this.file_tag = 'works'; });
    common_list_push('works_table', tags, list);
};

//member資料列欄位填入
var member_list_push = function (list, pp) {

    var tags = ['id', 'company', 'name', 'account', 'email', 'tel', 'member_is_enabled', 'checkcode'];
    common_list_push('member_table', tags, list);
};

//course_member資料列欄位填入
var course_member_list_push = function (list, pp) {
    
    var tags = ['course_member_company', 'course_member_name', 'course_member_account', 'course_member_tel', 'course_member_id', 'course_id', 'course_member_is_enabled'];
    common_list_push('course_member_table', tags, list);
};

//ad資料列欄位填入
var ad_list_push = function (list, pp) {

    var tags = ['index', 'id', 'title', 'ad_url', 'ad_content', 'target', 'is_enabled', 'sort', 'file'];
    $(list).each(function () { this.file_tag = 'ad'; });
    common_list_push('ad_table', tags, list);
};

//survey_member資料列欄位填入
var survey_member_list_push = function (list, pp) {

    var tags = ['survey_id', 'course_member_name', 'survey_date_created'];
    common_list_push('survey_member_table', tags, list);
};


//search資料列欄位填入
var search_list_push = function (list, pp) {

    //var tags = ['id', 'title', 'address', 'date_start', 'date_end', 'is_enabled', 'survey_count', 'has_en_us', 'course_location', 'course_time', 'day_type', 'day_type_text', 'course_member_is_enabled_html', 'survey_status_html'];
    var tags = ['title', 'search_logo', 'search_brief', 'search_date', 'search_url', 'search_target', 'view'];
    common_list_push('search_table', tags, list);
};















//ABOUT資料表單欄位填入
var about_form_push = function (data, pp, isLockForm) {

    var tags = ['file'];                                                      //特殊欄位標記
    data.file_tag = 'about';
    common_form_push('about_form', tags, data, pp, isLockForm);
};

//INTRO資料表單欄位填入
var intro_form_push = async function (data, pp, isLockForm) {

    var tags = ['file'];                                                      //特殊欄位標記
    data.file_tag = 'intro';
    common_form_push('intro_form', tags, data, pp, isLockForm);
};

//maker_car資料表單欄位填入
var maker_car_form_push = function (data, pp, isLockForm) {

    var tags = [];                                                      //特殊欄位標記
    common_form_push('maker_car_form', tags, data, pp, isLockForm);
};

//tour_point資料表單欄位填入
var tour_point_form_push = function (data, pp, isLockForm) {

    var tags = ['time', 'leave_time', 'date_start', 'date_end', 'is_force_post', 'is_enabled'];                 //特殊欄位標記
    common_form_push('tour_point_form', tags, data, pp, isLockForm);
};

//maker資料表單欄位填入
var maker_form_push = async function (data, pp, isLockForm) {

    var tags = ['is_enabled', 'file'];                                              //特殊欄位標記
    data.file_tag = 'maker';
    common_form_push('maker_form', tags, data, pp, isLockForm);
};

//course資料表單欄位填入
var course_form_push = async function (data, pp, isLockForm) {

    var tags = ['time', 'date_start', 'date_end', 'is_force_post', 'is_enabled', 'file'];                 //特殊欄位標記
    data.file_tag = 'course';
    common_form_push('course_form', tags, data, pp, isLockForm);
};

//news資料表單欄位填入
var news_form_push = async function (data, pp, isLockForm) {

    var tags = ['date_start', 'date_end', 'is_force_post', 'is_enabled', 'file'];                 //特殊欄位標記
    data.file_tag = 'news';
    common_form_push('news_form', tags, data, pp, isLockForm);
};

//album資料表單欄位填入
var album_form_push = async function (data, pp, isLockForm) {

    var tags = ['date_start', 'date_end', 'is_force_post', 'is_enabled', 'file'];                 //特殊欄位標記
    data.file_tag = 'album';
    common_form_push('album_form', tags, data, pp, isLockForm);
};

//video資料表單欄位填入
var video_form_push = async function (data, pp, isLockForm) {

    var tags = ['date_start', 'date_end', 'is_force_post', 'is_enabled'];                 //特殊欄位標記
    common_form_push('video_form', tags, data, pp, isLockForm);
};

//equip資料表單欄位填入
var equip_form_push = function (data, pp, isLockForm) {

    var tags = ['date_start', 'date_end', 'is_force_post', 'is_enabled', 'file'];                 //特殊欄位標記
    data.file_tag = 'equip';
    common_form_push('equip_form', tags, data, pp, isLockForm);
};

//equip_cate資料表單欄位填入
var equip_cate_form_push = function (data, pp, isLockForm) {

    var tags = ['date_start', 'date_end', 'is_force_post', 'is_enabled'];                 //特殊欄位標記
    common_form_push('equip_cate_form', tags, data, pp, isLockForm);
};

//works資料表單欄位填入
var works_form_push = async function (data, pp, isLockForm) {

    var tags = ['date_start', 'date_end', 'is_force_post', 'is_enabled', 'file'];                 //特殊欄位標記
    data.file_tag = 'works';
    common_form_push('works_form', tags, data, pp, isLockForm);
};

//member資料表單欄位填入
var member_form_push = async function (data, pp, isLockForm) {
    
    var tags = ['gender', 'birth', 'member_location', 'member_title', 'company-school', 'title-department'];        //特殊欄位標記
    common_form_push('member_form', tags, data, pp, isLockForm);
};

//member資料表單欄位填入
var hide_member_form_push = async function (data, pp, isLockForm) {

    var tags = ['gender', 'birth', 'member_location', 'member_title', 'company-school', 'title-department'];        //特殊欄位標記
    common_form_push('hide_member_form', tags, data, pp, isLockForm);
};

//course_member資料表單欄位填入
var course_member_form_push = async function (data, pp, isLockForm) {

    var tags = ['gender', 'birth', 'member_location', 'member_title'];                                              //特殊欄位標記
    data.member.course_member_is_enabled = data.course_member.is_enabled;
    common_form_push('course_member_form', tags, data.member, pp, true);
};


//survey_detail資料表單欄位填入
var survey_detail_form_push = async function (data, pp, isLockForm) {

    var tags = ['survey_course_title', 'survey_member_name', 'a1', 'a2', 'a3', 'a4', 'a5', 'f1', 'f2', 'f3', 'f4', 'f5'];                  //特殊欄位標記
    common_form_push('survey_detail_form', tags, data, pp, true);
};




















var common_form_push = function (formName, tags, data, pp, isLockForm) {

    if (!formName || !data) return;

    var form = $('#' + formName);           //抓表單物件
    if (form.length < 1) return;            //沒抓到表單物件就離開

    var _data = Object.entries(data);
    $(_data).each(function () {
        if (form.find('#' + this[0]).length == 1) form.find('#' + this[0]).val(this[1]);
        if (this[0] == 'content') { CKEDITOR.instances.content.setData(this[1]); }
        if (this[0] == 'intro') { CKEDITOR.instances.intro.setData(this[1]); }
    });

    //特殊欄位處理
    $(tags).each(function () {

        var tag = this.toString();

        var colObj = form.find("#" + tag);          //以#...識別欄位物件
        if (colObj.length == 1) {                   //有抓到欄位物件才繼續

            switch (tag) {

                case 'time': colObj.val(data.time ? moment(data.time).format('YYYY/MM/DD HH:mm') : ''); break;
                case 'leave_time': colObj.val(data.leave_time ? moment(data.leave_time).format('YYYY/MM/DD HH:mm') : ''); break;
                case 'birth': colObj.val(data.birth ? moment(data.birth).format('YYYY-MM-DD') : ''); break;
                case 'date_start': colObj.val(data.date_start ? moment(data.date_start).format('YYYY/MM/DD HH:mm') : ''); break;
                case 'date_end': colObj.val(data.date_end ? moment(data.date_end).format('YYYY/MM/DD HH:mm') : ''); break;
                case 'is_force_post': if (data.is_force_post == 1) colObj.prop('checked', true); break;

                case 'member_location': colObj.val(data.location); break;
                case 'member_title': colObj.val(data.title); break;

                case 'company-school': colObj.val(data.company + (data.company && data.school ? '/' : '') + data.school); break;
                case 'title-department': colObj.val(data.title + (data.title && data.department ? '/' : '') + data.department); break;

                default: break;
            }
        }


        var colObj = form.find("[name=" + tag + "]");           //以[name=...]識別欄位物件
        if (colObj.length > 0) {                                //有抓到欄位物件才繼續

            switch (tag) {

                case 'is_enabled': form.find("[name=" + tag + "][value=" + data.is_enabled + "]").prop('checked', true); break;
                case 'gender': form.find("[name=" + tag + "][value=" + data.gender + "]").prop('checked', true); break;

                case 'survey_course_title': form.find("[name=" + tag + "]").html(data.course.title); break;
                case 'survey_member_name': form.find("[name=" + tag + "]").html(data.member.name); break;

                case 'a1': colObj.find("[data-val=" + data.survey.a1 + "]").html('V'); break;
                case 'a2': colObj.find("[data-val=" + data.survey.a2 + "]").html('V'); break;
                case 'a3': colObj.find("[data-val=" + data.survey.a3 + "]").html('V'); break;
                case 'a4': colObj.find("[data-val=" + data.survey.a4 + "]").html('V'); break;
                case 'a5': colObj.find("[data-val=" + data.survey.a5 + "]").html('V'); break;
                case 'f1': form.find("[name=" + tag + "]").html(GetOptionText('course_source', data.survey.f1)); break;
                case 'f2': form.find("[name=" + tag + "]").html(GetOptionText('course_join_reason', data.survey.f2)); break;
                case 'f3': form.find("[name=" + tag + "]").html(data.survey.f3); break;
                case 'f4': form.find("[name=" + tag + "]").html(data.survey.f4); break;
                case 'f5': form.find("[name=" + tag + "]").html(data.survey.f5); break;

                default: break;
            }
        }


        if (tag == 'file') {

            file_upload_data_init(form, data.file_id);                //初始檔案上傳的相關資料
            data.files = fileReload(data.file_id, data.file_tag);                    //抓此資料物件的附檔資料
        }

    });

    //是否鎖住表單設定
    if (isLockForm) {
        form.find('input').each(function () { $(this).attr('disabled', ''); });
        form.find('select').each(function () { $(this).attr('disabled', ''); });
        form.find('[nolock]').each(function () { $(this).attr('disabled', false); });
    }


    form.data(data);
};



var common_list_push = function (tableName, tags, list) {
    if (!tableName || !tags || tags.length < 1) return;

    var table = $('#' + tableName);
    if (!table) return;

    //將舊資料列先清空
    table.find('[data-row=db_row]').each(function () { $(this).remove(); });

    if ($('#data_empty_tip').length == 1) $('#data_empty_tip').show();
    table.hide();
    
    if (list.length < 1) return;

    if ($('#data_empty_tip').length == 1) $('#data_empty_tip').hide();
    table.show();

    var tempRow = table.find('[data-row=temp_row]');
    if (!tempRow) return;

    var index = 1;

    $(list).each(function () {
        var newRow = tempRow.clone();
        newRow.show();
        newRow.attr('data-row', 'db_row');

        var rowContent = newRow.html();
        var data = this;

        $(tags).each(function () {

            var tag = this.toString();
            switch (tag) {
                //string類
                case 'index': rowContent = rowContent.replaceAll('{' + tag + '}', index++); break;
                case 'id': rowContent = rowContent.replaceAll('{' + tag + '}', data.id); break;
                case 'title': rowContent = rowContent.replaceAll('{' + tag + '}', data.title); break;
                case 'sort': rowContent = rowContent.replaceAll('{' + tag + '}', data.sort); break;
                case 'address': rowContent = rowContent.replaceAll('{' + tag + '}', data.address); break;
                case 'tel': rowContent = rowContent.replaceAll('{' + tag + '}', data.tel); break;
                case 'email': rowContent = rowContent.replaceAll('{' + tag + '}', data.email); break;
                case 'website': rowContent = rowContent.replaceAll('{' + tag + '}', data.website); break;
                case 'is_enabled': rowContent = rowContent.replaceAll('{' + tag + '}', data.is_enabled == 1 ? langTrans('common-yes-option-name') : langTrans('common-no-option-name')); break;
                case 'equip_cate': rowContent = rowContent.replaceAll('{' + tag + '}', data.equip_cate_name); break;
                case 'group': rowContent = rowContent.replaceAll('{' + tag + '}', data.group); break;
                case 'company': rowContent = rowContent.replaceAll('{' + tag + '}', data.company); break;
                case 'name': rowContent = rowContent.replaceAll('{' + tag + '}', data.name); break;
                case 'member_is_enabled': rowContent = rowContent.replaceAll('{' + tag + '}', data.is_enabled == 1 ? langTrans('member-checked-activate-status') : langTrans('member-unchecked-activate-status')); break;
                case 'account': rowContent = rowContent.replaceAll('{' + tag + '}', data.account); break;
                case 'checkcode': rowContent = rowContent.replaceAll('{' + tag + '}', data.checkcode); break;

                    //tour_point_title
                case 'tour_point_title': rowContent = rowContent.replaceAll('{' + tag + '}', "<a " + (data.website ? ("href='" + (data.website.indexOf('http') > -1 ? data.website : ("http://" + data.website) ) + "'") : "") + " target=_blank >" + data.title + "</a>"); break;
                case 'tour_point_location': rowContent = rowContent.replaceAll('{' + tag + '}', data.location); break;
                //case 'tour_point_address': rowContent = rowContent.replaceAll('{' + tag + '}', GetOptionText('taiwan_district', data.district) + data.address); break;
                case 'tour_point_address': {
                    var pp = GetPageParam();
                    if(pp.now_lang == 'zh_hant')
                        rowContent = rowContent.replaceAll('{' + tag + '}', GetOptionText('taiwan_district', data.district) + data.address);
                    else if (pp.now_lang == 'en_us')
                        rowContent = rowContent.replaceAll('{' + tag + '}', data.address + ', ' + GetOptionText('taiwan_district', data.district) + ', Taiwan (R.O.C.)');
                } break;

                case 'course_location': rowContent = rowContent.replaceAll('{' + tag + '}', data.location); break;
                case 'day_type': rowContent = rowContent.replaceAll('{' + tag + '}', data.day_type); break;
                case 'day_type_text': rowContent = rowContent.replaceAll('{' + tag + '}', GetOptionText('day_type_texts', data.day_type)); break;

                case 'course_member_account': rowContent = rowContent.replaceAll('{' + tag + '}', data.member.account); break;
                case 'course_member_company': rowContent = rowContent.replaceAll('{' + tag + '}', data.member.company); break;
                case 'course_member_name': rowContent = rowContent.replaceAll('{' + tag + '}', data.member.name); break;
                case 'course_member_tel': rowContent = rowContent.replaceAll('{' + tag + '}', data.member.tel); break;
                case 'course_member_id': rowContent = rowContent.replaceAll('{' + tag + '}', data.course_member.id); break;
                case 'course_id': rowContent = rowContent.replaceAll('{' + tag + '}', data.course.id); break;
                case 'course_member_is_enabled': rowContent = rowContent.replaceAll('{' + tag + '}', data.course_member.is_enabled == 1 ? langTrans('course-member-checked-activate-status') : langTrans('course-member-unchecked-activate-status')); break;

                case 'course_member_is_enabled_html': {
                    var html = "<span class='" + (data.member_is_enabled == 1 ? '' : 'no-') + "sign-up'>" + (data.member_is_enabled == 1 ? langTrans('course-member-checked-activate-status') : langTrans('course-member-unchecked-activate-status'))  + "</span>";
                    rowContent = rowContent.replaceAll('{' + tag + '}', html);
                } break;

                case 'survey_status_html': {
                    if (data.survey_status) {
                        var html = '';

                        if (data.survey_status == 'unopened') html = "<span class='half'>" + langTrans('member-course-search-status-unopened-option-name') + "</span>";                      //未開放
                        if (data.survey_status == 'opened') html = "<a href='/Member/Survey?course_id=" + data.id + "' class='write-survey'>" + langTrans('member-course-search-status-opened-option-name') + "</a>";      //已開放
                        if (data.survey_status == 'done') html = "<span class='sign-up'>" + langTrans('member-course-search-status-done-option-name') + "</span>"       //已填寫
                        
                        rowContent = rowContent.replaceAll('{' + tag + '}', html);
                    }
                } break;

                case 'search_logo': rowContent = rowContent.replaceAll('{' + tag + '}', data.unit == 'video' ? "https://img.youtube.com/vi/" + (data.url.indexOf('?v=') > -1 ? data.url.split('?v=')[1].split('&')[0] : '') + "/0.jpg" : data.logo_img); break;
                case 'search_date': rowContent = rowContent.replaceAll('{' + tag + '}', moment(data.date).format('MMM. DD.YYYY')); break;
                case 'search_url': rowContent = rowContent.replaceAll('{' + tag + '}', data.url ? data.url : 'javascript:void(0);'); break;
                case 'search_target': rowContent = rowContent.replaceAll('{' + tag + '}', data.unit == 'video' ? '_blank' : '_self'); break;
                case 'search_brief': {

                    $('#html_peel_area').html(data.content);
                    var peelHtml = $('#html_peel_area').text();
                    var brief = peelHtml.length > 50 ? peelHtml.substr(0, 50) : peelHtml;
                    rowContent = rowContent.replaceAll('{' + tag + '}', brief);
                    //rowContent = rowContent.replaceAll('{' + tag + '}', data.content.length > 100 ? data.content.substr(0, 100) : data.content);
                } break;
                    
                case 'view': rowContent = rowContent.replaceAll('{' + tag + '}', data.unit != 'equip' ? "<div class='users'><i class='fa fa-users' aria-hidden='true'></i> | " + data.view + "</div>" : ""); break;


                case 'ad_url': rowContent = rowContent.replaceAll('{' + tag + '}', data.url); break;
                case 'ad_content': rowContent = rowContent.replaceAll('{' + tag + '}', data.content); break;

                case 'survey_id': rowContent = rowContent.replaceAll('{' + tag + '}', data.survey.id); break;
                case 'survey_count':
                    if (data.survey_count || data.survey_count == 0) rowContent = rowContent.replaceAll('{' + tag + '}', data.survey_count);
                    break;

                case 'has_en_us': rowContent = rowContent.replaceAll('{' + tag + '}', data.has_en_us ? langTrans('common-yes-option-name') : langTrans('common-no-option-name')); break;

                //超連結類
                case 'url': rowContent = rowContent.replaceAll('{' + tag + '}', "<a target='_blank' href='" + data.url + "'>影片外部連結</a>"); break;        //後台用

                //時間類
                case 'time': rowContent = rowContent.replaceAll('{' + tag + '}', data.time ? moment(data.time).format('YYYY/MM/DD，HH:mm') : ''); break;
                case 'date_start': rowContent = rowContent.replaceAll('{' + tag + '}', data.date_start ? moment(data.date_start).format('YYYY/MM/DD，HH:mm') : ''); break;
                case 'date_end': rowContent = rowContent.replaceAll('{' + tag + '}', data.date_end ? moment(data.date_end).format('YYYY/MM/DD，HH:mm') : ''); break;
                case 'course_time': rowContent = rowContent.replaceAll('{' + tag + '}', data.time ? moment(data.time).format('YYYY-MM-DD') : ''); break;

                case 'survey_date_created': rowContent = rowContent.replaceAll('{' + tag + '}', data.survey.date_created ? moment(data.survey.date_created).format('YYYY/MM/DD HH:mm') : ''); break;

                case 'tour_point_date': rowContent = rowContent.replaceAll('{' + tag + '}', data.time ? moment(data.time).format('YYYY-MM-DD') : ''); break;
                //case 'tour_point_time': rowContent = rowContent.replaceAll('{' + tag + '}', data.time ? moment(data.time).format('HH:mm') : ''); break;
                case 'tour_point_time': {

                    var timeStr = '';
                    if (data.time && data.leave_time) {
                        timeStr = moment(data.time).format('HH:mm') + '-' + moment(data.leave_time).format('HH:mm');
                    }
                    rowContent = rowContent.replaceAll('{' + tag + '}', timeStr);
                } break;

                //檔案
                case 'file': {
                    rowContent = rowContent.replaceAll('{file_id}', data.file_id);
                    data.files = fileReload(data.file_id, data.file_tag);
                } break;

                default: break;
            }
        });

        newRow.data(data);
        newRow.html(rowContent);

        
        if (tags.indexOf('target') > -1) {                                                  //特殊欄位處理 (連結目標)
            var setObj = newRow.find('[name*=row_target][value=' + data.target + ']');
            if (setObj.length == 1) setObj.attr('checked', 'checked');
        }

        if (tags.indexOf('is_enabled') > -1) {                                              //特殊欄位處理 (是否啟用)
            var setObj = newRow.find('[name*=row_is_enabled][value=' + data.is_enabled + ']');
            if (setObj.length == 1) setObj.attr('checked', 'checked');
        }

        table.append(newRow);
    });

};

















var course_view_push = async function (data) {

    var form = $('#course_form');
    var html = form.html();

    var cover_img = '', cover_url = '';
    var files = await fileReload(data.file_id);
    if (files) {
        var cover = files.filter(x => x.img_size == '');
        if (cover.length == 1) cover_url = cover[0].path;
    }
    if (cover_url) cover_img = "<img src=" + cover_url + " alt=" + data.title + ">";

    html = html.replaceAll('{title}', data.title);
    html = html.replaceAll('{cover_img}', cover_img ? cover_img : '');       //<img src="{cover_url}" alt="{title}">
    html = html.replaceAll('{applicants_number}', data.sign_up_list.length);
    html = html.replaceAll('{time}', data.time ? moment(data.time).format('YYYY-MM-DD') : '');
    html = html.replaceAll('{content}', data.content);
    html = html.replaceAll('{date_start}', data.date_start ? moment(data.date_start).format('YYYY-MM-DD HH:mm') : '-');
    html = html.replaceAll('{date_end}', data.date_end ? moment(data.date_end).format('YYYY-MM-DD HH:mm') : '-');
    html = html.replaceAll('{location}', data.location);
    html = html.replaceAll('{seat}', data.seat);
    html = html.replaceAll('{website}', data.website ? (data.website.indexOf('http') > -1 ? data.website : 'http://' + data.website) : '');
    html = html.replaceAll('{recurit_status}', langTrans('recurit-status-' + data.recurit_status));

    var buttonHtml = '';
    if (data.recurit_status == 'recruiting') {
        if (!$('#login_account').val()) {   //尚未登入的狀態
            buttonHtml = "<a class='enroll' href='#' onclick=\"alert(langTrans('error_200006'));return false;\"  title=" + langTrans('recurit-status-sign-up') + " >" + langTrans('recurit-status-sign-up') + "</a>";
        }
        else {  //已登入，尚未報名的狀態
            buttonHtml = "<a class='enroll' href='/Course/Signup?id=" + data.id + "'  title=" + langTrans('recurit-status-sign-up') + " >" + langTrans('recurit-status-sign-up') + "</a>";
        }
    }
    if (data.recurit_status == 'unopened') buttonHtml = "<div class='deadline'>" + langTrans('recurit-status-' + data.recurit_status) + "</div>";   //尚未招生
    if (data.recurit_status == 'closed') buttonHtml = "<div class='deadline'>" + langTrans('recurit-status-' + data.recurit_status) + "</div>";     //招生截止
    if (data.recurit_status == 'signed') buttonHtml = "<div class='sign_up_success'>" + langTrans('recurit-status-' + data.recurit_status) + "</div>";     //已報名
    
    html = html.replaceAll('{sign_up_button}', buttonHtml);

    form.html(html);
};




