﻿
//管理者端 about 表單的即時驗證
var admin_about_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('about_form', obj, input_id);
};

//管理者端 intro 表單的即時驗證
var admin_intro_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('intro_form', obj, input_id);
};

//管理者端 maker_car 表單的即時驗證
var admin_maker_car_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('maker_car_form', obj, input_id);
};

//管理者端 tour_point 表單的即時驗證
var admin_tour_point_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('tour_point_form', obj, input_id);
};

//管理者端 maker 表單的即時驗證
var admin_maker_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('maker_form', obj, input_id);
};

////管理者端 course 表單的即時驗證
//var admin_course_form_realtime_validate = function (obj, input_id) {

//    return common_form_realtime_validate('course_form', obj, input_id);
//};

//管理者端 news 表單的即時驗證
var admin_news_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('news_form', obj, input_id);
};

//管理者端 album 表單的即時驗證
var admin_album_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('album_form', obj, input_id);
};

//管理者端 video 表單的即時驗證
var admin_video_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('video_form', obj, input_id);
};

//管理者端 equip 表單的即時驗證
var admin_equip_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('equip_form', obj, input_id);
};

//管理者端 equip_cate 表單的即時驗證
var admin_equip_cate_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('equip_cate_form', obj, input_id);
};

//管理者端 works 表單的即時驗證
var admin_works_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('works_form', obj, input_id);
};

//管理者端 member 表單的即時驗證
var admin_member_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('member_form', obj, input_id);
};

//管理者端 ad 表單的即時驗證
var admin_ad_form_realtime_validate = function (obj, input_id) {

    return common_form_realtime_validate('ad_form', obj, input_id);
};









//管理者端 about 表單的驗證
var admin_about_form_validate = function (data, pp) {

    var tags = ['title', 'content'];
    return common_form_validate(data, pp, 'about_form', tags);
};

//管理者端 intro 表單的驗證
var admin_intro_form_validate = function (data, pp) {

    var tags = ['title', 'content'];
    return common_form_validate(data, pp, 'intro_form', tags);
};

//管理者端 maker_car 表單的驗證
var admin_maker_car_form_validate = function (data, pp) {

    var tags = ['title', 'content'];
    return common_form_validate(data, pp, 'maker_car_form', tags);
};

//管理者端 tour_point 表單的驗證
var admin_tour_point_form_validate = function (data, pp) {

    var tags = ['title', 'location', 'district', 'address', 'latitude', 'longitude', 'time', 'leave_time', 'website', 'date_start', 'date_end'];
    return common_form_validate(data, pp, 'tour_point_form', tags);
};

//管理者端 maker 表單的驗證
var admin_maker_form_validate = function (data, pp) {

    var tags = ['title', 'intro', 'tel', 'email', 'website',];
    return common_form_validate(data, pp, 'maker_form', tags);
};

////管理者端 course 表單的驗證
//var admin_course_form_validate = function (data, pp) {

//    var tags = ['title', 'content', 'district', 'address', 'website', 'seat', 'time', 'course_date_start', 'course_date_end'];
//    return common_form_validate(data, pp, 'course_form', tags);
//};

//管理者端 news 表單的驗證
var admin_news_form_validate = function (data, pp) {

    var tags = ['title', 'content', 'date_start', 'date_end'];
    return common_form_validate(data, pp, 'news_form', tags);
};

//管理者端 album 表單的驗證
var admin_album_form_validate = function (data, pp) {

    var tags = ['title', 'date_start', 'date_end'];
    return common_form_validate(data, pp, 'album_form', tags);
};

//管理者端 video 表單的驗證
var admin_video_form_validate = function (data, pp) {

    var tags = ['title', 'url', 'date_start', 'date_end'];
    return common_form_validate(data, pp, 'video_form', tags);
};

//管理者端 equip 表單的驗證
var admin_equip_form_validate = function (data, pp) {

    var tags = ['title', 'equip_cate', 'content', 'date_start', 'date_end'];
    return common_form_validate(data, pp, 'equip_form', tags);
};

//管理者端 equip_cate 表單的驗證
var admin_equip_cate_form_validate = function (data, pp) {

    var tags = ['title', 'date_start', 'date_end'];
    return common_form_validate(data, pp, 'equip_cate_form', tags);
};

//管理者端 works 表單的驗證
var admin_works_form_validate = function (data, pp) {

    var tags = ['title', 'group', 'content', 'date_start', 'date_end'];
    return common_form_validate(data, pp, 'works_form', tags);
};

//管理者端 member 表單的驗證
var admin_member_form_validate = function (data, pp) {

    var tags = ['account', 'name', 'birth', 'tel', 'email', 'motive', 'password', 'confirm_password'];
    return common_form_validate(data, pp, 'member_form', tags);
};

//管理者端 ad 表單的驗證
var admin_ad_form_validate = function (data, pp) {

    var tags = ['title', 'ad_url'];
    return common_form_validate(data, pp, 'ad_form', tags);
};

//user端 survey 表單的驗證
var user_survey_form_validate = function (data, pp) {

    var tags = ['f3', 'f4', 'f5'];
    return common_form_validate(data, pp, 'survey', tags);
};


//管理者端 about row的驗證
//var admin_about_row_validate = function (trigger, data, pp) {
//    var ckMap = new Map();

//    var msg = '', tag = '';
//    var alerts = [];
//    var msgArray = [];

//    var validateTags = [
//        'title'
//    ];
//    $(validateTags).each(function () {
//        var ret = admin_about_row_realtime_validate(trigger, null, this.toString());
//        if (ret.msgArray.length > 0) ckMap.set(ret.tag, ret.msgArray);
//    });

//    alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
//        //$('#about_form [data-alert=summary]'),
//    ];
//    $(alerts).each(function () { this.html(''); });                                         //clear alert object text

//    if (ckMap.size > 0) {
//        msgArray = [langTrans('msg_100002')];
//        $(alerts).each(function () { this.html(msgArray.join(' ')); });                         //add alert object text
//        ckMap.set('summary', msgArray);
//    }

//    return ckMap;
//};

//管理者端 about 列表的即時驗證
//var admin_about_row_realtime_validate = function (trigger, obj, input_id) {
//    if (!trigger) return;
//    if (!obj && !input_id) return;                                  //資料不全，直接離開

//    var row = $(trigger).closest('[data-row=db_row]');
//    var dom_id = '';
//    if (obj) {                                                      //有直接指定DOM的情況
//        obj = $(obj);
//        dom_id = obj.attr('id');
//    }
//    else {
//        if (row.find('#' + input_id).length == 1) {     //未直接指定DOM，但有給DOM的ID
//            obj = row.find('#' + input_id)
//            dom_id = input_id;
//        }
//        else { return; }                                            //資料不全，直接離開
//    }

//    var ck_result = true;
//    var msg = '';
//    var inputs = []; var alerts = []; var msgArray = [];

//    switch (dom_id) {                                               //依不同DOM ID來判斷欄位值是否錯誤

//        case 'title': {
//            inputs = [                                                                                                  //有哪些要檢查的input
//                { input: row.find('#' + dom_id), value: row.find('#' + dom_id).val() },
//            ];
//            alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
//                row.find('[data-alert=' + dom_id + ']'),
//            ];
//            //有哪些判斷式
//            msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

//        } break;


//        default: break;
//    }

//    validate_style_helper(inputs, alerts, msgArray);

//    return { tag: dom_id, msgArray: msgArray };

//};


//管理者端 intro row的驗證
//var admin_intro_row_validate = function (trigger, data, pp) {
//    var ckMap = new Map();

//    var msg = '', tag = '';
//    var alerts = [];
//    var msgArray = [];

//    var validateTags = [
//        'title'
//    ];
//    $(validateTags).each(function () {
//        var ret = admin_intro_row_realtime_validate(trigger, null, this.toString());
//        if (ret.msgArray.length > 0) ckMap.set(ret.tag, ret.msgArray);
//    });

//    alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
        
//    ];
//    $(alerts).each(function () { this.html(''); });                                         //clear alert object text

//    if (ckMap.size > 0) {
//        msgArray = [langTrans('msg_100002')];
//        $(alerts).each(function () { this.html(msgArray.join(' ')); });                         //add alert object text
//        ckMap.set('summary', msgArray);
//    }

//    return ckMap;
//};


//管理者端 intro 列表的即時驗證
//var admin_intro_row_realtime_validate = function (trigger, obj, input_id) {
//    if (!trigger) return;
//    if (!obj && !input_id) return;                                  //資料不全，直接離開

//    var row = $(trigger).closest('[data-row=db_row]');
//    var dom_id = '';
//    if (obj) {                                                      //有直接指定DOM的情況
//        obj = $(obj);
//        dom_id = obj.attr('id');
//    }
//    else {
//        if (row.find('#' + input_id).length == 1) {     //未直接指定DOM，但有給DOM的ID
//            obj = row.find('#' + input_id)
//            dom_id = input_id;
//        }
//        else { return; }                                            //資料不全，直接離開
//    }

//    var ck_result = true;
//    var msg = '';
//    var inputs = []; var alerts = []; var msgArray = [];

//    switch (dom_id) {                                               //依不同DOM ID來判斷欄位值是否錯誤

//        case 'title': {
//            inputs = [                                                                                                  //有哪些要檢查的input
//                { input: row.find('#' + dom_id), value: row.find('#' + dom_id).val() },
//            ];
//            alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
//                row.find('[data-alert=' + dom_id + ']'),
//            ];
//            //有哪些判斷式
//            msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

//        } break;


//        default: break;
//    }

//    validate_style_helper(inputs, alerts, msgArray);

//    return { tag: dom_id, msgArray: msgArray };

//};



//管理者端 maker_car row的驗證
//var admin_maker_car_row_validate = function (trigger, data, pp) {
//    var ckMap = new Map();

//    var msg = '', tag = '';
//    var alerts = [];
//    var msgArray = [];

//    var validateTags = [
//        'title'
//    ];
//    $(validateTags).each(function () {
//        var ret = admin_maker_car_row_realtime_validate(trigger, null, this.toString());
//        if (ret.msgArray.length > 0) ckMap.set(ret.tag, ret.msgArray);
//    });

//    alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object

//    ];
//    $(alerts).each(function () { this.html(''); });                                         //clear alert object text

//    if (ckMap.size > 0) {
//        msgArray = [langTrans('msg_100002')];
//        $(alerts).each(function () { this.html(msgArray.join(' ')); });                         //add alert object text
//        ckMap.set('summary', msgArray);
//    }

//    return ckMap;
//};


//管理者端 maker_car 列表的即時驗證
//var admin_maker_car_row_realtime_validate = function (trigger, obj, input_id) {
//    if (!trigger) return;
//    if (!obj && !input_id) return;                                  //資料不全，直接離開

//    var row = $(trigger).closest('[data-row=db_row]');
//    var dom_id = '';
//    if (obj) {                                                      //有直接指定DOM的情況
//        obj = $(obj);
//        dom_id = obj.attr('id');
//    }
//    else {
//        if (row.find('#' + input_id).length == 1) {     //未直接指定DOM，但有給DOM的ID
//            obj = row.find('#' + input_id)
//            dom_id = input_id;
//        }
//        else { return; }                                            //資料不全，直接離開
//    }

//    var ck_result = true;
//    var msg = '';
//    var inputs = []; var alerts = []; var msgArray = [];

//    switch (dom_id) {                                               //依不同DOM ID來判斷欄位值是否錯誤

//        case 'title': {
//            inputs = [                                                                                                  //有哪些要檢查的input
//                { input: row.find('#' + dom_id), value: row.find('#' + dom_id).val() },
//            ];
//            alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
//                row.find('[data-alert=' + dom_id + ']'),
//            ];
//            //有哪些判斷式
//            msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

//        } break;


//        default: break;
//    }

//    validate_style_helper(inputs, alerts, msgArray);

//    return { tag: dom_id, msgArray: msgArray };

//};




var validate_style_helper = function (inputs, alerts, msgArray) {

    //$(inputs).each(function () { cl('inputs test', this); });                               //inputs object console log
    //$(alerts).each(function () { cl('alerts test', this); });                               //alerts object console log

    $(inputs).each(function () { this.input.removeClass('required-border'); });             //clear input alert style
    $(alerts).each(function () { this.html(''); });                                         //clear alert object text

    if (msgArray.length > 0) {
        $(inputs).each(function () { this.input.addClass('required-border'); });            //add input alert style
        $(alerts).each(function () { this.html(msgArray.join(' ')); });                     //add alert object text
    }
};


var common_form_validate = function (data, pp, form_name, validateTags) {

    var ckMap = new Map();

    if (!form_name || !validateTags) return ckMap;                    //資料不全，離開

    var msg = '', tag = '';
    var alerts = []; var msgArray = [];

    $(validateTags).each(function () {
        var ret = common_form_realtime_validate(form_name, null, this.toString());
        if (ret.msgArray.length > 0) ckMap.set(ret.tag, ret.msgArray);
    });

    alerts = [$('#' + form_name + ' [data-alert=summary]'),];                         //有哪些要輸出錯誤提醒的alert object
    $(alerts).each(function () { this.html(''); });                                         //clear alert object text

    if (ckMap.size > 0) {
        msgArray = [langTrans('msg_100002')];
        $(alerts).each(function () { this.html(msgArray.join(' ')); });                         //add alert object text
        ckMap.set('summary', msgArray);
    }

    return ckMap;
};



var common_form_realtime_validate = function (form_name, obj, input_id) {

    if (!form_name) return;
    if (!obj && !input_id) return;                                  //資料不全，直接離開

    var dom_id = '';
    if (obj) {                                                      //有直接指定DOM的情況
        dom_id = $(obj).attr('id');
    }
    else {
        if ($("#" + form_name + " #" + input_id).length == 1) {     //未直接指定DOM，但有給DOM的ID
            dom_id = input_id;
        }
        //else { return; }                                            //資料不全，直接離開
    }

    var msg = '';
    var inputs = []; var alerts = []; var msgArray = [];

    if (dom_id && $('#' + form_name + ' #' + dom_id).length > 0) {            //有依照tag找到DOM才繼續

        switch (dom_id) {                                               //依不同DOM ID來判斷欄位值是否錯誤

            case 'name':
            case 'group':
            case 'seat':
            case 'address':
            case 'tel':
            case 'motive':
            case 'f3':              //問卷表單第3個回饋
            case 'f4':              //問卷表單第4個回饋
            case 'f5':              //問卷表單第5個回饋
            case 'title': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;
            case 'content': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: CKEDITOR.instances.content.getData() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'location': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;
            case 'district': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=address]'),
                ];
                //有哪些判斷式
                msg = ck_select_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;
            case 'address': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            //case 'time': {
            //    inputs = [                                                                                                  //有哪些要檢查的input
            //        { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
            //    ];
            //    alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
            //        $('#' + form_name + ' [data-alert=' + dom_id + ']'),
            //    ];
            //    //有哪些判斷式
            //    msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };
            //    msg = ck_date_format(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            //} break;
            case 'time': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #time'), value: $('#' + form_name + ' #time').val() },
                    { input: $('#' + form_name + ' #leave_time'), value: $('#' + form_name + ' #leave_time').val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_date_format(inputs[0].value); if (msg) {
                    msgArray.push(langTrans(msg));
                    inputs = [                                                                                                  //有哪些要檢查的input
                        { input: $('#' + form_name + ' #time'), value: $('#' + form_name + ' #time').val() },
                    ];
                    break;
                };
                msg = ck_double_date_order(inputs[0].value, inputs[1].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'leave_time': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #time'), value: $('#' + form_name + ' #time').val() },
                    { input: $('#' + form_name + ' #leave_time'), value: $('#' + form_name + ' #leave_time').val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_date_format(inputs[1].value); if (msg) {
                    msgArray.push(langTrans(msg));
                    inputs = [                                                                                                  //有哪些要檢查的input
                        { input: $('#' + form_name + ' #leave_time'), value: $('#' + form_name + ' #leave_time').val() },
                    ];
                    break;
                };
                msg = ck_double_date_order(inputs[0].value, inputs[1].value); if (msg) {
                    msgArray.push(langTrans(msg));
                    alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                        $('#' + form_name + ' [data-alert=time]'),
                    ];
                    break;
                };

            } break;

            case 'website': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_url_format(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'birth': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_date_format(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'date_start': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #date_start'), value: $('#' + form_name + ' #date_start').val() },
                    { input: $('#' + form_name + ' #date_end'), value: $('#' + form_name + ' #date_end').val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_date_format(inputs[0].value); if (msg) {
                    msgArray.push(langTrans(msg));
                    inputs = [                                                                                                  //有哪些要檢查的input
                        { input: $('#' + form_name + ' #date_start'), value: $('#' + form_name + ' #date_start').val() },
                    ];
                    break;
                };
                msg = ck_double_date_order(inputs[0].value, inputs[1].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'date_end': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #date_start'), value: $('#' + form_name + ' #date_start').val() },
                    { input: $('#' + form_name + ' #date_end'), value: $('#' + form_name + ' #date_end').val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_date_format(inputs[1].value); if (msg) {
                    msgArray.push(langTrans(msg));
                    inputs = [                                                                                                  //有哪些要檢查的input
                        { input: $('#' + form_name + ' #date_end'), value: $('#' + form_name + ' #date_end').val() },
                    ];
                    break;
                };
                msg = ck_double_date_order(inputs[0].value, inputs[1].value); if (msg) {
                    msgArray.push(langTrans(msg));
                    alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                        $('#' + form_name + ' [data-alert=date_start]'),
                    ];
                    break;
                };

            } break;

            case 'intro': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: CKEDITOR.instances.intro.getData() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'account': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };
                msg = ck_email_format(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'password': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_password_length_limit(inputs[0].value, 6, 8); if (msg) { msgArray.push(langTrans(msg)); break; };
                msg = ck_password_english_number_format(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;
            case 'confirm_password': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #password'), value: $('#' + form_name + ' #password').val() },
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_password_length_limit(inputs[1].value, 6, 8); if (msg) { msgArray.push(langTrans(msg)); break; };
                msg = ck_password_english_number_format(inputs[1].value); if (msg) { msgArray.push(langTrans(msg)); break; };
                msg = ck_confirm_password_same(inputs[0].value, inputs[1].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'email': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_email_format(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'ad_url':
            case 'url': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_text_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };
                msg = ck_url_format(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'equip_cate': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_select_empty(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;

            case 'latitude': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_latitude_format(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;
            case 'longitude': {
                inputs = [                                                                                                  //有哪些要檢查的input
                    { input: $('#' + form_name + ' #' + dom_id), value: $('#' + form_name + ' #' + dom_id).val() },
                ];
                alerts = [                                                                                                  //有哪些要輸出錯誤提醒的alert object
                    $('#' + form_name + ' [data-alert=' + dom_id + ']'),
                ];
                //有哪些判斷式
                msg = ck_longitude_format(inputs[0].value); if (msg) { msgArray.push(langTrans(msg)); break; };

            } break;


            default: break;
        }

        validate_style_helper(inputs, alerts, msgArray);

    }

    return { tag: dom_id, msgArray: msgArray };

};







//以下為新式驗證方式





//抓第一個表單驗證失敗的控制項
var take_first_invalid_obj = function (form_tag) {

    var first_invalid_obj = '';
    var rules = form_validate_rule_controller(form_tag);                                    //取得特定表單的全部驗證條件(動態撈)
    $(rules).each(function () {             //執行檢查
        if (!first_invalid_obj) {
            var msg = '';
            if (this.is_enabled()) msg = this.check();
            if (msg) first_invalid_obj = this.check_inputs[0];
        }
    });
    return first_invalid_obj;
};


//執行表單驗證
var form_validate = function (form_tag, input_tag) {

    var ckMap = new Map();
    var rules = form_validate_rule_controller(form_tag);                                    //取得特定表單的全部驗證條件(動態撈)
    if (input_tag) { rules = rules.filter(x => x.tags.indexOf(input_tag) > -1); }           //依tag篩選驗證條件
    cl('rules after filter, input_tag: ' + input_tag, rules);

    $(rules).each(function () {             //執行檢查

        var msg = '';
        if (this.is_enabled()) {            //有額外規則成立(如下拉選單值為-1)才使用規則的情況? 新增is_enabled attr 預設 return true
            ck_style_controller(this.check_inputs, this.alerts, null, false);       //關閉錯誤提醒
            msg = this.check();                                                     //judge
            ck_style_controller(this.check_inputs, this.alerts, msg, true);         //開啟錯誤提醒
        }
        if (msg) ckMap.set(this.tags[0], [msg]);
    });
    
    return ckMap;
};

//控制項錯誤提醒的開關
var ck_style_controller = function (inputs, alerts, msg, isShow) {
    if (isShow) {
        if (msg) {                                          //如有取到錯誤訊息 就重新顯示錯誤提示
            $(inputs).each(function () {
                this.addClass('required-border');
                if (this.attr('id') == 'agree') this.addClass('checkbox_alert');
            });
            $(alerts).each(function () { this.html(langTrans(msg)); });
        }
    }
    else {
        $(inputs).each(function () {             //judge前 先將錯誤提示取消掉
            this.removeClass('required-border');
            if (this.attr('id') == 'agree') this.removeClass('checkbox_alert');
        });
        $(alerts).each(function () { this.html(''); });
    }
};

//收集所有有需要檢查的input，並綁定驗證事件(onblur)
var collect_all_check_input_and_bind_event = function (form_tag) {

    var input_ids = [];
    var inputs = [];

    var rules = form_validate_rule_controller(form_tag);            //取得檢查規則(動態)

    $(rules).each(function () {                 //開始收集input
        var check_inputs = this.check_inputs;
        var isDynamic = this.dynamic;
        $(check_inputs).each(function () {
            if (isDynamic) {            //動態控制項的CASE
                var input_id = this.attr('id') + '_' + this.attr('data-child-id');        //動態的控制項名稱格式 ex: employer_1
                if (input_ids.indexOf(input_id) < 0) { input_ids.push(input_id); inputs.push(this); }
            } else {                    //非動態控制項的CASE
                if (input_ids.indexOf(this.attr('id')) < 0) { input_ids.push(this.attr('id')); inputs.push(this); }
            }
        });
    });

    $(inputs).each(function () {                //將每個input綁定驗證事件
        if (this.attr('data-child-id')) {       //動態控制項的CASE

            //動態的控制項名稱格式 ex: employer_1
            var input_tag = (this.attr('id').indexOf('period_') > -1) ? this.attr('id') : this.attr('id') + '_' + this.attr('data-child-id');
            //var input_tag = this.attr('id') + '_' + this.attr('data-child-id');

            this.bind("blur", function () { form_validate(form_tag, input_tag); });
        }
        else {                                  //非動態控制項的CASE
            this.bind("blur", function () { form_validate(form_tag, this.id); });

        }
    });
    cl('collect_all_check_input_and_bind_event()', inputs);

    //綁定事件後 順便驗證
    $(rules).each(function () {             //執行檢查
        if (this.is_enabled()) {            //有額外規則成立(如下拉選單值為-1)才使用規則的情況? 新增is_enabled attr 預設 return true
            ck_style_controller(this.check_inputs, this.alerts, null, false);       //關閉錯誤提醒
            msg = this.check();                                                     //judge
            ck_style_controller(this.check_inputs, this.alerts, msg, true);         //開啟錯誤提醒
        }
    });
};


//依tag來決定要哪張表單的驗證規則
var form_validate_rule_controller = function (tag) {

    var rules = '';
    switch (tag) {
        case 'member': rules = member_validate_rules(); break;
        case 'hide_member': rules = hide_member_validate_rules(); break;
        case 'login': rules = login_validate_rules(); break;
        case 'password': rules = password_validate_rules(); break;
        case 'forgot': rules = forgot_validate_rules(); break;
        case 'course': rules = course_validate_rules(); break;
        case 'contact': rules = contact_validate_rules(); break;

        case 'course_search': rules = course_search_validate_rules(); break;
        case 'member_course_search': rules = member_course_search_validate_rules(); break;

        case 'about': rules = about_form_validate_rules(); break;
        default: break;
    }
    return rules;
};




//會員表單的檢查規則
var member_validate_rules = function () {

    var form = $('#member_form');           //表單物件
    var rules = [                       //檢查規則
        {   //帳號檢查
            tags: ['account'],
            group_inputs: [],
            check_inputs: [form.find('#account'),],
            values: [form.find('#account').val(),],
            alerts: [form.find('[data-alert=account]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //帳號檢查
            tags: ['account'],
            group_inputs: [],
            check_inputs: [form.find('#account'),],
            values: [form.find('#account').val(),],
            alerts: [form.find('[data-alert=account]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && this.values[0]; },
            check: function () { return ck_email_format(...(this.values)); },
        },

        {   //姓名檢查
            tags: ['name'],
            group_inputs: [],
            check_inputs: [form.find('#name'),],
            values: [form.find('#name').val(),],
            alerts: [form.find('[data-alert=name]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //生日檢查
            tags: ['birth'],
            group_inputs: [],
            check_inputs: [form.find('#birth')],
            values: [form.find('#birth').val()],
            alerts: [form.find('[data-alert=birth]')],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_date_format(...(this.values)); },
        },

        {   //電話檢查
            tags: ['tel'],
            group_inputs: [],
            check_inputs: [form.find('#tel')],
            values: [form.find('#tel').val()],
            alerts: [form.find('[data-alert=tel]')],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //EMAIL檢查
            tags: ['email'],
            group_inputs: [],
            check_inputs: [form.find('#email')],
            values: [form.find('#email').val()],
            alerts: [form.find('[data-alert=email]')],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_email_format(...(this.values)); },
        },

        {   //學習動機檢查
            tags: ['motive'],
            group_inputs: [],
            check_inputs: [form.find('#motive'),],
            values: [form.find('#motive').val(),],
            alerts: [form.find('[data-alert=motive]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#password')],
            values: [form.find('#password').val()],
            alerts: [form.find('[data-alert=password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1; },
            check: function () { return ck_password_length_limit(...(this.values), 6, 8); },
        },
        {   //密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#password')],
            values: [form.find('#password').val()],
            alerts: [form.find('[data-alert=password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && !ck_password_length_limit(...(this.values), 6, 8); },
            check: function () { return ck_password_english_number_format(...(this.values)); },
        },

        {   //確認密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#confirm_password')],
            values: [form.find('#confirm_password').val()],
            alerts: [form.find('[data-alert=confirm_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1; },
            check: function () { return ck_password_length_limit(...(this.values), 6, 8); },
        },
        {   //確認密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#confirm_password')],
            values: [form.find('#confirm_password').val()],
            alerts: [form.find('[data-alert=confirm_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && !ck_password_length_limit(...(this.values), 6, 8); },
            check: function () { return ck_password_english_number_format(...(this.values)); },
        },
        {   //確認密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#confirm_password')],
            values: [form.find('#password').val(), form.find('#confirm_password').val()],
            alerts: [form.find('[data-alert=confirm_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && !ck_password_english_number_format(this.values[1]); },
            check: function () { return ck_confirm_password_same(...(this.values)); },
        },
        
    ];

    cl('all rules', rules);
    return rules;
};


//會員表單的檢查規則
var hide_member_validate_rules = function () {

    var form = $('#hide_member_form');           //表單物件
    var rules = [                       //檢查規則
        {   //帳號檢查
            tags: ['account'],
            group_inputs: [],
            check_inputs: [form.find('#account'),],
            values: [form.find('#account').val(),],
            alerts: [form.find('[data-alert=account]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //帳號檢查
            tags: ['account'],
            group_inputs: [],
            check_inputs: [form.find('#account'),],
            values: [form.find('#account').val(),],
            alerts: [form.find('[data-alert=account]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && this.values[0]; },
            check: function () { return ck_email_format(...(this.values)); },
        },

        {   //姓名檢查
            tags: ['name'],
            group_inputs: [],
            check_inputs: [form.find('#name'),],
            values: [form.find('#name').val(),],
            alerts: [form.find('[data-alert=name]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //生日檢查
            tags: ['birth'],
            group_inputs: [],
            check_inputs: [form.find('#birth')],
            values: [form.find('#birth').val()],
            alerts: [form.find('[data-alert=birth]')],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_date_format(...(this.values)); },
        },

        {   //電話檢查
            tags: ['tel'],
            group_inputs: [],
            check_inputs: [form.find('#tel')],
            values: [form.find('#tel').val()],
            alerts: [form.find('[data-alert=tel]')],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //EMAIL檢查
            tags: ['email'],
            group_inputs: [],
            check_inputs: [form.find('#email')],
            values: [form.find('#email').val()],
            alerts: [form.find('[data-alert=email]')],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_email_format(...(this.values)); },
        },

        {   //學習動機檢查
            tags: ['motive'],
            group_inputs: [],
            check_inputs: [form.find('#motive'),],
            values: [form.find('#motive').val(),],
            alerts: [form.find('[data-alert=motive]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#password')],
            values: [form.find('#password').val()],
            alerts: [form.find('[data-alert=password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1; },
            check: function () { return ck_password_length_limit(...(this.values), 6, 8); },
        },
        {   //密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#password')],
            values: [form.find('#password').val()],
            alerts: [form.find('[data-alert=password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && !ck_password_length_limit(...(this.values), 6, 8); },
            check: function () { return ck_password_english_number_format(...(this.values)); },
        },

        {   //確認密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#confirm_password')],
            values: [form.find('#confirm_password').val()],
            alerts: [form.find('[data-alert=confirm_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1; },
            check: function () { return ck_password_length_limit(...(this.values), 6, 8); },
        },
        {   //確認密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#confirm_password')],
            values: [form.find('#confirm_password').val()],
            alerts: [form.find('[data-alert=confirm_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && !ck_password_length_limit(...(this.values), 6, 8); },
            check: function () { return ck_password_english_number_format(...(this.values)); },
        },
        {   //確認密碼檢查
            tags: ['password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#confirm_password')],
            values: [form.find('#password').val(), form.find('#confirm_password').val()],
            alerts: [form.find('[data-alert=confirm_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && !ck_password_english_number_format(this.values[1]); },
            check: function () { return ck_confirm_password_same(...(this.values)); },
        },

    ];

    cl('all rules', rules);
    return rules;
};


//登入表單的檢查規則
var login_validate_rules = function () {

    var form = $('#login_form');           //表單物件
    var rules = [                       //檢查規則
        {   //帳號檢查
            tags: ['account'],
            group_inputs: [],
            check_inputs: [form.find('#account'),],
            values: [form.find('#account').val(),],
            alerts: [form.find('[data-alert=account]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //帳號檢查
            tags: ['account'],
            group_inputs: [],
            check_inputs: [form.find('#account'),],
            values: [form.find('#account').val(),],
            alerts: [form.find('[data-alert=account]'),],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_email_format(...(this.values)); },
        },

        {   //密碼檢查
            tags: ['password'],
            group_inputs: [],
            check_inputs: [form.find('#password'),],
            values: [form.find('#password').val(),],
            alerts: [form.find('[data-alert=password]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //驗證碼檢查
            tags: ['captcha'],
            group_inputs: [],
            check_inputs: [form.find('#captcha'),],
            values: [form.find('#captcha').val(),],
            alerts: [form.find('[data-alert=captcha]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

    ];

    cl('all rules', rules);
    return rules;
};


//密碼更改表單的檢查規則
var password_validate_rules = function () {

    var form = $('#password_form');           //表單物件
    var rules = [                       //檢查規則

        {   //舊密碼檢查
            tags: ['password'],
            group_inputs: [],
            check_inputs: [form.find('#password')],
            values: [form.find('#password').val()],
            alerts: [form.find('[data-alert=password]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //新密碼檢查
            tags: ['new_password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#new_password')],
            values: [form.find('#new_password').val()],
            alerts: [form.find('[data-alert=new_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1; },
            check: function () { return ck_password_length_limit(...(this.values), 6, 8); },
        },
        {   //新密碼檢查
            tags: ['new_password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#new_password')],
            values: [form.find('#new_password').val()],
            alerts: [form.find('[data-alert=new_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && !ck_password_length_limit(...(this.values), 6, 8); },
            check: function () { return ck_password_english_number_format(...(this.values)); },
        },

        {   //確認密碼檢查
            tags: ['new_password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#confirm_password')],
            values: [form.find('#confirm_password').val()],
            alerts: [form.find('[data-alert=confirm_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1; },
            check: function () { return ck_password_length_limit(...(this.values), 6, 8); },
        },
        {   //確認密碼檢查
            tags: ['new_password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#confirm_password')],
            values: [form.find('#confirm_password').val()],
            alerts: [form.find('[data-alert=confirm_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && !ck_password_length_limit(...(this.values), 6, 8); },
            check: function () { return ck_password_english_number_format(...(this.values)); },
        },
        {   //確認密碼檢查
            tags: ['new_password', 'confirm_password'],
            group_inputs: [],
            check_inputs: [form.find('#confirm_password')],
            values: [form.find('#new_password').val(), form.find('#confirm_password').val()],
            alerts: [form.find('[data-alert=confirm_password]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && !ck_password_english_number_format(this.values[1]); },
            check: function () { return ck_confirm_password_same(...(this.values)); },
        },

    ];

    cl('all rules', rules);
    return rules;
};




//忘記密碼表單的檢查規則
var forgot_validate_rules = function () {

    var form = $('#forgot_form');           //表單物件
    var rules = [                       //檢查規則
        {   //帳號檢查
            tags: ['account'],
            group_inputs: [],
            check_inputs: [form.find('#account'),],
            values: [form.find('#account').val(),],
            alerts: [form.find('[data-alert=account]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //帳號檢查
            tags: ['account'],
            group_inputs: [],
            check_inputs: [form.find('#account'),],
            values: [form.find('#account').val(),],
            alerts: [form.find('[data-alert=account]'),],
            is_enabled: function () { return this.check_inputs[0].length == 1 && this.values[0]; },
            check: function () { return ck_email_format(...(this.values)); },
        },

        {   //姓名檢查
            tags: ['name'],
            group_inputs: [],
            check_inputs: [form.find('#name'),],
            values: [form.find('#name').val(),],
            alerts: [form.find('[data-alert=name]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //電話檢查
            tags: ['tel'],
            group_inputs: [],
            check_inputs: [form.find('#tel')],
            values: [form.find('#tel').val()],
            alerts: [form.find('[data-alert=tel]')],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        


    ];

    cl('all rules', rules);
    return rules;
};



//課程搜尋表單的檢查規則
var course_search_validate_rules = function () {

    var form = $('#course_search');           //表單物件
    var rules = [                       //檢查規則
        {   //開始時間檢查
            tags: ['date_start'],
            group_inputs: [],
            check_inputs: [],
            values: [form.find('#start-year').val() + '/' + form.find('#start-month').val() + '/' + form.find('#start-date').val()],
            alerts: [form.find('[data-alert=date_start]')],
            is_enabled: function () { return true; },
            check: function () { return ck_date_format(...(this.values)); },
        },
        {   //結束時間檢查
            tags: ['date_end'],
            group_inputs: [],
            check_inputs: [],
            values: [form.find('#end-year').val() + '/' + form.find('#end-month').val() + '/' + form.find('#end-date').val()],
            alerts: [form.find('[data-alert=date_end]')],
            is_enabled: function () { return true; },
            check: function () { return ck_date_format(...(this.values)); },
        },
        {   //起訖時間順序檢查
            tags: ['date_order'],
            group_inputs: [],
            check_inputs: [],
            values: [
                form.find('#start-year').val() + '/' + form.find('#start-month').val() + '/' + form.find('#start-date').val(),
                form.find('#end-year').val() + '/' + form.find('#end-month').val() + '/' + form.find('#end-date').val()],
            alerts: [],
            is_enabled: function () { return !ck_date_format(this.values[0]) && !ck_date_format(this.values[1]); },
            check: function () { return ck_double_date_order(...(this.values)); },
        },
    ];

    cl('all rules', rules);
    return rules;
};

//會員中心會員課程搜尋表單的檢查規則
var member_course_search_validate_rules = function () {

    var form = $('#member_course_search');           //表單物件
    var rules = [                       //檢查規則
        {   //開始時間檢查
            tags: ['date_start'],
            group_inputs: [],
            check_inputs: [],
            values: [form.find('#start-year').val() + '/' + form.find('#start-month').val() + '/' + form.find('#start-date').val()],
            alerts: [form.find('[data-alert=date_start]')],
            is_enabled: function () { return form.find('#start-year').val() != '請選擇'; },
            check: function () { return ck_date_format(...(this.values)); },
        },
    ];

    cl('all rules', rules);
    return rules;
};


//課程表單的檢查規則
var course_validate_rules = function () {

    var form = $('#course_form');           //表單物件
    var rules = [                       //檢查規則
        {   //課程名稱檢查
            tags: ['title'],
            group_inputs: [],
            check_inputs: [form.find('#title'),],
            values: [form.find('#title').val(),],
            alerts: [form.find('[data-alert=title]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //地點檢查
            tags: ['location'],
            group_inputs: [],
            check_inputs: [form.find('#location'),],
            values: [form.find('#location').val(),],
            alerts: [form.find('[data-alert=location]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        

        {   //上課時間檢查
            tags: ['time'],
            group_inputs: [],
            check_inputs: [form.find('#time')],
            values: [form.find('#time').val()],
            alerts: [form.find('[data-alert=time]')],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //上課時間檢查
            tags: ['time'],
            group_inputs: [],
            check_inputs: [form.find('#time')],
            values: [form.find('#time').val()],
            alerts: [form.find('[data-alert=time]')],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_date_format(...(this.values)); },
        },

        {   //內容檢查
            tags: ['content'],
            group_inputs: [],
            check_inputs: [form.find('#content')],
            values: [CKEDITOR.instances.content.getData()],
            alerts: [form.find('[data-alert=content]')],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //發布時間檢查
            tags: ['date_start'],
            group_inputs: [],
            check_inputs: [form.find('#date_start')],
            values: [form.find('#date_start').val()],
            alerts: [form.find('[data-alert=date_start]')],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //發布時間檢查
            tags: ['date_start'],
            group_inputs: [],
            check_inputs: [form.find('#date_start')],
            values: [form.find('#date_start').val()],
            alerts: [form.find('[data-alert=date_start]')],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_date_format(...(this.values)); },
        },

        {   //結束時間檢查
            tags: ['date_end'],
            group_inputs: [],
            check_inputs: [form.find('#date_end')],
            values: [form.find('#date_end').val()],
            alerts: [form.find('[data-alert=date_end]')],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //結束時間檢查
            tags: ['date_end'],
            group_inputs: [],
            check_inputs: [form.find('#date_end')],
            values: [form.find('#date_end').val()],
            alerts: [form.find('[data-alert=date_end]')],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_date_format(...(this.values)); },
        },

        {   //開始&結束時間順序檢查
            tags: ['date_start', 'date_end'],
            group_inputs: [],
            check_inputs: [form.find('#date_start'), form.find('#date_end')],
            values: [form.find('#date_start').val(), form.find('#date_end').val()],
            alerts: [form.find('[data-alert=date_start]')],
            is_enabled: function () { return !ck_date_format(this.values[0]) && !ck_date_format(this.values[1]); },
            check: function () { return ck_double_date_order(...(this.values)); },
        },

        {   //報名人數檢查
            tags: ['seat'],
            group_inputs: [],
            check_inputs: [form.find('#seat'),],
            values: [form.find('#seat').val(),],
            alerts: [form.find('[data-alert=seat]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //報名人數檢查
            tags: ['seat'],
            group_inputs: [],
            check_inputs: [form.find('#seat'),],
            values: [form.find('#seat').val(),],
            alerts: [form.find('[data-alert=seat]'),],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_int_format(...(this.values)); },
        },
        
    ];

    cl('all rules', rules);
    return rules;
};


//聯絡我們的檢查規則
var contact_validate_rules = function () {

    var form = $('#contact_form');           //表單物件
    var rules = [                       //檢查規則
        {   //姓名檢查
            tags: ['contact_name'],
            group_inputs: [],
            check_inputs: [form.find('#contact_name'),],
            values: [form.find('#contact_name').val(),],
            alerts: [form.find('[data-alert=contact_name]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //email檢查
            tags: ['contact_email'],
            group_inputs: [],
            check_inputs: [form.find('#contact_email'),],
            values: [form.find('#contact_email').val(),],
            alerts: [form.find('[data-alert=contact_email]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },
        {   //email檢查
            tags: ['contact_email'],
            group_inputs: [],
            check_inputs: [form.find('#contact_email'),],
            values: [form.find('#contact_email').val(),],
            alerts: [form.find('[data-alert=contact_email]'),],
            is_enabled: function () { return this.values[0]; },
            check: function () { return ck_email_format(...(this.values)); },
        },

        {   //電話檢查
            tags: ['contact_telphone'],
            group_inputs: [],
            check_inputs: [form.find('#contact_telphone'),],
            values: [form.find('#contact_telphone').val(),],
            alerts: [form.find('[data-alert=contact_telphone]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //信件標題檢查
            tags: ['contact_mail_header'],
            group_inputs: [],
            check_inputs: [form.find('#contact_mail_header'),],
            values: [form.find('#contact_mail_header').val(),],
            alerts: [form.find('[data-alert=contact_mail_header]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //信件內容檢查
            tags: ['contact_message'],
            group_inputs: [],
            check_inputs: [form.find('#contact_message'),],
            values: [form.find('#contact_message').val(),],
            alerts: [form.find('[data-alert=contact_message]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //驗證碼檢查
            tags: ['contact_captcha'],
            group_inputs: [],
            check_inputs: [form.find('#contact_captcha'),],
            values: [form.find('#contact_captcha').val(),],
            alerts: [form.find('[data-alert=contact_captcha]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

    ];

    cl('all rules', rules);
    return rules;
};


//about form 的檢查規則
var about_form_validate_rules = function () {

    var form = $('#about_form');           //表單物件
    var rules = [                       //檢查規則
        {   //標題檢查
            tags: ['title'],
            group_inputs: [],
            check_inputs: [form.find('#title'),],
            values: [form.find('#title').val(),],
            alerts: [form.find('[data-alert=title]'),],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

        {   //內容檢查
            tags: ['content'],
            group_inputs: [],
            check_inputs: [form.find('#content')],
            values: [CKEDITOR.instances.content.getData()],
            alerts: [form.find('[data-alert=content]')],
            is_enabled: function () { return true; },
            check: function () { return ck_text_empty(...(this.values)); },
        },

    ];

    cl('about_form_validate_rules all rules', rules);
    return rules;
};