﻿

var fileAllowFormat = ["csv", "doc", "docx", "ods", "odt", "pdf", "ppt", "pptx", "txt", "xls", "xlsx", "bmp", "gif", "jpeg", "jpg", "png"];
var imgAllowFormat = ["bmp", "gif", "jpeg", "jpg", "png"];
var allowMBSize = 5;
var allowSize = 1024 * 1024 * allowMBSize;

var default_img_url = '../Content/Front/assets/img/activity/activity01.jpg';

//同時只能上傳一個檔案的判斷(上傳覆蓋)
var onlyOneFile = ["intro"];
var onlyOnePic = ["intro_img", "maker_logo", "course_logo", "news_logo", "equip", "works_cover", "ad_pc", "ad_mobile"];



if ($('[name=allowMBSize]').length > 0) {
    $('[name=allowMBSize]').each(function () { $(this).html(allowMBSize); });
}
if ($('[name=fileAllowFormat]').length > 0) {
    $('[name=fileAllowFormat]').each(function () { $(this).html(fileAllowFormat.join(', ')); });
}
if ($('[name=imgAllowFormat]').length > 0) {
    $('[name=imgAllowFormat]').each(function () { $(this).html(imgAllowFormat.join(', ')); });
}




var fileUpload2 = function (upload_area_obj, uploader) {
    var pp = GetPageParam();
    cl('fileUpload2 . upload_area_obj', upload_area_obj);
    cl('fileUpload2 . uploader', uploader);

    loadingIconShow(upload_area_obj);                   //顯示loading圖示

    //取得user上傳的檔案
    var files = uploader.files;
    cl('fileUpload2 files', files);

    var filetype = $(uploader).attr('data-filetype');
    var limitcount = $(uploader).attr('data-limitcount');
    var tag = $(uploader).attr('data-tag');
    var file_id = $(uploader).attr('data-file_id');
    var classname = $(uploader).attr('data-classname');

    


    //目前檔案數量
    var nowFileCount = 0;

    //檢查目前已上傳檔案數量
    //if (attrs.filetype == 'img') {
    //    if (scope.d.dataObj.imgs) {
    //        nowFileCount = scope.d.dataObj.imgs.filter(x => x.tag == attrs.tag).length / 3;
    //    }
    //}
    //else if (attrs.filetype == 'file') {
    //    if (scope.d.dataObj.files) {
    //        nowFileCount = scope.d.dataObj.files.filter(x => x.tag == attrs.tag).length;
    //    }
    //}

    //特殊規則
    if (tag == 'avatar') nowFileCount = 0;
    //if (scope.d.dataObj.files != null) {
    //    nowFileCount = scope.d.dataObj.files.filter(x => x.tag == attrs.tag).length;
    //}
    //檔案最大上傳限制數量

    //cl('nowFileCount', nowFileCount);
    //cl('limitcount', limitcount);

    var formdata = new FormData();
    //已達最大上傳數量
    if (nowFileCount >= limitcount) {
        alert('oops,too much file');
        loadingIconHide(upload_area_obj);                   //隱藏loading圖示
        return;
    }
    //還有空間可以上傳
    else {
        limitcount = limitcount - nowFileCount;
        //cl('limitcount', limitcount);
        var file_append_success_count = 0;
        for (var i = 0; i < files.length; i++) {
            if (limitcount-- > 0) {

                var exten = files[i].name.split('.')[files[i].name.split('.').length - 1];
                if (filetype == 'file' && fileAllowFormat.indexOf(exten.toLowerCase()) < 0) {
                    alert('檔案名稱: ' + files[i].name + ', 檔案格式不符規定');
                }
                else if (filetype == 'img' && imgAllowFormat.indexOf(exten.toLowerCase()) < 0) {
                    alert('檔案名稱: ' + files[i].name + ', 檔案格式不符規定');
                }
                else if (files[i].size > allowSize) {
                    alert('檔案名稱: ' + files[i].name + ', 檔案大小超過5MB');
                }
                else {
                    formdata.append(i, files[i]);
                    file_append_success_count++;            //符合需求的檔案個數
                }
            }
            else {
                break;
            }
        }


        if (file_append_success_count < 1) {                //無任何檔案符合需求
            loadingIconHide(upload_area_obj);                   //隱藏loading圖示
            return;
        }
    }

    var uploadPath = '';
    if (filetype == 'file') uploadPath = '/Shared/FileUpload';
    else if (filetype == 'img') uploadPath = '/Shared/ImgUpload';

    $.ajax({
        url: uploadPath + '?obj_id=' + file_id + '&classname=' + classname + '&tag=' + tag + '&checkcode=' + pp.file_checkcode,
        data: formdata,
        type: 'POST',
        dataType: 'json',
        cache: false,
        processData: false,
        contentType: false,
        success: function ($ret) {
            console.log($ret);
            if ($ret.isSuccess == true) {
                loadingIconHide(upload_area_obj);                   //隱藏loading圖示
                fileReload(file_id, tag);
            }
        },
        error: function ($ret) {
            cl('error $ret: ', $ret);
            alert('oops');
            loadingIconHide(upload_area_obj);                   //隱藏loading圖示
        },
    });


    $(uploader).val('');            //clear the uploader
};


var loadingIconShow = function (upload_area_obj) {

    if (upload_area_obj.find('[name=uploader_loading]').length == 1) {
        upload_area_obj.find('[name=uploader_loading]').show();
    }
};
var loadingIconHide = function (upload_area_obj) {

    if (upload_area_obj.find('[name=uploader_loading]').length == 1) {
        upload_area_obj.find('[name=uploader_loading]').hide();
    }
};


var trigger_file_command_execute = async function (trigger) {
    var ret = '';
    if (!trigger) return;

    var file_command = $(trigger).closest('[data-row=db_row]').find('[name=file_command]');
    //if (file_command.length != 1) return;
    if (file_command.length < 1) return;

    if (file_command.length > 1) {                              //一個資料列中有多個檔案上傳系統的CASE
        var file_command_length = file_command.length;
        for (var q = 0; q < file_command_length; q++) {

            cl('file_command_group ' + q + 'th', $(trigger).closest('[data-row=db_row]').find('[name=file_command]:eq(' + q + ')'));
            var file_command = $(trigger).closest('[data-row=db_row]').find('[name=file_command]:eq(' + q + ')').data();
            if (!file_command) continue;
            if (!file_command.off) file_command.off = [];
            for (var i = 0; i < file_command.off.length; i++) {
                var c = file_command.off[i];
                file_command.off[i] = c.obj_id + ';' + c.type + ';' + c.tag + ';' + c.name;
            }

            ret = await ajax_post3('Shared', 'FilesSwitch', file_command, null);
        }
    }

    if (file_command.length == 1) {                         //一個資料列中只有一個檔案上傳系統的CASE
        file_command = file_command.data();
        cl('file_command', file_command);
        if (!file_command.off) file_command.off = [];
        for (var i = 0; i < file_command.off.length; i++) {
            var c = file_command.off[i];
            file_command.off[i] = c.obj_id + ';' + c.type + ';' + c.tag + ';' + c.name;
        }
        ret = await ajax_post3('Shared', 'FilesSwitch', file_command, null);
    }
    
    ret.isSuccess = true;

    return ret;
};


var file_command_execute = async function (form_name) {
    var ret = '';
    if (!form_name) return;

    var form = $('#' + form_name);
    if (form.length != 1) return;

    if (form.find('[name=file_command]').length < 1) return;

    var file_command_length = form.find('[name=file_command]').length;
    for (var q = 0; q < file_command_length; q++) {

        cl('file_command_group ' + q + 'th', form.find('[name=file_command]:eq(' + q + ')'));
        var file_command = form.find('[name=file_command]:eq(' + q + ')').data();
        if (!file_command) continue;
        if (!file_command.off) file_command.off = [];
        for (var i = 0; i < file_command.off.length; i++) {
            var c = file_command.off[i];
            file_command.off[i] = c.obj_id + ';' + c.type + ';' + c.tag + ';' + c.name;
        }

        ret = await ajax_post3('Shared', 'FilesSwitch', file_command, null);
    }
    ret.isSuccess = true;

    return ret;
};


//var file_command_execute = async function () {
//    var ret = '';
//    if ($('[name=file_command]').length < 1) return;

//    var file_command_length = $('[name=file_command]').length;
//    for (var q = 0; q < file_command_length ; q++){

//        cl('file_command_group ' + q + 'th', $('[name=file_command]:eq(' + q + ')'));
//        var file_command = $('[name=file_command]:eq(' + q + ')').data();
//        if (!file_command) continue;

//        for (var i = 0; i < file_command.off.length; i++) {
//            var c = file_command.off[i];
//            file_command.off[i] = c.obj_id + ';' + c.type + ';' + c.tag + ';' + c.name;
//        }

//        ret = await ajax_post3('Shared', 'FilesSwitch', file_command, null);
//    }
//    ret.isSuccess = true;

//    return ret;
//};

var file_upload_data_init = function (form, file_id) {

    form.find('[data-file_id]').each(function () {              //註冊檔案編號
        $(this).attr('data-file_id', file_id);
    });
    form.find('[data-file_area]').each(function () {            //註冊檔案編號
        $(this).attr('data-file_area', file_id);
        $(this).attr('data-file_checkcode', new Date().getTime().toString());           //註冊檔案驗證碼(每次載入頁面都會更換)
    });

    form.find('[name=file_command]').each(function () {
        $(this).attr('data-file_id', file_id);              //註冊檔案編號
        $(this).data({ on: [], off: [] });                  //初始檔案資料命令格式
    });
};


var fileReload = async function (file_id, FVR_tag) {
    var pp = GetPageParam();

    var input = {
        obj_id: file_id,
        endmode: pp.end,
        checkcode: pp.file_checkcode,
    }
    var $ret = await ajax_post2('Shared', 'FilesSearch', input, pp);
    cl('fileReload ret', $ret);

    var files = $ret.data1;
    if (files && files.length > 0) {

        //$('[name=file_command]').each(function () {
        $('[name=file_command][data-file_id=' + file_id + ']').each(function () {

            var file_command = $(this).data();
            if (!file_command.off) file_command.off = [];
            var tag = $(this).attr('data-tag');

            //同時只能上傳一個檔案的判斷(上傳覆蓋)
            var filetype = ''; var fileOnlyCount = 0;
            if (onlyOneFile.indexOf(tag) > -1) { filetype = 'file'; fileOnlyCount = 1; }
            if (onlyOnePic.indexOf(tag) > -1) { filetype = 'img'; fileOnlyCount = 3; }
            if (filetype) {

                var finds = files.filter(x => x.tag == tag && x.type == filetype);                                                    //file類型的如有多個檔案，只留第一個，其他刪除
                if (finds.length > fileOnlyCount) {
                    for (var i = fileOnlyCount; i < finds.length; i++) {
                        var f = finds[i];
                        if (filetype == 'file') file_command.off.push({ obj_id: f.obj_id, type: filetype, tag: tag, name: f.id });
                        if (filetype == 'img') file_command.off.push({ obj_id: f.obj_id, type: filetype, tag: tag, name: f.name });
                    }
                }
            }

            //檢查OFF區中有的物件就先移除掉不顯示
            for (var i = 0; i < files.length; i++) {

                if (!file_command.off) continue;
                var f = files[i];
                if (file_command.off.filter(x => x.type == f.type && x.tag == tag && x.name == f.id).length > 0) {            //需要移除的檔案
                    files.splice(i, 1); i = i - 1; continue;
                }
                if (file_command.off.filter(x => x.type == f.type && x.tag == tag && x.name == f.name).length > 0) {          //需要移除的圖片
                    files.splice(i, 1); i = i - 1; continue;
                }
            }

            //將新上傳的物件更新至file command ON區(is_deleted & is_enabled 皆為0 )
            var readyToOnFiles = files.filter(x => x.tag == tag && x.is_deleted == 0 && x.is_enabled == 0);
            file_command.on = readyToOnFiles;

            $(this).data(file_command);
            cl('file_command ' + $(this).attr('data-tag'), file_command);

        });
    }

    

    //fileViewRefresh(file_id, files);
    cl('FVR_tag', FVR_tag);
    if (FVR_tag) {
        fileViewRefresh(file_id, files, FVR_tag);

        //在這裡載入並掛載每個檔案的語言資料? lang_obj_load() 掛載在替代文字欄位框?
        var file_note_list = document.querySelectorAll('[name=file_note]');
        if (file_note_list.length > 0) {
            for (var i = 0; i < file_note_list.length; i++) {
                //alert(file_note_list[i].getAttribute('data-file_id'));
                var file_id = file_note_list[i].getAttribute('data-file_id');
                var noteObj = { file_id: file_id };
                noteObj = await lang_obj_load('file', file_id, noteObj, pp);
                $(file_note_list[i]).data(noteObj);
            }
        }
    }

    return files;
}



var ImageRemove = async function (obj_id, tag, img_name) {

    if (confirm('確定移除?')) {

        //var target = $('[name=file_command][data-tag=' + tag + ']');
        var target = $('[name=file_command][data-tag=' + tag + '][data-file_id=' + obj_id + ']');
        //if ($('#file_command').length == 1) {
        if (target.length == 1) {
            //var file_command = $('#file_command').data();
            var file_command = target.data();
            if (!file_command.off) file_command.off = [];
            file_command.off.push({ obj_id: obj_id, type: 'img', tag: tag, name: img_name });
            target.data(file_command);
            cl('file_command_' + target.attr('data-tag'), file_command);

            fileReload(obj_id, tag);
        }
    }
};

var FileRemove = async function (obj_id, tag, delete_id) {

    if (confirm('確定移除?')) {

        var target = $('[name=file_command][data-tag=' + tag + ']');

        if (target.length == 1) {
            var file_command = target.data();
            if (!file_command.off) file_command.off = [];
            
            file_command.off.push({ obj_id: obj_id, type: 'file', tag: tag, name: delete_id });
            target.data(file_command);
            cl('file_command', file_command);

            fileReload(obj_id, tag);
        }
    }
};


var fileViewRefresh = function (file_id, files, tag) {

    cl('fileViewRefresh ' + file_id + ' ' + tag, null);
    var pp = GetPageParam();
    //if (pp && pp.unit) {                    //依不同頁面情況，有不同的刷新檔案預覽狀態行為

        //cl('fileViewRefresh . unit', pp.unit);
        var file_area_obj = '';                             
        //if ($('[data-file_area=' + file_id + ']').length == 1) {        //找到檔案上傳需要局部更新的區域物件
        if ($('[data-file_area=' + file_id + ']').length > 0) {        //找到檔案上傳需要局部更新的區域物件
            file_area_obj = $('[data-file_area=' + file_id + ']');
        }
        else { return; }

        //switch (pp.unit) {
        switch (tag) {

            //intro
            case 'intro_img':
            case 'intro': {
                var tagArray = ['intro', 'intro_img'];

                for (var i = 0; i < tagArray.length; i++) {                                                 //clear file view
                    var tag = tagArray[i]; file_area_obj.find('[name=' + tag + '_file_view]').html('');
                }

                if (files && files.length > 0) {
                    for (var i = 0; i < tagArray.length; i++) {
                        var tag = tagArray[i];
                        if (files.filter(x => x.tag == tag).length > 0) {

                            if (file_area_obj.find('[name=' + tag + '_file_view]').length == 1) {

                                var size = '';
                                if (tag == 'intro_img') { size = 'S' }

                                var f = files.filter(x => x.tag == tag && x.img_size == size)[0];
                                var html = '';

                                if (tag == 'intro') {
                                    html += "<a target='_blank' href='" + f.path + "'>" + f.name.split('@')[1] + '.' + f.exten + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                    if (pp.datamode != 'list') {
                                        html += "<input type='button' value='刪除檔案' onclick='FileRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.id + "\")' />";
                                    }
                                }
                                if (tag == 'intro_img') {
                                    html += "<a target='_blank' href='" + f.path + "'><img style='max-height:150px; max-width:150px' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                    html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />";
                                }
                                file_area_obj.find('[name=' + tag + '_file_view]').html(html);
                            }
                        }
                    }
                }

            } break;

            //about
            case 'about': {
                var tagArray = ['about'];

                for (var i = 0; i < tagArray.length; i++) {                                                 //clear file view
                    var tag = tagArray[i]; file_area_obj.find('[name=' + tag + '_file_view]').html('');
                }

                if (files && files.length > 0) {
                    for (var i = 0; i < tagArray.length; i++) {
                        var tag = tagArray[i];
                        if (files.filter(x => x.tag == tag).length > 0) {

                            if (file_area_obj.find('[name=' + tag + '_file_view]').length == 1) {

                                var html = '';
                                var array = files.filter(x => x.tag == tag);
                                $(array).each(function () {
                                    var f = this;
                                    html += "<a target='_blank' href='" + f.path + "'>" + f.name.split('@')[1] + '.' + f.exten + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                    html += "<input type='button' value='刪除檔案' onclick='FileRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.id + "\")' /><br />";
                                });
                                file_area_obj.find('[name=' + tag + '_file_view]').html(html);
                            }
                        }
                    }
                }

            } break;

            //maker
            case 'maker_logo':
            case 'maker_work':
            case 'maker': {
                var tagArray = ['maker_logo', 'maker_work'];

                for (var i = 0; i < tagArray.length; i++) {                                                 //clear file view
                    var tag = tagArray[i]; file_area_obj.find('[name=' + tag + '_view]').html('');
                }

                if (files && files.length > 0) {
                    for (var i = 0; i < tagArray.length; i++) {
                        var tag = tagArray[i];
                        if (files.filter(x => x.tag == tag).length > 0) {

                            if (file_area_obj.find('[name=' + tag + '_view]').length == 1) {

                                var size = '';
                                if (tag == 'maker_logo') { size = 'S' }
                                else if (tag == 'maker_work') { size = 'S' }

                                var array = files.filter(x => x.tag == tag && x.img_size == size);
                                var html = '';

                                if (tag == 'maker_logo') {
                                    $(array).each(function () {
                                        var f = this;

                                        html += "<a target='_blank' href='" + f.path + "'><img style='max-height:150px; max-width:150px' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                        html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />";
                                    });
                                }
                                if (tag == 'maker_work') {
                                    var rowFiles = [];
                                    for (var fid = 0; fid < array.length ; fid++) {
                                        if (rowFiles.length < 4) {
                                            rowFiles.push(array[fid]);
                                        }

                                        if (rowFiles.length == 4 || fid == array.length - 1) {

                                            html += "<li>";
                                            $(rowFiles).each(function () {
                                                var f = this;
                                                html += "<a target='_blank' href='" + f.path + "'><img style='max-height:150px; max-width:150px' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;";
                                                html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />&nbsp;&nbsp;&nbsp;&nbsp;";
                                            });
                                            html += "</li>"
                                            rowFiles = [];
                                        }
                                    }
                                }

                                file_area_obj.find('[name=' + tag + '_view]').html(html);
                            }
                        }
                    }
                }

            } break;

            //course
            case 'course_logo':
            case 'course': {
                var tagArray = ['course_logo'];

                for (var i = 0; i < tagArray.length; i++) {                                                 //clear file view
                    var tag = tagArray[i]; file_area_obj.find('[name=' + tag + '_view]').html('');
                }

                if (files && files.length > 0) {
                    for (var i = 0; i < tagArray.length; i++) {
                        var tag = tagArray[i];
                        if (files.filter(x => x.tag == tag).length > 0) {

                            if (file_area_obj.find('[name=' + tag + '_view]').length == 1) {

                                var size = '';
                                if (tag == 'course_logo') { size = 'S' }
                                
                                var array = files.filter(x => x.tag == tag && x.img_size == size);
                                var html = '';
                                $(array).each(function () {
                                    var f = this;
                                    html += "<a target='_blank' href='" + f.path + "'><img style='max-height:150px; max-width:150px' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                    html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />";
                                });
                                file_area_obj.find('[name=' + tag + '_view]').html(html);
                            }
                        }
                    }
                }

            } break;

            //news
            case 'news_logo':
            case 'news': {
                var tagArray = ['news_logo'];

                for (var i = 0; i < tagArray.length; i++) {                                                 //clear file view
                    var tag = tagArray[i]; file_area_obj.find('[name=' + tag + '_view]').html('');
                }

                if (files && files.length > 0) {
                    for (var i = 0; i < tagArray.length; i++) {
                        var tag = tagArray[i];
                        if (files.filter(x => x.tag == tag).length > 0) {

                            if (file_area_obj.find('[name=' + tag + '_view]').length == 1) {

                                var size = '';
                                if (tag == 'news_logo') { size = 'S' }

                                var array = files.filter(x => x.tag == tag && x.img_size == size);
                                var html = '';
                                $(array).each(function () {
                                    var f = this;
                                    html += "<a target='_blank' href='" + f.path + "'><img style='max-height:150px; max-width:150px' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                    html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />";
                                });
                                file_area_obj.find('[name=' + tag + '_view]').html(html);
                            }
                        }
                    }
                }

            } break;


            //album
            case 'album': {
                var tagArray = ['album'];

                for (var i = 0; i < tagArray.length; i++) {                                                 //clear file view
                    var tag = tagArray[i]; file_area_obj.find('[name=' + tag + '_view]').html('');
                }

                if (files && files.length > 0) {
                    for (var i = 0; i < tagArray.length; i++) {
                        var tag = tagArray[i];
                        if (files.filter(x => x.tag == tag).length > 0) {

                            if (file_area_obj.find('[name=' + tag + '_view]').length == 1) {

                                var size = '';
                                if (tag == 'album') { size = 'S' }

                                var array = files.filter(x => x.tag == tag && x.img_size == size);
                                var html = '';

                                if (tag == 'album') {
                                    //var rowFiles = [];
                                    //for (var fid = 0; fid < array.length; fid++) {
                                        //if (rowFiles.length < 4) {
                                        //    rowFiles.push(array[fid]);
                                        //}

                                        //if (rowFiles.length == 4 || fid == array.length - 1) {

                                            html += "<li style='display: inline-block'>";
                                            //$(rowFiles).each(function () {
                                            $(array).each(function () {
                                                var f = this;
                                                html += "<div style='display: inline-block; padding:10px; margin-right:20px'>";
                                                html += "<a target='_blank' href='" + f.oriPath + "'><img style='height:150px;' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;";
                                                html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />&nbsp;&nbsp;&nbsp;&nbsp;";
                                                html += "<br /><br /><div style='margin-bottom:20px'>替代文字：<input type='text' name='file_note' data-file_id='" + f.id + "' value='" + f.note + "' /></div>";
                                                html += "</div>";
                                            });
                                            html += "</li>"
                                            //rowFiles = [];
                                        //}
                                    //}
                                }

                                file_area_obj.find('[name=' + tag + '_view]').html(html);
                            }
                        }
                    }
                }

            } break;


            //equip
            case 'equip': {
                var tagArray = ['equip'];

                for (var i = 0; i < tagArray.length; i++) {                                                 //clear file view
                    var tag = tagArray[i]; file_area_obj.find('[name=' + tag + '_view]').html('');
                }

                if (files && files.length > 0) {
                    for (var i = 0; i < tagArray.length; i++) {
                        var tag = tagArray[i];
                        if (files.filter(x => x.tag == tag).length > 0) {

                            if (file_area_obj.find('[name=' + tag + '_view]').length == 1) {

                                var size = '';
                                if (tag == 'equip') { size = 'S' }

                                var array = files.filter(x => x.tag == tag && x.img_size == size);
                                var html = '';
                                $(array).each(function () {
                                    var f = this;
                                    html += "<a target='_blank' href='" + f.path + "'><img style='max-height:150px; max-width:150px' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                    html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />";
                                });
                                file_area_obj.find('[name=' + tag + '_view]').html(html);
                            }
                        }
                    }
                }

            } break;

            //works
            case 'works_cover': 
            case 'works_photo': 
            case 'works_raw':
            case 'works': {
                var tagArray = ['works_cover', 'works_photo', 'works_raw'];

                for (var i = 0; i < tagArray.length; i++) {                                                 //clear file view
                    var tag = tagArray[i]; file_area_obj.find('[name=' + tag + '_view]').html('');
                }

                if (files && files.length > 0) {
                    for (var i = 0; i < tagArray.length; i++) {
                        var tag = tagArray[i];
                        if (files.filter(x => x.tag == tag).length > 0) {

                            if (file_area_obj.find('[name=' + tag + '_view]').length == 1) {

                                var size = '';
                                if (tag == 'works_cover') { size = 'S' }
                                else if (tag == 'works_photo') { size = 'S' }

                                var array = files.filter(x => x.tag == tag && x.img_size == size);
                                var html = '';

                                if (tag == 'works_cover') {
                                    $(array).each(function () {
                                        var f = this;

                                        html += "<a target='_blank' href='" + f.path + "'><img style='max-height:150px; max-width:150px' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                        html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />";
                                    });
                                }
                                if (tag == 'works_photo') {
                                    var rowFiles = [];
                                    for (var fid = 0; fid < array.length; fid++) {
                                        if (rowFiles.length < 4) {
                                            rowFiles.push(array[fid]);
                                        }

                                        if (rowFiles.length == 4 || fid == array.length - 1) {

                                            html += "<li>";
                                            $(rowFiles).each(function () {
                                                var f = this;
                                                html += "<a target='_blank' href='" + f.oriPath + "'><img style='max-height:150px; max-width:150px' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;";
                                                html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />&nbsp;&nbsp;&nbsp;&nbsp;";
                                            });
                                            html += "</li>";
                                            rowFiles = [];
                                        }
                                    }
                                }
                                if (tag == 'works_raw') {
                                    $(array).each(function () {
                                        var f = this;
                                        html += "<a target='_blank' href='" + f.path + "'>" + f.name.split('@')[1] + '.' + f.exten + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                        html += "<input type='button' value='刪除檔案' onclick='FileRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.id + "\")' /><br />";
                                    });
                                }
                                file_area_obj.find('[name=' + tag + '_view]').html(html);
                            }
                        }
                    }
                }

            } break;

            //ad
            case 'ad_pc': 
            case 'ad_mobile': 
            case 'ad': {
                var tagArray = ['ad_pc', 'ad_mobile'];

                for (var i = 0; i < tagArray.length; i++) {                                                 //clear file view
                    var tag = tagArray[i]; file_area_obj.find('[name=' + tag + '_img_view]').html('');
                }

                if (files && files.length > 0) {
                    for (var i = 0; i < tagArray.length; i++) {
                        var tag = tagArray[i];
                        if (files.filter(x => x.tag == tag).length > 0) {

                            if (file_area_obj.find('[name=' + tag + '_img_view]').length == 1) {

                                var f = files.filter(x => x.tag == tag)[0];
                                var html = '';
                                html += "<a target='_blank' href='" + f.path + "'><img style='max-height:150px; max-width:150px' src='" + f.path + "' align='middle' /></a>&nbsp;&nbsp;&nbsp;&nbsp;";
                                html += "<input type='button' value='刪除檔案' onclick='ImageRemove(\"" + f.obj_id + "\", \"" + f.tag + "\", \"" + f.name + "\")' />&nbsp;&nbsp;&nbsp;&nbsp;";
                                file_area_obj.find('[name=' + tag + '_img_view]').html(html);
                            }
                        }
                    }
                }

            } break;


            default: break;
        }

    //}

};



var file_note_save = async function () {

    var file_note_list = document.querySelectorAll('[name=file_note]');
    if (file_note_list.length > 0) {
        for (var i = 0; i < file_note_list.length; i++) {
            var file_id = file_note_list[i].getAttribute('data-file_id');
            var note = $(file_note_list[i]).val();
            var $note_save_ret = await ajax_post3('Shared', 'FilesNoteSave', { file_id: file_id, note: note }, null);
        }
    }
};







var importExcel = function (upload_area_obj, uploader) {

    if (confirm('確認匯入?') == false) { $(uploader).val(''); return; }

    var pp = GetPageParam();
    cl('fileUpload2 . upload_area_obj', upload_area_obj);
    cl('fileUpload2 . uploader', uploader);

    loadingIconShow(upload_area_obj);                   //顯示loading圖示

    //取得user上傳的檔案
    var files = uploader.files;
    cl('fileUpload2 files', files);

    var tag = $(uploader).attr('data-tag');

    var formdata = new FormData();

    var file_append_success_count = 0;
    for (var i = 0; i < files.length; i++) {

        formdata.append(i, files[i]);
        file_append_success_count++;            //符合需求的檔案個數
    }

    if (file_append_success_count < 1) {                //無任何檔案符合需求
        loadingIconHide(upload_area_obj);                   //隱藏loading圖示
        return;
    }

    var uploadPath = '/Member/MemberDataImport';

    $.ajax({
        url: uploadPath + '?tag=' + tag,
        data: formdata,
        type: 'POST',
        dataType: 'json',
        cache: false,
        processData: false,
        contentType: false,
        success: function ($ret) {
            loadingIconHide(upload_area_obj);                   //隱藏loading圖示
            cl('importExcel $ret', $ret);
            if ($ret.isSuccess == true) {
                alert('更新成功');

                //fileReload(file_id, tag);
            }

            if ($ret.isSuccess == false) {

                //var ckObjs = $ret.data1;
                var msg = import_error_obj_to_msg($ret.data1);
                if (msg) alert(msg);
            }
            func_member_start();        //重整畫面
        },
        error: function ($ret) {
            cl('error $ret: ', $ret);
            alert('oops');
            loadingIconHide(upload_area_obj);                   //隱藏loading圖示
        },
    });

    $(uploader).val('');
};


//msg format: 第 x 行 [xxx]:xxxxx; [xxx]:xxxxx; ...
//msg format: 第x+1行 [xxx]:xxxxx; [xxx]:xxxxx; ...
var import_error_obj_to_msg = function (ckObjs) {
    var ret = '';
    if (!ckObjs || ckObjs.length == 0) return;

    $(ckObjs).each(function () {

        var row_number = this.row_number;
        ret += '第' + row_number + '行 ';

        var ckObj = this.checkResult;
        $(ckObj).each(function () {

            var tag = this.checkTag;
            ret += '[' + tag + ']: ';

            var err = '';
            if (this.checkResult.length > 0) {
                err = this.checkResult[0];
            }
            ret += langTrans('error_' + err) + '; ';
            //var errArray = this.checkResult[0];
        });

        ret += '\r';        //換行
    });

    return ret;
};