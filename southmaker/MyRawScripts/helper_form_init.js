﻿
//ABOUT表單初始
var about_form_init = async function (pp) {

    var tags = ['ckContent', 'dataInit', 'file', 'language'];
    common_form_init('about_form', 'about', tags, pp);
};

//INTRO表單初始
var intro_form_init = async function (pp) {

    var tags = ['ckContent', 'dataInit', 'file', 'language'];
    common_form_init('intro_form', 'intro', tags, pp);
};

//maker_car表單初始
var maker_car_form_init = async function (pp) {

    var tags = ['ckContent', 'dataInit', 'language'];
    common_form_init('maker_car_form', 'maker_car', tags, pp);
};

//tour_point表單初始
var tour_point_form_init = async function (pp) {

    var tags = ['taiwan_district_options', 'dataInit', 'language'];
    common_form_init('tour_point_form', 'tour_point', tags, pp);
};

//前台tour_point搜尋表單初始
var tour_point_search_form_init = async function (pp) {

    var tags = ['taiwan_district_options', 'tour_point_history_options'];
    common_form_init('tour_point_search', 'tour_point', tags, pp);
};


//maker表單初始
var maker_form_init = async function (pp) {

    var tags = ['ckIntro', 'dataInit', 'file', 'language'];
    common_form_init('maker_form', 'maker', tags, pp);
};

//course表單初始
var course_form_init = async function (pp) {

    var tags = ['ckContent', 'taiwan_district_options', 'day_type_options', 'dataInit', 'file', 'language', 'course_default_address'];
    common_form_init('course_form', 'course', tags, pp);
};

//news表單初始
var news_form_init = async function (pp) {

    var tags = ['ckContent', 'dataInit', 'file', 'language'];
    common_form_init('news_form', 'news', tags, pp);
};

//album表單初始
var album_form_init = async function (pp) {

    var tags = ['dataInit', 'file', 'language'];
    common_form_init('album_form', 'album', tags, pp);
};

//video表單初始
var video_form_init = async function (pp) {

    var tags = ['dataInit', 'language'];
    common_form_init('video_form', 'video', tags, pp);
};

//equip表單初始
var equip_form_init = async function (pp) {

    var tags = ['equip_cate_options', 'dataInit', 'file', 'ckContent', 'language'];
    common_form_init('equip_form', 'equip', tags, pp);
};

//equip_cate表單初始
var equip_cate_form_init = async function (pp) {

    var tags = ['dataInit', 'language'];
    common_form_init('equip_cate_form', 'equip_cate', tags, pp);
};

//works表單初始
var works_form_init = async function (pp) {

    var tags = ['ckContent', 'dataInit', 'file', 'language'];
    common_form_init('works_form', 'works', tags, pp);
};

//member表單初始
var member_form_init = async function (pp) {

    var tags = ['dataInit', 'member_checkcode', 'account_disabled', 'occupation_options', 'highest_education_options', 'course_source_options', 'employ_status_options'];
    common_form_init('member_form', 'member', tags, pp);
};

//隱藏member表單初始
var hide_member_form_init = async function (pp) {

    var tags = ['dataInit', 'member_checkcode', 'account_disabled', 'occupation_options', 'highest_education_options', 'course_source_options', 'employ_status_options'];
    common_form_init('hide_member_form', 'member', tags, pp);
};

//course_member表單初始
var course_member_form_init = async function (pp) {

    var tags = ['highest_education_options', 'course_source_options', 'employ_status_options'];
    common_form_init('course_member_form', 'course_member', tags, pp);
};

//ad表單初始
var ad_form_init = async function (pp) {

    var tags = ['dataInit', 'file'];
    common_form_init('ad_form', 'ad', tags, pp);
};

//survey表單初始
var survey_form_init = async function (pp) {

    var tags = ['dataInit', 'f1', 'f2'];
    common_form_init('survey', 'survey', tags, pp);
};








var common_form_init = function (form_name, unit_name, tags, pp) {
    if (!form_name || !unit_name) return;                                               //資料不齊全就直接離開

    var form = $('#' + form_name);                          //抓目標表單物件
    if (form.length < 1) return;                            //目標抓不到就直接離開

    $(tags).each(async function () {

        var tag = this.toString();
        switch (tag) {

            case 'ckContent': {                                                             //將content文字框套入ckeditor功能
                if (form.find('#content').length == 1) {
                    CKEDITOR.replace('content', {
                        filebrowserImageBrowseUrl: '../Scripts/ckeditor/ckfinder/ckfinder.html?type=Images',
                    });
                }
            } break;

            case 'ckIntro': {                                                               //將intro文字框套入ckeditor功能
                if (form.find('#intro').length == 1) {
                    CKEDITOR.replace('intro', {
                        filebrowserImageBrowseUrl: '../Scripts/ckeditor/ckfinder/ckfinder.html?type=Images',
                    });
                }
            } break;

            case 'course_default_address': {                                                               //將課程地址預設填入南方基地的地址
                if (form.find('#district').length == 1) form.find('#district').val(14);
                if (form.find('#address').length == 1) form.find('#address').val(langTrans('common-offical-address'));
            } break;

            case 'dataInit': {                                                              //如果是新增類型的表單就把預設資料物件掛到表單上
                var id = getReq('id');
                if (!id) {                  //新增狀態      
                    
                    var initObj = await DataObjInit(unit_name);

                    if (tags.indexOf('member_checkcode') > -1) {                                //如tags裡有member_checkcode才處理

                        initObj.checkcode = new Date().getTime().toString();               //發一個新的checkcode
                        //initObj.langs = [];                                                 //初始語言欄位
                    }

                    if (tags.indexOf('language') > -1) {                                //如tags裡有language才處理
                        initObj.langs = [];
                        var main_lang_obj = await DataObjInit('language_data');                 //新增主要語言版本(中文版)
                        main_lang_obj.unit = unit_name;
                        main_lang_obj.lang = 'zh_hant';
                        main_lang_obj = common_lang_pull_by_main_data(unit_name, main_lang_obj, initObj);
                        initObj.langs.push(main_lang_obj);
                    }

                    form.data(initObj);


                    if (tags.indexOf('file') > -1) {                                //如tags裡有file才處理檔案上傳

                        file_upload_data_init(form, initObj.file_id);               //初始檔案上傳的相關資料
                    }
                }
                else {                      //編輯狀態

                    if (tags.indexOf('account_disabled') > -1) {                                //如tags裡有account_disabled才處理

                        //lock帳號欄位 不允許編輯
                        if (form.find('#account').length == 1) form.find('#account').attr('disabled', '');
                    }
                }
            } break;

            case 'language': {                                   //資料語言 相關控制器的處理

                //<select id="lang_switch" onchange="lang_switch($(this).val(), 'about')"></select>
                //<input type="hidden" id="now_lang" value="zh_hant" />

                var html = "<select id='lang_switch' onchange='lang_switch($(this).val(), \"" + unit_name + "\")'>";
                $(language_options).each(function () { html += optionCreater(this.value, this.text); });
                html += "</select>";
                html += "<input type='hidden' id='now_lang' value='zh_hant' />";
                if ($("#language_ctrl").length == 1) $("#language_ctrl").html(html);

            } break;

            case 'day_type_options': {                                   //課程時間類型 下拉選單的選項處理
                var optionHtml = '';
                $(day_type_options).each(function () { optionHtml += optionCreater(this.value, this.text); });
                if (form.find("#day_type").length == 1) form.find("#day_type").html(optionHtml);

            } break;

            case 'tour_point_history_options': {
                var optionHtml = '';
                var nowYear = (new Date().getFullYear());

                optionHtml += optionCreater('', langTrans('tour-point-search-form-history-label-name'));       //預設選項
                for (var i = nowYear ; i >= 2015 ; i--){
                    optionHtml += optionCreater(i, i - 1911);   //選項文字需顯示民國年份
                }
                if (form.find("#history").length == 1) form.find("#history").html(optionHtml);
            } break;

            case 'taiwan_district_options': {                                   //台灣縣市 下拉選單的選項處理
                var optionHtml = '';
                $(taiwan_district_options).each(function () { optionHtml += optionCreater(this.value, this.text); });     
                if (form.find("#district").length == 1) form.find("#district").html(optionHtml);

            } break;

            case 'equip_cate_options': {                                   //設備類別 下拉選單的選項處理
                var optionHtml = '';
                optionHtml += "<option value=0>" + langTrans('common-select-default-option-name') + "</option>";
                var q = await QueryObjInit('equip_cate');
                q.pageSize = 100;
                var $ret = await ajax_post2('Equip', 'EquipCateSearch', q, pp);          //抓資料
                if ($ret && $ret.hasData) {
                    $($ret.data1).each(function () { optionHtml += optionCreater(this.id, this.title); });
                    if (form.find("#equip_cate").length == 1) form.find("#equip_cate").html(optionHtml);
                }

                var data = form.data();
                if (data && form.find("#equip_cate").length == 1) form.find("#equip_cate").val(data.equip_cate);

            } break;
            case 'occupation_options': {                                   //職業 下拉選單的選項處理
                var optionHtml = '';
                $(occupation_options).each(function () { optionHtml += optionCreater(this.value, this.text); });
                if (form.find("#occupation").length == 1) form.find("#occupation").html(optionHtml);

            } break;
            case 'highest_education_options': {                                   //學歷 下拉選單的選項處理
                var optionHtml = '';
                $(highest_education_options).each(function () { optionHtml += optionCreater(this.value, this.text); });
                if (form.find("#degree").length == 1) form.find("#degree").html(optionHtml);

            } break;

            case 'course_source_options': {                                   //得知來源管道 下拉選單的選項處理
                var optionHtml = '';
                $(course_source_options).each(function () { optionHtml += optionCreater(this.value, this.text); });
                if (form.find("#source").length == 1) form.find("#source").html(optionHtml);
            } break;

            case 'f1': {                                   //得知來源管道 下拉選單的選項處理
                var optionHtml = '';
                $(course_source_options).each(function () { optionHtml += optionCreater(this.value, this.text); });
                if (form.find("#f1").length == 1) form.find("#f1").html(optionHtml);

            } break;

            case 'f2': {                                   //得知來源管道 下拉選單的選項處理
                var optionHtml = '';
                $(course_join_reason_options).each(function () { optionHtml += optionCreater(this.value, this.text); });
                if (form.find("#f2").length == 1) form.find("#f2").html(optionHtml);

            } break;

            case 'employ_status_options': {                                   //就業狀態 下拉選單的選項處理
                var optionHtml = '';
                $(employ_status_options).each(function () { optionHtml += optionCreater(this.value, this.text); });
                if (form.find("#employ_status").length == 1) form.find("#employ_status").html(optionHtml);

            } break;



            default: break;
        }
    });

    alert_push2(form_name, new Map());          //清空訊息顯示
};