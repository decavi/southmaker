﻿

//載入語言資料(一開始載入頁面時執行)
var lang_obj_load = async function (unit, obj_id, data, pp) {

    data.langs = [];

    if (unit && obj_id) {
        var langQ = await QueryObjInit('language_data');
        langQ.unit = unit;
        langQ.obj_id = obj_id;
        var $langRet = await ajax_post2('Shared', 'LangDataSearch', langQ, pp);          //抓語言資料
        cl('LangDataSearch $langRet, unit: ' + unit + ' , obj_id: ' + obj_id, $langRet);
        if ($langRet && $langRet.hasData) data.langs = $langRet.data1;
    }

    var main_lang_obj = await DataObjInit('language_data');                 //新增主要語言版本(中文版)
    main_lang_obj.unit = unit;
    main_lang_obj.obj_id = obj_id;
    main_lang_obj.lang = 'zh_hant';
    main_lang_obj = common_lang_pull_by_main_data(unit, main_lang_obj, data);
    data.langs.push(main_lang_obj);

    return data;
};

//保存語言資料 (當主資料保存成功之後)
var lang_obj_save = async function (data, obj_id) {

    for (var i = 0; i < data.langs.length; i++) {
        if (data.langs[i].lang != 'zh_hant') {
            data.langs[i].obj_id = obj_id;
            var $langRet = await ajax_post3('Shared', 'LangDataSave', data.langs[i], null);
        }
    }

};



//切換語言資料
var lang_switch = async function (lang, unit) {

    var langBeforeChange = $('#now_lang').val();            //抓切換前的語言


    var form = get_form_helper(unit);                       //抓表單&表單物件
    var data = get_form_data_helper(unit);
    if (!data) return;

    
    for (var i = 0; i < data.langs.length; i++) {           //暫存切換前的語言資料
        if (data.langs[i].lang == langBeforeChange) {
            data.langs[i] = common_lang_pull(unit, data.langs[i]);
        }
    }


    var langObj = data.langs.filter(x => x.lang == lang);
    if (langObj.length > 0) {                               //有找到指定的語言資料
        langObj = langObj[0];
    }
    else {                                                  //無找到語言資料
        langObj = await DataObjInit('language_data');       //初始語言資料
        langObj.lang = lang;
        langObj.unit = unit;
        data.langs.push(langObj);                           //新增至語言資料陣列中
    }
    common_lang_push(unit, langObj);                        //將所選擇的語言資料更新至畫面

    form.data(data);                        //更新表單物件
    //cl('data', data);

    $('#now_lang').val(lang);                       //紀錄切換後的語言
};


var get_form_data_helper = function (unit) {

    var form = get_form_helper(unit);
    return form.data();
}

var get_form_helper = function (unit) {

    var form = '';
    switch (unit) {

        case 'about': form = $('#about_form'); break;
        case 'intro': form = $('#intro_form'); break;
        case 'maker_car': form = $('#maker_car_form'); break;
        case 'tour_point': form = $('#tour_point_form'); break;
        case 'maker': form = $('#maker_form'); break;
        case 'course': form = $('#course_form'); break;
        case 'news': form = $('#news_form'); break;
        case 'album': form = $('#album_form'); break;
        case 'video': form = $('#video_form'); break;
        case 'equip': form = $('#equip_form'); break;
        case 'equip_cate': form = $('#equip_cate_form'); break;
        case 'works': form = $('#works_form'); break;
        default: break;
    }

    return form;
};


var common_lang_push = function (unit, data) {

    var form = get_form_helper(unit);
    switch (unit) {

        case 'about':
        case 'intro': 
        case 'maker_car':
            {
                form.find("#title").val(data.text1);
                form.find("#keyword").val(data.text2);
                form.find("#description").val(data.text3);
                CKEDITOR.instances.content.setData(!data.content1 ? '' : data.content1);
            } break;

        case 'tour_point':
            {
                form.find("#title").val(data.text1);
                form.find("#location").val(data.text2);
                form.find("#address").val(data.text3);
            } break;
        case 'maker':
            {
                form.find("#title").val(data.text1);
                form.find("#member").val(data.text2);
                form.find("#exp").val(data.text3);
                CKEDITOR.instances.intro.setData(!data.content1 ? '' : data.content1);
            } break;
        case 'course':
            {
                form.find("#title").val(data.text1);
                form.find("#location").val(data.text2);
                form.find("#address").val(data.text3);
                CKEDITOR.instances.content.setData(!data.content1 ? '' : data.content1);
            } break;
        case 'news':
        case 'equip':
            {
                form.find("#title").val(data.text1);
                CKEDITOR.instances.content.setData(!data.content1 ? '' : data.content1);
            } break;
        case 'album':
        case 'video':
        case 'equip_cate':
            {
                form.find("#title").val(data.text1);
            } break;
        case 'works':
            {
                form.find("#title").val(data.text1);
                form.find("#group").val(data.text2);
                form.find("#tool").val(data.text3);
                CKEDITOR.instances.content.setData(!data.content1 ? '' : data.content1);
            } break;


        default: break;
    }
};

var common_lang_pull = function (unit, langObj) {

    if (!langObj) return;

    var form = get_form_helper(unit);
    switch (unit) {

        case 'about':
        case 'intro': 
        case 'maker_car':
            {
                langObj.text1 = form.find("#title").val();
                langObj.text2 = form.find("#keyword").val();
                langObj.text3 = form.find("#description").val();
                langObj.content1 = CKEDITOR.instances.content.getData();
            } break;

        case 'tour_point': {
            langObj.text1 = form.find("#title").val();
            langObj.text2 = form.find("#location").val();
            langObj.text3 = form.find("#address").val();
        } break;
        case 'maker': {
            langObj.text1 = form.find("#title").val();
            langObj.text2 = form.find("#member").val();
            langObj.text3 = form.find("#exp").val();
            langObj.content1 = CKEDITOR.instances.intro.getData();
        } break;
        case 'course': {
            langObj.text1 = form.find("#title").val();
            langObj.text2 = form.find("#location").val();
            langObj.text3 = form.find("#address").val();
            langObj.content1 = CKEDITOR.instances.content.getData();
        } break;
        case 'news':
        case 'equip':
            {
                langObj.text1 = form.find("#title").val();
                langObj.content1 = CKEDITOR.instances.content.getData();
            } break;
        case 'album':
        case 'video':
        case 'equip_cate':
            {
                langObj.text1 = form.find("#title").val();
            } break;
        case 'works': {
            langObj.text1 = form.find("#title").val();
            langObj.text2 = form.find("#group").val();
            langObj.text3 = form.find("#tool").val();
            langObj.content1 = CKEDITOR.instances.content.getData();
        } break;

        default: break;
    }
    return langObj;
};

var common_lang_pull_by_main_data = function (unit, langObj, main_data) {

    if (!langObj) return;

    switch (unit) {

        case 'about':
        case 'intro':
        case 'maker_car':
            {
                langObj.text1 = main_data.title;
                langObj.text2 = main_data.keyword;
                langObj.text3 = main_data.description;
                langObj.content1 = main_data.content;
            } break;

        case 'tour_point': {
            langObj.text1 = main_data.title;
            langObj.text2 = main_data.location;
            langObj.text3 = main_data.address;
        } break;
        case 'maker': {
            langObj.text1 = main_data.title;
            langObj.text2 = main_data.member;
            langObj.text3 = main_data.exp;
            langObj.content1 = main_data.intro;
        } break;
        case 'course': {
            langObj.text1 = main_data.title;
            langObj.text2 = main_data.location;
            langObj.text3 = main_data.address;
            langObj.content1 = main_data.content;
        } break;
        case 'news':
        case 'equip':
            {
                langObj.text1 = main_data.title;
                langObj.content1 = main_data.content;
            } break;
        case 'album':
        case 'video':
        case 'equip_cate':
            {
                langObj.text1 = main_data.title;
            } break;
        case 'works': {
            langObj.text1 = main_data.title;
            langObj.text2 = main_data.group;
            langObj.text3 = main_data.tool;
            langObj.content1 = main_data.content;
        } break;

        default: break;
    }

    return langObj;
};



var translate_multi = async function (pp, datas) {

    if (!pp || !datas || datas.length < 1) return datas;

    for (var i = 0; i < datas.length ; i++) datas[i] = await translate(pp, datas[i]);

    return datas;
};



var translate = async function (pp, main_data) {

    if (!pp || !main_data) return main_data;        //參數不全 原資料退回
    if(pp.now_lang == 'zh_hant') return main_data   //目前語言為中文，不須翻譯

    //抓翻譯資料
    var langQ = await QueryObjInit('language_data');
    //langQ.unit = pp.unit;
    langQ.unit = main_data.unit ? main_data.unit : pp.unit;
    langQ.lang = pp.now_lang;
    langQ.obj_id = main_data.id;
    var $langRet = await ajax_post2('Shared', 'LangDataSearch', langQ, pp);          //抓語言資料

    var langObj = '';
    if ($langRet && $langRet.hasData) langObj = $langRet.data1[0];
    //cl('translate langObj', langObj);
    //if (!langObj) return main_data;         //沒抓到語言資料 原資料退回
    if (!langObj) {
        langObj = await DataObjInit('language_data');
        langObj.text1 = '';
        langObj.text2 = '';
        langObj.text3 = '';
        langObj.content1 = '';
    }

    main_data.lang = pp.now_lang;

    //依不同類別翻譯不同物件屬性欄位
    switch (langQ.unit) {

        case 'about':
        case 'intro':
        case 'maker_car':
            {
                main_data.title = langObj.text1;
                main_data.keyword = langObj.text2;
                main_data.description = langObj.text3;
                main_data.content = langObj.content1;
            } break;

        case 'tour_point': {
            main_data.title = langObj.text1;
            main_data.location = langObj.text2;
            main_data.address = langObj.text3;
        } break;
        case 'maker': {
            main_data.title = langObj.text1;
            main_data.member = langObj.text2;
            main_data.exp = langObj.text3;
            main_data.intro = langObj.content1;
        } break;
        case 'course': {
            main_data.title = langObj.text1;
            main_data.location = langObj.text2;
            main_data.address = langObj.text3;
            main_data.content = langObj.content1;
        } break;
        case 'news':
        case 'equip':
            {
                main_data.title = langObj.text1;
                main_data.content = langObj.content1;
            } break;
        case 'album':
        case 'video':
        case 'equip_cate':
            {
                main_data.title = langObj.text1;
            } break;
        case 'works': {
            main_data.title = langObj.text1;
            main_data.group = langObj.text2;
            main_data.tool = langObj.text3;
            main_data.content = langObj.content1;
        } break;

        default: break;
    }

    return main_data;
};