﻿var admin_parent_menu = [                       //後台主選單集合

    { name: '關於基地', url: '', view: true, p: 0, c: 1 },
    { name: '課程資訊', url: '/Admin/Course', view: true, p: 0, c: 2 },
    { name: '最新消息', url: '', view: true, p: 0, c: 3 },
    { name: '設備介紹', url: '', view: true, p: 0, c: 4 },
    { name: '作品介紹', url: '/Admin/Works', view: true, p: 0, c: 5 },
    { name: '會員資訊', url: '/Admin/Member', view: true, p: 0, c: 6 },
    { name: '廣告管理', url: '/Admin/AD', view: true, p: 0, c: 7 },
    { name: '問卷管理', url: '/Admin/CourseSurvey', view: true, p: 0, c: 8 },
];

var admin_child_menu = [                        //後台副選單集合

    { name: '創客簡介', url: '/Admin/Intro', unit: 'intro', view: true, p: 1 },
    { name: '基地介紹', url: '/Admin/About', unit: 'about', view: true, p: 1 },
    { name: 'Maker Car', url: '/Admin/MakerCar', unit: 'maker_car', view: true, p: 1 },
    { name: '巡迴點', url: '/Admin/TourPoint', unit: 'tour_point', view: true, p: 1 },
    { name: '進駐創客', url: '/Admin/Maker', unit: 'maker', view: true, p: 1 },
    { name: '活動資訊', url: '/Admin/News', unit: 'news', view: true, p: 3 },
    { name: '活動相簿', url: '/Admin/Album', unit: 'album', view: true, p: 3 },
    { name: '影片集錦', url: '/Admin/Video', unit: 'video', view: true, p: 3 },
    { name: '類別', url: '/Admin/EquipCate', unit: 'equip_cate', view: true, p: 4 },
    { name: '設備資訊', url: '/Admin/Equip', unit: 'equip', view: true, p: 4 },

    { name: '', url: '/Admin/AboutEdit', unit: 'about', view: false, p: 1 },
    { name: '', url: '/Admin/IntroEdit', unit: 'intro', view: false, p: 1 },
    { name: '', url: '/Admin/MakerCarEdit', unit: 'maker_car', view: false, p: 1 },
    { name: '', url: '/Admin/TourPointEdit', unit: 'tour_point', view: false, p: 1 },
    { name: '', url: '/Admin/MakerEdit', unit: 'maker', view: false, p: 1 },
    { name: '', url: '/Admin/CourseEdit', unit: 'course', view: false, p: 2 },
    { name: '', url: '/Admin/CourseMemberEdit', unit: 'course_member', view: false, p: 2 },
    { name: '', url: '/Admin/NewsEdit', unit: 'news', view: false, p: 3 },
    { name: '', url: '/Admin/AlbumEdit', unit: 'album', view: false, p: 3 },
    { name: '', url: '/Admin/VideoEdit', unit: 'video', view: false, p: 3 },
    { name: '', url: '/Admin/EquipCateEdit', unit: 'equip_cate', view: false, p: 4 },
    { name: '', url: '/Admin/EquipEdit', unit: 'equip', view: false, p: 4 },
    { name: '', url: '/Admin/WorksEdit', unit: 'works', view: false, p: 5 },
    { name: '', url: '/Admin/MemberEdit', unit: 'member', view: false, p: 6 },
    { name: '', url: '/Admin/SurveySummary', unit: 'survey_summary', view: false, p: 8 },
    { name: '', url: '/Admin/SurveyMember', unit: 'survey_member', view: false, p: 8 },
    { name: '', url: '/Admin/SurveyDetail', unit: 'survey_detail', view: false, p: 8 },

];


var page_title_controller = function (pp) {

    if ($('#page_title').length < 1) return;

    var title = '';

    if (pp) {

        switch (pp.unit) {

            case 'about': {
                if (pp.datamode == 'list') title = '基地介紹';
                if (pp.datamode == 'edit') title = '基地介紹 新增/編輯';
            } break;
            case 'intro': {
                if (pp.datamode == 'list') title = '創客簡介';
                if (pp.datamode == 'edit') title = '創客簡介 新增/編輯';
            } break;
            case 'maker_car': {
                if (pp.datamode == 'list') title = 'Maker Car';
                if (pp.datamode == 'edit') title = 'Maker Car 新增/編輯';
            } break;
            case 'tour_point': {
                if (pp.datamode == 'list') title = 'Maker Car巡迴點';
                if (pp.datamode == 'edit') title = 'Maker Car巡迴點 新增/編輯';
            } break;
            case 'maker': {
                if (pp.datamode == 'list') title = '進駐創客';
                if (pp.datamode == 'edit') title = '進駐創客 新增/編輯';
            } break;
            case 'course': {
                if (pp.datamode == 'list') title = '課程資訊';
                if (pp.datamode == 'edit') title = '課程資訊 新增/編輯';
            } break;
            case 'course_member': {
                if (pp.datamode == 'list') title = '課程資訊-查看會員';
                if (pp.datamode == 'edit') title = '課程資訊-會員審核';
            } break;
            case 'news': {
                if (pp.datamode == 'list') title = '活動資訊';
                if (pp.datamode == 'edit') title = '活動資訊 新增/編輯';
            } break;
            case 'album': {
                if (pp.datamode == 'list') title = '活動相簿';
                if (pp.datamode == 'edit') title = '活動相簿 新增/編輯';
            } break;
            case 'video': {
                if (pp.datamode == 'list') title = '影片集錦';
                if (pp.datamode == 'edit') title = '影片集錦 新增/編輯';
            } break;
            case 'equip_cate': {
                if (pp.datamode == 'list') title = '設備類別';
                if (pp.datamode == 'edit') title = '設備類別 新增/編輯';
            } break;
            case 'equip': {
                if (pp.datamode == 'list') title = '設備資訊';
                if (pp.datamode == 'edit') title = '設備資訊 新增/編輯';
            } break;
            case 'works': {
                if (pp.datamode == 'list') title = '作品介紹';
                if (pp.datamode == 'edit') title = '作品介紹 新增/編輯';
            } break;
            case 'member': {
                if (pp.datamode == 'list') title = '會員資訊';
                if (pp.datamode == 'edit') title = '會員資訊 編輯';
            } break;
            case 'ad': {
                if (pp.datamode == 'list') title = '廣告管理';
                if (pp.datamode == 'edit') title = '';
            } break;
            case 'course_survey': {
                if (pp.datamode == 'list') title = '問卷管理';
                if (pp.datamode == 'edit') title = '';
            } break;
            case 'survey_summary': {
                if (pp.datamode == 'list') title = '問卷統計';
                if (pp.datamode == 'edit') title = '';
            } break;
            case 'survey_member': {
                if (pp.datamode == 'list') title = '查看會員';
                if (pp.datamode == 'edit') title = '';
            } break;
            case 'survey_detail': {
                if (pp.datamode == 'list') title = '問卷內容';
                if (pp.datamode == 'edit') title = '';
            } break;

            default: break;
        }

    }

    $('#page_title').html(title);
};




//抓頁面頂端的參數
var GetPageParam = function () {
    var obj = { objName: '' };

    var tagArray = [
        'end',      //前台還後台{ front, back }
        'unit',     //單元名稱
        'datamode', //資料呈現的模式{ list, edit }
        'pager',    //是否需要分頁器 { 0, 1 }
        'userno',   //使用者帳號(如有登入後)
        'usertype', //使用者類別(如有登入後)
        'userscope', //使用者類別(如有登入後)
        'file_checkcode', //檔案上傳驗證碼(每次載入頁面都會更換)
        'login_account',    //目前登入使用者的帳號
        'login_member_id',  //目前登入使用者的會員編號
        'now_lang',  //目前語言版本
    ];

    if ($("#end_tag").length == 1) obj.end = $("#end_tag").val();
    if ($("#unit_tag").length == 1) obj.unit = $("#unit_tag").val();
    if ($("#datamode_tag").length == 1) obj.datamode = $("#datamode_tag").val();
    if ($("#pager_tag").length == 1) obj.pager = $("#pager_tag").val();
    if ($("#userno").length == 1) obj.account_id = $("#userno").val();
    if ($("#usertype").length == 1) obj.usertype = $("#usertype").val();
    if ($("#userscope").length == 1) obj.userscope = $("#userscope").val();
    if ($("#login_account").length == 1) obj.login_account = $("#login_account").val();
    if ($("#login_member_id").length == 1) obj.login_member_id = $("#login_member_id").val();
    if ($("#now_lang").length == 1) obj.now_lang = $("#now_lang").val();

    if ($("[data-file_checkcode]").length > 0) obj.file_checkcode = $("[data-file_checkcode]").attr('data-file_checkcode');

    return obj;
};








var common_edit_cancel = async function (page) {
    if (confirm('未儲存的資料可能會遺失，確定取消?')) {
        location = '/Admin/' + page;
    }
};







var pagerHelper = function ($ret, pp) {

    //分頁相關的處理
    if (pp.pager == 1) {
        var pager = $ret.data3;
        var totalCount = $ret.data2;
        if (pager && pager.length > 0) {
            var pagerHtml = '';
            //if (pp.end == 'front') pagerHtml = front_pager_html(pager, totalCount);
            if (pp.end == 'front') {
                front_pager_html2(pager, totalCount);
            //    var obj = front_pager_html2(pager, totalCount);
            //    cl('new pager!!', obj);
            //        if ($('[name=pager]').length > 0) {
            //            $('[name=pager]').each(function () { $(this).html(obj.html()); });
            //        }
            }
            if (pp.end == 'back') {
                pagerHtml = admin_pager_html(pager, totalCount);
                var pagerHtml = admin_pager_html(pager, totalCount);
                if ($('[name=pager]').length > 0) {
                    $('[name=pager]').each(function () { $(this).html(pagerHtml); });
                }
            }
        }
    }

};



//管理者端 分頁器產生器
var admin_pager_html = function (pager, totalCount) {
    if (pager.length < 1 || totalCount < 1) return '';

    var selectedIndex = 0;
    var nowPage = pager.filter(x => x.active == 'active');
    if (nowPage.length > 0) {
        nowPage = nowPage[0];
        selectedIndex = nowPage.no;
    }

    //format: 
    //目前在第<span>1</span>頁，
    //共<span >1</span>頁<br>
    //<a>第一頁</a>&nbsp;
    //<a>上一頁</a>&nbsp;
    //<a>1</a>&nbsp;
    //<a>下一頁</a>&nbsp;
    //<a>最後頁</a>

    var html = '';
    html += '目前在第<span>' + selectedIndex + '</span>頁，共<span>' + pager.length + '</span>頁<br>';

    //跳至第一頁的按鈕
    var pageItem = pager[0];
    var url = pageItem.active == 'active' ? '' : 'href=' + pageItem.href;
    html += "<a " + url + " >第一頁</a>&nbsp;";
    //跳至上一頁的按鈕
    pageItem = pager[(selectedIndex - 1 < 0) ? 0 : (selectedIndex - 1)];
    url = pageItem.active == 'active' ? '' : 'href=' + pageItem.href;
    html += "<a " + url + " >上一頁</a>&nbsp;";

    //每一分頁的按鈕
    for (var i = 0; i < pager.length ; i++) {

        pageItem = pager[i];
        url = pageItem.active == 'active' ? '' : 'href=' + pageItem.href;
        html += "<a " + url + " >" + pageItem.no + "</a>&nbsp;";
    }

    //跳至下一頁的按鈕
    pageItem = pager[(selectedIndex + 1 >= pager.length) ? pager.length - 1 : (selectedIndex + 1)];
    url = pageItem.active == 'active' ? '' : 'href=' + pageItem.href;
    html += "<a " + url + " >下一頁</a>&nbsp;";
    //跳至最後一頁的按鈕
    pageItem = pager[pager.length - 1];
    url = pageItem.active == 'active' ? '' : 'href=' + pageItem.href;
    html += "<a " + url + " >最後頁</a>";

    return html;
};


//前端 分頁器產生器
var front_pager_html = function (pager, totalCount) {
    if (pager.length < 1 || totalCount < 1) return '';

    var selectedIndex = 0;
    var nowPage = pager.filter(x => x.active == 'active');
    if (nowPage.length > 0) {
        nowPage = nowPage[0];
        selectedIndex = nowPage.no;
    }

    //format: 
    //<nav aria-label="頁碼導航">
    //    <ul class="pagination">
    //        <li class="active"><a href="#" title="1">1</a></li >
    //        <li><a href="#" title="2">2</a></li>
    //        <li><a href="#" title="3">3</a></li>
    //    </ul>
    //</nav>
    //    <div class="previous">
    //        <a href="#" aria-label="上一頁">
    //            <i class="fa fa-angle-left" aria-hidden="true"></i>
    //            <span aria-hidden="true">上一頁</span>
    //        </a>
    //    </div>
    //    <div class="next">
    //        <a href="#" aria-label="下一頁">
    //            <span aria-hidden="true">下一頁</span>
    //            <i class="fa fa-angle-right" aria-hidden="true"></i>
    //        </a>
    //    </div>

    var html = '';
    
    html += "<nav aria-label='頁碼導航'>";          //頁碼部分
    html += "<ul class='pagination'>";
    for (var i = 0; i < pager.length; i++) {

        var pageItem = pager[i];
        var activeTag = pageItem.active != 'active' ? '' : 'class=active';
        var url = 'href=' + pageItem.href;
        html += "<li><a " + activeTag + " " + url + " title='" + pageItem.no + "' >" + pageItem.no + "</a></li>";
    }
    html += "</ul></nav>";

    var pageItem = pager[(selectedIndex - 1 <= 0) ? 0 : (selectedIndex - 1 - 1)];
    html += "<div class='previous'>";              //上一頁按鈕部分
    html += "<a href='" + pageItem.href + "' aria-label='" + langTrans('front-pager-prev-button-name') + "'><span aria-hidden='true'>" + langTrans('front-pager-prev-button-name') + "</span></a>";
    html += "</div>";

    pageItem = pager[(selectedIndex + 1 >= pager.length) ? pager.length - 1 : (selectedIndex + 1)];
    html += "<div class='next'>";              //下一頁按鈕部分
    html += "<a href='" + pageItem.href + "' aria-label='" + langTrans('front-pager-next-button-name') + "'><span aria-hidden='true'>" + langTrans('front-pager-next-button-name') + "</span></a>";
    html += "</div>";
    
    return html;
};

//前端 分頁器產生器
var front_pager_html2 = function (pager, totalCount) {
    if (pager.length < 1 || totalCount < 1) return;

    var selectedIndex = 0;
    var nowPage = pager.filter(x => x.active == 'active');
    if (nowPage.length > 0) {
        nowPage = nowPage[0];
        selectedIndex = nowPage.no;
    }
    cl('pager', pager);
    //format: 
    //<nav aria-label="頁碼導航">
    //    <ul class="pagination">
    //        <li class="active"><a href="#" title="1">1</a></li >
    //        <li><a href="#" title="2">2</a></li>
    //        <li><a href="#" title="3">3</a></li>
    //    </ul>
    //</nav>
    //    <div class="previous">
    //        <a href="#" aria-label="上一頁">
    //            <i class="fa fa-angle-left" aria-hidden="true"></i>
    //            <span aria-hidden="true">上一頁</span>
    //        </a>
    //    </div>
    //    <div class="next">
    //        <a href="#" aria-label="下一頁">
    //            <span aria-hidden="true">下一頁</span>
    //            <i class="fa fa-angle-right" aria-hidden="true"></i>
    //        </a>
    //    </div>

    if ($('[name=pager]').length < 1) return;

    var html = '';
    html += "<nav aria-label='頁碼導航'>"; //頁碼部分
    html += "<ul class='pagination' id='pagination'>";
    html += "</ul></nav>";

    $('[name=pager]').html(html);

    var isSet = 0;
    var obj = $('#pagination').twbsPagination({
        totalPages: pager.length,
        visiblePages: 10,
        // Text labels
        first: "",
        prev: "<span aria-hidden='true'>" + langTrans('front-pager-prev-button-name') + "</span>",
        next: "<span aria-hidden='true'>" + langTrans('front-pager-next-button-name') + "</span>",
        last: "",
        // pagination Classes
        paginationClass: 'pagination',
        nextClass: 'next',
        prevClass: 'previous',
        pageClass: 'page',
        activeClass: 'active',
        disabledClass: 'disabled',
        startPage: parseInt(nowPage.text),
        onPageClick: function (event, page) {
            console.info(page);
            $(".page a").attr("href", "?p=" + page);
            $(".page a").attr("title", page);

            if (isSet == 1) location = pager[page-1].href;
            isSet = 1;                                          //isSet為1代表頁碼器已ready好
        }
    });
    
    console.info(obj.data());
    var pageItem = pager[selectedIndex - 1 <= 0 ? 0 : selectedIndex - 1 - 1];
    $('.previous a').attr("aria-label", langTrans('front-pager-prev-button-name'));
    $('.previous a').attr("href", pageItem.href);

    pageItem = pager[selectedIndex + 1 >= pager.length ? pager.length - 1 : selectedIndex + 1];
    $('.next a').attr("aria-label", langTrans('front-pager-next-button-name'));
    $('.next a').attr("href", pageItem.href);
};



var backend_menu_push = function () {
    
    var html = '';

    var parent_node = '', child_node = '';
    if (admin_parent_menu.filter(x => x.url.toLowerCase() == location.pathname.toLowerCase()).length > 0) {
        parent_node = admin_parent_menu.filter(x => x.url.toLowerCase() == location.pathname.toLowerCase())[0];     //抓目前所在的主選單節點
    }

    if (admin_child_menu.filter(x => x.url.toLowerCase() == location.pathname.toLowerCase()).length > 0) {
        child_node = admin_child_menu.filter(x => x.url.toLowerCase() == location.pathname.toLowerCase())[0];       //抓目前所在的子選單節點
        if (admin_parent_menu.filter(x => x.c == child_node.p).length > 0) {
            parent_node = admin_parent_menu.filter(x => x.c == child_node.p)[0];                                    //藉由子選單節點來找出目前所在的主選單節點
        }
    }
    cl('backend_menu . parent_node', parent_node);
    cl('backend_menu . child_node', child_node);

    html += '<ul>';
    $(admin_parent_menu).each(function () {                                             //產生主選單
        var url = '';
        if (this.url) {
            url = this.url;
        }
        else {
            if (admin_child_menu.filter(x => x.p == this.c).length > 0) {
                url = admin_child_menu.filter(x => x.p == this.c)[0].url;
            }
        }
        //var url = this.url ? this.url : admin_child_menu.filter(x => x.p == this.c)[0].url;
        var styleTag = (parent_node == this) ? 'style=background:#FFF' : '';
        html += '<li>';
        html += '<a href=' + url + ' ' + styleTag + '>' + this.name + '</a>';
        html += '</li>';
    });
    html += '</ul>';
    if ($('[name=main_menu]').length == 1) $('[name=main_menu]').html(html);            //產生主選單

    html = '';
    if (parent_node) {                                                                  //(有找到主節點才需要產生副選單)
        var pp = GetPageParam();
        admin_child_menu = admin_child_menu.filter(x => x.p == parent_node.c);          
        $(admin_child_menu).each(function () {                                          //產生副選單

            if (this.view) {
                var styleTag = (pp.unit == this.unit) ? 'style=background:#E1E3B0' : '';
                html += '<li>';
                html += '<a href=' + this.url + ' ' + styleTag + '>' + this.name + '</a>';
                html += '</li>';
            }
        });
        if ($('[name=sub_menu]').length == 1) $('[name=sub_menu]').html(html);          //副選單產生
    }
};


$('[comingsoon]').each(function () {
    $(this).bind('click', function () { alert('coming soon...'); return false; });
});




var paramController = function (queryObj) {

    var tags = [];
    var pArray = [];
    if (location.search) {
        pArray = location.search.split('?')[1].split('&');
        for (var i = 0; i < pArray.length; i++) {
            tags.push(pArray[i].split('=')[0]);
        }
    }

    if (queryObj != null) {
        queryObj.urlParam = location.search;
        var urlParam = location.search;
        for (var i = 0; i < tags.length; i++) {
            var paramKey = tags[i] + '=';
            if (urlParam.indexOf(paramKey) > -1) {
                var paramVal = urlParam.split(paramKey)[1].split('&')[0];

                switch (tags[i]) {
                    //頁碼
                    case 'p': if (isNaN(parseInt(paramVal)) == false && paramVal > 0) { queryObj.pageNo = paramVal; } break;
                    //每頁顯示筆數
                    case 'size': if (isNaN(parseInt(paramVal)) == false && paramVal > 0) { queryObj.pageSize = paramVal; } break;
                    //資料ID
                    case 'id': if (isNaN(parseInt(paramVal)) == false && paramVal > 0) { queryObj.id = paramVal; } break;
                    //搜尋關鍵字
                    case 'keyword': queryObj.keyword = decodeURIComponent(paramVal); break;
                    case 'search_keyword': queryObj.search_keyword = decodeURIComponent(paramVal); break;
                    case 'search_keyword2': queryObj.search_keyword = decodeURIComponent(paramVal); break;


                    //查詢起始日期
                    case 'date_start': if (paramVal) { queryObj.date_start = paramVal; } break;
                    //查詢結束日期
                    case 'date_end': if (paramVal) { queryObj.date_end = paramVal; } break;

                    //查詢日期
                    case 'date': if (paramVal) { queryObj.date = paramVal; } break;
                    
                    case 'enabled': if (paramVal) { queryObj.is_enabled = paramVal; } break;
                    //查詢月結單逾繳狀況篩選
                    case 'overdue': if (paramVal) { queryObj.is_overdue = paramVal; } break;
                    //查詢用戶權限
                    case 'scope': if (paramVal) { queryObj.scope = paramVal; } break;
                    //查詢申請修改實名驗證的機構用戶篩選
                    case 'realname_modify': if (paramVal) { queryObj.is_realname_update = paramVal; } break;

                    //資料ID
                    case 'course_id': if (isNaN(parseInt(paramVal)) == false && paramVal > 0) { queryObj.course_id = paramVal; } break;

                    //課程招生中篩選
                    case 'is_recruiting': if(isNaN(parseInt(paramVal)) == false) { queryObj.is_recruiting = paramVal; } break;
                    //尚未招生篩選
                    case 'is_unopened': if(isNaN(parseInt(paramVal)) == false) { queryObj.is_unopened = paramVal; } break;
                    //招生截止篩選
                    case 'is_closed': if (isNaN(parseInt(paramVal)) == false) { queryObj.is_closed = paramVal; } break;

                    //課程問卷填寫狀況篩選
                    case 'survey_status': if (paramVal) { queryObj.survey_status = paramVal; } break;

                    //設備類別ID
                    case 'equip_cate': if (isNaN(parseInt(paramVal)) == false) { queryObj.equip_cate = paramVal; } break;
                    //巡迴點歷史年份篩選
                    case 'history_year': if (isNaN(parseInt(paramVal)) == false) { queryObj.history_year = paramVal; } break;

                    default: break;
                }
            }
        }
    }
    return queryObj;
};




var saveDetailViewFromJS = function (unit, data_id) {

    var input = { unit: unit, data_id: data_id, };
    ajax_post3('Shared', 'SaveDetailViewFromJS', input, null);
};

var getDetailView = async function (unit, obj_id) {
    var view = 0;
    var input = { unit: unit, obj_id: obj_id, };
    view = await ajax_post2('Shared', 'DetailViewSearch', input, null);
    return view;
};



var global_search_redirect = function () {
    var p = [];
    var keyword = $("#global_search").length == 1 ? $("#global_search").val().trim() : '';      //關鍵字篩選
    if (keyword) p.push({ name: 'search_keyword', v: keyword });

    location = redirectAssistant('/Home/Search', p);
};
var global_search_redirect2 = function () {
    var p = [];
    var keyword = $("#global_search2").length == 1 ? $("#global_search2").val().trim() : '';      //關鍵字篩選
    if (keyword) p.push({ name: 'search_keyword2', v: keyword });

    location = redirectAssistant('/Home/Search', p);
};

//搜尋值填入
var global_search_value_push = function () {

    $('#global_search').val(getReq('search_keyword'));
    $('#global_search2').val(getReq('search_keyword2'));
};


//翻譯器
var langTrans = function (tag) {
    var transObj = tag;
    if (langObj[tag]) transObj = langObj[tag];
    return transObj;
};

var findAndTrans = function () {
    $('[trans-attr]').each( async function () {
        var attrArray = $(this).attr('trans-attr').split(',');
        //alert($(this).attr('html'));
        for (var i = 0; i < attrArray.length; i++) {

            if (attrArray[i] == 'text') {
                $(this).text(langTrans($(this).text()));
            }
            else {
                $(this).attr(attrArray[i], langTrans($(this).attr(attrArray[i])));
            }
        }
    });
};

var findAndTrans2 = function () {
    $('[transAttr]').each(function () {
        var attrArray = $(this).attr('transAttr').split(',');
        //alert($(this).attr('html'));
        for (var i = 0; i < attrArray.length; i++) {

            var rule = attrArray[i];
            if (rule.indexOf(':') > -1) {
                var attrName = rule.split(':')[0];
                var key = rule.split(':')[1];
                if (attrName == 'text') {
                    $(this).html(langTrans(key));
                }
                else {
                    $(this).attr(attrName, langTrans(key));
                }
            }
            else {
                cl('wrong rule?', this);
            }
        }
    });
};


var languageSwitch = async function (lang) {
    await ajax_post2('Shared', 'LanguageSwitch', { lang: lang }, null);
    location = location;
};

var logout = async function () {            //登出
    if (confirm(langTrans('q_100004')) == false) return;
    var $ret = await ajax_post3('Member', 'Logout', {}, null);
    if ($ret && $ret.isSuccess) { location = '/Home/Index'; }
};


var user_login_update_captcha = function () { update_captcha('user_login'); };
var user_contact_update_captcha = function () { update_captcha('user_contact'); };
var admin_login_update_captcha = function () { update_captcha('admin_login'); };
var update_captcha = async function (tag) {

    var ret = await ajax_post2('Shared', 'UpdateCaptchaCode', { tag: tag}, null);
    cl('UpdateCaptchaCode', ret);
    if (ret && ret.isSuccess) {
        if ($('#captcha_img').length == 1) $('#captcha_img').attr('src', ret.data1);
    }
};

var captcha_voice = function (tag) {
    setTimeout(function () { captcha_pos_voice(tag, 1); }, 900);
    setTimeout(function () { captcha_pos_voice(tag, 2); }, 1800);
    setTimeout(function () { captcha_pos_voice(tag, 3); }, 2700);
    setTimeout(function () { captcha_pos_voice(tag, 4); }, 3600);
};

var captcha_pos_voice = async function (tag, pos) {
    var ret = await ajax_post2('Shared', 'GetCaptchaVoice', { tag: tag, pos: pos }, null);
    if (ret && ret.isSuccess) {
        $('#myaudio').attr('src', 'https://eyemail.ey.gov.tw/WWW/sound/' + ret.data1 + '.mp3');
        document.getElementById('myaudio').play();
    }
};


var httpsRedirect = function () {

    cl('location', location);
    if (location.href.indexOf('http://') == 0) {
        location = location.href.replace('http://', 'https://');
    }
};


var fillPageTitle = function () {

    var node2title = '', node3title = '', node4title = '';
    var crumb = $('.breadcrumb');
    if (crumb.length == 1) {

        var nodes = crumb.find('li');
        if (nodes.length > 1) node2title = crumb.find('li:eq(1) a').html();
        if (nodes.length > 2) node3title = crumb.find('li:eq(2) a').html();
        if (nodes.length > 3) node4title = crumb.find('li:eq(3) a').html();

        var pageTitle = $('title').html();
        if (node2title) pageTitle += ' - ' + node2title;
        if (node3title) pageTitle += ' - ' + node3title;
        if (node4title) pageTitle += ' - ' + node4title;
        $('title').html(pageTitle);
    }
};




httpsRedirect();

findAndTrans();
findAndTrans2();

fillPageTitle();




