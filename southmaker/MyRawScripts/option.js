﻿

var taiwan_district_options = [         //後台用

    { text: 'common-select-default-option-name', value: '0' },
    { text: 'taiwan_district_options-01', value: '1' },
    { text: 'taiwan_district_options-02', value: '2' },
    { text: 'taiwan_district_options-03', value: '3' },
    { text: 'taiwan_district_options-04', value: '4' },
    { text: 'taiwan_district_options-05', value: '5' },
    { text: 'taiwan_district_options-06', value: '6' },
    { text: 'taiwan_district_options-07', value: '7' },
    { text: 'taiwan_district_options-08', value: '8' },
    { text: 'taiwan_district_options-09', value: '9' },
    { text: 'taiwan_district_options-10', value: '10' },
    { text: 'taiwan_district_options-11', value: '11' },
    { text: 'taiwan_district_options-12', value: '12' },
    { text: 'taiwan_district_options-13', value: '13' },
    { text: 'taiwan_district_options-14', value: '14' },
    { text: 'taiwan_district_options-15', value: '15' },
    { text: 'taiwan_district_options-16', value: '16' },
    { text: 'taiwan_district_options-17', value: '17' },
    { text: 'taiwan_district_options-18', value: '18' },
    { text: 'taiwan_district_options-19', value: '19' },
    { text: 'taiwan_district_options-20', value: '20' },
    { text: 'taiwan_district_options-21', value: '21' },
    { text: 'taiwan_district_options-22', value: '22' },
    { text: 'taiwan_district_options-23', value: '23' },
];


var highest_education_options = [       //前後台用
    { text: 'common-select-default-option-name', value: '0' },
    { text: 'highest_education_options-01', value: '1' },
    { text: 'highest_education_options-02', value: '2' },
    { text: 'highest_education_options-03', value: '3' },
    { text: 'highest_education_options-04', value: '4' },
    { text: 'highest_education_options-05', value: '5' },
];  

var course_source_options = [           //前後台用
    { text: 'course_source_options-01', value: '1' },
    { text: 'course_source_options-02', value: '2' },
    { text: 'course_source_options-03', value: '3' },
    { text: 'course_source_options-04', value: '4' },
    { text: 'course_source_options-05', value: '5' },
    { text: 'course_source_options-06', value: '6' },
    { text: 'course_source_options-07', value: '7' },
];  

var course_join_reason_options = [      //前後台用
    { text: 'course_join_reason_options-01', value: '1' },
    { text: 'course_join_reason_options-02', value: '2' },
    { text: 'course_join_reason_options-03', value: '3' },
    { text: 'course_join_reason_options-04', value: '4' },
    { text: 'course_join_reason_options-05', value: '5' },
];  

var employ_status_options = [       //前後台用
    { text: 'common-select-default-option-name', value: '0' },
    { text: 'employ_status_options-01', value: '1' },
    { text: 'employ_status_options-02', value: '2' },
    { text: 'employ_status_options-03', value: '3' },
    { text: 'employ_status_options-04', value: '4' },
];  


var language_options = [            //後台用
    { text: '中文版', value: 'zh_hant' },
    { text: '英文版', value: 'en_us' },
];

var occupation_options = [          //前後台用
    { text: 'common-select-default-option-name', value: '0' },
    { text: 'occupation_options-01', value: '1' },
    { text: 'occupation_options-02', value: '2' },
    { text: 'occupation_options-03', value: '3' },
    { text: 'occupation_options-04', value: '4' },
    { text: 'occupation_options-05', value: '5' },
    { text: 'occupation_options-06', value: '6' },
    { text: 'occupation_options-07', value: '7' },
    { text: 'occupation_options-08', value: '8' },
    { text: 'occupation_options-09', value: '9' },
    { text: 'occupation_options-10', value: '10' },
    { text: 'occupation_options-11', value: '11' },
];

var day_type_options = [        //後台用
    { text: '全天 (10:00~17:00)', value: '1' },
    { text: '上午 (09:00~12:00)', value: '2' },
    { text: '下午 (13:00~17:00)', value: '3' },
    { text: '夜間 (18:30~21:30)', value: '4' },
];

var day_type_texts = [          //前台用
    { text: 'day_type_texts-01', value: '1' },
    { text: 'day_type_texts-02', value: '2' },
    { text: 'day_type_texts-03', value: '3' },
    { text: 'day_type_texts-04', value: '4' },
];




var GetOptionText = function (optionType, v) {
    var ret = '';
    var obj;
    if (optionType && v) {
        switch (optionType) {
            case 'taiwan_district': { obj = taiwan_district_options.filter(x => x.value == v); } break;
            case 'highest_education': { obj = highest_education_options.filter(x => x.value == v); } break;
            case 'course_source': { obj = course_source_options.filter(x => x.value == v); } break;
            case 'course_join_reason': { obj = course_join_reason_options.filter(x => x.value == v); } break;
            case 'employ_status': { obj = employ_status_options.filter(x => x.value == v); } break;
            case 'occupation': { obj = occupation_options.filter(x => x.value == v); } break;
            case 'day_type': { obj = day_type_options.filter(x => x.value == v); } break;
            case 'day_type_texts': { obj = day_type_texts.filter(x => x.value == v); } break;
            
            default: break;
        }
    }

    if (obj && obj.length > 0) ret = langTrans(obj[0].text);
    return ret;
};